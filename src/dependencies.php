<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c['settings']['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};



/*Database connection with pdo*/
$container['db'] = function ($c) {

    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    return $pdo;
};

$container['Api/Upload'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Upload.php';
    return new Upload($c);
};

/*============ Braintree Payment Controllers Starts ==========*/
$container['Api/Payment'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Payment.php';
    return new Payment($c);
};
/*============ Braintree Payment Controllers Ends ==========*/

/*=============== API CONTROLLERS STARTS =========================*/
$container['Api/User'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/User.php';
    return new User($c);
};

$container['Api/Client'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Client.php';
    return new Client($c);
};

$container['Api/Artist'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Artist.php';
    return new Artist($c);
};

$container['Api/Category'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Category.php';
    return new Category($c);
};

$container['Api/Booking'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Booking.php';
    return new Booking($c);
};

$container['Api/Common'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Common.php';
    return new Common($c);
};

$container['Api/Referral'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Referral.php';
    return new Referral($c);
};

$container['Api/Subscription'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Subscription.php';
    return new Subscription($c);
};

$container['Api/Cronjob'] = function ($c) {
    require_once APP_DIR . '/app/Controllers/Api/Cronjob.php';
    return new Cronjob($c);
};

/*=============== API CONTROLLERS ENDS ===========================*/

/*===============Admin Controllers=====================*/
/*Login*/
$container['Admin/Login'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Login.php';
    return new Login($c);
};

/*Dashboard*/
$container['Admin/Dashboard'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Dashboard.php';
    return new Dashboard($c);
};


/*Category*/
$container['Admin/Category'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Category.php';
    return new Category($c);
};

/*Artist*/
$container['Admin/Artist'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Artist.php';
    return new Artist($c);
};

/*Client*/
$container['Admin/Client'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Client.php';
    return new Client($c);
};

/*Client*/
$container['Admin/Settings'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Settings.php';
    return new Settings($c);
};

/*Transaction*/
$container['Admin/Transaction'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Transaction.php';
    return new Transaction($c);
};

/*Payment*/
$container['Admin/Payment'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Payment.php';
    return new Payment($c);
};

/*Referral*/
$container['Admin/Referral'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Referral.php';
    return new Referral($c);
};

/*Payment info*/
$container['Admin/Payment_info'] = function ($c) {
    require APP_DIR . '/app/Controllers/Admin/Payment_info.php';
    return new Payment_info($c);
};
/*===============End Admin Controllers=====================*/



/*Payment info*/
$container['Front/Home'] = function ($c) {
    require APP_DIR . '/app/Controllers/Front/Home.php';
    return new Home($c);
};