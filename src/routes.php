<?php
//header("HTTP/1.0 404 Not Found");

$app->post('/api/send_mail', 'Api/User:send_mail');

$app->post('/api/ram/unearby', 'Api/User:nearby');/*Create get_nearby*/

$app->get('/api/ram/unearby', 'Api/User:nearby_get');/*Create get_nearby_get with get parameters*/

$app->post('/api/cancel_subscription', 'Api/Subscription:cancel_subscription');/*cancel_subscription*/

$app->post('/api/update_subscription', 'Api/Subscription:update_subscription');/*update_subscription*/

$app->post('/api/renew_subscription', 'Api/Subscription:renew_subscription');/*renew_subscription*/

$app->post('/api/send_notifications', 'Api/Subscription:send_notifications');/*send_notification*/

//$app->post('/api/get_mua_subscription', 'Api/Subscription:get_mua_subscription');/*get_mua_subscription*/

$app->post('/api/get/subscription', 'Api/Subscription:get_mua_subscription');/*get services*/



/*====================== Payment API Controllers Starts =====================*/
$app->post('/api/manage/subscription', 'Api/Subscription:manage_subscription');/*manage subscription*/
$app->get('/api/manage/subscription', 'Api/Subscription:manage_subscription');/*manage subscription*/

$app->post('/api/create/subscription', 'Api/Payment:create_subscription');/*create subscription*/
$app->get('/api/create/subscription', 'Api/Payment:create_subscription');/*create subscription*/


$app->post('/api/payment/braintree', 'Api/Payment:braintree');/*payment*/
$app->get('/api/payment/braintree', 'Api/Payment:braintree');/*payment*/

$app->post('/api/payment/braintree_token', 'Api/Payment:braintree_token');/*payment*/
$app->get('/api/payment/braintree_token', 'Api/Payment:braintree_token');/*payment*/

$app->post('/api/payment/braintree_checkout', 'Api/Payment:braintree_checkout');/*payment*/
$app->get('/api/payment/braintree_checkout', 'Api/Payment:braintree_checkout');/*payment*/

$app->post('/api/payment/get_user_card_details', 'Api/Payment:get_user_card_details');/*payment*/
$app->get('/api/payment/get_user_card_details', 'Api/Payment:get_user_card_details');/*payment*/

$app->post('/api/payment/get_user_card_details_list', 'Api/Payment:get_user_card_details_list');/*payment get card list*/
$app->get('/api/payment/get_user_card_details_list', 'Api/Payment:get_user_card_details_list');/*payment get card list*/

$app->post('/api/payment/set_user_default_card', 'Api/Payment:set_user_default_card');/*payment get card list*/
$app->get('/api/payment/set_user_default_card', 'Api/Payment:set_user_default_card');/*payment get card list*/

$app->post('/api/deleted_card', 'Api/Payment:deleted_card');/*payment get card list*/
$app->get('/api/deleted_card', 'Api/Payment:deleted_card');/*payment get card list*/

$app->post('/api/payment/add/customerId', 'Api/Payment:add_customerId');/*for add customerId*/ 
$app->get('/api/payment/add/customerId', 'Api/Payment:add_customerId');/*for add customerId*/

$app->post('/api/payment/add/user_card_details', 'Api/Payment:add_user_card_details');/*for add card*/ 
$app->get('/api/payment/add/user_card_details', 'Api/Payment:add_user_card_details');/*for add card*/

$app->post('/api/payment/payment_with_token', 'Api/Payment:payment_with_token');/*for add card*/ 
$app->get('/api/payment/payment_with_token', 'Api/Payment:payment_with_token');/*for add card*/

$app->post('/api/payment/update/user_card_details', 'Api/Payment:update_user_card_details');/*for update card*/ 
$app->get('/api/payment/update/user_card_details', 'Api/Payment:update_user_card_details');/*for update card*/

$app->post('/api/payment/delete/user_card_details', 'Api/Payment:delete_user_card_details');/*for update card*/ 
$app->get('/api/payment/delete/user_card_details', 'Api/Payment:delete_user_card_details');/*for update card*/

$app->post('/api/payment/braintree_test', 'Api/Payment:braintree_test');/*for testing*/ 
$app->get('/api/payment/braintree_test', 'Api/Payment:braintree_test');/*for testing*/

$app->post('/api/payment/braintree_payment_test', 'Api/Payment:braintree_payment_test');/*for testing*/ 
$app->get('/api/payment/braintree_payment_test', 'Api/Payment:braintree_payment_test');/*for testing*/

$app->post('/api/user_payment_info', 'Api/Payment:user_payment_info');/*for testing*/ 
$app->get('/api/user_payment_info', 'Api/Payment:user_payment_info');/*for testing*/


$app->post('/api/verify_payment_card', 'Api/Payment:verify_payment_card');
$app->get('/api/verify_payment_card', 'Api/Payment:verify_payment_card');


/*======================Payment API Controllers Starts =====================*/

/*======================API Controllers Starts =====================*/

/* User Controller */

$app->post('/api/fb_gmail_login', 'Api/User:fb_gmail_login');/*fbLogin*/

$app->post('/api/get_username', 'Api/User:get_username');/*Regsitration*/

$app->post('/api/register', 'Api/User:register');/*Regsitration*/

$app->get('/api/register', 'Api/User:register_get');/*Regsitration with get parameters*/

$app->post('/api/login', 'Api/User:login');/*Login*/

$app->get('/api/login', 'Api/User:login_get');/*Login with get parameters*/

$app->post('/api/social_login', 'Api/User:fb_gmail_login');/*fbLogin*/

$app->get('/api/social_login', 'Api/User:fblogin_get');/*fbLogin with get parameters*/

$app->post('/api/forget_password', 'Api/User:forget_password');/*Forget password*/

$app->get('/api/forget_password', 'Api/User:forget_password_get');/*Forget password with get parameters*/

$app->post('/api/reset/password', 'Api/User:reset_password');/*Forget password*/

$app->get('/api/reset/password', 'Api/User:reset_password');/*Forget password with get parameters*/

$app->post('/api/get_mua', 'Api/User:get_mua');/*Forget password*/

$app->get('/api/get_mua', 'Api/User:get_mua');/*Forget password with get parameters*/

$app->any('/api/term_condition', 'Api/User:term_condition');/*term condition*/

$app->any('/api/get_earned_data', 'Api/User:get_earned_data');/*Total earned in mua*/

$app->post('/api/add_user_location', 'Api/User:add_user_location');/*add_user_location*/

$app->post('/api/get_user_location', 'Api/User:get_user_location');/*add_user_location*/

$app->post('/api/edit_user_location', 'Api/User:edit_user_location');/*add_user_location*/

$app->post('/api/deleted_user_location', 'Api/User:deleted_user_location');/*add_user_location*/

$app->post('/api/user_send_mail', 'Api/User:user_send_mail');/*add_user_location*/

$app->post('/api/user_mail_list', 'Api/User:user_mail_list');/*add_user_location*/

$app->post('/api/help', 'Api/User:user_help');/*add_user_location*/







/*Client Controller*/
$app->post('/api/get_skintone_list', 'Api/Client:get_skintone_list');/*for testing*/ 

$app->post('/api/client/get_payment', 'Api/Client:get_payment');/*for testing*/ 

$app->post('/api/add/client/info', 'Api/Client:create_client');/*Create client*/

$app->get('/api/add/client/info', 'Api/Client:create_client_get');/*Create client with get parameters*/

$app->post('/api/edit/client/profile', 'Api/Client:edit_client_profile');/*Create edit_client_profile*/

$app->get('/api/edit/client/profile', 'Api/Client:edit_client_profile_get');/*Create edit_client_profile_get with get parameters*/

$app->post('/api/set_skin_ton', 'Api/Client:set_skin_ton');/*Create set_skin_ton*/

$app->get('/api/set_skin_ton', 'Api/Client:set_skin_ton_get');/*Create set_skin_ton_get with get parameters*/

$app->post('/api/get_client', 'Api/Client:get_client');/*Create get_client*/

$app->get('/api/get_client', 'Api/Client:get_client_get');/*Create edit_client_get with get parameters*/

$app->post('/api/add/rating/client', 'Api/Client:add_rating');/*Create get_client*/

$app->get('/api/add/rating/client', 'Api/Client:add_rating_get');/*Create edit_client_get with get parameters*/

$app->post('/api/client/rating/reviews', 'Api/Client:get_client_rating');/*Create get_rating*/

$app->get('/api/client/rating/reviews', 'Api/Client:get_client_rating_get');/*Create eget_rating_get with get parameters*/

$app->post('/api/find/nearby', 'Api/Client:get_nearby');/*Create get_nearby*/

$app->get('/api/find/nearby', 'Api/Client:get_nearby_get');/*Create get_nearby_get with get parameters*/

$app->post('/api/add/favorite', 'Api/Client:add_favorite');/*Create add_favorite*/

$app->get('/api/add/favorite', 'Api/Client:add_favorite_get');/*Create add_favorite_get with get parameters*/

$app->post('/api/find/favorite', 'Api/Client:get_favorite_list');/*Create add_favorite*/

$app->get('/api/find/favorite', 'Api/Client:get_favorite_list_get');/*Create add_favorite_get with get parameters*/

$app->post('/api/update/user/location', 'Api/Client:add_client_long');/*Create add_client_long*/

$app->get('/api/update/user/location', 'Api/Client:add_client_long_get');/*Create add_client_long_get with get parameters*/

$app->post('/api/mua/full/info', 'Api/Client:get_mua_full_info');/*Create add_client_long*/

$app->get('/api/mua/full/info', 'Api/Client:get_mua_full_info_get');/*Create add_client_long_get with get parameters*/

$app->post('/api/search/mua', 'Api/Client:search_mua');/*Create add_client_long*/

$app->get('/api/search/mua', 'Api/Client:search_mua_get');/*Create add_client_long_get with get parameters*/



/*======================Client Controllers Ends =====================*/



/*Artist Controller*/
$app->post('/api/mua/available-time-slot', 'Api/Artist:get_artist_availabile_time_slot');/*get services*/

$app->get('/api/mua/available-time-slot', 'Api/Artist:get_artist_availabile_time_slot_get');/*get services with get parameters*/

$app->post('/api/mua/available-time-slot-test', 'Api/Artist:get_artist_availabile_time_slot_test');/*get services*/

$app->get('/api/mua/available-time-slot-test', 'Api/Artist:get_artist_availabile_time_slot_test_get');/*get services with get */


$app->post('/api/get/services', 'Api/Artist:get_services');/*get services*/

$app->get('/api/get/services', 'Api/Artist:get_services_get');/*get services with get parameters*/

$app->post('/api/artist_profile', 'Api/Artist:artist_profile');/*Create artist_profile*/

$app->get('/api/artist_profile', 'Api/Artist:artist_profile_get');/*Create artist_profile with get parameters*/

$app->post('/api/edit_artist_profile', 'Api/Artist:edit_artist_profile');/*Create edit_artist_profile*/

$app->get('/api/edit_artist_profile', 'Api/Artist:edit_artist_profile_get');/*Create edit_artist_profile with get parameters*/

$app->post('/api/find/artist/profile', 'Api/Artist:get_artist_profile');/*Create get_artist_profile*/

$app->get('/api/find/artist/profile', 'Api/Artist:get_artist_profile_get');/*Create get_artist_profile with get parameters*/

$app->post('/api/add_portfolio', 'Api/Artist:add_portfolio');/*Create add_portfolio*/

$app->get('/api/add_portfolio', 'Api/Artist:add_portfolio_get');/*Create add_portfolio with get parameters*/

$app->post('/api/edit_portfolio', 'Api/Artist:edit_portfolio');/*Create add_portfolio*/

$app->get('/api/edit_portfolio', 'Api/Artist:edit_portfolio_get');/*Create add_portfolio with get parameters*/

$app->post('/api/get_portfolio', 'Api/Artist:get_portfolio');/*Create get_portfolio*/

$app->get('/api/get_portfolio', 'Api/Artist:get_portfolio_get');/*Create get_portfolio with get parameters*/

$app->post('/api/deleted_portfolio', 'Api/Artist:deleted_portfolio');/*Create get_portfolio*/

$app->get('/api/deleted_portfolio', 'Api/Artist:deleted_portfolio_get');/*Create get_portfolio with get parameters*/

$app->post('/api/artist_availability', 'Api/Artist:artist_availability');/*Create artist_availability*/

$app->get('/api/artist_availability', 'Api/Artist:artist_availability_get');/*Create artist_availability with get parameters*/

$app->post('/api/edit_artist_availability', 'Api/Artist:edit_artist_availability');/*Create artist_availability*/

$app->get('/api/edit_artist_availability', 'Api/Artist:edit_artist_availability_get');/*Create artist_availability with get parameters*/


$app->post('/api/find/client', 'Api/Artist:get_mua_client');/*Create artist_availability*/

$app->get('/api/find/client', 'Api/Artist:get_mua_client_get');/*Create artist_availability with get parameters*/

$app->post('/api/find/mua/timeline', 'Api/Artist:get_mua_timeline');/*Create get_mua_time_line*/

$app->get('/api/find/mua/timeline', 'Api/Artist:get_mua_timeline_get');/*Create get_mua_time_line_get with get parameters*/

$app->post('/api/mua/action', 'Api/Artist:mua_action');/*Create mua_action*/

$app->get('/api/mua/action', 'Api/Artist:mua_action_get');/*Create mua_action_get with get parameters*/



$app->post('/api/artist/rating/reviews', 'Api/Artist:get_artist_rating');/*Create get_rating*/

$app->get('/api/artist/rating/reviews', 'Api/Artist:get_artist_rating_get');/*Create eget_rating_get with get parameters*/

$app->any('/api/get_type_of_arts', 'Api/Artist:get_type_of_arts');/*Get list of type-or-art*/

/*======================Artist Controllers Ends =====================*/



/*Category controller*/
$app->post('/api/select_category', 'Api/Category:select_category');/*Create get_portfolio*/

$app->get('/api/get_category', 'Api/Category:get_category_get');/*Create get_portfolio with get parameters*/

$app->post('/api/add_category', 'Api/Category:add_category');/*Create add_category*/

$app->get('/api/add_category', 'Api/Category:add_category_get');/*Create add_category_get with get parameters*/

$app->post('/api/mua_category', 'Api/Category:mua_category');/*Create add_category*/

$app->get('/api/mua_category', 'Api/Category:mua_category_get');/*Create add_category_get with get parameters*/

$app->post('/api/delete_category', 'Api/Category:delete_category');/*Create add_category*/

$app->get('/api/delete_category', 'Api/Category:delete_category_get');/*Create add_category_get with get parameters*/

$app->post('/api/edit_category', 'Api/Category:edit_category');/*Create edit_category*/

$app->get('/api/edit_category', 'Api/Category:edit_category_get');/*Create edit_category_get with get parameters*/

$app->post('/api/get/skinton', 'Api/Category:get_skinton');/*Create edit_category*/

$app->get('/api/get/skinton', 'Api/Category:get_skinton_get');/*Create edit_category_get with get parameters*/

$app->post('/api/get_child_cids', 'Api/Category:get_child_cids');/*Create edit_category*/

$app->post('/api/get_category', 'Api/Category:get_category');/*Create get_portfolio*/

/*======================API Controllers Ends =====================*/

/*======================Common API for client - artist start =====================*/

$app->post('/api/upload/image', 'Api/Common:upload_image');/*Create upload_image*/

$app->post('/api/update/image', 'Api/Common:update_upload_image');/*Create update_upload_image*/

/*======================Common API for client - artist end =====================*/



/*======================API Booking Controllers Ends =====================*/
$app->post('/api/get/booking/info', 'Api/Booking:get_booking_info');/*Create get_booking_info*/

$app->get('/api/get/booking/info', 'Api/Booking:get_booking_info_get');/*Create get_booking_info_get with get parameters*/

$app->post('/api/add/booking', 'Api/Booking:booking');/*Create booking*/

$app->get('/api/add/booking', 'Api/Booking:booking_get');/*Create booking_get with get parameters*/

$app->post('/api/find/client/booking', 'Api/Booking:get_client_booking');/*Create get_booking*/

$app->get('/api/find/client/booking', 'Api/Booking:get_client_booking_get');/*Create get_booking with get parameters*/

$app->post('/api/client/privious/booking', 'Api/Booking:client_privious_booking');/*Create client_privious_booking*/

$app->get('/api/client/privious/booking', 'Api/Booking:client_privious_booking_get');/*Create client_privious_booking_get with get parameters*/

$app->post('/api/find/mua/booking', 'Api/Booking:get_mua_booking');/*Create get_mua_booking*/

$app->get('/api/find/mua/booking', 'Api/Booking:get_mua_booking_get');/*Create get_mua_booking_get with get parameters*/

$app->post('/api/find/mua/privious/booking', 'Api/Booking:get_mua_privious_booking');/*Create get_mua_privious_booking*/

$app->get('/api/find/mua/privious/booking', 'Api/Booking:get_mua_privious_booking_get');/*Create get_mua_privious_booking_get with get parameters*/

$app->post('/api/set/service/status', 'Api/Booking:set_service_status');/*Create set_service_status*/

$app->get('/api/set/service/status', 'Api/Booking:set_service_status_get');/*Create set_service_status_get with get parameters*/

$app->post('/api/get/service/status', 'Api/Booking:get_service_status');/*Create set_service_status*/

$app->get('/api/get/service/status', 'Api/Booking:get_service_status_get');/*Create set_service_status_get with get parameters*/

$app->post('/api/mua/history', 'Api/Booking:mua_history');/*Create mua_history*/

$app->get('/api/mua/history', 'Api/Booking:mua_history_get');/*Create mua_history with get parameters*/

$app->post('/api/client/history', 'Api/Booking:client_history');/*Create client_history*/

$app->get('/api/client/history', 'Api/Booking:client_history_get');/*Create client_history with get parameters*/
/*======================API Booking Controllers Ends =====================*/



/*======================Referral API Controllers Starts =====================*/
$app->post('/api/celebrity_code', 'Api/Referral:celebrity_code');
$app->post('/api/get_celebrity_code', 'Api/Referral:get_celebrity_code');

$app->post('/api/get_referral_code', 'Api/Referral:get_referral_code');
$app->get('/api/get_referral_code', 'Api/Referral:get_referral_code_get');

$app->post('/api/use/referral-code', 'Api/Referral:use_referral_code');
$app->get('/api/use/referral-code', 'Api/Referral:use_referral_code_get');

$app->post('/api/add/user_account_info', 'Api/Referral:edit_user_account_info');
$app->get('/api/add/user_account_info', 'Api/Referral:edit_user_account_info_get');

$app->post('/api/get/user_account_info', 'Api/Referral:get_user_account_info');
$app->get('/api/get/user_account_info', 'Api/Referral:get_user_account_info_get'); 


$app->post('/api/get/notification', 'Api/Referral:get_notification');
$app->get('/api/get/notification', 'Api/Referral:get_notification_get');

$app->post('/api/update/notification', 'Api/Referral:update_notification');
$app->get('/api/update/notification', 'Api/Referral:update_notification_get');

$app->post('/api/ios_notification_count', 'Api/Referral:ios_notification_count');

$app->post('/api/add/gift_info', 'Api/Referral:add_gift_info');
$app->get('/api/add/gift_info', 'Api/Referral:add_gift_info_get');

$app->post('/api/user/promocode_info', 'Api/Referral:user_promocode_info');
$app->get('/api/user/promocode_info', 'Api/Referral:user_promocode_info_get');


$app->post('/api/mua/promocode_info', 'Api/Referral:mua_promocode_info');
$app->get('/api/mua/promocode_info', 'Api/Referral:mua_promocode_info_get');


$app->post('/api/get_used_referral_list', 'Api/Referral:get_used_referral_list');
$app->get('/api/get_used_referral_list', 'Api/Referral:get_used_referral_list_get');



/*======================API Referral Controllers Ends =====================*/


/*======================API Cronjob Controllers Starts =====================*/
$app->post('/api/send_notification', 'Api/Cronjob:send_notification');
$app->get('/api/send_notification', 'Api/Cronjob:send_notification_get');

/*======================API Cronjob Controllers Ends =====================*/


/*======================Admin Controllers=====================*/
/* Login controller */
$app->any('/admin/login', 'Admin/Login:index');/*Login*/

$app->any('/admin/logout', 'Admin/Login:logout');/*Login*/

$app->any('/admin/forgot_password', 'Admin/Login:forgot_password');/*Login*/

/* Dashboard controller */
$app->any('/admin/dashboard', 'Admin/Dashboard:index');/*Dashboard*/

$app->any('/admin/change_password', 'Admin/Dashboard:change_password');/*Admin change password*/

$app->any('/admin/profile', 'Admin/Dashboard:profile');/*Admin Profile*/

$app->any('/admin/update_admin_image', 'Admin/Dashboard:update_admin_image');/*Update admin image*/

$app->any('/admin', 'Admin/Dashboard:index');/*Dashboard*/

/* Category controller */
$app->any('/admin/category', 'Admin/Category:index');/*Category*/

$app->any('/admin/category/{id}', 'Admin/Category:index');/*Category*/

$app->any('/admin/add-category', 'Admin/Category:add_category');/*Add category*/

$app->any('/admin/get_sub_category_ajax', 'Admin/Category:get_sub_category_ajax');/*Get category*/

$app->any('/admin/category_change_status_ajax', 'Admin/Category:category_change_status_ajax');/*Change category status*/

$app->any('/admin/delete_category', 'Admin/Category:delete_category');/*Delete category*/

$app->any('/admin/get_category_to_edit', 'Admin/Category:get_category_to_edit');/*Get category to edit */

$app->any('/admin/update_category', 'Admin/Category:update_category');/*Get category to edit */

$app->any('/admin/change_category_image', 'Admin/Category:change_category_image');/*Change category image */

$app->any('/admin/update_category_portfolio', 'Admin/Category:update_category_portfolio');/*Change category portfolio */

$app->any('/admin/get_crop_model', 'Admin/Category:get_crop_model');/*Change category portfolio */

/* Artist controller */
$app->any('/admin/artist', 'Admin/Artist:index');/*Artist*/

$app->any('/admin/show_mua_used_referrel_modal/{id}', 'Admin/Artist:show_mua_used_referrel_modal');/*Client*/

$app->any('/admin/show_used_celebrity_referral/{id}', 'Admin/Artist:show_used_celebrity_referral');/*Client*/

$app->any('/admin/get_paid_referrel_amount/{id}', 'Admin/Artist:get_paid_referrel_amount');/*Client*/

$app->any('/admin/artist/{page}', 'Admin/Artist:index');/*Artist with page no.*/

$app->any('/admin/get_artist_availability', 'Admin/Artist:get_artist_availability');/*Getting artist availability*/

$app->any('/admin/artist_change_status', 'Admin/Artist:artist_change_status');/*Getting artist availability*/

$app->any('/admin/artist_profile', 'Admin/Artist:artist_profile');/*Getting artist Profile*/

$app->any('/admin/change_artist_availability_status', 'Admin/Artist:change_artist_availability_status');/*Getting artist Profile*/

$app->any('/admin/artist_category', 'Admin/Artist:artist_category');/*Getting artist category*/

$app->any('/admin/artist_portfolio', 'Admin/Artist:artist_portfolio');/*Getting artist category*/

$app->any('/admin/artist_portfolio/{page}', 'Admin/Artist:artist_portfolio');/*Getting artist category*/

$app->any('/admin/artist_client', 'Admin/Artist:artist_client');/*Getting artist client*/

$app->any('/admin/artist_request', 'Admin/Artist:artist_request');/*Getting artist requist*/

$app->any('/admin/artist_request/{page}', 'Admin/Artist:artist_request');/*Getting artist requist*/

$app->any('/admin/change_artist_request_status/{id}/{status}', 'Admin/Artist:change_artist_request_status');/*Change artist request status*/

$app->any('/admin/artist_commision', 'Admin/Artist:artist_commision');/*Artsist commision*/

$app->any('/admin/change_artist_commision', 'Admin/Artist:change_artist_commision');/*Artsist commision*/

$app->any('/admin/artist_certificate', 'Admin/Artist:artist_certificate');/*artist_certificate*/

$app->any('/admin/change_certificate_status', 'Admin/Artist:change_certificate_status');/*Artsist commision*/



/* Client controller */
$app->any('/admin/client', 'Admin/Client:index');/*Client*/

$app->any('/admin/show_client_used_referrel_modal/{id}', 'Admin/Client:show_client_used_referrel_modal');/*Client*/




$app->any('/admin/client/{page}', 'Admin/Client:index');/*Client pagination*/

$app->any('/admin/client_change_status', 'Admin/Client:client_change_status');/*Client change status*/

$app->any('/admin/client_profile', 'Admin/Client:client_profile');/*Client change status*/

/* Settings controller */
$app->any('/admin/common/term-condition', 'Admin/Settings:term_condition');/*common settings*/

$app->any('/admin/common/notification', 'Admin/Settings:notification');/*common settings*/

$app->any('/admin/common/settings', 'Admin/Settings:common');/*common settings*/

$app->any('/admin/plan', 'Admin/Settings:plan');/*Plan list*/

$app->any('/admin/plan/get_subscription_plan_list', 'Admin/Settings:get_subscription_plan_list');/*Update payment info*/

$app->any('/admin/add_plan', 'Admin/Settings:add_plan');/*Plan list*/

$app->any('/admin/edit_plan/{id}', 'Admin/Settings:add_plan');/*Plan list*/

$app->any('/admin/change_plan_status', 'Admin/Settings:change_plan_status');/*Plan list*/

$app->any('/admin/delete_plan/{id}', 'Admin/Settings:delete_plan');/*Delete plan*/

$app->any('/admin/settings/skintone', 'Admin/Settings:skintone');/*Skintone Setting*/

$app->any('/admin/skintone/upate/{id}', 'Admin/Settings:skintone_update');/*Skintone Setting*/

$app->any('/admin/settings/upate_skintone_status_ajax', 'Admin/Settings:upate_skintone_status_ajax');/*Skintone Setting*/

$app->any('/admin/update_skintone', 'Admin/Settings:update_skintone');/*Skintone Setting*/

$app->any('/admin/edit_skinton', 'Admin/Settings:edit_skinton');/*Plan list*/

$app->any('/admin/settings/lipscolor', 'Admin/Settings:lipscolor');/*Skintone Setting*/

$app->any('/admin/update_lipscolor', 'Admin/Settings:update_lipscolor');/*Skintone Setting*/

$app->any('/admin/settings/eyeshadow', 'Admin/Settings:eyeshadow');/*Skintone Setting*/

$app->any('/admin/update_eyeshadow', 'Admin/Settings:update_eyeshadow');/*Skintone Setting*/

$app->any('/admin/settings/upate_lipscolor_status_ajax', 'Admin/Settings:upate_lipscolor_status_ajax');/*Skintone Setting*/

$app->any('/admin/settings/upate_eyeshadow_status_ajax', 'Admin/Settings:upate_eyeshadow_status_ajax');/*Skintone Setting*/

$app->any('/admin/email', 'Admin/Settings:user_email_list');/*user_email_list Setting*/

$app->any('/admin/email/{page}', 'Admin/Settings:user_email_list');/*user_email_list*/

$app->any('/admin/email_cooment_model', 'Admin/Settings:email_cooment_model');/*Payment info*/

$app->any('/admin/update_email_cooment', 'Admin/Settings:update_email_cooment');/*Payment info*/

$app->any('/admin/admin_email_model', 'Admin/Settings:admin_email_model');/*Payment info*/



/* payment controller */
$app->any('/admin/earning/artist', 'Admin/Payment:earning_artist_list');
$app->any('/admin/earning/artist/{id}', 'Admin/Payment:earning_artist_list');


$app->any('/admin/artist/transaction/{id}', 'Admin/Payment:artist_transaction');


/* Transation controller */
$app->any('/admin/transaction', 'Admin/Transaction:index');/*Transactions*/

$app->any('/admin/transaction/{id}', 'Admin/Transaction:index');/*Transactions*/

$app->any('/admin/deleted/subscription/{id}', 'Admin/Transaction:deleted_subscription');/*deleted_subscription*/





$app->any('/admin/client_payment_info', 'Admin/Transaction:client_payment_info');/*Client transation*/

$app->any('/admin/client_payment_info/{id}', 'Admin/Transaction:client_payment_info');/*Client transation*/

$app->any('/admin/payment_info_mua', 'Admin/Transaction:payment_info_mua');/*Transactions*/

$app->any('/admin/payment_info_mua/{id}', 'Admin/Transaction:payment_info_mua');/*Transactions*/

$app->any('/admin/get_paid_data', 'Admin/Transaction:get_paid_data');

$app->any('/admin/add_paid_data', 'Admin/Transaction:add_paid_data');

$app->any('/admin/payment_received', 'Admin/Transaction:payment_received');/*Getting artist Profile*/

$app->any('/admin/total_payment_received', 'Admin/Transaction:total_payment_received');/*Getting artist Profile*/

/* Referral controller */
$app->any('/admin/add-referral', 'Admin/Referral:add_referral');/*Add referral*/

$app->any('/admin/referral_list', 'Admin/Referral:referral_list');/*Celebrity Referral list*/

$app->any('/admin/show_celebrity_modal/{id}', 'Admin/Referral:show_celebrity_modal');/*Celebrity Referral list*/




$app->any('/admin/referral_list/{page}', 'Admin/Referral:referral_list');/*Celebrity Referral list*/

$app->any('/admin/referral_list_user', 'Admin/Referral:referral_list_user');/*User Referral list*/

$app->any('/admin/referral_list_user/{page}', 'Admin/Referral:referral_list_user');/*User Referral list*/

$app->any('/admin/change_referral_status', 'Admin/Referral:change_referral_status');/*Change referral status*/

$app->any('/admin/update_referral/{id}', 'Admin/Referral:update_referral');/*Update referral*/

/* Payment info controller */
$app->any('/admin/payment_info', 'Admin/Payment_info:index');/*Payment info*/

$app->any('/admin/payment_info/{page}', 'Admin/Payment_info:index');/*Payment info*/

$app->any('/admin/update_payment_info/{id}', 'Admin/Payment_info:update_payment_info');/*Update payment info*/

$app->any('/admin/update_payment_info', 'Admin/Payment_info:update_payment_info');/*Update payment info*/

$app->any('/admin/pay_to_user/{id}', 'Admin/Payment_info:pay_to_user');/*Update payment info*/

$app->any('/admin/pay_to_user', 'Admin/Payment_info:pay_to_user');/*Update payment info*/



/*======================End Admin Controllers=====================*/

$app->any('/', 'Front/Home:index');



//for getting user encoded id for testing purpose and not for api purpose 
$app->post('/api/user/encode', 'Api/User:user_encode');


$app->post('/api/artist/decode_base64_call_back', 'Api/Artist:decode_base64_call_back');
$app->post('/api/artist/encode_base64_call_back', 'Api/Artist:encode_base64_call_back');



