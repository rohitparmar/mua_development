<?php 
//define here base url
define('BASE_URL', 'http://dev.bebebellamuas.com/');


//merchant_account_id
//define('MARCHANT_ACCOUNT_ID', '');



define('FORGET_PASSWORD_MSG', 'Your Password Has Been Changed!');

define('BOOKING_MSG', 'New Booking!');

define('REGISTER_MSG', 'Welcome To Bébé Bella!');

define('BOOKING_GIFT_MSG', 'This Is Bebebella Gift For You From Your Friends!');

define('PAYMENT_MSG', 'Booking Payment For Client!');


define('TERM_CONDITION', 'app/Views/Front/term_condition');

//Cateogry  image path
define('CATEGORY_DEFAULT_IMAGE', BASE_URL.'category-uploaded-image/default_cat.png');

define('CERTIFICATE_DEFAULT_IMAGE', BASE_URL.'artist-client-upload-file/artist/certificate/default_certificate.png');

define('SKINTONE_DEFAULT_IMAGE', 'category-uploaded-image/skintone/default_skintone.png');

define('SKINTONE_IMAGE', 'category-uploaded-image/skintone/');

//Artist profile image path
define('ARTIST_PROFILE_IMG', 'artist-client-upload-file/artist/profile/');

//Artist portfolio image path
define('ARTIST_PORTFOLIO_IMG', 'artist-client-upload-file/artist/portfolio/');

//Artist certificate image path
define('ARTIST_CERTIFICATE_IMG', 'artist-client-upload-file/artist/certificate/');

//Client profile image path
define('CLIENT_PROFILE_IMG', 'artist-client-upload-file/client/profile/');

//Complain file path
define('CLIENT_COMPLAIN_FILE', 'artist-client-upload-file/complain-file-img/');

//Subscription paln image path
define('SUBSCRIPTION_PLAN_IMG', 'artist-client-upload-file/subscription-plan-img/');

//Admin profile image path
define('ADMIN_PROFILE_IMG', 'admin-uploaded-file/profile-img/');

//Category image path
define('CATEGORY_IMG', 'category-uploaded-image/');

//Category portfolio before image path
define('CATEGORY_PORTFOLIO_AFTER_IMG', 'category-uploaded-image/portfolio-images/after/');

//Category portfolio before image path
define('CATEGORY_PORTFOLIO_BEFORE_IMG', 'category-uploaded-image/portfolio-images/before/');

//Admin email id
define('ADMIN_MAIL', 'deepak.kumar@synergytop.com');

//Site name
define('SITE_NAME', 'Mua');

//Arial Distance limit in miles
define('ARIAL_DISTANCE', 25);

//availble time slot unit
define('TIME_SLOT_UNIT', 15);

//availble time slot unit
define('PAYMENT_INTERVAL', 180);  //in seconds

//for notification api access key and sender id
//define('API_ACCESS_KEY', 'AIzaSyAWeV2sNxgAWVVEU2bpi60IGYhAfsIP_5Y');
//define('SENDER_ID', 140098469003);

define('API_ACCESS_KEY', 'AAAAL-tUecQ:APA91bG_gvZflv8WBHqB-sNos-FQI-UprG0Xpjlh2YUCVrOwKQ4hjYq1EDmG-koU6BMGIQt4uvauLiMEU4mPlpDld6Qzf_I3D104KggU-iPL_jmS_myuG3MqXQjwfqOfN0dpW-JpZHIQ');
define('SENDER_ID', 119936027406);
 
//Master languages
define('ENGLISH', 'eng');
define('SPANISH', 'spn');



/*
	|------------------------------------------------------------------------------------
	| Email template iamge constants starts
	|------------------------------------------------------------------------------------
*/
define('LOGO_IMAGE', BASE_URL.'artist-client-upload-file/email/logo.png');

define('GIFT_IMAGE', BASE_URL.'artist-client-upload-file/email/giftboxes.png');



define('PAYMENT_SUCCESS_ENG', 'Subscription Plan Successfully Updated.');
define('PAYMENT_SUCCESS_SPN', 'Plan de suscripción actualizado correctamente.');


define('UPDATE_PAYMENT_ENG', 'Please update your payment information.');
define('UPDATE_PAYMENT_SPN', 'Actualice su información de pago.');

define('PLAN_UPDATE_ENG', 'You have already changed your plan. That plan will take effect at the beginning of the next billing cycle.');
define('PLAN_UPDATE_SPN', 'Ya ha cambiado su plan. Ese plan entrará en vigor al comienzo del siguiente ciclo de facturación.');


define('TIME_DURATION_ENG', 'Please Wait '.PAYMENT_INTERVAL.' To Confirm This Booking');
define('TIME_DURATION_SPN', 'Espere '.PAYMENT_INTERVAL.' para confirmar esta reserva');

define('TIME_DURATION_END_ENG', 'Please Wait '.PAYMENT_INTERVAL.' To End This Booking');
define('TIME_DURATION_END_SPN', 'Por favor espere '.PAYMENT_INTERVAL.' para terminar esta reserva');


define('CELEBRITY_CODE_SU_ENG', 'Celebrity code applied successfully.');
define('CELEBRITY_CODE_SU_SPN', 'Codigo de celebridad aplicado correctamente.');


define('CELEBRITY_CODE_ER_ENG', 'The code has expired.');
define('CELEBRITY_CODE_ER_SPN', 'El código ha caducado.');


define('CELEBRITY_CODE_ER_1_ENG', 'This code has been already used.');
define('CELEBRITY_CODE_ER_1_SPN', 'Este codigo ya se ha utilizado.');

define('PAYMENT_FAILED_ENG', 'Your payment has been failed.');
define('PAYMENT_FAILED_SPN', 'Su pago ha sido fallido.');

define('INVALID_USER_ENG', 'User does not exist.');
define('INVALID_USER_SPN', 'el usuario no existe.');

define('INVALID_PLAN_ENG', 'Please select valid plan.');
define('INVALID_PLAN_SPN', 'Seleccione un plan válido');