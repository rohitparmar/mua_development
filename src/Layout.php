<?php

class Layout {

    protected $app;

    public function __construct($app) {
        $this->app = $app;
    }
    
    public function _view($template, $data = array(), $view = false) {
        $template = APP_DIR . '/app/Views/'.$template;
        extract($data);
        ob_start();
        if (!ini_get('short_open_tag') && config_item('rewrite_short_tags') === TRUE) {
            echo eval('?>' . preg_replace('/;*\s*\?>/', '; ?>', str_replace('<?=', '<?php echo ', file_get_contents($template))));
        } else {
            include_once($template); // include() vs include_once() allows for multiple views with the same name
        }
        // Return the file data if requested
        if ($view) {
            $buffer = ob_get_contents();
            @ob_end_clean();
            return $buffer;
        }
    }
    
    public function view($res, $template, $data = array(), $view = TRUE) {
        return $this->app->renderer->render($res, $template, $data, $view);
    }

    public function layout($res, $template, $data = array(), $view = true) {
        $lData['notifications'] = $this->view($res, 'Admin/Common/notifications.php', $data, false);
        $lData['messages'] = $this->view($res, 'Admin/Common/messages.php', $data, false);
        $lData['admin_profile'] = $this->view($res, 'Admin/Common/admin_profile.php', $data, false);
        $lData['side_bar_search'] = $this->view($res, 'Admin/Common/side_bar_search.php', $data, false);
        $lData['side_bar_menu'] = $this->view($res, 'Admin/Common/side_bar_menu.php', $data, false);
        $lData['contents'] = $this->view($res, $template, $data, false);

        $this->view($res, 'Admin/Layout/layout.php', $lData, $view);
    }

    private function loader($loaderClass, $dir, $name = false) {
        $pagePath = APP_DIR . '/app/' . $dir . '/' . $loaderClass;
        $pagePath = current(explode('.php', $pagePath)) . '.php';
        if (!file_exists($pagePath)) {
            echo $loaderClass . ' not found';
            exit;
        }
        require_once $pagePath;
        $class = current(array_reverse(explode('/', $loaderClass)));

        if (!$name) {
            $name = $class;
        }
        $name = current(explode('.', $name));
        $class = current(explode('.', $class));
        //same class name calling
        $this->$name = new $class($this->app);
        //lower class calling
        $lname = strtolower($name);
        $this->$lname = new $class($this->app);
        return;
    }

    public function load_model($model, $name = false) {
        $this->loader($model, 'Models', $name);
        return;
    }

    public function load_library($library, $name = false) {
        $this->loader($library, 'Library', $name);
        return;
    }

}
