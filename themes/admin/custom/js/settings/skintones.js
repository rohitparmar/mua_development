/**
 * skintones change status
 */
function change_skintone_status(id, elem) {

    var url = $("#base_url").val() + "admin/settings/upate_skintone_status_ajax";

    $(elem).next('.js_loading_img').show();
    $(elem).hide();
    

    $.ajax({
        method: "POST",
        url: url,
        data: {"id": id}
    })
    .error(function (e) {

    })
    .success(function (result) {
        if ($(elem).find('.action').html()=='Inactive') {
            $(elem).find('.action').removeClass('label-danger').addClass('label-success');
            $(elem).find('.action').html('Active');
            $(elem).find('.action').prop("title","Click to Deactivate");
            
            $(elem).attr('status', 'true');
        } else {
            $(elem).find('.action').removeClass('label-success').addClass('label-danger');
            $(elem).find('.action').html('Inactive');
            $(elem).find('.action').prop("title","Click to Activate");
            $(elem).attr('status', 'false');
        }
    })
    .done(function (result) {
        $(elem).next('.js_loading_img').hide();
        $(elem).show();
    });
}


function skintone_show_list(value){
    if(value==1){
        $('.lan-1').show();
        $('.lan-2').hide();
    }else{
        $('.lan-2').show();
        $('.lan-1').hide();
    }
    
}


/*
||============================================
|| Function update_skintone
||============================================
*/
function update_skintone(id){
    var update_skintone_url = $("#base_url").val() + "admin/update_skintone";

    $.ajax({
        method:"POST",
        url:update_skintone_url,
        data:{id:id},
        async:"sync"
    })
    
    .error(function(e){ 
    })

    .success(function(result){
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })

    .complete(function (result) {

    });
}


/**
 * Artist commision
 */
$("body").on('click', '.js_update_skintone', function () {

    var name = $(".js_model_html .js_name").val();
    var id   = $('.js_id').val();
    var type = $('.js_type').val();
    
    
    var js_skintone_ajax_url  = $("#base_url").val() + "admin/edit_skinton";

    $.ajax({
        method: "POST",
        url: js_skintone_ajax_url,
        data: {id: id, name: name, type:type},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        location.reload(); 
    })
    .complete(function (result) {

    });
});


function update_lipscolor(id){
    var update_skintone_url = $("#base_url").val() + "admin/update_lipscolor";
    $.ajax({
        method:"POST",
        url:update_skintone_url,
        data:{id:id},
        async:'sync'
    })
    .error(function(e){

    })
    .success(function(result){
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function(result){

    });
}


function update_eyeshadow(id){
    var update_eyeshadow_url = $("#base_url").val() + "admin/update_eyeshadow";
    $.ajax({
        method:"POST",
        url:update_eyeshadow_url,
        data:{id:id},
        async:'sync'
    })
    .error(function(e){

    })
    .success(function(result){
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function(result){

    });
}



/**
 * change_lipscolor_status change status
 */
function change_lipscolor_status(id, elem) {

    var url = $("#base_url").val() + "admin/settings/upate_lipscolor_status_ajax";

    $(elem).next('.js_loading_img').show();
    $(elem).hide();
    

    $.ajax({
        method: "POST",
        url: url,
        data: {"id": id}
    })
    .error(function (e) {

    })
    .success(function (result) {
        if ($(elem).find('.action').html()=='Inactive') {
            $(elem).find('.action').removeClass('label-danger').addClass('label-success');
            $(elem).find('.action').html('Active');
            $(elem).find('.action').prop("title","Click to Deactivate");
            
            $(elem).attr('status', 'true');
        } else {
            $(elem).find('.action').removeClass('label-success').addClass('label-danger');
            $(elem).find('.action').html('Inactive');
            $(elem).find('.action').prop("title","Click to Activate");
            $(elem).attr('status', 'false');
        }
    })
    .done(function (result) {
        $(elem).next('.js_loading_img').hide();
        $(elem).show();
    });
}




/**
 * change_lipscolor_status change status
 */
function change_eyeshadow_status(id, elem) {
    

    var url = $("#base_url").val() + "admin/settings/upate_eyeshadow_status_ajax";

    $(elem).next('.js_loading_img').show();
    $(elem).hide();
    

    $.ajax({
        method: "POST",
        url: url,
        data: {"id": id}
    })
    .error(function (e) {

    })
    .success(function (result) {
        if ($(elem).find('.action').html()=='Inactive') {
            $(elem).find('.action').removeClass('label-danger').addClass('label-success');
            $(elem).find('.action').html('Active');
            $(elem).find('.action').prop("title","Click to Deactivate");
            
            $(elem).attr('status', 'true');
        } else {
            $(elem).find('.action').removeClass('label-success').addClass('label-danger');
            $(elem).find('.action').html('Inactive');
            $(elem).find('.action').prop("title","Click to Activate");
            $(elem).attr('status', 'false');
        }
    })
    .done(function (result) {
        $(elem).next('.js_loading_img').hide();
        $(elem).show();
    });
}


/*
||===============================================
|| Function artist certificate approval
||===============================================
*/
function artist_certificate(id) {
    var url = $("#base_url").val() + "admin/artist_certificate";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });
}

/*
||===============================================
|| Function change status artist certificate
||===============================================
*/
function js_artist_certificate() {
    var js_id      = $(".js_model_html .js_id").val();

    var js_status  = $('.js_model_html input[name=js_status]:checked').val(); //$(".js_model_html .js_status_1").val();

    var js_comment = $(".js_model_html .js_comment").val();
    
    var status_url = $("#base_url").val() + "admin/change_certificate_status";

    $.ajax({
        method:"POST",
        url:status_url,
        data:{js_id:js_id,js_status:js_status,js_comment:js_comment},
        async:'sync'
    })

    

    .error(function (e) {

    })

    .success(function(result){
        alert('Data updated successfully!!');
        location.reload();
    })
}


/*
||===============================================
|| Function email_model
||===============================================
*/
function email_model(id) {
    
   var status_url = $("#base_url").val() + "admin/email_cooment_model";

    $.ajax({
        method:"POST",
        url:status_url,
        data:{id:id},
        async:'sync'
    })

    

    .error(function (e) {

    })

    .success(function(result){
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
}


/*
||===============================================
|| Function email_model
||===============================================
*/
function update_email(id) {
    
    var status_url = $("#base_url").val() + "admin/update_email_cooment";
    var js_id      = $(".js_model_html .js_id").val();
    var js_status  = $(".js_model_html .js_status").val();
    var js_comment = $(".js_model_html .js_comment").val();
    
    var status_url = $("#base_url").val() + "admin/update_email_cooment";

    $.ajax({
        method:"POST",
        url:status_url,
        data:{js_id:js_id,js_status:js_status,js_comment:js_comment},
        async:'sync'
    })

    

    .error(function (e) {

    })

    .success(function(result){
        //location.reload();

        alert('update successfully');
        location.reload();

    })
}