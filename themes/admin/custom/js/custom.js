/**
 * Fetch sub category by ajax
 */
$("body").on('change', '.js_sub_category_select', function () {
    var id = $(this).val();
    var url = $("#base_url").val() + "admin/get_sub_category_ajax";
    var elem = $(this);
    
    var lang = 'eng';

    var parent_select = 0;
    if ($(this).find('option:selected').attr('data_parent') == 1) {
        parent_select = 1;
    }

    if (id) {

        $.ajax({
            method: "POST",
            url: url,
            data: {id: id, language: lang},
            async: 'sync'
        })
        .error(function (e) {

        })
        .success(function (result) {
            data = JSON.parse(result);

            $(elem).parent().nextAll('.js_sub_category_div').remove();

            var count = $("#add_category_frm .js_sub_category_div").length;

            if (data.length > 0) {
                var html = '<div class="form-group js_sub_category_div">';
                html += '<label>Sub category lavel ' + count + '</label>';
                html += '<select class="form-control js_sub_category_select" name="category[]">';
                html += '<option value="">--Select category--</option>';

                for (x in data) {
                    html += '<option value="' + data[x]['ca_fk_cm_id'] + '">' + data[x]['ca_name'] + '</option>';
                }

                html += '</select></div>';
                $(elem).parent().after(html);
            }

            if ($(".js_sub_category_select:last").val()) {
                $(".js_category_price").show();
                $(".js_category_skin_tone").show();
                $(".js_category_lips_color").show();
                $(".js_category_eye_shadow").show();
                $(".js_category_time").show();
            } else if (parent_select == 1) {
                $(".js_category_price").show();
                $(".js_category_skin_tone").show();
                $(".js_category_lips_color").show();
                $(".js_category_eye_shadow").show();
                $(".js_category_time").show();
            } else {
                /*$(".js_category_price").hide();
                 $(".js_category_skin_tone").hide();
                 $(".js_category_lips_color").hide();
                 $(".js_category_eye_shadow").hide();
                 $(".js_category_time").hide();*/
            }
        })
        .done(function (result) {

        });
    } else {

        $(elem).parent().nextAll('.js_sub_category_div').remove();

        if (parent_select == 1) {
            $(".js_category_price").show();
            $(".js_category_skin_tone").show();
            $(".js_category_lips_color").show();
            $(".js_category_eye_shadow").show();
            $(".js_category_time").show();
        }
    }
});

/*
 * Add Sub category checkbox
 */
$(".js_add_subcategory").click(function () {

    if ($(this).prop('checked')) {
        $(".js_sub_category_div").removeClass('hide');
    } else {
        $(".js_sub_category_select:first").val("");
        $(".js_sub_category_select:first").change();
        $(".js_sub_category_div").addClass('hide');
    }

    $(".js_category_lang[value=eng]").prop('checked',true);
    $(".js_category_skin_tone").removeClass('hide');
});

/*
 * Add spenish
 */
$(".js_category_lang").click(function () {

    if($(this).val() == 'spn'){
        $(".js_sub_category_div").removeClass('hide');
        $(".js_add_subcategory").prop('checked',false);
        $(".js_category_skin_tone").addClass('hide');
    }
    else{
        if ($(".js_add_subcategory").is(':checked') == false) {
            
            $(".js_sub_category_div").addClass('hide');
        }
        
        $(".js_category_skin_tone").removeClass('hide');
    }
});

/*
 * Add Sub category checkbox
 */
// $(".js_category_lang").click(function () {

//     $(".js_sub_category_select:first").val("");
//     $(".js_sub_category_select:first").change();

//     var id = 0;
//     var url = $("#base_url").val() + "admin/get_sub_category_ajax";
//     var lang = $(".js_category_lang:checked").val();

//     $.ajax({
//         method: "POST",
//         url: url,
//         data: {id: id, language: lang},
//         async: 'sync'
//     })
//     .error(function (e) {

//     })
//     .success(function (result) {
//         data = JSON.parse(result);

//         var html = '<option value="">--Select category--</option>';

//         for (x in data) {
//             html += '<option value="' + data[x]['cm_id'] + '">' + data[x]['ca_name'] + '</option>';
//         }

//         $(".js_sub_category_select").html(html);
//     })
//     .done(function (result) {

//     });
// });

/*
 * Add category form validation
 */
$("#add_category_frm").validate({
    rules: {
        sub_category_name: "required",
    },
});

/*

 $("body").on('click','.js_skin_tone_checkbox',function(){
    elem = $(this).parents('.checkbox');
    if($(this).is(':checked')){
        $(elem).find(".js_skin_tone_img").removeClass('hide');
        //return false;
    }
    else{
        //$(this).prop('checked',false);
        $(elem).find(".js_skin_tone_img").addClass('hide');
    }
 });

*/

 $("body").on('change','.js_skin_tone_img',function(){
    elem = $(this).parents('.checkbox');
    //$(elem).find(".js_skin_tone_checkbox").prop('checked',true);
    $(elem).find(".js_skin_tone_img").removeClass('hide');
    //return false;
 });


/**
 * Datatable
 */
$('#datatable').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "columnDefs": [
        {
            "targets": ["no_sort"],
            "sortable": false,
        },
    ]

});

$('#custom_datatable').DataTable({
    "paging": false,
    "lengthChange": false,
    "searching": false,
    "ordering": false,
    "info": false,
    "autoWidth": true,
    "columnDefs": [
        {
            "targets": ["no_sort"],
            "sortable": false,
        },
    ]
});



/**
 * Category change status
 */
function category_change_status(id, elem) {

    var url = $("#base_url").val() + "admin/category_change_status_ajax";

    var status = $(elem).attr('status');

    $(elem).next('.js_loading_img').show();
    $(elem).hide();

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id, status: status},
        async: 'sync'

    })
    .error(function (e) {

    })
    .success(function (result) {
        if (result == "true") {
            $(elem).find('.action').removeClass('label-danger').addClass('label-success');
            $(elem).find('.action').html('Active');
            $(elem).attr('status', 'true');
        } else {
            $(elem).find('.action').removeClass('label-success').addClass('label-danger');
            $(elem).find('.action').html('Inactive');
            $(elem).attr('status', 'false');
        }
    })
    .done(function (result) {
        $(elem).next('.js_loading_img').hide();
        $(elem).show();
    });
}



/**
 * Delete category
 */
function delete_category(id, elem) {

    var url = $("#base_url").val() + "admin/delete_category";

    var confirmed = confirm('Are you sure ?');

    if (confirmed) {
        $(elem).next('.js_loading_img').show();
        $(elem).hide();

        $.ajax({
            method: "POST",
            url: url,
            data: {id: id, },
            async: 'sync'

        })
        .error(function (e) {
            $(elem).next('.js_loading_img').hide();
            $(elem).show();
        })
        .success(function (result) {
            var data = JSON.parse(result);

            if (data['status'] == 'success') {
                $(elem).parent().parent().remove();
            } else {
                $(elem).next('.js_loading_img').hide();
                $(elem).show();
            }
        })
        .done(function (result) {

        });
    }
}

/*
 * Add category form validation
 */
$("body").on('submit','#edit_category_frm',function(){
    if(!$("#category_name").val())
    {
        $("#category_name").parents('.form-group').addClass('has-error');
        return false;
    }
});

/**
 * Edit category
 */
function open_model_for_edit_category(cat_id, last_category) {
    var url = $("#base_url").val() + "admin/get_category_to_edit";
    var lang = $(".js_lanaguage_select").val();
    $.ajax({
        method: "POST",
        url: url,
        data: {cat_id: cat_id,lang:lang},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);

        // var data = JSON.parse(result);
        // $("#edit_category_frm input[name=name]").val(data['ca_name']);
        // $("#edit_category_frm input[name=price]").val(data['ca_price']);
        // $("#edit_category_frm textarea[name=description]").val(data['ca_description']);
        // $("input[name=id]").val(id);
        // $("#edit_category_frm select[name=skin_tone]").val(data['ca_skintone']);
        // $("#edit_category_frm input[name=lips_color]").val(data['ca_lip_color']);
        // $("#edit_category_frm input[name=eye_shadow]").val(data['ca_eye_shadow']);
        // $("#edit_category_frm input[name=time]").val(data['ca_duration']);
        // $("#js_image_edit").attr('src', data['image']);
    })
    .done(function (result) {
    });
    $("#js_model_button").click();
    modal_width(1000);
    //$("#js_edit_category_button").click();
}
/**
 * Edit spanish
 */

function open_model_for_spanish_category(id){

    var url = $("#base_url").val() + "admin/get_category_to_edit";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id,lang:2},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);

        // var data = JSON.parse(result);
        // $("#edit_category_frm input[name=name]").val(data['ca_name']);
        // $("#edit_category_frm input[name=price]").val(data['ca_price']);
        // $("#edit_category_frm textarea[name=description]").val(data['ca_description']);
        // $("input[name=id]").val(id);
        // $("#edit_category_frm select[name=skin_tone]").val(data['ca_skintone']);
        // $("#edit_category_frm input[name=lips_color]").val(data['ca_lip_color']);
        // $("#edit_category_frm input[name=eye_shadow]").val(data['ca_eye_shadow']);
        // $("#edit_category_frm input[name=time]").val(data['ca_duration']);
        // $("#js_image_edit").attr('src', data['image']);
    })
    .done(function (result) {
    });
    $("#js_model_button").click();
    modal_width(1000);
}


/**
 * Delete category
 */
$("#js_change_image").change(function () {

    var url = $("#base_url").val() + "admin/change_category_image";
    var id = $('#js_change_image_frm input[name=id]').val();

    var fd = new FormData();
    var file_data = $('#js_change_image')[0].files; // for multiple files

    for (var i = 0; i < file_data.length; i++) {
        fd.append("image", file_data[i]);
    }

    var other_data = $('#js_change_image_frm').serializeArray();

    $.each(other_data, function (key, input) {
        fd.append(input.name, input.value);
    });

    $.ajax({
        method: "POST",
        url: url,
        data: fd,
        contentType: false,
        processData: false,
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        if (result) {
            $("#js_image_edit").attr('src', result);
            $(".js_tr_" + id + " img").attr('src', result);
        }
    })
    .done(function (result) {

    });
});


/**
 * Edit category form submit
 */
// $("#edit_category_frm").submit(function () {
//     if ($("#edit_category_frm").valid()) {

//         var url = $("#base_url").val() + "admin/update_category";

//         var name = $("#edit_category_frm input[name=name]").val();
//         var price = $("#edit_category_frm input[name=price]").val();
//         var description = $("#edit_category_frm textarea[name=description]").val();
//         var id = $("#edit_category_frm input[name=id]").val();

//         var skin_tone = $("#edit_category_frm select[name=skin_tone]").val();
//         var lips_color = $("#edit_category_frm input[name=lips_color]").val();
//         var eye_shadow = $("#edit_category_frm input[name=eye_shadow]").val();
//         var time = $("#edit_category_frm input[name=time]").val();

//         $.ajax({
//             method: "POST",
//             url: url,
//             data: {id: id, name: name, description: description, price: price, skin_tone: skin_tone, lips_color: lips_color, eye_shadow: eye_shadow, time: time},
//             async: 'sync'
//         })
//         .error(function (e) {

//         })
//         .success(function (result) {
//             data = JSON.parse(result);
//             if (data['status'] == 'success') {
//                 $(".js_tr_" + id + " .name").html(name);
//                 $(".js_tr_" + id + " .price").html(price);
//                 $(".js_tr_" + id + " .description").html(description);
//                 $(".js_tr_" + id + " .skin_tone").html(skin_tone);
//                 $(".js_tr_" + id + " .lips_color").html(lips_color);
//                 $(".js_tr_" + id + " .eye_shadow").html(eye_shadow);
//                 if (time && parseInt(time) > 0) {
//                     $(".js_tr_" + id + " .time").html(time);
//                 }
//                 $("#js_edit_category_button").click();
//                 alert('Category update successfully');
//             } else {
//                 alert(data['text']);
//             }
//         })
//         .done(function (result) {

//         });
//     }
//     return false;
// });

/**
 * Check artist availability
 */
function check_artist_availability(id) {
    var url = $("#base_url").val() + "admin/get_artist_availability";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);

    })
    .done(function (result) {

    });

    $("#js_model_button").click();
}

/**
 * Change artist availability status
 */
function change_artist_availability_status(id, elem) {
    var url = $("#base_url").val() + "admin/change_artist_availability_status";
    var status = $(elem).attr('status');

    $(elem).next('.js_loading_img').show();
    $(elem).hide();

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id, status: status},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(elem).attr('status', result);
        if (result == 'true') {
            $(elem).find('span').html('Active');
            $(elem).find('span').removeClass('label-danger').addClass('label-success');
        } else {
            $(elem).find('span').html('Inactive');
            $(elem).find('span').removeClass('label-success').addClass('label-danger');
        }
    })
    .complete(function (result) {
        $(elem).next('.js_loading_img').hide();
        $(elem).show();
    });
}


/**
 * Artist change status
 */
function artist_change_status(id, elem) {
    var url = $("#base_url").val() + "admin/artist_change_status";

    $(elem).next('.js_loading_img').show();
    $(elem).hide();

    var status = $(elem).attr('status');

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id, status: status},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        if (result == 'true') {
            $(elem).find('span').removeClass('label-danger').addClass('label-success');
            $(elem).find('span').html('Active');
        } else {
            $(elem).find('span').removeClass('label-success').addClass('label-danger');
            $(elem).find('span').html('Inactive');
        }
        $(elem).attr('status', result);
    })
    .complete(function (result) {
        $(elem).next('.js_loading_img').hide();
        $(elem).show();
    });
}

/**
 * Artist profile
 */
function artist_profile(id) {
    var url = $("#base_url").val() + "admin/artist_profile";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });

}

/**
 * Artist category
 */
function artist_category(id) {
    var url = $("#base_url").val() + "admin/artist_category";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });
}

/**
 * Artist protfolio
 */
function artist_portfolio(id) {
    var url = $("#base_url").val() + "admin/artist_portfolio";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        modal_width(800);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });
}

/**
 *Pagenation for models
 */
$("body").on('click', '.js_model_html .paginate_button', function () {
    var url = $(this).find('a').attr('url');

    $.ajax({
        method: "POST",
        url: url,
        data: {},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
    })
    .complete(function (result) {

    });
});

/*Set model width*/
function modal_width(width) {
    if (typeof (width) == 'number') {
        width = width + 'px';
    }
    $(".js-modal-width").css({'width': width});
}

/*Change model width*/
$('#js_model').on('hidden.bs.modal', function () {
    modal_width('');
});

/**
 * Artist client
 */
function artist_client(id) {
    var url = $("#base_url").val() + "admin/artist_client";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });
}

/**
 * Client change status
 */
function client_change_status(id, elem) {
    var url = $("#base_url").val() + "admin/client_change_status";

    $(elem).next('.js_loading_img').show();
    $(elem).hide();

    var status = $(elem).attr('status');

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id, status: status},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        if (result == 'true') {
            $(elem).find('span').removeClass('label-danger').addClass('label-success');
            $(elem).find('span').html('Active');
        } else {
            $(elem).find('span').removeClass('label-success').addClass('label-danger');
            $(elem).find('span').html('Inactive');
        }
        $(elem).attr('status', result);
    })
    .complete(function (result) {
        $(elem).next('.js_loading_img').hide();
        $(elem).show();
    });
}

/**
 * Change artist request status
 */
function change_artist_request_status(id, elem) {
    var confirm1 = confirm('Do you sure ?');
    if (confirm1) {
        var url = $("#base_url").val() + "admin/change_artist_request_status/" + id + "/" + $(elem).val();
        window.location.href = url;
    }
}

/**
 * Client profile
 */
function client_profile(id) {
    var url = $("#base_url").val() + "admin/client_profile";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });
}

/**
 * Artist commision
 */
function artist_commision(id) {
    var url = $("#base_url").val() + "admin/artist_commision";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });
}

/**
 * Artist commision
 */
$("body").on('click', '.js_change_artist_commision', function () {

    var commision = $(".js_model_html .js_commision").val();
    var id = $(".js_model_html .js_id").val();
    var url = $("#base_url").val() + "admin/change_artist_commision";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id, commision: commision},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".tr_" + id + " .commision").html(commision);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });
});

/*
 * Change password form validation
 */
$("#change_password_frm").validate({
    rules: {
        old_pass: "required",
        new_pass: {
            required: true,
            minlength: 8,
        },
        confirm_pass: {
            required: true,
            minlength: 8,
            equalTo: "#new_pass",
        }
    },
});

/**
 * Add plan form validation
 */
$("#add_plan_frm").validate({
    rules: {
        name: "required",
        amount: {
            required: true,
            number: true,
        },
        duration: {
            required: true,
            number: true,
        },
        //description: 'required',
        status: 'required'
    },
});

/**
 * Plan change status
 */
function change_plan_status(id, elem) {

    var url = $("#base_url").val() + "admin/change_plan_status";

    var status = $(elem).attr('status');

    $(elem).next('.js_loading_img').show();
    $(elem).hide();

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id, status: status},
        async: 'sync'

    })
    .error(function (e) {

    })
    .success(function (result) {
        if (result == "true") {
            $(elem).find('.action').removeClass('label-danger').addClass('label-success');
            $(elem).find('.action').html('Active');
            $(elem).attr('status', 'true');
        } else {
            $(elem).find('.action').removeClass('label-success').addClass('label-danger');
            $(elem).find('.action').html('Inactive');
            $(elem).attr('status', 'false');
        }
    })
    .done(function (result) {
        $(elem).next('.js_loading_img').hide();
        $(elem).show();
    });
}

/**
 * View transaction details
 */
function view_transaction_details(id) {
    var url = $("#base_url").val() + "admin/view_transaction_details";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });
}

/**
 * Form validation admin profile settings
 */
$("#profile_setting_frm").validate({
    rules: {
        name: "required",
        email: "required",
        phone: {
            required: true,
            number: true
        }
    },
});


/**
 * Change admin image
 */
$("#change_profile_img_frm").submit(function () {
    var formData = new FormData(this);

    var url = $("#base_url").val() + "admin/update_admin_image";

    $.ajax({
        method: "POST",
        url: url,
        data: formData,
        async: 'sync',
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false,
        beforeSend: function (e) {
            $(".js-profile-loader").show();
        }
    })
    .error(function (e) {

    })
    .success(function (result) {
        if (result) {
            $(".js_admin_image").attr('src', result);
        }
    })
    .complete(function (result) {
        $(".js-profile-loader").hide();
    });
    return false;
});

/**
 * Delete plan
 */
$(".delete_plan").click(function () {
    var confirmed = confirm("Are you sure ?");
    if (confirmed == true) {
        return true;
    } else {
        return false;
    }
});

/**
 * Change language
 */
$(".js_lanaguage_select").change(function () {
    // var url = $(".js_lanaguage_select option:selected").attr('url');
    // window.location.href = url;
    $("#js_lanaguage_select").click();
});

/**
 * On page ready events
 */
/*For remove alert if persent*/
setTimeout(function () {
    $(".alert").fadeOut("slow");
}, 2000);

/**
 * Update Portfolio model
 * @param id
 */
function open_model_for_update_category_porfolio(id) {

    var url = $("#base_url").val() + "admin/update_category_portfolio";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });
}

/**
 * Change category protfolio images
 */
$("body").on('change', '.js_caterogy_portfolio_img', function () {

    var url = $("#base_url").val() + "admin/get_crop_model";

    /* Flie upload */
//    var fd = new FormData();
    var file_data = $(this)[0].files[0]; // for multiple files
    
    var elem = $(this).attr('id');

//    fd.append('image', file_data);
//
//    fd.append('id', $("#category_id").val());
//
//    fd.append('type', $(this).attr('name'));

//    var elem = $(this)

//    if (file_data) {
//        var reader = new FileReader();
//
//        reader.onload = function (e) {
//            $(elem).parent().find('img').attr('src', e.target.result);
//        }
//
//        reader.readAsDataURL(file_data);
//    }


    /* Flie upload end */
//    $(".js_loading_img").removeClass('hide');

    $.ajax({
        method: "POST",
        url: url,
        data: {},
        async: 'sync',
    })
    .error(function (e) {

    })
    .success(function (result) {
        //$(elem).parent().find('img').attr('src',result);
        $(".js_secondary_model .js_secondary_model_html").html(result);
        
        if (file_data) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#croping_img").attr('src', e.target.result);
            }

            reader.readAsDataURL(file_data);
            
            setTimeout(function(){
                jcrop_on();
            },1000);
        }
        
        $('.js_secondary_model').modal('show'); 
        
        $(".js_crop_image_submit").attr("onclick","save_category_portfolio_img("+elem+",0)");
        $(".js_crop_image_skip").attr("onclick","save_category_portfolio_img("+elem+",1)");
    })
    .complete(function (result) {
//        $(".js_loading_img").addClass('hide');
        
    });
});

/**
 * Save category portfolio image
 * @param elem
 */
function save_category_portfolio_img(elem,skip){
    var url = $("#base_url").val() + "admin/update_category_portfolio";
    
    var file_data = $(elem)[0].files[0]; // for multiple files
    
    var fd = new FormData();
    
    fd.append('image', file_data);

    fd.append('id', $("#category_id").val());

    fd.append('type', $(elem).attr('name'));
    
    fd.append('x',$("#x1").val());
    
    fd.append('y',$("#y1").val());
    
    fd.append('skip',skip);
    
    $.ajax({
        method: "POST",
        url: url,
        data: fd,
        async: 'sync',
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false,
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_crop_image_close").click();
        $(elem).parent().find('img').attr('src',result);
    })
    .complete(function (result) {
        
    });
}

/**
 * Jcrop on
 * @param image
 */
var jcrop_api;
function jcrop_on(){
    $('#croping_img').Jcrop({
        onChange:   showCoords,
        onSelect:   showCoords,
        onRelease:  clearCoords,
        setSelect: [0, 600, 600, 0],
        minSize:    [600,600],
        maxSize:    [600,600]
      },function(){
        jcrop_api = this;
    });
}
$('#coords').on('change','input',function(e){
  var x1 = $('#x1').val(),
      x2 = $('#x2').val(),
      y1 = $('#y1').val(),
      y2 = $('#y2').val();
  jcrop_api.setSelect([x1,y1,x2,y2]);
});


// Simple event handler, called from onChange and onSelect
// event handlers, as per the Jcrop invocation above
function showCoords(c)
{
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
};

function clearCoords()
{
    $('#coords input').val('');
};

/**
 * Add referral form validation
 */
$("#add_referral_frm").validate({
    rules: {
        celebrity_name: "required",
        rebate_price: {
            required: true,
            number: true,
        },
        time_duration: {
            required: true,
            number: true,
        },
        //description: 'required',
        user_type: 'required'
    },
});

/* Change referral status */
function change_referral_status(id){
    var url = $("#base_url").val() + "admin/change_referral_status";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        if(result == true){
            $(".tr_"+id+" .status span").removeClass('label-danger').addClass('label-success').text('Active');
        }
        else{
            $(".tr_"+id+" .status span").removeClass('label-success').addClass('label-danger').text('Inactive');
        }
    })
    .complete(function (result) {

    });
}

/* Datepicker */
$( ".dp2" ).datepicker();

$('.date_range').daterangepicker();

/* Payment info update */
function update_payment_info_open_model(id){
    var url = $("#base_url").val() + "admin/update_payment_info/"+id;

    $.ajax({
        method: "POST",
        url: url,
        data: {},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);

    })
    .done(function (result) {

    });

    $("#js_model_button").click();
}

/* Pay to user */
function pay_to_user_open_model(id){
    var url = $("#base_url").val() + "admin/pay_to_user/"+id;

    $.ajax({
        method: "POST",
        url: url,
        data: {},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);

    })
    .done(function (result) {

    });

    $("#js_model_button").click();
    modal_width(1000);
}