

/**
 * change_paid_status status in admin
 */
function change_paid_status_old(id, elem) {
    var url = $("#base_url").val() + "admin/change_paid_status";

    $(elem).next('.js_loading_img').show();
    $(elem).hide();

    var status = $(elem).attr('status');

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id, status: status},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        if (result == 'true') {
            $(elem).find('span').removeClass('label-danger').addClass('label-success');
            $(elem).find('span').html('Active');
        } else {
            $(elem).find('span').removeClass('label-success').addClass('label-danger');
            $(elem).find('span').html('Inactive');
        }
        $(elem).attr('status', result);
    })
    .complete(function (result) {
        $(elem).next('.js_loading_img').hide();
        $(elem).show();
    });
}



/**
 * Artist profile
 */
 
function get_paid_data(id) {
    
    var url = $("#base_url").val() + "admin/get_paid_data";

    /*var $js_payment_type = $(".js_payment_type").val();
    var $js_payment_via = $(".js_payment_via").val();
    var $js_comment = $(".js_comment").val();*/

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();
    })
    .complete(function (result) {

    });

}


/**
 * Artist commision
 */
$("body").on('click', '.js_admin_payment_history_to_user', function () {

    var js_payment_type = $(".js_model_html .js_payment_type").val();
    var js_payment_via  = $(".js_model_html .js_payment_via").val();
    var js_comment      = $(".js_model_html .js_comment").val();
    var js_amount      = $(".js_model_html .js_amount").val();
    var js_account      = $(".js_model_html .js_account").val();
    
    var mua_id          = $('#_js_mua_id').val();
    
    
    //var id = $(".js_model_html .js_comment").val();
    var url       = $("#base_url").val() + "admin/add_paid_data";

    $.ajax({
        method: "POST",
        url: url,
        data: {js_payment_type: js_payment_type, js_payment_via: js_payment_via, js_comment: js_comment, mua_id: mua_id, js_amount: js_amount, js_account: js_account},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        console.log(result);
        if(result>0){
            alert('Payment made successfully.');
        }else{
            alert('They is some internal issue.\r\n please try after sometime.');
        }
    })
    .complete(function (result) {

    });
});













/**
 * Artist profile
 */
function payment_received(id) {
    var url = $("#base_url").val() + "admin/payment_received";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $("#js_model_button").click();

         
    })
    .complete(function (result) {

    });

}





function total_payment_received(id) {
    var url = $("#base_url").val() + "admin/total_payment_received";

    $.ajax({
        method: "POST",
        url: url,
        data: {id: id},
        async: 'sync'
    })
    .error(function (e) {

    })
    .success(function (result) {
        $(".js_model_html").html(result);
        $(".js_model_html").css({'width':'800px'});
        
        $("#js_model_button").click();

         
    })
    .complete(function (result) {

    });

}