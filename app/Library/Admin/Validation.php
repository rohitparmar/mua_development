<?php
class Validation{

	public $error = array();

	private $name;

	private $field;

	private $field_name;

	public $error_data = array();

	public function __construct(){

	}


	public function rules($name,$field,$rule){

		$this->name = $name;

		$this->field = $_POST[$field];

		$this->field_name = $field;

		$this->error_data[$field] = $_POST[$field];

		if($rule){
			$rules = explode('|',$rule);

			foreach($rules as $value){
				if(strpos($value,'[')){
					$rule = explode('[',$value);
					$rule[1] = str_replace(']','', $rule[1]);
					$this->$rule[0]($rule[1]);
				}
				else{
					$this->$value();
				}
			}
		}
	}


	public function required(){

		if(!$this->field) {
			$message = "The ".$this->name." field is required";
			$this->error[$this->field_name] = $message;
		}
	}


	public function email(){

		if(!filter_var($this->field, FILTER_VALIDATE_EMAIL)) {
			$message = "The ".$this->name." field is not a valid email";
			$this->error[$this->field_name] = $message;
		}
	}


	public function min_length($length){

		if(strlen($this->field) < $length) {
			$message = "The ".$this->name." field is minimum ".$length." character required";
			$this->error[$this->field_name] = $message;
		}
	}


	public function max_length($length){

		if(strlen($this->field) > $length) {
			$message = "The ".$this->name." field is maximum ".$length." character required";
			$this->error[$this->field_name] = $message;
		}
	}

	public function equalTo($equile){

		if($this->field != $_POST[$equile]) {
			$message = "Please enter the same value again";
			$this->error[$this->field_name] = $message;
		}
	}

	public function number(){

		if(!is_numeric($this->field)) {
			$message = "The ".$this->name." field is number";
			$this->error[$this->field_name] = $message;
		}
	}

	public function error($name){
		return isset($this->error[$name])?$this->error[$name]:"";
	}

	public function get_data($name){
		return isset($this->error_data[$name])?$this->error_data[$name]:"";
	}
}