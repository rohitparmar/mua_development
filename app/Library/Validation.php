<?php
class Validation{

	public $error = array();
	protected $name;
	protected $field;

	public function __construct(){

	}

	public function rules($name,$field,$rules){
		$this->name = $name;
		$this->field = $field;
		$rules = explode('|',$rules);

		foreach($rules as $value){
			call_user_func(array($this,$value));
		}
	}

	protected function email(){
		if (filter_var($this->field, FILTER_VALIDATE_EMAIL) === false) {
          $message = " ".$this->name." is not valid";
          $this->error[$this->name] = $message;
		}
	}

	protected function required(){
		if(!$this->field){
			$message = " ".$this->name." field is required";
			$this->error[$this->name] = $message;
		}
	}

	protected function number(){
		if(!is_numeric($this->field)){
			$message = " ".$this->name." is not a valid number";
			$this->error[$this->name] = $message;
		}
	}
}