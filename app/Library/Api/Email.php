<?php

class Email extends Layout{
	private $toEmail		= false;
	private $fromEmail 		= 'support@bebebellamuas.com';
	private $ccEmail 		= false;
	private $replyTo		= false;
	private $subject		= 'Bebebella Customer Support';
    private $message		= false;
	private $messageType 	= 'plain';

    public function __construct($app) {
        $this->app 			= $app;
		$this->fromEmail 	= 'support@bebebellamuas.com';
		$this->subject 		= 'Bebebella Customer Support';
    }
	
	public function to($toEmail){
		$this->toEmail = preg_replace('/[^a-zA-Z0-9\._\-@]/','',trim(strtolower($toEmail)));
	}
	
	public function from($fromEmail){
		$this->fromEmail = preg_replace('/[^a-zA-Z0-9\._\-@]/','',trim(strtolower($fromEmail)));
	}
	
	public function cc($ccEmail){
		$this->ccEmail = preg_replace('/[^a-zA-Z0-9\._\-@]/','',trim(strtolower($ccEmail)));
	}
	
	public function subject($subject){
		$this->subject = $subject;
	}
	
	public function messageType($messageType){
		$this->messageType = $messageType;
	}
	
	public function message($message){
		$this->message = $message;
	}
	
	public function send(){
		$headers = '';
		//main header
		if(trim($this->fromEmail)){
			$headers .= "From: ".$this->fromEmail. PHP_EOL;
		}
		if(trim($this->replyTo)){
			$headers .= "Reply-To: ".$this->replyTo. PHP_EOL;
		}
		if(trim($this->ccEmail)){
			$headers .= "CC: ".$this->ccEmail. PHP_EOL;
		}
		$headers .= "MIME-Version: 1.0".PHP_EOL;;
		$headers .= "Content-Type: text/".$this->messageType."; charset=ISO-8859-1". PHP_EOL;;

		return mail($this->toEmail,$this->subject,$this->message,$headers);
	}
	
}