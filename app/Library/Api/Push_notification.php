<?php

/* ---starts notification message constant--- */

define('BOOKING_Booking', 'booking');

define('PAYMENT_Payment', 'payment');

define('ACCEPTED_Artist', 'accepted');

define('DECLINED_Artist', 'declined');

define('START_Artist', 'start');

define('FINISHED_Artist', 'finished');

define('CANCELLED_Artist', 'cancelled');

/* ---ends notification message constant--- */

class Push_notification extends Layout {

    public $error = array();
    protected $name;
    protected $field;
    protected $app;
    private static $passphrase = 'joashp';

    public function __construct($app) {
        $this->app = $app;
        $this->load_model('Api/Client_model');
        $this->load_model('Api/Booking_model');
    }

    public function send_notification_booking($registrationId, $requsetId, $from_id, $devices_type, $lang) {


        $category_name_eng = '';
        $category_name_spn = '';

        $userData = $this->booking_model->get_mua_gcmID($from_id);

        $notification = $this->booking_model->get_notification(BOOKING_Booking);

        if (isset($notification) && $notification) {
            $title = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_title_spn'] : $notification['n_title_eng'];
            $message = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_message_spn'] : $notification['n_message_eng'];

            $category_name_eng = current($this->Client_model->get_category_name(decode_base64($requsetId), 1));
            $category_name_spn = current($this->Client_model->get_category_name(decode_base64($requsetId), 2));



            $un_message_eng = (isset($notification['n_message_eng']) ? $notification['n_message_eng'] : '') . ' of ' . (isset($category_name_eng['ca_name']) ? $category_name_eng['ca_name'] : '');

            $un_message_spn = (isset($notification['n_message_spn']) ? $notification['n_message_spn'] : '') . ' of ' . (isset($category_name_spn['ca_name']) ? $category_name_spn['ca_name'] : '');
        }



        $param = (object) array(
                    'user_id' => isset($registrationId['u_id']) ? $registrationId['u_id'] : '',
                    'from_id' => isset($from_id) ? $from_id : '',
                    'user_type' => isset($userData) ? $userData['u_type'] : '',
                    'title' => isset($title) ? $title : '',
                    'requsetId' => isset($requsetId) ? decode_base64($requsetId) : '',
                    'message' => isset($un_message_eng) ? $un_message_eng : '',
                    'message_spn' => isset($un_message_spn) ? $un_message_spn : '',
                    'type' => 'booking',
                    'date' => date("Y-m-d h:i:s")
        );


        $last_id = $this->Client_model->add_notification($param);

        if (isset($registrationId['u_device_type']) && $registrationId['u_device_type'] == '2') {

            $deviceToken = isset($registrationId['u_gcm_id']) ? $registrationId['u_gcm_id'] : '';

            $body['aps'] = array(
                'alert' => array(
                    'title' => isset($title) ? $title : '',
                    'body' => isset($message) ? $message : '',
                ),
                'sound' => 'default',
                'data' => array(
                    "u_type" => $registrationId['u_type'],
                    "requsetId" => $requsetId,
                    "notif_id" => encode_base64($last_id)
                )
            );

            $this->PushNotificationsIos($body, $deviceToken);
        } else {

            $fields = (object) array(
                        "to" => $registrationId['u_gcm_id'],
                        /* "notification" => (object)array(
                          "title" => "Bebe Bella",
                          ), */
                        "data" => (object) array(
                            "title" => isset($title) ? $title : '',
                            "u_type" => $registrationId['u_type'],
                            "requsetId" => $requsetId,
                            "notif_id" => encode_base64($last_id),
                            "body" => isset($message) ? $message : '',
                            "image" => "http://bebebellamuas.com/artist-client-upload-file/artist/certificate/ic_logo.png",
                            "image2" => "http://bebebellamuas.com/artist-client-upload-file/artist/certificate/notification.png"
                        )
            );
            $this->PushNotificationsAndroid($fields);
        }
    }

    /*
      ||=========================================================
      || @Function send notification by action
      ||=========================================================
     */

    public function send_notification_action($registrationId, $book_status, $requsetId, $from_id, $devices_type, $lang) {


        switch ($book_status) {
            case 'accepted':

                $notification = $this->booking_model->get_notification(ACCEPTED_Artist);

                if (isset($notification) && $notification) {

                    $title = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_title_spn'] : $notification['n_title_eng'];
                    $message = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_message_spn'] : $notification['n_message_eng'];

                    $category_name_eng = current($this->Client_model->get_category_name($requsetId, 1));
                    $category_name_spn = current($this->Client_model->get_category_name($requsetId, 2));



                    $un_message_eng = (isset($notification['n_message_eng']) ? $notification['n_message_eng'] : '') . ' of ' . (isset($category_name_eng['ca_name']) ? $category_name_eng['ca_name'] : '');

                    $un_message_spn = (isset($notification['n_message_spn']) ? $notification['n_message_spn'] : '') . ' of ' . (isset($category_name_spn['ca_name']) ? $category_name_spn['ca_name'] : '');
                }
                break;

            case 'declined':

                $notification = $this->booking_model->get_notification(DECLINED_Artist);

                if (isset($notification) && $notification) {
                    $title = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_title_spn'] : $notification['n_title_eng'];
                    $message = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_message_spn'] : $notification['n_message_eng'];

                    $category_name_eng = current($this->Client_model->get_category_name($requsetId, 1));
                    $category_name_spn = current($this->Client_model->get_category_name($requsetId, 2));

                    $un_message_eng = (isset($notification['n_message_eng']) ? $notification['n_message_eng'] : '') . ' of ' . (isset($category_name_eng['ca_name']) ? $category_name_eng['ca_name'] : '');

                    $un_message_spn = (isset($notification['n_message_spn']) ? $notification['n_message_spn'] : '') . ' of ' . (isset($category_name_spn['ca_name']) ? $category_name_spn['ca_name'] : '');
                }
                break;

            case 'start':

                $notification = $this->booking_model->get_notification(START_Artist);

                if (isset($notification) && $notification) {
                    $title = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_title_spn'] : $notification['n_title_eng'];
                    $message = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_message_spn'] : $notification['n_message_eng'];

                    $category_name_eng = current($this->Client_model->get_category_name($requsetId, 1));
                    $category_name_spn = current($this->Client_model->get_category_name($requsetId, 2));

                    $un_message_eng = (isset($notification['n_message_eng']) ? $notification['n_message_eng'] : '') . ' of ' . (isset($category_name_eng['ca_name']) ? $category_name_eng['ca_name'] : '');

                    $un_message_spn = (isset($notification['n_message_spn']) ? $notification['n_message_spn'] : '') . ' of ' . (isset($category_name_spn['ca_name']) ? $category_name_spn['ca_name'] : '');
                }
                break;

            case 'finished':

                $notification = $this->booking_model->get_notification(FINISHED_Artist);

                if (isset($notification) && $notification) {
                    $title = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_title_spn'] : $notification['n_title_eng'];
                    $message = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_message_spn'] : $notification['n_message_eng'];

                    $category_name_eng = current($this->Client_model->get_category_name($requsetId, 1));
                    $category_name_spn = current($this->Client_model->get_category_name($requsetId, 2));

                    $un_message_eng = (isset($notification['n_message_eng']) ? $notification['n_message_eng'] : '') . ' of ' . (isset($category_name_eng['ca_name']) ? $category_name_eng['ca_name'] : '');

                    $un_message_spn = (isset($notification['n_message_spn']) ? $notification['n_message_spn'] : '') . ' of ' . (isset($category_name_spn['ca_name']) ? $category_name_spn['ca_name'] : '');
                }
                break;

            case 'cancelled':

                $notification = $this->booking_model->get_notification(CANCELLED_Artist);

                if (isset($notification) && $notification) {
                    $title = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_title_spn'] : $notification['n_title_eng'];
                    $message = (isset($registrationId['u_lang']) && $registrationId['u_lang'] == 'spn') ? $notification['n_message_spn'] : $notification['n_message_eng'];

                    $category_name_eng = current($this->Client_model->get_category_name($requsetId, 1));
                    $category_name_spn = current($this->Client_model->get_category_name($requsetId, 2));

                    $un_message_eng = (isset($notification['n_message_eng']) ? $notification['n_message_eng'] : '') . ' of ' . (isset($category_name_eng['ca_name']) ? $category_name_eng['ca_name'] : '');

                    $un_message_spn = (isset($notification['n_message_spn']) ? $notification['n_message_spn'] : '') . ' of ' . (isset($category_name_spn['ca_name']) ? $category_name_spn['ca_name'] : '');
                }
                break;

            default:
                $message = 'The service has been';
                break;
        }

        $userData = $this->booking_model->get_mua_gcmID($from_id);


        $param = (object) array(
                    'user_id' => isset($registrationId['u_id']) ? $registrationId['u_id'] : '',
                    'from_id' => isset($from_id) ? $from_id : '',
                    'user_type' => isset($userData) ? $userData['u_type'] : '',
                    'title' => isset($title) ? $title : '',
                    'requsetId' => isset($requsetId) ? $requsetId : '',
                    'message' => isset($un_message_eng) ? $un_message_eng : '',
                    'message_spn' => isset($un_message_spn) ? $un_message_spn : '',
                    'type' => isset($book_status) ? $book_status : '',
                    'date' => date("Y-m-d h:i:s")
        );



        $last_id = $this->Client_model->add_notification($param);

        if (isset($registrationId['u_device_type']) && $registrationId['u_device_type'] == '2') {

            $deviceToken = isset($registrationId['u_gcm_id']) ? $registrationId['u_gcm_id'] : '';

            $body['aps'] = array(
                'alert' => array(
                    'title' => isset($title) ? $title : $book_status,
                    'body' => isset($message) ? $message : $book_status,
                ),
                'sound' => 'default',
                'data' => array(
                    "u_type" => $registrationId['u_type'],
                    "requsetId" => $requsetId,
                    "notif_id" => encode_base64($last_id)
                )
            );

            $this->PushNotificationsIos($body, $deviceToken);
        } else {

            $fields = (object) array(
                        "to" => $registrationId['u_gcm_id'],
                        /* "notification"  => (object)array(
                          "title" => "BeBe Bella",
                          "body"  => $book_msg
                          ), */
                        "data" => (object) array(
                            "title" => isset($title) ? $title : '',
                            "body" => isset($message) ? $message : '',
                            "u_type" => $registrationId['u_type'],
                            "requsetId" => $requsetId,
                            "notif_id" => encode_base64($last_id)
                        )
            );
            $this->PushNotificationsAndroid($fields);
        }
    }

    public function send_notification_payment($registrationId, $devices_type) {

        if (isset($devices_type) && $devices_type == 'ios') {

            $deviceToken = isset($registrationId['u_gcm_id']) ? $registrationId['u_gcm_id'] : '';

            $body['aps'] = array(
                'alert' => array(
                    'title' => 'Bebe Bella',
                    'body' => 'The service has been approved.',
                ),
                'sound' => 'default',
                "data" => array(
                    "u_type" => $registrationId['u_type']
                )
            );

            $this->PushNotificationsIos($body, $deviceToken);
        } else {

            $fields = (object) array(
                        "to" => $registrationId['u_gcm_id'],
                        "notification" => (object) array(
                            "title" => "Bebe Bella",
                            "body" => "payment",
                        /* "body"  => "The service has been approved." */
                        ),
                        "data" => (object) array(
                            "u_type" => $registrationId['u_type']
                        )
            );
            $this->PushNotificationsAndroid($fields);
        }
    }

    public function send_renew_reminder_notification($registrationId, $message) {



        if (isset($registrationId['u_device_type']) && $registrationId['u_device_type'] == 2) {

            $deviceToken = isset($registrationId['u_gcm_id']) ? $registrationId['u_gcm_id'] : '';

            $body['aps'] = array(
                'alert' => array(
                    'title' => 'Bebe Bella',
                    'body' => $message,
                ),
                'sound' => 'default',
                "data" => array(
                    "u_type" => $registrationId['u_type']
                )
            );

            $this->PushNotificationsIos($body, $deviceToken);
        } else {

            $fields = (object) array(
                        "to" => $registrationId['u_gcm_id'],
                        "notification" => (object) array(
                            "title" => "Bebe Bella",
                            "body" => $message,
                        /* "body"  => "The service has been approved." */
                        ),
                        "data" => (object) array(
                            "u_type" => $registrationId['u_type']
                        )
            );
            $this->PushNotificationsAndroid($fields);
        }
    }

    /* PushNotifications for Android mobile using the curl */

    private function PushNotificationsAndroid($fields) {

        
        

        $headers = array(
                        'Authorization:key=' . API_ACCESS_KEY,
                        'Content-Type: application/json'
                    );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
    }

    /* PushNotifications for ios mobile using the pam file and ssl certificate */

    private function PushNotificationsIos($body, $deviceToken) {


        $msg_payload = array(
            'mtitle' => 'Test push notification title',
            'mdesc' => 'Test push notification body',
        );
        $ctx = stream_context_create();


        // ck.pem is your certificate file
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'BellaAPNSProd.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', self::$passphrase);

        $fp = false;


        // Open a connection to the APNS server
        $fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx
        );

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);


        // Encode the payload as JSON
        $payload = json_encode($body);



        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        // Close the connection to the server
        fclose($fp);
        //ob_end_clean();

        /* if (!$result)

          echo 'Message not delivered' . PHP_EOL;
          else
          echo 'Message successfully delivered' . PHP_EOL; */
    }

}
