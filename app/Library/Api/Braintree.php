<?php

/*
  //for help
  //https://developers.braintreepayments.com/reference/response/subscription/php


  Braintree_Subscription::create()
  Braintree_Subscription::cancel()
  Braintree_Subscription::update()
  Braintree_Subscription::find()
  Braintree_Subscription::search()
  Braintree_Subscription::retryCharge()
 */

class Braintree {

    public $error = array();
    protected $name;
    protected $field;
    protected $_data;

    public function __construct() {
        include_once (APP_DIR . '/app/Third-Party/Api/braintree/Braintree.php');
        Braintree_Configuration::environment(ENVIRONMENT);
        Braintree_Configuration::merchantId(MERCHANTID);
        Braintree_Configuration::publicKey(PUBLICKEY);
        Braintree_Configuration::privateKey(PRIVATEKEY);
    }

    /**
     *
     * Author: synergytop->36
     * params: none
     * return: Array(
      'clientToken': $token,
      'error'		 : 0,
      'message'	 : ''
      )
     *
     * */
    public function generate_token() {
        try {
            $clientToken = Braintree_ClientToken::generate();

            $res = array(
                'clientToken' => $clientToken,
                'error' => 0,
                'message' => ''
            );
        } catch (Exception $e) {
            $res = array(
                'clientToken' => '',
                'error' => 1,
                'message' => $e->getMessage()
            );
        }
        return $res;
    }

    /**
     *
     * Author: synergytop->36
     * params: $nonceFromTheClient(string), $amount (double)
     * return: Array(
      'clientToken': $token,
      'error'		 : 0,
      'message'	 : ''
      )
     *
     * */
    public function get_nonce($nonceFromTheClient, $amount = 1) {
        $result = Braintree_Transaction::sale([
                    'amount' => $amount,
                    'paymentMethodNonce' => $nonceFromTheClient,
                    'options' => [
                        'submitForSettlement' => True,
                        'storeInVaultOnSuccess' => true
                    ]
        ]);
        return $result;
    }

    public function add_customerId($param = '') {

        /* s */

        $result = Braintree_Customer::create([
                    'firstName' => 'Mike',
                    'lastName' => 'Jones',
                    'company' => 'Jones Co.',
                    'email' => 'mike.jones@example.com',
                    'phone' => '281.330.8004',
                    'fax' => '419.555.1235',
                    'website' => 'http://example.com'
        ]);


        //print_r($result->customer);
        //print_r($result->customer->id); die; 
        // print_r($result->customer->Braintree_Customer['_attributes:protected']);     
        //die;

        return $result;
    }
	
	public function get_plan_list(){
		try {
            $result = Braintree_Plan::all();
        } catch (Exception $e) {
			$result = false;
        }
        return $result;
	}
	
    public function add_user_card_details($nonce) {
        try {
            $result = Braintree_Customer::create(array(
                        'paymentMethodNonce' => $nonce,
                        'creditCard' => array(
                            'options' => array(
                                'verifyCard' => true
                            )
                        )
            ));
        } catch (Exception $e) {

            echo 'Message: ' . $e->getMessage();
        }
        return $result;
    }

    public function verify_payment_card($nonce, $customer) {
        //print_r($nonce); die;


        /* try{ */
        $result = Braintree_PaymentMethod::create([
                    'customerId' => $customer,
                    'paymentMethodNonce' => $nonce,
                    'options' => [
                        'verifyCard' => true,
                        'verificationMerchantAccountId' => MERCHANTID,
                    ]
        ]);

        return $result;
        /*
          }catch(Exception $e){

          echo 'Message: ' .$e->getMessage();
          }
          return $result; */
    }

    protected function getData($aa) {
        print_r($aa);
        die;
        return $this->_data;
    }

    public function payment_with_token1($amount,$token) {
        //print_r($param);

        $result = Braintree_Transaction::sale(array(
                    'amount' => $amount,
                    'paymentMethodToken' => $token
        ));
        return $result;
    }
    
    public function payment_with_token($param) {
        //print_r($param);

        $result = Braintree_Transaction::sale(array(
                    'amount' => $param->amount,
                    'paymentMethodToken' => $param->token
        ));
        return $result;
    }

    /*
     * 
     */

    public function create_payment($amount, $nonceFromTheClient) {
        $card_detail = array(
            'amount' => $amount,
            'paymentMethodNonce' => $nonceFromTheClient,
            'options' => array(
                'submitForSettlement' => True,
                'storeInVaultOnSuccess' => true
            )
        );
        $result = Braintree_Transaction::sale($card_detail);
        return $result;
    }

    public function cancel_payment() {
        //for help
        //http://www.hurricanesoftwares.com/integrating-braintree-payment-gateway-with-php
        $result = Braintree_Subscription::cancel($result->subscription->id);
        return 'cancel_payment';
    }

    public function cancel_subscription() {
        //for help
        //http://www.hurricanesoftwares.com/integrating-braintree-payment-gateway-with-php
        $result = Braintree_Subscription::cancel($result->subscription->id);

        if ($result->success) {
            //echo("Subscription ID: " . $result->subscription->id . " cancelled<br />");
        } else {
            foreach ($result->errors->deepAll() as $error) {
                echo($error->code . ": " . $error->message . "<br />");
            }
            exit;
        }

        return true;
    }

    public function braintree_delete_card_token($card_token) {
        $result = Braintree_PaymentMethod::delete($card_token);
        return $result;
    }

    public function get_braintree_payment_details($card_token) {
        //print_r($card_token); die;
        $result = Braintree_PaymentMethod::find($card_token);
        return $result;
    }

    public function update_card_details($card_details) {
        //print_r($card_details); die;




        $result = Braintree_PaymentMethod::update(
                        $card_details->card_token, array(
                    //'cardholderName' => 'Doe Jhon',
                    'cvv' => $card_details->cvv,
                    'expirationMonth' => $card_details->expirationMonth,
                    'expirationYear' => $card_details->expirationYear,
                    'number' => $card_details->number,
                    'billingAddress' => array(
                        'streetAddress' => $card_details->streetAddress,
                        'extendedAddress' => $card_details->extendedAddress,
                        'locality' => $card_details->locality,
                        'region' => $card_details->region,
                        'postalCode' => $card_details->postalCode,
                        'options' => array(
                            'updateExisting' => true
                        )
                    )
                        )
        );
        return $result;
    }

    public function update_subscription() {
        $result = Braintree_Subscription::create([
                    'paymentMethodToken' => 'the_token',
                    'planId' => 'thePlanId',
                    'addOns' => [
                        'add' => [
                            [
                                'inheritedFromId' => 'addOnId1',
                                'amount' => '20.00'
                            ],
                            [
                                'inheritedFromId' => 'addOnId2',
                                'amount' => '30.00'
                            ]
                        ],
                        'update' => [
                            [
                                'existingId' => 'addOnId3',
                                'quantity' => 2
                            ],
                            [
                                'existingId' => 'addOnId4',
                                'quantity' => 3
                            ]
                        ],
                        'remove' => ['addOnId5', 'addOnId6']
                    ]
        ]);
        return true;
    }
    
    public function create_subscription($paymentMethodToken, $planId, $trialDuration=0) {
        $trial_period = ($trialDuration>0)?TRUE:FALSE;
        try {
            $result = Braintree_Subscription::create([
            'paymentMethodToken' => $paymentMethodToken, //'kxp92y', //fqg3yd',
            'planId' => $planId,
            'trialDuration' => $trialDuration,
            'trialPeriod' => $trial_period,
            'trialDurationUnit' => 'day'  //month, day
          ]);
        } catch (Exception $e) {
            $result = array();
        }
        return $result;
    }
    
    public function create_subscription_old($paymentMethodToken, $planId, $trialDuration=0) {
        
        $result = Braintree_Subscription::create([
            'paymentMethodToken' => $paymentMethodToken, //'kxp92y', //fqg3yd',
            'planId' => $planId,
            'trialDuration' => $trialDuration,
            'trialPeriod' => true,
            'trialDurationUnit' => 'day'  //month, day
          ]);
        print_r($result);
        die;
        
        
        $tomorrow = new DateTime("now + 0 day");
        $tomorrow->setTime(0,0,0);

        $result = Braintree_Subscription::create([
          'paymentMethodToken' => 'fqg3yd',
          'planId' => 'bb_1',
          'firstBillingDate' => $tomorrow
        ]);

        //$result->subscription->firstBillingDate;
        # tomorrow

        //$result->subscription->status;
        # Braintree_Subscription::PENDING
        
        return $result;
        die;
        
        
        $result = Braintree_Subscription::create([
                    'paymentMethodToken' => 'fqg3yd',
                    'planId' => 'bb_2'
        ]);
        
        return $result;

        //for help
        //https://developers.braintreepayments.com/reference/request/subscription/create/php

        $result = Braintree_Subscription::create([
                    'paymentMethodToken' => 'the_token',
                    'planId' => 'silver_plan'
        ]);


        $result = Braintree_Subscription::create([
                    'paymentMethodToken' => 'the_token',
                    'planId' => 'thePlanId',
                    'addOns' => [
                        'add' => [
                            [
                                'inheritedFromId' => 'addOnId1',
                                'amount' => '20.00'
                            ],
                            [
                                'inheritedFromId' => 'addOnId2',
                                'amount' => '30.00'
                            ]
                        ],
                        'update' => [
                            [
                                'existingId' => 'addOnId3',
                                'quantity' => 2
                            ],
                            [
                                'existingId' => 'addOnId4',
                                'quantity' => 3
                            ]
                        ],
                        'remove' => ['addOnId5', 'addOnId6']
                    ]
        ]);

        return 'make_subscription';
    }

}
