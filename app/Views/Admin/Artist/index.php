<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Artist</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Artist</li>
      <li class="active">Artist list</li>
    </ol>
</section>
<div class="box">
  <div class="box-body">

    <!-- Search -->
    <div class="row">
      <div class="col-sm-6">
      </div>
      <div class="col-sm-6">
        <div id="custom_datatable_filter" class="dataTables_filter">
          <form action="<?php echo base_url('admin/artist'); ?>">
            <label>Search:<input type="search" name="search" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['search'])?$_GET['search']:""; ?>" aria-controls="custom_datatable"></label>
          </form>
        </div>
      </div>
    </div>
    <!-- /Search -->

    <table id="custom_datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>City/Town</th>
          <!-- <th>Comission</th> -->
          <th>Referrel Info<br />Used/Earned</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $page_tab_options = 10; $i = 1; $i=$i+($page-1)*$page_tab_options; foreach($artist_list as $value){ ?>
        <?php 
          $status = ($value['u_status'] == 'true')?'Active':'Inactive';
          $status_class = ($value['u_status'] == 'true')?'label label-success':'label label-danger';
        ?>
          <tr class="tr_<?php echo $value['u_id']; ?>">
            <td><?php echo $i; ?></td>
            <td><?php echo $value['ap_name']; ?></td>
            <td><?php echo $value['u_email']; ?></td>
            <td><?php echo $value['u_phone']; ?></td>
            <td><?php echo $value['ap_city']; ?></td>
            <!-- <td class="commision"><?php echo ($value['ap_pay_commission'])?$value['ap_pay_commission']:$commision['s_value']; ?></td> -->
            <td>
              
              <a href="javascript:void(0);" onclick="get_mua_used_referrel(<?php echo $value['r_id']; ?>)">
                <span title="Total Used Referral"><?php echo $value['used_referral']; ?></span>/
                <span title="Total Earned Referral"><?php echo ($value['earned_referral'])?$value['earned_referral']:'0'; ?></span>
              </a>
            </td>
              
            
            <td>
              <a href="javascript:void(0);" status = "<?php echo $value['u_status']; ?>" onclick='artist_change_status(<?php echo $value['u_id']; ?>,$(this));'><span class="<?php echo $status_class; ?>"><?php echo $status; ?></span></a>
              <img src="<?php echo base_url('themes/admin/custom/images/loading.gif'); ?>" class="js_loading_img" style='display: none; width: 16px;'><!-- Status -->


              <a href="javascript:void(0);" class="hide" onclick="check_artist_availability(<?php echo $value['u_id']; ?>)"><img title='Availability' src="<?php echo base_url('themes/admin/custom/images/availability.gif'); ?>" width='20px;'></a><!-- Availability -->

              <a href="javascript:void(0);" onclick="artist_profile(<?php echo $value['u_id']; ?>)"><img title='Profile' src="<?php echo base_url('themes/admin/custom/images/profile.jpg'); ?>" width='20px;'></a><!-- Profile -->

              <a href="javascript:void(0);" class='hide' onclick="artist_category(<?php echo $value['u_id']; ?>)"><img title="Category" src="<?php echo base_url('themes/admin/custom/images/category.png'); ?>" width='20px;'></a><!-- Category -->

              <a href="javascript:void(0);" onclick="artist_portfolio(<?php echo $value['u_id']; ?>)"><img title="Portfolio" src="<?php echo base_url('themes/admin/custom/images/portfolio.png'); ?>" width='20px;'></a><!-- Portfolio -->

              <a href="javascript:void(0);" class='hide' onclick="artist_client(<?php echo $value['u_id']; ?>)"><img title="Client" src="<?php echo base_url('themes/admin/custom/images/client.png'); ?>" width='20px;'></a><!-- Artist client -->

              <!--  <a href="javascript:void(0);" onclick="artist_commision(<?php echo $value['u_id']; ?>)"><img title="Commision" src="<?php echo base_url('themes/admin/custom/images/commision.png'); ?>" width='20px;'></a> -->

              <a href="javascript:void(0);" onclick="artist_certificate(<?php echo $value['u_id']; ?>)"><img title="Certificate" src="<?php echo base_url('themes/admin/custom/images/certificate.png'); ?>" width='20px;'></a><!-- Commision -->
            </td>
          </tr>
        <?php $i++; } ?>
      </tbody>
    </table>

    <!-- Pagination -->
    <div class="row">
      <div class="col-sm-5">
          <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite"><strong>Showing <?php echo (($page - 1)*10)+1; ?> to <?php echo (($page - 1)*10)+count($artist_list); ?> of <?php echo $artist_count; ?> entries
        </strong></div>
      </div>
      <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
          <ul class="pagination">
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/artist/'.(1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">First</a></li>
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/artist/'.($page - 1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0"><<</a></li>
            <?php
              
              if($page == 1){
                $start = 1;
              }
              elseif((int)($artist_count/10) == 1){
                $start = $page - 1;
              }
              elseif($page == ((int)($artist_count/10)+1)){
                $start = $page - 2;
              }
              else{
                $start = $page-1;
              }
              for($i = 1; $i <= $page_tab_options; $i++){ 

                if($start > ceil(($artist_count/10))){
                  continue;
                }
              ?>
                <li class="paginate_button <?php echo ($page == $start)?'active':""; ?>"><a href="<?php echo base_url('admin/artist/'.$start).$query; ?>" aria-controls="datatable" data-dt-idx="1" tabindex="0"><?php echo $start; ?></a></li>
            <?php $start++; } ?>

            <li class="paginate_button next <?php echo ($page == ceil(($artist_count/10)))?"disabled":''; ?>" id="datatable_next"><a href="<?php echo ($page != ceil(($artist_count/10)))?(base_url('admin/artist/'.($page + 1)).$query):'javascript:void(0);'; ?>" aria-controls="datatable" data-dt-idx="2" tabindex="0">>></a></li>
            <li class="paginate_button previous <?php echo ($page == ceil($artist_count/$page_tab_options))?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != ceil($artist_count/$page_tab_options))?(base_url('admin/artist/'.(ceil($artist_count/$page_tab_options))).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">Last</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- /Pagination -->

  </div><!-- /.box-body -->
</div><!-- /.box -->


<!-- Profile Model -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog js-modal-width" role="document">
    <div class="modal-content js_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>
