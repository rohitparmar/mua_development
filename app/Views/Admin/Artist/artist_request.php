<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Artist</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Artist</li>
      <li class="active">Artist request</li>
    </ol>
</section>
<div class="box">
  <div class="box-body">

    <!-- Search -->
    <div class="row">
      <div class="col-sm-6">
      </div>
      <div class="col-sm-6">
        <div id="custom_datatable_filter" class="dataTables_filter">
          <form action="<?php echo base_url('admin/artist_request'); ?>">
            <label>Search:<input type="search" name="search" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['search'])?$_GET['search']:""; ?>" aria-controls="custom_datatable"></label>
          </form>
        </div>
      </div>
    </div>
    <!-- /Search -->
    
    <table id="custom_datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>City/Town</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; foreach($artist_list as $value){ ?>
        <?php 
          $status = ($value['u_status'] == 'true')?'Active':'Inactive';
          $status_class = ($value['u_status'] == 'true')?'label label-success':'label label-danger';
        ?>
          <tr class="tr_<?php echo $value['u_id']; ?>">
            <td><?php echo $i; ?></td>
            <td><?php echo $value['ap_name']; ?></td>
            <td><?php echo $value['u_email']; ?></td>
            <td><?php echo $value['u_phone']; ?></td>
            <td><?php echo $value['ap_city']; ?></td>
            <td>
              <select name="action" class="form-control" onchange="change_artist_request_status(<?php echo $value['u_id']; ?>,$(this),'Are you sure?')">
                <?php if($value['u_varified'] == 'new'){ ?>
                  <option value="new" <?php echo ($value['u_varified'] == 'new')?"selected":""; ?>>New</option>
                <?php } ?>
                <option value="on-hold" <?php echo ($value['u_varified'] == 'on-hold')?"selected":""; ?>>On-hold</option>
                <option value="accepted" <?php echo ($value['u_varified'] == 'accepted')?"selected":""; ?>>Accepted</option>
                <option value="rejected" <?php echo ($value['u_varified'] == 'rejected')?"selected":""; ?>>Rejected</option>
              </select><!-- Status -->

              <a href="javascript:void(0);" onclick="artist_profile(<?php echo $value['u_id']; ?>)"><img title='Profile' src="<?php echo base_url('themes/admin/custom/images/profile.jpg'); ?>" width='20px;'></a><!-- Profile -->

              <a href="javascript:void(0);" onclick="artist_portfolio(<?php echo $value['u_id']; ?>)"><img title="Portfolio" src="<?php echo base_url('themes/admin/custom/images/portfolio.png'); ?>" width='20px;'></a><!-- Portfolio -->
              
            </td>
          </tr>
        <?php $i++; } ?>
      </tbody>
    </table>

    <!-- Pagination -->
    <div class="row">
      <div class="col-sm-5">
        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php echo (($page - 1)*10)+1; ?> to <?php echo (($page - 1)*10)+count($artist_list); ?> of <?php echo $artist_count; ?> entries
        </div>
      </div>
      <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
          <ul class="pagination">
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/artist_request/'.($page - 1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">Previous</a></li>

            <?php
              if($page == 1){
                $start = 1;
              }
              elseif((int)($artist_count/10) == 1){
                $start = $page - 1;
              }
              elseif($page == ((int)($artist_count/10)+1)){
                $start = $page - 2;
              }
              else{
                $start = $page-1;
              }
              for($i = 1; $i <= 3; $i++){ 

                if($start > ceil(($artist_count/10))){
                  continue;
                }
              ?>
                <li class="paginate_button <?php echo ($page == $start)?'active':""; ?>"><a href="<?php echo base_url('admin/artist_request/'.$start).$query; ?>" aria-controls="datatable" data-dt-idx="1" tabindex="0"><?php echo $start; ?></a></li>
            <?php $start++; } ?>

            <li class="paginate_button next <?php echo ($page == ceil(($artist_count/10)))?"disabled":''; ?>" id="datatable_next"><a href="<?php echo ($page != ceil(($artist_count/10)))?(base_url('admin/artist_request/'.($page + 1)).$query):'javascript:void(0);'; ?>" aria-controls="datatable" data-dt-idx="2" tabindex="0">Next</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- /Pagination -->

  </div><!-- /.box-body -->
</div><!-- /.box -->


<!-- Profile Model -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog js-modal-width" role="document">
    <div class="modal-content js_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>