<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Bebebella Admin | Dashboard</title>

        <!-- Tell the browser to be responsive to screen width -->

        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <!-- Bootstrap 3.3.5 -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/bootstrap/css/bootstrap.min.css'); ?>">

        <!-- Font Awesome -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <!-- Ionicons -->

        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Theme style -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/dist/css/AdminLTE.min.css'); ?>">

        <!-- AdminLTE Skins. Choose a skin from the css/skins
    
             folder instead of downloading all of them to reduce the load. -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/dist/css/skins/_all-skins.min.css'); ?>">

        <!-- iCheck -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/plugins/iCheck/flat/blue.css'); ?>">

        <!-- Morris chart -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/plugins/morris/morris.css'); ?>">

        <!-- jvectormap -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>">

        <!-- Date Picker -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/plugins/datepicker/datepicker3.css'); ?>">

        <!-- Daterange picker -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/plugins/daterangepicker/daterangepicker-bs3.css'); ?>">

        <!-- bootstrap wysihtml5 - text editor -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">



        <!-- DataTables -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/plugins/datatables/dataTables.bootstrap.css'); ?>">



        <!-- Custom css -->

        <link rel="stylesheet" href="<?php echo base_url('themes/admin/custom/css/style.css'); ?>">



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>
    
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    
        <![endif]-->

    </head>

    <body class="hold-transition skin-blue sidebar-mini fixed">



        <div class="wrapper">



            <header class="main-header">

                <!-- Logo -->

                <a href="<?php echo base_url('admin'); ?>" class="logo">

                    <!-- mini logo for sidebar mini 50x50 pixels -->

                    <span class="logo-mini"><b>A</b></span>

                    <!-- logo for regular state and mobile devices -->

                    <span class="logo-lg"><b>Admin</b></span>

                </a>

                <!-- Header Navbar: style can be found in header.less -->

                <nav class="navbar navbar-static-top" role="navigation">

                    <!-- Sidebar toggle button-->

                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">

                        <span class="sr-only">Toggle navigation</span>

                    </a>

                    <div class="navbar-custom-menu">

                        <ul class="nav navbar-nav">

                            <!-- Messages: style can be found in dropdown.less-->

                            <?php if (isset($messages)) { ?>

                                <li class="dropdown messages-menu" style="display:none;">

                                    <?php echo $messages; ?>

                                </li>

                            <?php } ?>

                            <!-- Notifications: style can be found in dropdown.less -->

                            <?php if (isset($notifications)) { ?>

                                <li class="dropdown notifications-menu" style='display: none;'>

                                    <?php echo $notifications; ?>

                                </li>

                            <?php } ?>

                            <!-- User Account: style can be found in dropdown.less -->

                            <?php if (isset($admin_profile)) { ?>

                                <li class="dropdown user user-menu">

                                    <?php echo $admin_profile; ?>

                                </li>

                            <?php } ?>

                        </ul>

                    </div>

                </nav>

            </header>

            <!-- Left side column. contains the logo and sidebar -->

            <aside class="main-sidebar">

                <!-- sidebar: style can be found in sidebar.less -->

                <section class="sidebar">

                    <!-- search form -->

                    <?php // echo isset($side_bar_search)?$side_bar_search:'';?>

                    <!-- /.search form -->

                    <!-- sidebar menu: : style can be found in sidebar.less -->

                    <?php echo isset($side_bar_menu) ? $side_bar_menu : ''; ?>

                </section>

                <!-- /.sidebar -->

            </aside>



            <!-- Content Wrapper. Contains page content -->

            <div class="content-wrapper">

                <!-- Content Header (Page header) -->

                <section class="content-header" style="display:none;">

                    <h1>

                        Dashboard

                        <small>Control panel</small>

                    </h1>

                    <ol class="breadcrumb">

                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

                        <li class="active">Dashboard</li>

                    </ol>

                </section>



                <!-- Alert -->

                <?php echo isset($alert) ? $alert : ""; ?>

                <!-- /.Alert -->



                <!-- Main content -->

                <section class="content">

                    <!-- Small boxes (Stat box) -->

                    <div class="row">

                        <div class="col-lg-12">

                            <?php echo isset($contents) ? $contents : ''; ?>

                        </div><!-- ./col -->

                    </div><!-- /.row -->



                </section><!-- /.content -->

            </div><!-- /.content-wrapper -->









            <footer class="main-footer">

                <div class="pull-right hidden-xs">



                </div>

                <strong>Copyright &copy; 2016-2017

            </footer>



            <!-- Control Sidebar -->

            <aside class="control-sidebar control-sidebar-dark">

                <!-- Create the tabs -->

                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">

                    <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>

                </ul>

                <!-- Tab panes -->

                <div class="tab-content">

                    <!-- Home tab content -->

                    <div class="tab-pane" id="control-sidebar-home-tab">

                        <h3 class="control-sidebar-heading">Recent Activity</h3>

                        <ul class="control-sidebar-menu">

                            <li>

                                <a href="javascript::;">

                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                    <div class="menu-info">

                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                        <p>Will be 23 on April 24th</p>

                                    </div>

                                </a>

                            </li>

                            <li>

                                <a href="javascript::;">

                                    <i class="menu-icon fa fa-user bg-yellow"></i>

                                    <div class="menu-info">

                                        <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                        <p>New phone +1(800)555-1234</p>

                                    </div>

                                </a>

                            </li>

                            <li>

                                <a href="javascript::;">

                                    <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                    <div class="menu-info">

                                        <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                        <p>nora@example.com</p>

                                    </div>

                                </a>

                            </li>

                            <li>

                                <a href="javascript::;">

                                    <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                    <div class="menu-info">

                                        <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                        <p>Execution time 5 seconds</p>

                                    </div>

                                </a>

                            </li>

                        </ul><!-- /.control-sidebar-menu -->



                        <h3 class="control-sidebar-heading">Tasks Progress</h3>

                        <ul class="control-sidebar-menu">

                            <li>

                                <a href="javascript::;">

                                    <h4 class="control-sidebar-subheading">

                                        Custom Template Design

                                        <span class="label label-danger pull-right">70%</span>

                                    </h4>

                                    <div class="progress progress-xxs">

                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>

                                    </div>

                                </a>

                            </li>

                            <li>

                                <a href="javascript::;">

                                    <h4 class="control-sidebar-subheading">

                                        Update Resume

                                        <span class="label label-success pull-right">95%</span>

                                    </h4>

                                    <div class="progress progress-xxs">

                                        <div class="progress-bar progress-bar-success" style="width: 95%"></div>

                                    </div>

                                </a>

                            </li>

                            <li>

                                <a href="javascript::;">

                                    <h4 class="control-sidebar-subheading">

                                        Laravel Integration

                                        <span class="label label-warning pull-right">50%</span>

                                    </h4>

                                    <div class="progress progress-xxs">

                                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>

                                    </div>

                                </a>

                            </li>

                            <li>

                                <a href="javascript::;">

                                    <h4 class="control-sidebar-subheading">

                                        Back End Framework

                                        <span class="label label-primary pull-right">68%</span>

                                    </h4>

                                    <div class="progress progress-xxs">

                                        <div class="progress-bar progress-bar-primary" style="width: 68%"></div>

                                    </div>

                                </a>

                            </li>

                        </ul><!-- /.control-sidebar-menu -->



                    </div><!-- /.tab-pane -->

                    <!-- Stats tab content -->

                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->

                    <!-- Settings tab content -->

                    <div class="tab-pane" id="control-sidebar-settings-tab">

                        <form method="post">

                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">

                                <label class="control-sidebar-subheading">

                                    Report panel usage

                                    <input type="checkbox" class="pull-right" checked>

                                </label>

                                <p>

                                    Some information about this general settings option

                                </p>

                            </div><!-- /.form-group -->



                            <div class="form-group">

                                <label class="control-sidebar-subheading">

                                    Allow mail redirect

                                    <input type="checkbox" class="pull-right" checked>

                                </label>

                                <p>

                                    Other sets of options are available

                                </p>

                            </div><!-- /.form-group -->



                            <div class="form-group">

                                <label class="control-sidebar-subheading">

                                    Expose author name in posts

                                    <input type="checkbox" class="pull-right" checked>

                                </label>

                                <p>

                                    Allow the user to show his name in blog posts

                                </p>

                            </div><!-- /.form-group -->



                            <h3 class="control-sidebar-heading">Chat Settings</h3>



                            <div class="form-group">

                                <label class="control-sidebar-subheading">

                                    Show me as online

                                    <input type="checkbox" class="pull-right" checked>

                                </label>

                            </div><!-- /.form-group -->



                            <div class="form-group">

                                <label class="control-sidebar-subheading">

                                    Turn off notifications

                                    <input type="checkbox" class="pull-right">

                                </label>

                            </div><!-- /.form-group -->



                            <div class="form-group">

                                <label class="control-sidebar-subheading">

                                    Delete chat history

                                    <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>

                                </label>

                            </div><!-- /.form-group -->

                        </form>

                    </div><!-- /.tab-pane -->

                </div>

            </aside><!-- /.control-sidebar -->

            <!-- Add the sidebar's background. This div must be placed
      
                 immediately after the control sidebar -->

            <div class="control-sidebar-bg"></div>

        </div><!-- ./wrapper -->

        <?php if (isset($_SESSION['error'])) { ?>

            <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <h4><i class="icon fa fa-ban"></i> Error!</h4>

                <?php echo $_SESSION['error']; ?>

            </div>

            <?php unset($_SESSION['error']);
        } ?>



        <!-- Hidden input for base url -->

        <input type="hidden" id='base_url' value="<?php echo base_url(); ?>">



        <!-- jQuery 2.1.4 -->

        <script src="<?php echo base_url('themes/admin/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>

        <!-- jQuery UI 1.11.4 -->

        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

        <script>

            $.widget.bridge('uibutton', $.ui.button);

        </script>

        <!-- Bootstrap 3.3.5 -->

        <script src="<?php echo base_url('themes/admin/bootstrap/js/bootstrap.min.js'); ?>"></script>

        <!-- Morris.js charts -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

        <script src="<?php echo base_url('themes/admin/plugins/morris/morris.min.js'); ?>"></script>

        <!-- Sparkline -->

        <script src="<?php echo base_url('themes/admin/plugins/sparkline/jquery.sparkline.min.js'); ?>"></script>

        <!-- jvectormap -->

        <script src="<?php echo base_url('themes/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>"></script>

        <script src="<?php echo base_url('themes/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>"></script>

        <!-- jQuery Knob Chart -->

        <script src="<?php echo base_url('themes/admin/plugins/knob/jquery.knob.js'); ?>"></script>

        <!-- daterangepicker -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

        <script src="<?php echo base_url('themes/admin/plugins/daterangepicker/daterangepicker.js'); ?>"></script>

        <!-- datepicker -->

        <script src="<?php echo base_url('themes/admin/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>

        <!-- Bootstrap WYSIHTML5 -->

        <script src="<?php echo base_url('themes/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>

        <!-- Slimscroll -->

        <script src="<?php echo base_url('themes/admin/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>

        <!-- FastClick -->

        <script src="<?php echo base_url('themes/admin/plugins/fastclick/fastclick.min.js'); ?>"></script>

        <!-- AdminLTE App -->

        <script src="<?php echo base_url('themes/admin/dist/js/app.min.js'); ?>"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

        <script src="<?php echo base_url('themes/admin/dist/js/pages/dashboard.js'); ?>"></script>

        <!-- AdminLTE for demo purposes -->

        <script src="<?php echo base_url('themes/admin/dist/js/demo.js'); ?>"></script>

        <!-- Datatable -->

        <script src="<?php echo base_url('themes/admin/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>

        <script src="<?php echo base_url('themes/admin/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>



        <!-- Validation js -->

        <script src="<?php echo base_url('themes/admin/custom/js/validation.js'); ?>"></script>

        <!-- Custom js -->

        <script src="<?php echo base_url('themes/admin/custom/js/custom.js'); ?>"></script>



        <!-- Datepicker -->

    <!-- <link rel="stylesheet" href="<?php echo base_url('themes/admin/datepicker/css/datepicker.css'); ?>">

    <script src="<?php echo base_url('themes/admin/datepicker/js/bootstrap-datepicker.js'); ?>"></script> -->


    <script src="<?php echo base_url('themes/admin/custom/js/settings/skintones.js'); ?>"></script>

    <script src="<?php echo base_url('themes/admin/custom/js/transaction/mua_payment_info.js'); ?>"></script>




    </body>

</html>