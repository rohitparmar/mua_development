<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Client</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Client</li>
      <li class="active">Client list</li>
    </ol>
</section>
<div class="box">

  <div class="box-body">



    <!-- Search -->

    <div class="row">

      <div class="col-sm-6">

      </div>

      <div class="col-sm-6">

        <div id="custom_datatable_filter" class="dataTables_filter">

          <form action="<?php echo base_url('admin/client'); ?>">

            <label>Search:<input type="search" name="search" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['search'])?$_GET['search']:""; ?>" aria-controls="custom_datatable"></label>

          </form>

        </div>

      </div>

    </div>

    <!-- /Search -->



    <table id="example2" class="table table-bordered table-hover">

      <thead>

        <tr>

          <th>#</th>

          <th>Name</th>

          <th>Email Id</th>

          <th>City</th>

          <th>Phone No.</th>
          
          <th>Referrel Info<br />Used/Earned</th>

          <th>Action</th>

        </tr>

      </thead>

      <tbody>

        <?php $i = 1; foreach($client_list as $value){ ?>

          <?php

            $status = ($value['u_status'] == 'true')?'Active':'Inactive';

            $status_class = ($value['u_status'] == 'true')?'label label-success':'label label-danger';

          ?>

          <tr class="">

            <td><?php echo $i; ?></td>

            <td><?php echo $value['u_name']; ?></td>

            <td><?php echo $value['u_email']; ?></td>

            <td><?php echo $value['city']; ?></td>

            <td><?php echo $value['u_phone']; ?></td>
            
            <td>
            <a href="javascript:void(0);" onclick="get_client_used_referrel(<?php echo $value['r_id']; ?>)">
                <span title="Total Used Referral"><?php echo $value['used_referral']; ?></span>/
                <span title="Total Earned Referral"><?php echo ($value['earned_referral'])?$value['earned_referral']:'0'; ?></span>
            </a>
            </td>

            <td>

              <a href="javascript:void(0);" status = "<?php echo $value['u_status']; ?>" onclick='client_change_status(<?php echo $value['u_id']; ?>,$(this));'><span class="<?php echo $status_class; ?>"><?php echo $status; ?></span></a>

              <img src="<?php echo base_url('themes/admin/custom/images/loading.gif'); ?>" class="js_loading_img" style='display: none; width: 16px;'><!-- Status -->



              <a href="javascript:void(0);" onclick="client_profile(<?php echo $value['u_id']; ?>)"><img title="Profile" src="<?php echo base_url('themes/admin/custom/images/profile.jpg'); ?>" width='20px;'></a><!-- Profile -->

            </td>

          </tr>

        <?php $i++; } ?>

      </tbody>

      

    </table>



    <!-- Pagination -->

    <div class="row">

      <div class="col-sm-5">

        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php echo (($page - 1)*10)+1; ?> to <?php echo (($page - 1)*10)+count($client_list); ?> of <?php echo $client_count; ?> entries

        </div>

      </div>

      <div class="col-sm-7">

        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">

          <ul class="pagination">

            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/client/'.($page - 1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">Previous</a></li>



            <?php

              if($page == 1){

                $start = 1;

              }

              elseif((int)($client_count/10) == 1){

                $start = $page - 1;

              }

              elseif($page == ((int)($client_count/10)+1)){

                $start = $page - 2;

              }

              else{

                $start = $page-1;

              }

              for($i = 1; $i <= 3; $i++){ 



                if($start > ceil(($client_count/10))){

                  continue;

                }

              ?>

                <li class="paginate_button <?php echo ($page == $start)?'active':""; ?>"><a href="<?php echo base_url('admin/client/'.$start).$query; ?>" aria-controls="datatable" data-dt-idx="1" tabindex="0"><?php echo $start; ?></a></li>

            <?php $start++; } ?>



            <li class="paginate_button next <?php echo ($page == ceil(($client_count/10)))?"disabled":''; ?>" id="datatable_next"><a href="<?php echo ($page != ceil(($client_count/10)))?(base_url('admin/client/'.($page + 1)).$query):'javascript:void(0);'; ?>" aria-controls="datatable" data-dt-idx="2" tabindex="0">Next</a></li>

          </ul>

        </div>

      </div>

    </div>

    <!-- /Pagination -->



  </div>

</div>





<!-- Model -->

<!-- Button trigger modal -->

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>

  Launch demo modal

</button>



<!-- Modal -->

<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog js-modal-width" role="document">

    <div class="modal-content js_model_html">

      <!-- popup contents goes here -->

    </div>

  </div>

</div>