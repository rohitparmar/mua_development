
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Change password</h3>
    </div><!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" action="" method="post" id='change_password_frm'>
      <div class="box-body">

        <div class="form-group">
          <label class="col-sm-2 control-label">Old password</label>
          <div class="col-sm-10">
            <input type="password" name="old_pass" class="form-control" placeholder="Old password">
            <span class="error"><?php echo $Validation->error('old_pass'); ?></span>
            <span class="error"><?php echo isset($old_pass_error)?$old_pass_error:""; ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">New password</label>
          <div class="col-sm-10">
            <input type="password" name="new_pass" class="form-control" placeholder="New password" id="new_pass">
            <span class="error"><?php echo $Validation->error('new_pass'); ?></span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Confirm password</label>
          <div class="col-sm-10">
            <input type="password" name="confirm_pass" class="form-control" placeholder="Confirm password">
            <span class="error"><?php echo $Validation->error('confirm_pass'); ?></span>
          </div>
        </div>

      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary pull-right">Submit</button>
      </div><!-- /.box-footer -->
    </form>
  </div><!-- /.box -->