<div class="col-md-9">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
      <li><a href="#settings" data-toggle="tab">Settings</a></li>
      <li><a href="#change_password" data-toggle="tab">Change password</a></li>
    </ul>

    <div class="tab-content">

      <div class="active tab-pane" id="profile">
        <!-- Post -->
        <div class="post">
          <div class="user-block" style="position:relative;">
            <?php 
              if($profile['ad_image'] && file_exists(ADMIN_PROFILE_IMG.$profile['ad_image'])){
                $img_path = base_url(ADMIN_PROFILE_IMG.$profile['ad_image']);
              }
              else{
                $img_path = base_url(ADMIN_PROFILE_IMG.'profile.jpg');
              }
            ?>
            <div style="height:200px; width:150px; float:left;">
	            <img class="js-profile-loader" src="<?php echo base_url('themes/admin/custom/images/ajax-loader.gif');?>" style="position:absolute;bottom: 80%;left: 7%;height: auto !important; display:none;">
				
	            <img class="img-circle img-bordered-sm js_admin_image" style="width: 150px; height: 150px;" src="<?php echo $img_path; ?>" alt="user image" onclick="$('#change_profile_img').click();">

	            <center><button onclick="$('#change_profile_img').click();" class='btn btn-primary' style="margin-top: 10px;">Change image</button></center>
	            <form id="change_profile_img_frm" method="post" enctype="multipart/form-data">
	              <input type="file" name="file" id="change_profile_img" style="display:none;" onchange="$('#change_profile_img_frm').submit()">
	            </form>
			</div>
            <div style="margin-left: 153px; margin-top:10px;">
              <span class="username" style="font-size: 41px;">
                <a><?php echo $profile['ad_name']; ?></a>
              </span>
              <?php 
                $type = "";
                if($profile['ad_type'] == 'admin'){
                  $type = "Admin";
                }

                $status = "";
                if($profile['ad_status'] == 'true'){
                  $status = "Active";
                }
                else{
                  $status = 'Inactive';
                }
              ?>

              <ul style="font-size: x-large;">
                <li>Email : <span><?php echo $profile['ad_email']; ?></span></li>
                <li>Type : <span><?php echo $type; ?></span></li>
                <li>Status : <span><?php echo $status; ?></span></li>
              </ul>

            </div>
          </div><!-- /.user-block -->

          

        </div><!-- /.post -->
      </div><!-- /.tab-pane -->

      <div class="tab-pane" id="settings">

        <form class="form-horizontal" action="" method="post" id='profile_setting_frm'>
          <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
              <input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo $profile['ad_name']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $profile['ad_email']; ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-10">
              <input type="text" name="phone" class="form-control" placeholder="Phone" value="<?php echo $profile['ad_phone']; ?>">
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div>
          </div>
        </form>
      </div><!-- /.tab-pane -->

      <div class="tab-pane" id="change_password">

          <form class="form-horizontal" action="<?php echo base_url('admin/change_password'); ?>" method="post" id='change_password_frm'>
            <div class="box-body">

              <div class="form-group">
                <label class="col-sm-2 control-label">Old password</label>
                <div class="col-sm-10">
                  <input type="password" name="old_pass" class="form-control" placeholder="Old password">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">New password</label>
                <div class="col-sm-10">
                  <input type="password" name="new_pass" class="form-control" placeholder="New password" id="new_pass">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Confirm password</label>
                <div class="col-sm-10">
                  <input type="password" name="confirm_pass" class="form-control" placeholder="Confirm password">
                </div>
              </div>

            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div><!-- /.box-footer -->
          </form>

      </div><!-- /.tab-pane -->
    </div><!-- /.tab-content -->
  </div><!-- /.nav-tabs-custom -->
</div><!-- /.col -->