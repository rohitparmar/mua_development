<div class="row">

<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Dashboard</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
</section>
  
  <div class="col-lg-3 col-xs-4">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3><?php echo $artist_count['count']; ?></h3>
        <p>Total artist</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <!-- <a href="<?php echo base_url('admin/artist'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
    </div>
  </div><!-- ./col -->

  <div class="col-lg-3 col-xs-4">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo $client_count['count']; ?></h3>
        <p>Total client</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <!-- <a href="<?php echo base_url('admin/client'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
    </div>
  </div><!-- ./col -->

  <div class="col-lg-3 col-xs-4">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo $artist_request_count['count']; ?></h3>
        <p>Total artist request</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <!-- <a href="<?php echo base_url('admin/artist_request'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
    </div>
  </div><!-- ./col -->

  <div class="col-lg-3 col-xs-4">
    <!-- small box -->
    <div class="small-box bg-blue">
      <div class="inner">
        <h3><?php echo $booking_count['count']; ?></h3>
        <p>Total services</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      <!-- <a href="<?php echo base_url('admin/service'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
    </div>
  </div><!-- ./col -->

  <div class="col-lg-3 col-xs-4 hidden">
    <!-- small box -->
    <div class="small-box bg-red">
      <div class="inner">
        <h3>65</h3>
        <p>Unique Visitors</p>
      </div>
      <div class="icon">
        <i class="ion ion-pie-graph"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div><!-- ./col -->

</div>


<div class="row">
  <div class="col-lg-6">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Recent 5 artist</h3>
      </div><!-- /.box-header -->
      <div class="box-body no-padding">
        <table class="table table-condensed">
          <tbody>
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>Email</th>
              <th style="width: 40px">Status</th>
            </tr>
            <?php $i = 1; foreach($recent_5_artist as $value){
              $status = ($value['u_status'] == 'true')?'Active':'Inactive';
              $status_class = ($value['u_status'] == 'true')?'label label-success':'label label-danger';
            ?>
              <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $value['name']; ?></td>
                <td><?php echo $value['u_email']; ?></td>
                <td><span class="<?php echo $status_class; ?>"><?php echo $status; ?></span></td>
              </tr>
            <?php $i++; } ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div>
  </div>

  <div class="col-lg-6">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Recent 5 client</h3>
      </div><!-- /.box-header -->
      <div class="box-body no-padding">
         <table class="table table-condensed">
          <tbody>
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>Email</th>
              <th style="width: 40px">Status</th>
            </tr>
            <?php $i = 1; foreach($recent_5_client as $value){
              $status = ($value['u_status'] == 'true')?'Active':'Inactive';
              $status_class = ($value['u_status'] == 'true')?'label label-success':'label label-danger';
            ?>
              <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $value['u_name']; ?></td>
                <td><?php echo $value['u_email']; ?></td>
                <td><span class="<?php echo $status_class; ?>"><?php echo $status; ?></span></td>
              </tr>
            <?php $i++; } ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->


     </div>
  </div>
</div>








<!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('themes/admin/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('themes/admin/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('themes/admin/plugins/fastclick/fastclick.min.js');?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('themes/admin/plugins/dist/js/app.min.js');?>"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url('themes/admin/plugins/sparkline/jquery.sparkline.min.js');?>"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url('themes/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
    <script src="<?php echo base_url('themes/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="<?php echo base_url('themes/admin/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="<?php echo base_url('themes/admin/plugins/chartjs/Chart.min.js');?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo base_url('themes/admin/plugins/dist/js/pages/dashboard2.js');?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('themes/admin/plugins/dist/js/demo.js');?>"></script>