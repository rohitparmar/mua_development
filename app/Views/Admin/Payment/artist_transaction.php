<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Transaction</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Transaction</li>
      <li class="active">Client payment</li>
    </ol>
</section>
<div class="box">
  <div class="box-body">

    <!-- Search -->
    <div class="row">
      <div class="col-sm-6">
      </div>


      
    </div>
    <!-- /Search -->
    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Prices</th>
                <th>RPM</th>
                <th>Distance</th>
                <th>Commission</th>
                
                <th>Pay To Artist</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
          <?php
            if(isset($result) && $result){

              foreach ($result as $key => $value){
          ?>
            <tr>
                <?php $prices =  (isset($value['cr_commission']) && $value['cr_commission']>0)?$value['cr_commission']/2:0;?>
                <td><?php echo $value['cpi_service_amout'];?></td>
                <td><?php echo $value['cpi_booking_time_rate_per_mile'];?></td>
                <td><?php echo $value['cpi_booking_time_distance'];?></td>
                <td><?php echo $value['cr_commission']; //number_format(floor($prices*100)/100,2, '.', '');?>  </td>
                <td><?php echo $value['cr_com_grand_price']/2;?></td>
                <td><?php echo $value['cpi_created'];?></td>
            </tr>

            <?php  } } ?>
            
        </tbody>
</table>



  </div><!-- /.box-body -->
</div><!-- /.box -->


<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    
 




<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>