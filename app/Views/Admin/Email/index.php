<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Support</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Support</li>
      <li class="active">Email list</li>
    </ol>
</section>
<div class="box">
  <div class="box-body">

    <!-- Search -->
    <div class="row">
      <div class="col-sm-6">
      </div>
      <div class="col-sm-6">
        <div id="custom_datatable_filter" class="dataTables_filter">
          <form action="<?php echo base_url('admin/Support'); ?>">
            <label>Search:<input type="search" name="search" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['search'])?$_GET['search']:""; ?>" aria-controls="custom_datatable"></label>
          </form>
        </div>
      </div>
    </div>
    <!-- /Search -->

    <table id="custom_datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Email</th>
          <th>Subject</th>
          <th>Message</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; foreach($email_list as $value){ ?>
        <?php 
          
          $status_class = ($value['um_admin_status'] == 'request')?'label label-danger':'label label-success';
        ?>
          <tr class="tr_<?php echo $value['u_id']; ?>">
            <td> <?php echo $i; ?></td>
            <td> <?php echo $value['u_name']; ?></td>
            <td> <?php echo $value['u_email']; ?></td>
            <td> <?php echo $value['um_subject']; ?></td>
            <td> <?php echo $value['um_message']; ?></td>
            <td> 
            <a>
            <span class="<?php echo $status_class?>"><?php echo isset($value['um_admin_status'])?$value['um_admin_status']:''?></span></a>
            </td>
            <td>
              <a href="javascript:void(0);" onclick="email_model(<?php echo $value['um_id']; ?>)">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
              </a>
              
              <a href="javascript:void(0);" onclick="admin_email_model(<?php echo $value['um_id']; ?>)">
                Details
              </a>
            </td>
          </tr>
        <?php $i++; } ?>
      </tbody>
    </table>

    <!-- Pagination -->
    <div class="row">
      <div class="col-sm-5">
        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php echo (($page - 1)*10)+1; ?> to <?php echo (($page - 1)*10)+count($email_list); ?> of <?php echo $email_count; ?> entries
        </div>
      </div>
      <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
          <ul class="pagination">
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/email/'.(1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">First</a></li>
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/email/'.($page - 1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0"><<</a></li>
            <?php
              $page_tab_options = 10;
              if($page == 1){
                $start = 1;
              }
              elseif((int)($email_count/10) == 1){
                $start = $page - 1;
              }
              elseif($page == ((int)($email_count/10)+1)){
                $start = $page - 2;
              }
              else{
                $start = $page-1;
              }
              for($i = 1; $i <= $page_tab_options; $i++){ 

                if($start > ceil(($email_count/10))){
                  continue;
                }
              ?>
                <li class="paginate_button <?php echo ($page == $start)?'active':""; ?>"><a href="<?php echo base_url('admin/email/'.$start).$query; ?>" aria-controls="datatable" data-dt-idx="1" tabindex="0"><?php echo $start; ?></a></li>
            <?php $start++; } ?>

            <li class="paginate_button next <?php echo ($page == ceil(($email_count/10)))?"disabled":''; ?>" id="datatable_next"><a href="<?php echo ($page != ceil(($email_count/10)))?(base_url('admin/email/'.($page + 1)).$query):'javascript:void(0);'; ?>" aria-controls="datatable" data-dt-idx="2" tabindex="0">>></a></li>
            <li class="paginate_button previous <?php echo ($page == ceil($email_count/$page_tab_options))?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != ceil($email_count/$page_tab_options))?(base_url('admin/email/'.(ceil($email_count/$page_tab_options))).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">Last</a></li>
         </ul>
        </div>
      </div>
    </div>
    <!-- /Pagination -->

  </div><!-- /.box-body -->
</div><!-- /.box -->


<!-- Profile Model -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog js-modal-width" role="document">
    <div class="modal-content js_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>