<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Category</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Category</li>
      <li class="active">Add Category</li>
    </ol>
</section>
<div class="box box-primary col-md-6">
  <div class="box-header with-border">
    <h3 class="box-title"><?php echo $header['title']; ?></h3>
  </div><!-- /.box-header -->
  <?php if(isset($_SESSION['success']) && $_SESSION['success']) {unset($_SESSION['success'])?>
  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
    <h4>	<i class="icon fa fa-check"></i> Success!</h4>
    Category Added successfully
  </div>
  <?php }?>
  <!-- form start -->
  <form role="form" action='' method="post" id="add_category_frm" enctype="multipart/form-data">
    <div class="box-body">

      <div class="form-group">
        <label><input type="checkbox" name="add_subcategory" class="minimal js_add_subcategory" value="true">Add sub-category</label>
      </div>

      <div class="form-group js_sub_category_div hide">
        <label>Category</label>
        <select class="form-control js_sub_category_select" name="category[]">
          <option value="" data_parent='1'>--Select category--</option>
            <?php foreach($category_list as $value){ ?>
              <option value="<?php echo $value['ca_fk_cm_id']; ?>"><?php echo $value['ca_name']; ?></option>
            <?php } ?>
        </select>
      </div><!-- Main category -->

      <div class="form-group">
        <label>Category name</label>
        <input type="text" name="sub_category_name" class="form-control" placeholder="Category name">
      </div><!-- Category name -->

      <div class="form-group js_category_price">
          <div class="row">
            <div class="col-sm-3">
        <label>Price</label>
        <input type="number" name="sub_category_price" class="form-control" min="1" step="0.5" value="1"/>
      </div></div></div><!-- Price -->
      
      <div class="form-group js_category_time">
        <div class="row">
            <div class="col-sm-3">
                <label>Service Duration</label>

                <input type="number" name="sub_category_time" class="form-control" min="15" step="1" value="15"/>
          </div>
        </div>
      </div>    <!--Service Duration-->
      
      <div class="form-group">
        <label for="sub_category_desc">Description</label>
        <textarea name="sub_category_desc" class="form-control" id="sub_category_desc" placeholder="Description" rows="5"></textarea>
      </div><!-- Description -->

      <div class="form-group js_category_skin_tone">
        <label>Skintones</label>
        <?php $skin_tones = array_chunk($skin_tone, 2);?>
        <div class="row">
            <?php foreach($skin_tones as $skin_tone){?>
            <div class="col-sm-<?php echo 12/count($skin_tones);?>">
                <?php foreach($skin_tone as $value){ ?>
                <div class="checkbox well">
                  <label>
                      <input type="checkbox" class="js_skin_tone_checkbox" name="sub_category_skin_tone[<?php echo $value['ms_id']; ?>]" value="<?php echo $value['ms_id']; ?>">
                    <?php echo $value['ms_name']; ?>
                  </label>
                  <input type="file" class="js_skin_tone_img" name="sub_category_skintone_img[<?php echo $value['ms_id']; ?>]">
                </div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
      </div><!-- Skin tone -->
      
      <div class="form-group js_category_skin_tone">
        <label>Color of Lips</label>
        <?php $lips_colors = array_chunk($lips_color, 2); ?>
        <div class="row">
            <?php foreach($lips_colors as $lips_color){?>
            <div class="col-sm-<?php echo 12/count($lips_colors);?>">
                <?php foreach($lips_color as $value){ ?>
                <div class="checkbox well">
                  <label>
                      <input type="checkbox" name="lips_color[]" value="<?php echo $value['mlc_id']; ?>">
                    <?php echo $value['mlc_name']; ?>
                  </label>
                </div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
      </div><!-- Color of lips -->
      
      <div class="form-group js_category_skin_tone">
        <label>Eye Shadows</label>
        <?php $eyeshadows = array_chunk($eyeshadow, 2); ?>
        <div class="row">
            <?php foreach($eyeshadows as $eyeshadow){?>
            <div class="col-sm-<?php echo 12/count($eyeshadows);?>">
                <?php foreach($eyeshadow as $value){ ?>
                <div class="checkbox well">
                  <label>
                      <input type="checkbox" name="eyeshadow[]" value="<?php echo $value['me_id']; ?>">
                    <?php echo $value['me_name']; ?>
                  </label>
                </div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
      </div><!-- Eye Shadows -->
	  <div class="row">
                    <div class="col-sm-4">
      <div class="form-group">
        <label for="sub_image">Image (jpg, jpeg, png, gif)</label>
        <input type="file" name="sub_image" class="form-control" id="sub_image" placeholder="Description">
      </div>

      <div class="form-group">
        <label><input type="radio" name="status" checked class="minimal" value="true">Active</label>
        <label><input type="radio" name="status" class="minimal" value="false">Inactive</label>
      </div>
      </div></div>

    </div><!-- /.box-body -->
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
  <!-- form end -->

</div>