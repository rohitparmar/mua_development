<script src="<?php echo base_url('themes/admin/custom/js/category/category.js');?>"></script>
<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Category</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Category</li>
      <li class="active">Category list</li>
    </ol>
</section>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="box-title">
                    <?php if ((isset($category_list[0]['mc_fk_parent_id'])) && ($category_list[0]['mc_fk_parent_id'] == 0)) { ?>
                        Main category list
                        <?php
                    } elseif ((isset($category_list[0]['mc_fk_parent_id'])) && ($category_list[0]['mc_fk_parent_id'] != 0)) {
                        ?>
                        <?php echo ucfirst($category_list[0]['parent_name']); ?>
                        <?php
                    }
                    ?>
                </h3>
            </div>
            <div class="col-sm-6">
                <div id="datatable_filter" class="dataTables_filter">
                    <form class="inline">
                        <select class="js_lanaguage_select selectpicker" name="lang">
                            <option value="1" <?php echo ($language == 1) ? "selected" : ""; ?>>English</option>
                            <option value="2" <?php echo ($language == 2) ? "selected" : ""; ?>>Spanish</option>
                        </select>
                        <input type="submit" name="" class="hide" id="js_lanaguage_select">
                    </form>
                    <?php
                    if ((isset($category_list[0]['cm_fk_parent_id'])) && ($category_list[0]['cm_fk_parent_id'] != 0)) {
                        ?>
                        <a href='<?php echo base_url('admin/category/' . base64_encode($category_list[0]['parent_id']).'?lang='.$language); ?>'><button class="btn btn-primary">Back</button></a>
                        <?php
                    }
                    ?>
                    <a href="<?php echo base_url('admin/add-category'); ?>"><button class="btn btn-primary">Add category</button></a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="datatable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Skintone</th>
                        <th>Color of lips</th>
                        <th>Eye shadow</th>
                        <th>Time</th>
                        <th class="no_sort">Description</th>
                        <th class="no_sort">Sub category</th>
                        <th class="no_sort">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($category_list as $value) { //print_r($value); die;
                        ?>
                        <?php
                        $change_status_class = ($value['cm_status'] == 'true') ? "label label-success" : "label label-danger";
                        ?>
                        <tr class='js_tr_<?php echo $value['cm_id']; ?>'>
                            <td><?php echo $i; ?></td>
                            <td class="image">
                                <?php
                                   
                                $cat_img = "default_cat.png";
                                   if($value['cm_image'] && file_exists(CATEGORY_IMG.$value['cm_image'])){
                                       $cat_img = $value['cm_image'];
                                   }
                                ?>
                                <img width = '35px' src="<?php echo base_url(CATEGORY_IMG . $cat_img); ?>">
                            </td>
                            <td class='name'><?php echo ucfirst($value['ca_name']); ?></td>
                            <td>
                                <?php if (!$value['sub_category'] && $value['cm_price']) { ?>
                                    $<b class='price'><?php echo ucfirst($value['cm_price']); ?></b>
                                <?php } ?>
                            </td>
                            <td class='skin_tone'>
                                <?php $v=explode(',',$value['skin_tone']); 
                                    foreach($v as $rr){
                                        if($rr)
                                        echo '<span>&#8680;</span>'.$rr.'<br>';
                                    }
                                ?>
                            </td>
                            <td class='lips_color'>
                                
                                <?php $v=explode(',',$value['lips_color']); 
                                    foreach($v as $rr){
                                        if($rr)
                                        echo '<span>&#8680;</span>'.ucfirst($rr).'<br>';
                                    }
                                ?>
                                
                                </td>
                            <td class='eye_shadow'>
                                <?php $v=explode(',',$value['eyeshadow']); 
                                    foreach($v as $rr){
                                        if($rr)
                                        echo '<span>&#8680;</span>'.ucfirst($rr).'<br>';
                                    }
                                ?>
                                </td>
                            <td class='time'><?php echo ($value['cm_duration']) ? $value['cm_duration']." minutes" : ""; ?></td>
                            <td class="description"><?php echo $value['ca_description']; ?></td>
                            <td>
                                <?php if ($value['sub_category']) { ?>
                                    <a href="<?php echo base_url('admin/category/' . base64_encode($value['sub_category']).'?lang='.$language); ?>"><button class="btn btn-primary">Sub category</button></a>
                                <?php } ?>
                            </td>
                            <td width="15%">
                                <a href="javascript:void(0);" onclick='category_change_status(<?php echo $value["cm_id"]; ?>, $(this))' status='<?php echo $value["cm_status"]; ?>'><span class="action <?php echo $change_status_class ?>"><?php echo ($value['cm_status'] == 'true') ? "Active" : "Inactive"; ?></span></a>
                                <img style="display: none; width: 16px;" class='js_loading_img' src="<?php echo base_url('themes/admin/custom/images/loading.gif'); ?>">
                                
                                <a href="javascript:void(0);" onclick="open_model_for_edit_category(<?php echo $value['cm_id']; ?>,<?php echo ($value['sub_category']) ? 0 : 1; ?>)"><button class="fa fa-pencil-square-o btn-primary"></button></a>
                                
                                <?php if (!$value['sub_category']) { ?>
                                    <a href="javascript:void(0);" onclick="delete_category(<?php echo $value['cm_id']; ?>, $(this))"><button class="fa fa-remove btn-danger"></button></a>
                                    <img style="display: none; width: 16px;" class='js_loading_img' src="<?php echo base_url('themes/admin/custom/images/loading.gif'); ?>">
                                    <a href="javascript:void(0);" onclick="open_model_for_update_category_porfolio(<?php echo $value['cm_id']; ?>)"><button class="fa fa-folder-open btn-info"></button></a>
                                <?php } ?>
                                 
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Edit category model -->
<button type="button" class="btn btn-primary btn-lg" id='js_edit_category_button'  data-toggle="modal" data-target="#js_edit_category" style="display: none;">
    Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_edit_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit category rome is here</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8">
                        <form id='edit_category_frm'>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Category name</label>
                                <input type="" name="name" class="form-control" id="exampleInputEmail1" placeholder="Name">
                            </div>

                            <div class="form-group">
                                <label>Price</label>
                                <input type="" name="price" class="form-control" placeholder="Price">
                            </div><!-- Price -->

                            <div class="form-group">
                                <label>Skin tone</label>
                                <select name="skin_tone" class="form-control">
                                    <option value="">Select skin tone</option>
                                    <option value="light">Light</option>
                                    <option value="medium">Medium</option>
                                    <option value="brown">Brown</option>
                                    <option value="dark">Dark</option>
                                    <option value="ebony">Ebony</option>
                                </select>
                            </div><!-- Skin tone -->

                            <div class="form-group">
                                <label>Color of lips</label>
                                <input type="" name="lips_color" class="form-control" placeholder="Color of lips">
                            </div><!-- Color of lips -->

                            <div class="form-group">
                                <label>Eye shadow</label>
                                <input type="" name="eye_shadow" class="form-control" placeholder="Eye shadow">
                            </div><!-- Eye shadow -->

                            <div class="form-group">
                                <label>Time</label>
                                <input type="" name="time" class="form-control" placeholder="Time">
                            </div><!-- Time -->

                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="description" rows="3" placeholder="Description"></textarea>
                            </div>
                            <input type="text" name='id' class="hide">

                        </form>
                    </div>

                    <div class="col-sm-4">
                        <form method="post" enctype="multipart/form-data" id="js_change_image_frm">
                            <img src="" id="js_image_edit" width="200">
                            <input type="button" onclick='$("#js_change_image").click()' class="btn btn-primary" value="Change image">
                            <input type="file" name="image" id="js_change_image" class="hide">

                            <input type="text" name='id' class="hide">
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick='$("#edit_category_frm").submit();'>Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Profile Model -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog js-modal-width" role="document">
    <div class="modal-content js_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade js_secondary_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content js_secondary_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>