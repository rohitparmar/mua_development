<!DOCTYPE html>

<html>

<head>

	<title>Admin Login</title>

  <link rel="stylesheet" href="<?= base_url('themes/admin/bootstrap/css/bootstrap.min.css'); ?>" />

  <link rel="stylesheet" href="<?= base_url('themes/admin/custom/css/style.css'); ?>" />

  <link rel="stylesheet" href="<?= base_url('themes/admin/admin-basic.css'); ?>" />

  <script src="<?= base_url('themes/admin/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>

  <!-- Bootstrap 3.3.5 -->

  <script src="<?= base_url('themes/admin/bootstrap/js/bootstrap.min.js'); ?>"></script>

</head>

<body>

 <div class="log_wrapper">

    <h4>Login</h4>

    <form action="" method='post'>

      <div class="form-group">

        <input  type="text" class="form-control" name="email" id="email" placeholder="Email" />

        <?php if(isset($form_error['email'])){ ?>

          <label class="error"><?php echo $form_error['email']; ?></label>

        <?php } ?>

      </div>

      <div class="form-group">

        <input  type="password" class="form-control" name="pass" id="pass" placeholder="Password" />

        <?php if(isset($form_error['password'])){ ?>

          <label class="error"><?php echo $form_error['password']; ?></label>

        <?php } ?>

        <?php if(isset($_SESSION['error'])){ ?>

          <label class="error"><?php echo $_SESSION['error']; ?></label>

        <?php } 

            unset($_SESSION['error']);

        ?>

      </div>

      <div class="form-group">

        <input class="btn btn-default" type="submit" name="submit" id="submit" value="Submit">

        <span><a href="<?php echo base_url('admin/forgot_password'); ?>">Forgot password</a></span>

      </div>

   </form>

   </div>

</body>

</html>