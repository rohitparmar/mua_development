<!DOCTYPE html>
<html>
<head>
  <title>Forgot password</title>
  <link rel="stylesheet" href="<?= base_url('themes/admin/bootstrap/css/bootstrap.min.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('themes/admin/custom/css/style.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('themes/admin/admin-basic.css'); ?>" />
  <script src="<?= base_url('themes/admin/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="<?= base_url('themes/admin/bootstrap/js/bootstrap.min.js'); ?>"></script>
</head>
<body>
 <div class="log_wrapper">
    <h4>Fogot password</h4>
    <form action="" method='post'>
      <div class="form-group">

        <input  type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $validation->get_data('email'); ?>" />

        <span class="error"><?php echo $validation->error('email'); ?></span>
        <span class="error"><?php echo isset($error)?$error:""; ?></span>

      </div>
      <div class="form-group">
        <input class="btn btn-default" type="submit" name="submit" id="submit">
        <span><a href="<?php echo base_url('admin'); ?>">Login</a></span>
      </div>
   </form>
   </div>
</body>
</html>