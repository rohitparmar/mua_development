<ul class="sidebar-menu">

  <li class="header">MAIN NAVIGATION</li>

  <!-- Dashboard -->

  <li class="treeview"> <a href="<?php echo base_url('admin'); ?>"> <i class="fa fa-th"></i> <span>Dashboard</span></a> </li>

  <!-- Category -->

  <li class="treeview"> <a href="#"> <i class="fa fa-share"></i> <span>Category</span> <i class="fa fa-angle-left pull-right"></i> </a>

    <ul class="treeview-menu">

      <li class=""><a href="<?php echo base_url('admin/category'); ?>"><i class="fa fa-circle-o"></i> Category list</a></li>

      <li class=""><a href="<?php echo base_url('admin/add-category'); ?>"><i class="fa fa-circle-o"></i>Add category</a></li>

    </ul>

  </li>

  <!-- Artist -->

  <li class="treeview"> <a href="#"> <i class="fa fa-share"></i> <span>Artist</span> <i class="fa fa-angle-left pull-right"></i> </a>

    <ul class="treeview-menu">

      <li class=""><a href="<?php echo base_url('admin/artist'); ?>"><i class="fa fa-circle-o"></i> Artist</a></li>

      <li class=""><a href="<?php echo base_url('admin/artist_request'); ?>"><i class="fa fa-circle-o"></i> Artist request</a></li>

    </ul>

  </li>

  <!-- Client -->

  <li class="treeview"> <a href="#"> <i class="fa fa-share"></i> <span>Client</span> <i class="fa fa-angle-left pull-right"></i> </a>

    <ul class="treeview-menu">

      <li class=""><a href="<?php echo base_url('admin/client'); ?>" ><i class="fa fa-circle-o"></i>Client</a></li>

    </ul>

  </li>

  <!-- Settings -->

  <li class="treeview"> <a href="#"> <i class="fa fa-share"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i> </a>

    <ul class="treeview-menu">

      <li class=""><a href="<?php echo base_url('admin/common/settings'); ?>" ><i class="fa fa-circle-o"></i>Common</a></li>

      <li class=""><a href="<?php echo base_url('admin/plan'); ?>" ><i class="fa fa-circle-o"></i>Plan</a></li>

      <li class=""><a href="<?php echo base_url('admin/settings/skintone'); ?>" ><i class="fa fa-circle-o"></i>Skin Tone</a></li>
      
      <li class=""><a href="<?php echo base_url('admin/settings/lipscolor'); ?>" ><i class="fa fa-circle-o"></i>Lips Color</a></li>

      <li class=""><a href="<?php echo base_url('admin/settings/eyeshadow'); ?>" ><i class="fa fa-circle-o"></i>Eyeshadow</a></li>

      <!--<li class="hidden"><a href="<?php echo base_url('admin/common/term-condition'); ?>" ><i class="fa fa-circle-o"></i>Term Condition</a></li>-->

      <li class=""><a href="<?php echo base_url('admin/common/notification'); ?>" ><i class="fa fa-circle-o"></i>Notification</a></li>

    </ul>

  </li>

  <!-- Transaction -->

  <!-- <li class="treeview"> <a href="#"> <i class="fa fa-share"></i> <span>Transaction</span> <i class="fa fa-angle-left pull-right"></i> </a>
    <ul class="treeview-menu">
      <li class=""><a href="<?php echo base_url('admin/transaction'); ?>" ><i class="fa fa-circle-o"></i>Artist payment</a></li>
      <li class=""><a href="<?php echo base_url('admin/client_payment_info'); ?>" ><i class="fa fa-circle-o"></i>Client payment</a></li>
      <li class=""><a href="<?php echo base_url('admin/payment_info_mua'); ?>"><i class="fa fa-circle-o"></i>Payment info</a></li>
    </ul>
  </li> -->
  
  <!-- Payment -->

  <li class="treeview"> <a href="#"> <i class="fa fa-share"></i> <span>Payment</span> <i class="fa fa-angle-left pull-right"></i> </a>
    <ul class="treeview-menu">
      <li class=""><a href="<?php echo base_url('admin/transaction'); ?>" ><i class="fa fa-circle-o"></i>Subscription Payment</a></li>
      <li class=""><a href="<?php echo base_url('admin/earning/artist'); ?>" ><i class="fa fa-circle-o"></i>Artist Earning</a></li>
    </ul>
  </li>

  <!-- Referral -->

  <li class="treeview"> <a href="#"> <i class="fa fa-share"></i> <span>Referral</span> <i class="fa fa-angle-left pull-right"></i> </a>

    <ul class="treeview-menu">

      <li class=""><a href="<?php echo base_url('admin/referral_list'); ?>" ><i class="fa fa-circle-o"></i>Celebrity referral</a></li>

      <li class=""><a href="<?php echo base_url('admin/referral_list_user'); ?>" ><i class="fa fa-circle-o"></i>User referral</a></li>

      <li class=""><a href="<?php echo base_url('admin/add-referral'); ?>" ><i class="fa fa-circle-o"></i>Add Celebrity referral</a></li>

    </ul>

  </li>



  <!-- E-mail -->

  <li class="treeview"> <a href="#"> <i class="fa fa-share"></i> <span>Support</span> <i class="fa fa-angle-left pull-right"></i> </a>
    <ul class="treeview-menu">
      <li class=""><a href="<?php echo base_url('admin/email'); ?>" ><i class="fa fa-circle-o"></i>Email List</a></li>
    </ul>
  </li>

</ul>