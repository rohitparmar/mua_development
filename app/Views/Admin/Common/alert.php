<?php if (isset($_SESSION['warning']) || isset($_SESSION['success']) || isset($_SESSION['info']) || isset($_SESSION['error'])) {?>
            <?php if (isset($_SESSION['warning'])) {?>
            <div class="alert alert-warning fade in">
                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa fa-warning"></i>
                <strong>Warning - </strong> <?php echo $_SESSION['warning'];?>
                <?php unset($_SESSION['warning']); ?>
            </div>
            <?php }?>
            <?php if (isset($_SESSION['success'])) {?>
            <div class="alert alert-success fade in">
                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa fa-check"></i>
                <strong>Success - </strong> <?php echo $_SESSION['success'];?>
                <?php unset($_SESSION['success']); ?>
            </div>
            <?php }?>
            <?php if (isset($_SESSION['info'])) {?>
            <div class="alert alert-info fade in">
                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa fa-info"></i>
                <strong>Info - </strong> <?php echo $_SESSION['info'];?>
                <?php unset($_SESSION['info']); ?>
            </div>
            <?php }?>
            <?php if (isset($_SESSION['error'])) {?>
            <div class="alert alert-danger fade in">
                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa fa-times"></i>
                <strong>Error - </strong> <?php echo $_SESSION['error'];?>
                <?php unset($_SESSION['error']); ?>
            </div>
            <?php }?>
<?php }?>