<?php
  $img_path = base_url('themes/admin/dist/img/user2-160x160.jpg');
  if($_SESSION['admin_info']['image'] && file_exists(ADMIN_PROFILE_IMG.$_SESSION['admin_info']['image'])){
    $img_path = base_url(ADMIN_PROFILE_IMG.$_SESSION['admin_info']['image']);
  }
?>
<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="<?php echo $img_path;?>" class="user-image js_admin_image" alt="User Image"> <span class="hidden-xs"><?php echo $_SESSION['admin_info']['name']; ?></span> </a>
<ul class="dropdown-menu">
  <!-- User image -->
  <li class="user-header"> <img src="<?php echo $img_path; ?>" class="img-circle js_admin_image" alt="User Image">
    <p> <?php echo $_SESSION['admin_info']['name']; ?> </p>
  </li>
  
  <!-- Menu Footer-->
  <li class="user-footer">
    <div class="">
    	<a href="<?php echo base_url('admin/profile'); ?>" class="btn btn-default btn-flat">Profile</a>

    	<a href="<?php echo base_url('admin/logout'); ?>" class="btn btn-default btn-flat pull-right">Sign out</a> </div>
  </li>
</ul>
