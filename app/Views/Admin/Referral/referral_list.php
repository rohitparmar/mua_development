<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Celebrity Referral</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Celebrity Referral</li>
      <li class="active">Celebrity Referral list</li>
    </ol>
</section>
<div class="box">
  <div class="box-body">

    <!-- Search -->
    <div class="row">
      <div class="col-sm-2">
        <div id="custom_datatable_filter" class="dataTables_filter">
          <form action="<?php echo base_url('admin/referral_list'); ?>">
            <label>Search:<input type="search" name="search" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['search'])?$_GET['search']:""; ?>" aria-controls="custom_datatable"></label>
          </form>
        </div>
      </div>
    </div>
    <!-- /Search -->

    <table id="custom_datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Celebrity name</th>
          <th>Code</th>
          <th>Referral Used</th>
          <th>Start date</th>
          <th>End date</th>
          <th>Time duration</th>
          <th>Created Date</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=1; foreach($referral_list as $value){ ?>
        <tr class='tr_<?php echo $value['r_id']; ?>'>
          <td><?php echo $value['r_name']; ?></td>
          <td><?php echo $value['r_code'] ?></td>
         
          <td>
              <a href="javascript:void(0);" onclick="get_used_referrel(<?php echo $value['r_id']; ?>)">
                <?php echo $value['count']; ?>
              </a>
          </td>
          
          <td><?php echo date('m/d/Y',strtotime($value['r_start_date'])); ?></td>
          <td><?php echo date('m/d/Y',strtotime($value['r_end_date'])); ?></td>
          <td>
            <?php
                $start_date = strtotime($value['r_start_date']);
                $end_date = strtotime($value['r_end_date']);
        			  $diff = $end_date-$start_date;
        			  echo $days = floor($diff/86400);
            ?> day
          </td>
          <td><?php echo date('Y-m-d',strtotime($value['r_created_date'])); ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>

    <!-- Pagination -->
    <div class="row">
      <div class="col-sm-5">
        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php echo (($page - 1)*10)+1; ?> to <?php echo (($page - 1)*10)+count($referral_list); ?> of <?php echo $referral_count; ?> entries
        </div>
      </div>
      <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
          <ul class="pagination">
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/referral_list/'.(1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">First</a></li>
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/referral_list/'.($page - 1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0"><<</a></li>
            
            <?php
              $page_tab_options = 10;
              if($page == 1){
                $start = 1;
              }
              elseif((int)($referral_count/10) == 1){
                $start = $page - 1;
              }
              elseif($page == ((int)($referral_count/10)+1)){
                $start = $page - 2;
              }
              else{
                $start = $page-1;
              }
              for($i = 1; $i <= $page_tab_options; $i++){ 

                if($start > ceil(($referral_count/10))){
                  continue;
                }
              ?>
                <li class="paginate_button <?php echo ($page == $start)?'active':""; ?>"><a href="<?php echo base_url('admin/referral_list/'.$start).$query; ?>" aria-controls="datatable" data-dt-idx="1" tabindex="0"><?php echo $start; ?></a></li>
            <?php $start++; } ?>

            <li class="paginate_button next <?php echo ($page == ceil(($referral_count/10)))?"disabled":''; ?>" id="datatable_next"><a href="<?php echo ($page != ceil(($referral_count/10)))?(base_url('admin/referral_list/'.($page + 1)).$query):'javascript:void(0);'; ?>" aria-controls="datatable" data-dt-idx="2" tabindex="0">>></a></li>
            <li class="paginate_button previous <?php echo ($page == ceil($referral_count/$page_tab_options))?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != ceil($referral_count/$page_tab_options))?(base_url('admin/referral_list/'.(ceil($referral_count/$page_tab_options))).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">Last</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- /Pagination -->

  </div><!-- /.box-body -->
</div><!-- /.box -->


<!-- Profile Model -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog js-modal-width" role="document">
    <div class="modal-content js_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>
