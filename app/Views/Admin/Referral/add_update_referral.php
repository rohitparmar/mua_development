<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Referral</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Referral</li>
      <li class="active">Add referral</li>
    </ol>
</section>
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><?php echo $header['title']; ?></h3>
  </div><!-- /.box-header -->
  <!-- form start -->
  <form role="form" action="" method="post" id="add_referral_frm">
    <div class="box-body">

      <div class="form-group">
        <label>Name of the celebrity</label>
        <input type="text" placeholder="Name of the celebrity" class="form-control" name="celebrity_name" value="<?php echo $ViewData->view_data('celebrity_name'); ?>">
      </div><!-- Celebrity name -->
      
      <div class="form-group">
        <label>Celebrity Referrel Code</label>
        <input type="text" placeholder="Celebrity Referrel Code (Please put no special character as like space, & , # etc.)" class="form-control" name="celebrity_code" value="<?php echo $ViewData->view_data('celebrity_code'); ?>">
      </div><!-- Celebrity name -->

      <div class="form-group">
        <label>Rebate price</label>
        <input type="text" placeholder="Rebate price" class="form-control" name="rebate_price" value="<?php echo $ViewData->view_data('rebate_price'); ?>">
      </div><!-- Rebate price -->

      <div class="form-group">
        <label>Date range</label>
        <input type="text" placeholder="Date range" class="form-control date_range" name="date_range" value="<?php echo $ViewData->view_data('date_range'); ?>">
      </div><!--  Date range -->

      <div class="form-group hide">
        <label>User type</label>
        <select class="form-control" name="user_type">
          <!-- <option value="">User type</option> -->
          <option value="mua" <?php echo ($ViewData->view_data('user_type')=='mua')?"selected":""; ?>>Artist</option>
          <option value="client" <?php echo ($ViewData->view_data('user_type')=='client')?"selected":""; ?>>Client</option>
        </select>
      </div><!--  User type -->

    </div><!-- /.box-body -->

    <div class="box-footer">
      <button class="btn btn-primary" type="submit">Generate Referral</button>
    </div>
  </form>
</div>