<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Profile</h4>
</div>
<div class="modal-body">
    <!-- Profile -->
    <div class="box box-primary col-md-8">
      <div class="box-body box-profile row">
        <div class="col-md-4">
          <?php if($profile['image'] && file_exists(ARTIST_PROFILE_IMG.$profile['image'])){ ?>
            <img class="profile-user-img img-responsive" style="width: 100%;" src="<?php echo base_url(ARTIST_PROFILE_IMG.$profile['image']); ?>" alt="User profile picture">
          <?php } else{ ?>
            <img class="profile-user-img img-responsive" style="width: 100%;" src="<?php echo base_url(ARTIST_PROFILE_IMG.'default.jpg'); ?>" alt="User profile picture">
          <?php } ?>
        </div>
        <div class="col-md-8">
          <h3 class="profile-username text-center js_name"><?php echo $profile['ap_profile_name']; ?></h3>
          <p class="text-muted text-center js_phone"><?php echo $profile['ap_phone']; ?></p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Email</b><br><a class="js_email"><?php echo $profile['ap_email']; ?></a>
            </li>
            <li class="list-group-item">
              <b>City</b><br><a class="js_city"><?php echo $profile['ap_city']; ?></a>
            </li>
            <li class="list-group-item">
              <b>Website</b><br><a class="js_website"><?php echo $profile['ap_website']; ?></a>
            </li>
            <li class="list-group-item">
              <b>About</b><br><a class="js_about"><?php echo $profile['ap_about']; ?></a>
            </li>
          </ul>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>