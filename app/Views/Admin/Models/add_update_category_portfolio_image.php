<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Update portfolio</h4>
</div>
<div class="modal-body">
        <div class="row">
            <input type="hidden" id="category_id" value="<?php echo $id; ?>">
            <img style="width: 16px;" class='js_loading_img hide' src="<?php echo base_url('themes/admin/custom/images/loading.gif'); ?>">
            <div class="col-sm-6">
                <?php 
                    $before_image = base_url(CATEGORY_PORTFOLIO_BEFORE_IMG."default.png");
                    if(file_exists(CATEGORY_PORTFOLIO_BEFORE_IMG.$portfolio['cm_before_image']) && $portfolio['cm_before_image']){
                        $before_image = base_url(CATEGORY_PORTFOLIO_BEFORE_IMG.$portfolio['cm_before_image']);
                    }
                ?>
                <center>
                    <img src="<?php echo $before_image; ?>" class="js_before_image" title="Before image" width="200px" height="200px">
                    <button onclick="$('#category_before_image').click();" class="btn" style="margin-top: 10px; ">Change before image</button>
                    <input type="file" name="before" id="category_before_image" accept="image/jpeg,image/png" class="hide js_caterogy_portfolio_img">
                </center>
            </div>
            <div class="col-sm-6">
                <?php 
                    $after_image = base_url(CATEGORY_PORTFOLIO_AFTER_IMG."default.png");
                    if(file_exists(CATEGORY_PORTFOLIO_AFTER_IMG.$portfolio['cm_after_image']) && $portfolio['cm_after_image']){
                        $after_image = base_url(CATEGORY_PORTFOLIO_AFTER_IMG.$portfolio['cm_after_image']);
                    }
                ?>
                <center>
                    <img src="<?php echo $after_image; ?>" class="js_before_image" title="After image" width="200px" height="200px">
                    <button onclick="$('#category_after_image').click();" class="btn" style="margin-top: 10px; ">Change after image</button>
                    <input type="file" name="after" id="category_after_image" accept="image/jpeg,image/png" class="hide js_caterogy_portfolio_img">
                </center>
            </div>
        </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>