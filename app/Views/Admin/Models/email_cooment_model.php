<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Email</h4>
</div>
<div class="modal-body">
	

  <div class="box box-info">
    <!-- form start -->
    <form class="form-horizontal">
      <div class="box-body">

      

        <?php if(isset($result) && $result){?>
  
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Status</label>
          <div class="col-sm-3">
            <select name="action" class="form-control js_status">
              <option value="request" <?php echo (isset($result[0]['um_admin_status']) && $result[0]['um_admin_status'] == 'request')?'selected':''?>>Request</option>
              <option value="on-hold" <?php echo (isset($result[0]['um_admin_status']) && $result[0]['um_admin_status'] == 'on-hold')?'selected':''?>>On-hold</option>
              <option value="processing" <?php echo (isset($result[0]['um_admin_status']) && $result[0]['um_admin_status'] == 'processing')?'selected':''?>>Processing</option>
              <option value="complete" <?php echo (isset($result[0]['um_admin_status']) && $result[0]['um_admin_status'] == 'complete')?'selected':''?>>Complete</option>
            </select>
           </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Comment</label>
          <div class="col-sm-10">

            <textarea class="form-control js_comment" type="text" name="comment" rows="5" placeholder="Comment"> <?php echo isset($result[0]['um_comment'])?$result[0]['um_comment']:'Comment';?> </textarea>
            
            
          </div>
        </div>
        <input class="js_id" value="<?php echo $result[0]['um_id']?>" type="hidden">
        
      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="button" class="btn btn-info pull-right" onclick="update_email()">Update</button>
      </div><!-- /.box-footer -->
    </form>
  </div>
  

<?php }else{ ?>
	No data found.
<?php } ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<style type="text/css">
  
.input[type="checkbox"], input[type="radio"] { margin:12px 0 0 0;  }
.js_comment{ width: 80%; }

</style>