<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Commision</h4>
</div>
<div class="modal-body">
  
  <!-- Horizontal Form -->
  <div class="box box-info">
    <!-- form start -->
    <form class="form-horizontal">
      <div class="box-body">

        <div class="form-group">
          <label class="col-sm-2 control-label">Commision</label>
          <div class="col-sm-10">
            <input type="number" step='.1' class="form-control js_commision" placeholder="Commision" value='<?php echo $commision; ?>'>
          </div>
        </div>
        <input type="hidden" class="js_id" value="<?php echo $id; ?>">
        
      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancle</button>
        <button type="button" class="btn btn-info pull-right js_change_artist_commision">Submit</button>
      </div><!-- /.box-footer -->
    </form>
  </div><!-- /.box -->

</div>