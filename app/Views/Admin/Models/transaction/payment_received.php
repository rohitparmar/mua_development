<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Received Payment</h4>
</div>
<div class="modal-body">
    <!-- Profile -->
    <div class="box box-primary col-md-12">
      <div class="box-body box-profile row">
        
        <div class="col-md-12">
          
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th width="5%">#</th>
          <th>Payment Type</th>
          <th>Payment Mode</th>
          <th>Amount Paid</th>
          <th>Bank Account</th>
          <th>Comment</th>
          <th>Date</th>
          
        </tr>
      </thead>
      <tbody>

      <?php 
      
      

      $i = 1;
      foreach($payment_received as $value){ ?>
       
        <tr>  
            <td> <?php echo $i++;?> </td>
            <td> <?php echo isset($value['aph_payment_type'])?$value['aph_payment_type']:'';?>  </td>
            <td> <?php echo isset($value['aph_payment_via'])?$value['aph_payment_via']:'';?> </td>
            <td> $<?php echo isset($value['aph_payment_amount'])?$value['aph_payment_amount']:0;?> </td>
            <td> <?php echo isset($value['aph_bank_account'])?$value['aph_bank_account']:'';?> </td>
            <td> <?php echo isset($value['aph_payment_comment'])?$value['aph_payment_comment']:'';?> </td>
            <td> <?php echo isset($value['aph_updated'])?date('m-d-Y',strtotime($value['aph_updated'])):'';?> </td>
        </tr>
      
      <?php } ?>
     </tbody>
    </table>

          
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>