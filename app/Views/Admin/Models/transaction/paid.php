<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Pay Now</h4>
</div>
<div class="modal-body">
  
  <!-- Horizontal Form -->
  <div class="box box-info">
    <!-- form start -->
    <form class="form-horizontal">
      <div class="box-body">

        <input type="hidden" name="mua_id" id="_js_mua_id" value="<?php echo $mua_id;?>">
            <div class="clearfix"></div>
            <div class="row">

              <div class="form-group">
                <label class="col-sm-4 control-label">Payment Type</label>
                <div class="col-sm-4">
                  <select class="form-control js_payment_type" name="payment_type">
                      <option value="booking"> booking </option>
                      <option value="refund"> refund </option>
                  </select>
                </div>
              </div>


              <div class="form-group">
                <label class="col-sm-4 control-label">Payment Mode</label>
                <div class="col-sm-4">
                  <select class="form-control js_payment_via" name="payment_via">
                      <option value="bank_account"> Bank account </option>
                      <option value="paypal"> Paypal </option>
                      <option value="check"> Check </option>
                  </select>
                </div>
              </div>


              <div class="form-group">
                <label class="col-sm-4 control-label">Enter Amount</label>
                <div class="col-sm-4">
                  <input class="form-control js_amount" type="number" name="amount" min="1" value="10" placeholder="Enter Amount" aria-required="true" aria-invalid="true">
                </div>
              </div>


              <div class="form-group">
                <label class="col-sm-4 control-label">Bank Account</label>
                <div class="col-sm-4">
                  <input class="form-control js_account" type="text" name="account" placeholder="Enter Bank Account" value="<?php echo  isset($account_info['uai_account_no'])?$account_info['uai_account_no']:''?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-4 control-label">Routing No</label>
                <div class="col-sm-4">
                  <input class="form-control js_account" type="text" name="uai_rounting_no" placeholder="Enter Routing No" value="<?php echo  isset($account_info['uai_rounting_no'])?$account_info['uai_rounting_no']:''?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-4 control-label">Account Name</label>
                <div class="col-sm-4">
                  <input class="form-control js_account" type="text" name="uai_account_name" placeholder="Enter Account Name" value="<?php echo  isset($account_info['uai_account_name'])?$account_info['uai_account_name']:''?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-4 control-label">Paypal Id</label>
                <div class="col-sm-4">
                  <input class="form-control js_account" type="text" name="uai_paypal_id" placeholder="Enter Paypal Id" value="<?php echo  isset($account_info['uai_paypal_id'])?$account_info['uai_paypal_id']:''?>">
                </div>
              </div>


              
              <div class="form-group">
                <label class="col-sm-4 control-label">Enter Comment</label>
                <div class="col-sm-4">
                  <textarea class="form-control js_comment" type="text" name="comment" rows="5"> </textarea>
                  <input type="hidden" class="js_id" value="<?php echo $id; ?>">
                </div>
              </div>

              
            </div>
          </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-info pull-right js_admin_payment_history_to_user">Submit</button>
      </div><!-- /.box-footer -->
    </form>
  </div><!-- /.box -->

</div>

