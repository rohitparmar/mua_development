<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Payment</h4>
</div>
<div class="modal-body">
    <!-- Profile -->
    <div class="box box-primary col-md-12">
      <div class="box-body box-profile row">
        
        <div class="col-md-12">
          
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th width="5%">#</th>

          <th> Image</th>
          <th> Name</th>
          <th> Service Amount</th>
          <th> RPM Amount</th>
          <th> Rebate Amount</th>
          <th> Total Rebate Amount </th>
          <th> Amount Recived</th>
          <th> Services Name</th>
          <th> Date</th>
          
        </tr>
      </thead>
      <tbody>



      <?php
      $i = 1;

      foreach($payment_received as $value){ ?>
       
        <tr>  
            <td> <?php echo $i++;?> </td>
            
            <td> <img src="<?php 
				$img = isset($value['client_image'])?
				(file_exists('artist-client-upload-file/client/profile/'.$value['client_image'])?'../artist-client-upload-file/client/profile/'.$value['client_image']:'../artist-client-upload-file/artist/profile/default.jpg')
				
				:'../artist-client-upload-file/artist/profile/default.jpg';
			
			 echo base_url($img);
			
			
			?>"  style="width:60px; margin:10px; border:2px solid #CCCCCC;" /> </td>
            
            
            
            
            
            
            <td> <?php echo isset($value['client_name'])?$value['client_name']:'';?>  </td>
            
            <td> $<?php echo isset($value['service_amout'])?$value['service_amout']:0;?> </td>
            
            <td> $<?php echo isset($value['rpm'])?$value['rpm']:0;;?> </td>
            
            <td> $<?php echo isset($value['rebate_amount'])?$value['rebate_amount']:0;?> </td>
            <td> $<?php echo isset($value['total_rebate_amount'])?$value['total_rebate_amount']:0;?> </td>
            <td> $<?php echo isset($value['ammount_recvied'])?$value['ammount_recvied']:0;?> </td>
            <td> <?php echo isset($value['services_name'])?$value['services_name']:'';?> </td>
            <td> <?php echo isset($value['ammount_recvied_date'])?date('m-d-Y',strtotime($value['ammount_recvied_date'])):'';?> </td>
        </tr>
      
      <?php } ?>
     </tbody>
    </table>

          
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>