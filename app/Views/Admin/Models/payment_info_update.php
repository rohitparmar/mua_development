<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Update payment info</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">
            <form id='payment_info_update_frm' method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/update_payment_info'); ?>">

                <div class="form-group">
                    <label>Account no</label>
                    <input type="" name="account_no" class="form-control" placeholder="Account no" value="<?php echo $payment_info['uai_account_no']; ?>">
                </div><!-- Account no -->

                <div class="form-group">
                    <label>Rounting no</label>
                    <input type="" name="rounting_no" class="form-control" placeholder="Rounting no" value="<?php echo $payment_info['uai_rounting_no']; ?>">
                </div><!-- Rounting no -->

                <div class="form-group">
                    <label>Account name</label>
                    <input type="" name="account_name" class="form-control" placeholder="Account name" value="<?php echo $payment_info['uai_account_name']; ?>">
                </div><!-- Account name -->

                <div class="form-group">
                    <label>Paypal id</label>
                    <input type="" name="paypal_id" class="form-control" placeholder="Paypal id" value="<?php echo $payment_info['uai_paypal_id']; ?>">
                </div><!-- Paypal id -->

                <div class="form-group">
                    <label>Payment preference</label>
                    <select name="payment_preference" class="form-control">
                        <option value="check" <?php echo ($payment_info['uai_payment_preference'] == 'check')?'selected':""; ?>>Check</option>
                        <option value="account" <?php echo ($payment_info['uai_payment_preference'] == 'account')?'selected':""; ?>>Account</option>
                        <option value="paypal" <?php echo ($payment_info['uai_payment_preference'] == 'paypal')?'selected':""; ?>>Paypal</option>
                    </select>
                </div><!-- Payment preference -->

                <input type="submit" value="Submit" class="btn btn-primary">
                <input type="hidden" name="id" value="<?php echo $payment_info['uai_id']; ?>">
            </form>
        </div>
    </div>
</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>