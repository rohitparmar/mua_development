<?php $lang = ($lang == 2) ? 'spn' : 'eng'; ?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Edit category</h4>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-sm-12">
      <form id='edit_category_frm' method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/update_category'); ?>">
        <input type="text" name='cat_id' value="<?php echo $cat_id; ?>" class="hide">
        <div class="form-group">
          <label>Language</label>
          <div class="form-group">
            <label>
            <input onclick="show_related_info('eng')" type="radio" name="lang" class="minimal" value="eng" checked="checked">
            &nbsp;&nbsp;English</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label>
            <input onclick="show_related_info('spn')" type="radio" name="lang" class="minimal" value="spn" <?php if ($lang == 'spn') { ?> checked="checked"<?php } ?>>
            &nbsp;&nbsp;Spanish</label>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Current Category Name: </label>
          <labe><?php echo $category['ca_name']; ?></labe>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Category Name</label>
          <input type="" name="name" class="form-control" id="category_name" placeholder="Name" value="<?php echo $category['ca_name']; ?>">
        </div>
        <div class="form-group">
          <label for="sub_category_desc">Category Description</label>
          <textarea name="description" class="form-control" id="sub_category_desc" placeholder="Description" rows="5"><?php echo $category['ca_description']; ?></textarea>
        </div>
        <!-- Description -->
        <div class="form-group js_category_price">
          <div class="row">
            <div class="col-sm-3">
              <label>Price</label>
              <input type="number" name="price" class="form-control" min="0" step="0.5" value="<?php echo $category['cm_price']; ?>"/>
            </div>
          </div>
        </div>
        <!-- Price -->
        <div class="form-group js_category_time">
          <div class="row">
            <div class="col-sm-3">
              <label>Service Duration</label>
              <input type="number" name="duration" class="form-control" min="0" step="1" value="<?php echo $category['cm_duration']; ?>"/>
            </div>
          </div>
        </div>
        <!--Service Duration-->
        <div class="form-group js_category_skin_tone">
          <label>Skintones</label>
          <?php
                    $skin_tones = $skin_tone; //echo '<pre>'; print_r($skin_tones); $row['ms_fk_ms_id']
					
					
					
                    ?>
          <div class="row">
            <?php foreach ($skin_tones as $row) {
                            if ($row['language'] == 1) {$i = 0;
                                 ?>
            <div class="col-sm-4 js-skintone-language-<?php echo $row['language']; ?>">
              <div class="col-sm-9 checkbox well" style="margin-top:2px; height:82px !important;">
                <label>
                    
                <?php foreach ($skintone_info as $inf) {$i = 0;
                                                if (strtolower($row['ms_id']) == strtolower($inf['mcm_service_id'])) {
                                    
                                        $i = 1; ?>
                <input checked="checked" type="checkbox" class="js_skin_tone_checkbox" name="eng_skin_tone[<?php echo $row['ms_id']; ?>]" value="<?php echo $row['ms_id']; ?>">
                <?php }
                                            } ?>
                <?php if ($i == 1) { ?>
                <input  type="checkbox" class="js_skin_tone_checkbox" name="eng_skin_tone[<?php echo $row['ms_id']; ?>]" value="<?php echo $row['ms_id']; ?>">
                <?php } ?>
                
                
                <input  type="checkbox" class="js_skin_tone_checkbox" name="eng_skin_tone[<?php echo $row['ms_id']; ?>]" value="<?php echo $row['ms_id']; ?>">
                <?php echo $row['ms_name']; ?> </label>
                  <input type="file" class="js_skin_tone_img" name="sub_category_skintone_img_eng[<?php echo $row['ms_id']; ?>]">
              </div>
              <div class="col-sm-3 well" style="margin-top:2px; height:82px !important;">
              	<?php foreach ($skintone_info as $inf) {
                                                if ($row['ms_id'] == $inf['mcm_service_id']) {
												if($inf['mcm_img']!=''){
                                                   ?>
                <img src="<?php echo base_url(CATEGORY_IMG.'skintone/'.$inf['mcm_img']);?>" width="97%" height="97%" />
                <?php }}}?>
              	
              </div>
            </div>
            
            <?php }
} ?>
            <?php foreach ($skin_tones as $row) {
                                        if ($row['language'] == 2) {
                                            $i = 0; ?>
            <div class="col-sm-4 js-skintone-language-<?php echo $row['language']; ?> <?php if ($row['language'] == 2) { ?> hidden<?php } ?>">
              <div class="col-sm-9 checkbox well" style="margin-top:2px; height:82px !important;">
                <label>
                <?php
				
				 foreach ($skintone_info as $inf) {
				 	if (strtolower($row['ms_id']) == strtolower($inf['mcm_service_id'])) {
                                    
                                        $i = 1; ?>
                <input checked="checked" type="checkbox" class="js_skin_tone_checkbox" name="spn_skin_tone[<?php echo $row['ms_id']; ?>]" value="<?php echo $row['ms_id']; ?>">
                <?php }
        } ?>
                <?php if ($i == 0) { ?>
                <input  type="checkbox" class="js_skin_tone_checkbox" name="spn_skin_tone[<?php echo $row['ms_id']; ?>]" value="<?php echo $row['ms_id']; ?>">
                <?php } ?>
                <?php echo $row['ms_name']; ?> </label>
                  <input type="file" class="js_skin_tone_img" name="sub_category_skintone_img_spn[<?php echo $row['ms_id']; ?>]">
              </div>
              <div class="col-sm-3 well" style="margin-top:2px; height:82px !important;">
              	<?php foreach ($skintone_info as $inf) {
                                                if ($row['ms_id'] == $inf['mcm_service_id']) {
												if($inf['mcm_img']!=''){
                                                   ?>
                <img src="<?php echo base_url(CATEGORY_IMG.'skintone/'.$inf['mcm_img']);?>" width="97%" height="97%" />
                <?php }}}?>
              	
              </div>
              
            </div>
            <?php }
                        } ?>
          </div>
        </div>
        <!-- Skin tone -->
        <div class="form-group js_category_skin_tone">
          <label>Color of Lips</label>
          <?php
                                    $lips_colors = $lips_color;
                                    //$selected_lips_color = explode(',', $category['lips_color']);
                                    ?>
          <div class="row">
            <?php foreach ($lips_colors as $row) {
                            if ($row['language'] == 1) {
                                $i = 0; ?>
            <div class="col-sm-4 js-skintone-language-<?php echo $row['language']; ?>">
              <div class="checkbox well">
                <label>
                <?php foreach ($lipcolor_info as $inf) {
                                                if ($row['mlc_id'] == $inf['mcm_service_id']) {
                                                    $i = 1; ?>
                <input checked="checked" type="checkbox" class="js_skin_tone_checkbox" name="eng_lip_color[<?php echo $row['mlc_id']; ?>]" value="<?php echo $row['mlc_id']; ?>">
                <?php }
                                            } ?>
                <?php if ($i == 0) { ?>
                <input  type="checkbox" class="js_skin_tone_checkbox" name="eng_lip_color[<?php echo $row['mlc_id']; ?>]" value="<?php echo $row['mlc_id']; ?>">
                <?php } ?>
                <?php echo $row['mlc_name']; ?> </label>
              </div>
            </div>
            <?php }
} ?>
            <?php foreach ($lips_colors as $row) {
    if ($row['language'] == 2) {
        $i = 0; ?>
            <div class="col-sm-4 js-skintone-language-<?php echo $row['language']; ?> <?php if ($row['language'] == 2) { ?> hidden<?php } ?>">
              <div class="checkbox well">
                <label>
                <?php foreach ($lipcolor_info as $inf) {
                                    if ($row['mlc_id'] == $inf['mcm_service_id']) {
                                        $i = 1; ?>
                <input checked="checked" type="checkbox" class="js_skin_tone_checkbox" name="spn_lip_color[<?php echo $row['mlc_id']; ?>]" value="<?php echo $row['mlc_id']; ?>">
                <?php }
                                            } ?>
                <?php if ($i == 0) { ?>
                <input  type="checkbox" class="js_skin_tone_checkbox" name="spn_lip_color[<?php echo $row['mlc_id']; ?>]" value="<?php echo $row['mlc_id']; ?>">
                <?php } ?>
                <?php echo $row['mlc_name']; ?> </label>
              </div>
            </div>
            <?php }
} ?>
          </div>
        </div>
        <!-- Color of lips -->
        <div class="form-group js_category_skin_tone">
          <label>Eye Shadows</label>
          <?php
                                    $eyeshadows = $eyeshadow;
                                    ?>
          <div class="row">
            <?php foreach ($eyeshadows as $row) {
                                        if ($row['language'] == 1) {
                                            $i = 0; ?>
            <div class="col-sm-4 js-skintone-language-<?php echo $row['language']; ?>">
              <div class="checkbox well">
                <label>
                <?php foreach ($eyeshadow_info as $inf) {
            if ($row['me_id'] == $inf['mcm_service_id']) {
                $i = 1; ?>
                <input checked="checked" type="checkbox" class="js_skin_tone_checkbox" name="eng_eye_shadow[<?php echo $row['me_id']; ?>]" value="<?php echo $row['me_id']; ?>">
                <?php }
        } ?>
                <?php if ($i == 0) { ?>
                <input  type="checkbox" class="js_skin_tone_checkbox" name="eng_eye_shadow[<?php echo $row['me_id']; ?>]" value="<?php echo $row['me_id']; ?>">
                <?php } ?>
                <?php echo $row['me_name']; ?> </label>
              </div>
            </div>
            <?php }
} ?>
            <?php foreach ($eyeshadows as $row) {
    if ($row['language'] == 2) {
        $i = 0; ?>
            <div class="col-sm-4 js-skintone-language-<?php echo $row['language']; ?>">
              <div class="checkbox well">
                <label>
                <?php foreach ($eyeshadow_info as $inf) {
            if ($row['me_id'] == $inf['mcm_service_id']) {
                $i = 1; ?>
                <input checked="checked" type="checkbox" class="js_skin_tone_checkbox" name="spn_eye_shadow[<?php echo $row['me_id']; ?>]" value="<?php echo $row['me_id']; ?>">
                <?php }
        } ?>
                <?php if ($i == 0) { ?>
                <input  type="checkbox" class="js_skin_tone_checkbox" name="spn_eye_shadow[<?php echo $row['me_id']; ?>]" value="<?php echo $row['me_id']; ?>">
                <?php } ?>
                <?php echo $row['me_name']; ?> </label>
              </div>
            </div>
            <?php }
} ?>
          </div>
        </div>
        <!-- Eye Shadows -->
        <div class="row">
          <div class="col-sm-8">
            <div id="img_upload" class="form-group">
              <label for="sub_image">Image (jpg, jpeg, png, gif)</label>
              <input type="file" name="sub_image" class="form-control" id="sub_image" placeholder="Description">
            </div>
            <div class="form-group">
              <label>
              <input type="radio" name="status" class="minimal" checked="checked" value="true">
              &nbsp;&nbsp;Active</label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <label>
              <input type="radio" name="status" class="minimal" value="false">
              &nbsp;&nbsp;Inactive</label>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
<script>
    function show_related_info(lang) {
        if (lang == 'spn') {
            $('.js-skintone-language-2').removeClass('hidden');
            $('.js-skintone-language-1').addClass('hidden');

            $('.js-lipscolor-language-2').removeClass('hidden');
            $('.js-lipscolor-language-1').addClass('hidden');

            $('.js-eyeshadow-language-2').removeClass('hidden');
            $('.js-eyeshadow-language-1').addClass('hidden');

            $('#img_upload').hide();

        } else {
            $('.js-skintone-language-2').addClass('hidden');
            $('.js-skintone-language-1').removeClass('hidden');

            $('.js-lipscolor-language-2').addClass('hidden');
            $('.js-lipscolor-language-1').removeClass('hidden');

            $('.js-eyeshadow-language-2').addClass('hidden');
            $('.js-eyeshadow-language-1').removeClass('hidden');

            $('#img_upload').show();
        }
    }
    show_related_info('<?php echo $lang; ?>');
</script>
