<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Availability</h4>
  </div>
  <div class="modal-body">
<?php if($availability){ ?>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<th>Day</th>
				<th>From</th>
				<th>To</th>
				<th>Status</th>
				<th style="display: none;">Edit</th>
			</tr>
		</tbody>
		<?php foreach($availability as $value){ ?>
			<?php 
				$status = ($value['aa_active_status'] == 'true')?'Active':'Inactive';
				$status_class = ($value['aa_active_status'] == 'true')?'label label-success':'label label-danger'; 
			?>
			<tr class="tr_<?php echo $value['aa_id']; ?>">
				<td><?php echo $value['aa_days']; ?></td>
				<td><?php echo $value['aa_time_from']; ?></td>
				<td><?php echo $value['aa_time_to']; ?></td>
				<td>
					<a href="javascript:void(0);" status='<?php echo $value['aa_active_status']; ?>' onclick='change_artist_availability_status(<?php echo $value['aa_id']; ?>,$(this))'><span class="<?php echo $status_class; ?>"><?php echo $status; ?></span></a>
					<img src="<?php echo base_url('themes/admin/custom/images/loading.gif'); ?>" class='js_loading_img' style='width: 16px; display: none;'>
				</td>
				<td style="display: none;"><a href="javascript:void(0);"><button class="btn btn-default">Edit</button></a></td>
			</tr>
		<?php } ?>
	</table>
<?php }else{ ?>
	No data found.
<?php } ?>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>