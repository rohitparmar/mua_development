<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Lipscolor</h4>
</div>
<div class="modal-body">
  
<form class="form-horizontal">
      <div class="box-body">

        
            <div class="clearfix"></div>
            <div class="row">

              <div class="form-group">
                <label class="col-sm-4 control-label">Lipscolor name</label>
                <div class="col-sm-4">
                  <input type="text" class="js_name" value="<?php echo isset($result[0]['mlc_name'])?$result[0]['mlc_name']:'';?>"></input>
                  <input type="hidden" name="id" class="js_id" value="<?php echo isset($result[0]['mlc_id'])?$result[0]['mlc_id']:'';?>">
                  <input type="hidden" class="js_type" value="lipscolor">
                </div>
              </div>

              
            </div>
          </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-info pull-right js_update_skintone">Submit</button>
      </div><!-- /.box-footer -->
    </form>
</div>

