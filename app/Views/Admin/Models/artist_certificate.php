<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Artist Certificate</h4>
</div>
<div class="modal-body">
	

  <div class="box box-info">
    <!-- form start -->
    <form class="form-horizontal">
      <div class="box-body">

        <?php if(isset($result) && $result){?>
  
        <?php if(file_exists(ARTIST_CERTIFICATE_IMG.'/'.$result[0]['im_image_name']) && $result[0]['im_image_name']){ ?>
        <img src="<?php echo base_url(ARTIST_CERTIFICATE_IMG.'/'.$result[0]['im_image_name']); ?>" width="100%">
        <?php } else{ ?>
          <img src="<?php echo CERTIFICATE_DEFAULT_IMAGE; ?>" width="100%">
        <?php } ?>  
       
        <div class="form-group">
          <label class="col-sm-2 control-label" for="f-option">Active</label>
          <div class="col-sm-10">
            <input type="radio" value="true" name="js_status" class="js_status_1" placeholder="Comment" id="f-option"
            <?php echo (isset($result[0]['ap_certificate_status']) && $result[0]['ap_certificate_status'] == 'true')?'checked="checked"':''?>
            >
            </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label" for="f-option1">Inactive</label>
          <div class="col-sm-10">
            <input type="radio" value="false" name="js_status" class="js_status_2" placeholder="Comment" id="f-option1"
            <?php echo (isset($result[0]['ap_certificate_status']) && $result[0]['ap_certificate_status'] == 'false')?'checked="checked"':''?>>
           </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Comment</label>
          <div class="col-sm-10">

            <textarea class="form-control js_comment" type="text" name="comment" rows="5" placeholder="Comment"> <?php echo isset($result[0]['ap_comment'])?$result[0]['ap_comment']:'Comment';?> </textarea>
            
            
          </div>
        </div>
        <input class="js_id" value="<?php echo $result[0]['im_fk_u_id']?>" type="hidden">
        
      </div><!-- /.box-body -->
      <div class="box-footer">
        <button type="button" class="btn btn-info pull-right" onclick="js_artist_certificate()">Update</button>
      </div><!-- /.box-footer -->
    </form>
  </div>
  

<?php }else{ ?>
	No data found.
<?php } ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<style type="text/css">
  
.input[type="checkbox"], input[type="radio"] { margin:12px 0 0 0;  }
.js_comment{ width: 80%; }

</style>