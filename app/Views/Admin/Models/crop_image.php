
<script src="<?php echo base_url('themes/admin'); ?>/image_crop/js/jquery.Jcrop.js"></script>
<!--<link rel="stylesheet" href="<?php echo base_url('themes/admin'); ?>/image_crop/css/main.css" type="text/css" />-->
<!--<link rel="stylesheet" href="<?php echo base_url('themes/admin'); ?>/image_crop/css/demos.css" type="text/css" />-->
<link rel="stylesheet" href="<?php echo base_url('themes/admin'); ?>/image_crop/css/jquery.Jcrop.css" type="text/css" />

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Crop image</h4>
</div>
<div class="modal-body">
    <div style="width: 800px; height: 800px; overflow: auto;">
        <img id="croping_img" name="image" src="">
    </div>

    <div class="hide">
        <input type="text" size="4" id="x1" name="x" />
        <input type="text" size="4" id="y1" name="y" />
        <input type="text" size="4" id="x2"  />
        <input type="text" size="4" id="y2" />
        <input type="text" size="4" id="w" name="w" />
        <input type="text" size="4" id="h" name="h" />
    </div>

</div>
<div class="modal-footer">
  <input type="submit" value="Save" class="btn btn-default js_crop_image_submit">
  <input type="submit" value="Skip" class="btn btn-default js_crop_image_skip">
  <button type="button" class="btn btn-default js_crop_image_close" data-dismiss="modal">Close</button>
</div>