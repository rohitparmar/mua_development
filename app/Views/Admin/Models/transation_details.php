<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Transaction details</h4>
</div>
<div class="modal-body">
	<?php if(is_array($details)){ ?>
		<table class='table'>
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Value</th>
				</tr>
			</thead>
			<tbody>
			<?php $i =1; foreach($details as $key => $value){ ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo ucfirst($key); ?></td>
					<td><?php if(is_array($value)){ }else{ echo $value;} ?></td>
				</tr>
			<?php $i++; } ?>
			</tbody>
		</table>
	<?php }else{ ?>
		No data found.
	<?php } ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>