<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Update payment info</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-6">
            <h1>Information</h1>
            <div class="form-group">
                <label>Account no</label>:
                <?php echo $payment_info['uai_account_no']; ?>
            </div><!-- Account no -->

            <div class="form-group">
                <label>Rounting no</label>:
                <?php echo $payment_info['uai_rounting_no']; ?>
            </div><!-- Rounting no -->

            <div class="form-group">
                <label>Account name</label>:
                <?php echo $payment_info['uai_account_name']; ?>
            </div><!-- Account name -->

            <div class="form-group">
                <label>Paypal id</label>:
                <?php echo $payment_info['uai_paypal_id']; ?>
            </div><!-- Paypal id -->

            <div class="form-group">
                <label>Payment preference</label>:
                <?php echo ucfirst($payment_info['uai_payment_preference']); ?>
            </div><!-- Payment preference -->
        </div>
        <div class="col-sm-6">
            <h1>Pay</h1>
            <form action="<?php echo base_url('admin/pay_to_user'); ?>" method="post">
                
                <div class="form-group">
                    <label>Payment type</label>:
                    <select name="payment_type" class="form-control">
                        <option value="refund">Refund</option>
                        <option value="booking">Booking</option>
                    </select>
                </div><!-- Payment type -->

                <div class="form-group">
                    <label>Payment via</label>:
                    <select name="payment_via" class="form-control">
                        <option value="paypal">Paypal</option>
                        <option value="bank_account">Bank account</option>
                        <option value="check">Check</option>
                    </select>
                </div><!-- Payment via -->

                <div class="form-group">
                    <label>Payment amount</label>:
                    <input type="text" name="payment_amount" class="form-control">
                </div><!-- Payment amount -->

                <div class="form-group">
                    <label>Payment date</label>:
                    <input type="" name="payment_date" class="form-control dp2">
                </div><!-- Payment date -->

                <div class="form-group">
                    <label>Payment comment</label>:
                    <input type="" name="payment_comment" class="form-control">
                </div><!-- Payment comment -->

                <input type="submit" value="Submit">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
            </form>
        </div>
    </div>
</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<script type="text/javascript">
    $( ".dp2" ).datepicker();
</script>