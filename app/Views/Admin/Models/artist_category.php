<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Category</h4>
</div>
<div class="modal-body">
	<?php if($category){ ?>
	<table class="table table-condensed">
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Price</th>
			<th>Product</th>
			<th>Duration</th>
		</tr>
	  <?php $i = 1; foreach($category as $value){ ?>
	  	<tr>
	  		<td><?php echo $i; ?></td>
	  		<td><?php echo ucfirst($value['cm_name']); ?></td>
	  		<td><?php echo $value['ac_price']; ?></td>
	  		<td><?php echo $value['ac_products']; ?></td>
	  		<td><?php echo $value['ac_duration']; ?></td>
	  	</tr>
	  <?php $i++; } ?>
	</table>
	<?php }else{ ?>
		No data found.
	<?php } ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>