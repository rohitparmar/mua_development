<style type="text/css">
  .bebb_table_admin tr td{
    padding: 10px;
    border: 1px solid #ddd;
  }
    .bebb_table_admin tr th{
    padding: 10px;
    border: 1px solid #ddd;
  }
  .bebb_modlbody{
    max-height: 400px;
overflow-y: scroll;
  }
  .modal-dialog.js-modal-width{
    width: 60%;
  }
  .yes{
      color:green;
      font-weight: bold;
  }
  .no{
      color:red;
      font-weight: bold;
  }
  .mua-info{
      margin: 10px 15px;
  }
  .bolder{
      font-weight: bolder;
  }
  .summary{
      border-bottom: 1px double #005580;
      margin-bottom: 10px;
  }
</style>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel"> Details </h4>
</div>
<div class="modal-body bebb_modlbody">
	

  <div class="box box-info">
    <!-- form start -->
    
      

        <?php if(isset($result) && $result){ ?>


          <div class="col-sm-12 mua-info">
            <div class="row bolder">
                <div class="col-sm-12 summary">SUMMARY</div>
            </div>
            <div class="row bolder">
                <div class="col-sm-6">Total CLIENT who provided service</div>
                <div class="col-sm-6">: <?php echo isset($booked_list)?$booked_list:'';?></div>
            </div>
            <div class="row bolder">
                <div class="col-sm-6">Total CLIENT who didn't provide service</div>
                <div class="col-sm-6">: <?php echo isset($not_booked_list)?$not_booked_list:'';?></div>
            </div>
            <div class="row bolder">
                <div class="col-sm-6">CLIENT total referrel earning</div>
                <div class="col-sm-6">: $<?php echo isset($total_earning)?$total_earning:'';?></div>
            </div>
        </div>


        <div class="row bolder">
            <div class="col-sm-12 summary">DETAILS</div>
        </div>
        <div class="box-body">
          <div class="col-sm-12">

            

            <table class="bebb_table_admin" style="width: 100%;">
              <thead>
                <tr>
                  <th>Email</th>
                  <th>Booking Status</th>
                  <th>Rebate Amount</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($result as $key => $value) {?>
                <tr>
                  <td> <?php echo isset($value['u_email'])?$value['u_email']:'';?> </td>
                  <td> <?php echo ($value['booking'])?'<label class="yes">Yes</label>':'<label class="no">No</label>';?> </td>
                  <td> <?php echo ($value['r_rebate'])?$value['r_rebate']:'';?> </td>
                  <td> <?php echo isset($value['ur_used_date'])?$value['ur_used_date']:'';?> </td>
                </tr>
                
                </tr>
                <?php } ?>
              </tbody>
            </table>
            
            
          </div>
        </div><!-- /.box-body -->
      
    
  
  

<?php  } else{ ?>
	No data found.
<?php } ?>
</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

