<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title" id="myModalLabel">Portfolio</h4>
</div>
<div class="modal-body">
	
<?php if($protfolio){ ?>
	<table class="table">
		<tr>
			<th>#</th>
      <th>Before</th>
      <th>After</th>
			<th>Name</th>
			<th>Category</th>
			<th>Description</th>
		</tr>
		<?php $i=1; foreach($protfolio as $value){ ?>
			<?php $images = json_decode($value['ap_image_path']); ?>
			<tr>
				<td><?php echo $i; ?></td>
        <td>
          <?php if(file_exists(ARTIST_PORTFOLIO_IMG.'/'.$value['portfolio_image_before']) && $value['portfolio_image_before']){ ?>
            <img src="<?php echo base_url(ARTIST_PORTFOLIO_IMG.'/'.$value['portfolio_image_before']); ?>" width='40px'>
          <?php } else{ ?>
            <img src="<?php echo base_url(ARTIST_PORTFOLIO_IMG.'/default_cat.png'); ?>" width='40px'>
          <?php } ?>
          
        </td>

        <td>
          <?php if(file_exists(ARTIST_PORTFOLIO_IMG.'/'.$value['portfolio_image_after']) && $value['portfolio_image_after']){ ?>
            <img src="<?php echo base_url(ARTIST_PORTFOLIO_IMG.'/'.$value['portfolio_image_after']); ?>" width='40px'>
          <?php } else{ ?>
            <img src="<?php echo base_url(ARTIST_PORTFOLIO_IMG.'/default_cat.png'); ?>" width='40px'>
          <?php } ?>
        </td>

				<td><?php echo $value['ap_name']; ?></td>
				<td><?php echo ucfirst($value['cm_name']); ?></td>
				<td><?php echo $value['ap_description']; ?></td>
			</tr>
		<?php $i++; } ?>
	</table>

	<!-- Pagination -->
    <div class="row">
      <div class="col-sm-5">
        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php echo (($page - 1)*10)+1; ?> to <?php echo (($page - 1)*10)+count($protfolio); ?> of <?php echo $count; ?> entries
        </div>
      </div>
      <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
          <ul class="pagination">
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="javascript:void(0);" url="<?php echo ($page != 1)?(base_url('admin/artist_portfolio/'.($page - 1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">Previous</a></li>

            <?php
              if($page == 1){
                $start = 1;
              }
              elseif((int)($count/10) == 1){
                $start = $page - 1;
              }
              elseif($page == ((int)($count/10)+1)){
                $start = $page - 2;
              }
              else{
                $start = $page-1;
              }
              for($i = 1; $i <= 3; $i++){ 

                if($start > ((int)($count/10)+1)){
                  continue;
                }
              ?>
                <li class="paginate_button <?php echo ($page == $start)?'active':""; ?>"><a href="javascript:void(0);" url="<?php echo base_url('admin/artist_portfolio/'.$start).$query; ?>" aria-controls="datatable" data-dt-idx="1" tabindex="0"><?php echo $start; ?></a></li>
            <?php $start++; } ?>

            <li class="paginate_button next <?php echo ($page == ceil(($count/10)))?"disabled":''; ?>" id="datatable_next"><a href="javascript:void(0);" url="<?php echo ($page != ceil(($count/10)))?(base_url('admin/artist_portfolio/'.($page + 1)).$query):'javascript:void(0);'; ?>" aria-controls="datatable" data-dt-idx="2" tabindex="0">Next</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- /Pagination -->

<?php }else{ ?>
	No data found.
<?php } ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>