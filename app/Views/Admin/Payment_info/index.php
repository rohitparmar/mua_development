<div class="box">
  <div class="box-body">

    <!-- Search -->
    <div class="row">
      <div class="col-sm-6">
      </div>
      <div class="col-sm-6">
        <div id="custom_datatable_filter" class="dataTables_filter">
          <form action="<?php echo base_url('admin/payment_info'); ?>">
            <label>Search:<input type="search" name="search" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['search'])?$_GET['search']:""; ?>" aria-controls="custom_datatable"></label>
          </form>
        </div>
      </div>
    </div>
    <!-- /Search -->

    <table id="custom_datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Account no</th>
          <th>Rounting no</th>
          <th>Account name</th>
          <th>Paypal id</th>
          <th>Payment preference</th>
          <th>Created</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; foreach($list as $value){ ?>
          <tr class="tr_<?php echo $value['uai_id']; ?>">
            <td><?php echo $i; ?></td>
            <td><?php echo $value['name']; ?></td>
            <td><?php echo $value['uai_account_no']; ?></td>
            <td><?php echo $value['uai_rounting_no']; ?></td>
            <td><?php echo $value['uai_account_name']; ?></td>
            <td><?php echo $value['uai_paypal_id']; ?></td>
            <td><?php echo ucfirst($value['uai_payment_preference']); ?></td>
            <td><?php echo $value['uai_created']; ?></td>
            <td>
              <a class='btn btn-default' href='javascript:void(0);' onclick="update_payment_info_open_model(<?php echo $value['uai_id']; ?>)">Update</a><!-- Update -->

              <a class='btn btn-default' href='javascript:void(0);' onclick="pay_to_user_open_model(<?php echo $value['uai_id']; ?>)">Pay</a><!-- Pay -->
            </td>
          </tr>
        <?php $i++; } ?>
      </tbody>
    </table>

    <!-- Pagination -->
    <div class="row">
      <div class="col-sm-5">
        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php echo (($page - 1)*10)+1; ?> to <?php echo (($page - 1)*10)+count($list); ?> of <?php echo $count; ?> entries
        </div>
      </div>
      <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
          <ul class="pagination">
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/payment_info/'.($page - 1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">Previous</a></li>

            <?php
              if($page == 1){
                $start = 1;
              }
              elseif((int)($count/10) == 1){
                $start = $page - 1;
              }
              elseif($page == ((int)($count/10)+1)){
                $start = $page - 2;
              }
              else{
                $start = $page-1;
              }
              for($i = 1; $i <= 3; $i++){ 

                if($start > ceil(($count/10))){
                  continue;
                }
              ?>
                <li class="paginate_button <?php echo ($page == $start)?'active':""; ?>"><a href="<?php echo base_url('admin/payment_info/'.$start).$query; ?>" aria-controls="datatable" data-dt-idx="1" tabindex="0"><?php echo $start; ?></a></li>
            <?php $start++; } ?>

            <li class="paginate_button next <?php echo ($page == ceil(($count/10)))?"disabled":''; ?>" id="datatable_next"><a href="<?php echo ($page != ceil(($count/10)))?(base_url('admin/payment_info/'.($page + 1)).$query):'javascript:void(0);'; ?>" aria-controls="datatable" data-dt-idx="2" tabindex="0">Next</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- /Pagination -->

  </div><!-- /.box-body -->
</div><!-- /.box -->


<!-- Profile Model -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog js-modal-width" role="document">
    <div class="modal-content js_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>
