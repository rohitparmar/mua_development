<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Transaction</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Transaction</li>
      <li class="active">Payment info</li>
    </ol>
</section>
<div class="box">
  <div class="box-body">

    <!-- Search -->
    <div class="row">
      <div class="col-sm-6">
      </div>
      <div class="col-sm-6">
        <div id="custom_datatable_filter" class="dataTables_filter">
          <form action="<?php echo base_url('admin/payment_info_mua'); ?>">
            <label>Search:<input type="search" name="search" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['search'])?$_GET['search']:""; ?>" aria-controls="custom_datatable2"></label>
          </form>
        </div>
      </div>
    </div>
    <!-- /Search -->

    <table id="custom_datatable2" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th width="5%">#</th>
          <th>Artist name</th>
          <th>Phone</th>
          <th>Email</th>
          <th>Pay bank account</th>
          <th>Routing Number</th>
          <th>Pay pal account</th>
          <!-- <th>Check Mail address</th> -->
          <th>Total earned</th>
          <!-- <th>Total earned this week</th> -->
          <th>Last Payment</th>
          <th>Paid </th>
          <!-- <th>Total earned this month </th>
          <th>Total earned up to date.</th> -->
          <!-- <th>Action </th> -->
        </tr>
      </thead>
      <tbody>

        <tr  class="tr_<?php echo $value['api_id']; ?>">
          <?php $i = 1; foreach($transaction as $value){ 
            $status = ($value['u_payment_varification'] == 'true')?'Pay Now':'Pay Now';
            $status_class = ($value['u_payment_varification'] == 'true')?'label label-success':'label label-danger';
          ?>
          <tr class="tr_<?php echo $value['api_id']; ?>">
            <td> <?php echo $i; ?> </td>
            <td> <?php echo $value['ap_profile_name']?$value['ap_profile_name']:$value['u_name']; ?> </td>
            <td> <?php echo isset($value['u_phone'])?$value['u_phone']:''; ?> </td>
            <td> <?php echo isset($value['u_email'])?$value['u_email']:''; ?> </td>
            <td> <?php echo isset($value['uai_account_no'])?$value['uai_account_no']:''; ?> </td>
            <td> <?php echo isset($value['uai_rounting_no'])?$value['uai_rounting_no']:''; ?> </td>
            <td> <?php echo isset($value['uai_paypal_id'])?$value['uai_paypal_id']:''; ?> </td>
            <td> 
            
            <!-- <a href="javascript:void(0);">
              <span class="">
                <?php echo isset($value['daily'])?$value['daily']:0; ?> &nbsp;&nbsp;(daily)
              </span>
            </a>
            <br> -->

            
              <span style="margin:1px 0px;">
                 $<?php echo isset($value['weekly'])?$value['weekly']:0; ?> &nbsp;&nbsp;(weekly)
              </span>
            
            <br>
            
           
              <span>
                $<?php echo isset($value['monthly'])?$value['monthly']:0; ?> &nbsp;&nbsp; (monthly)
              </span>
            
            <br>
            <?php if(isset($value['total']) && $value['total']){?>
            
            <a href="javascript:void(0);" onclick="total_payment_received(<?php echo $value['u_id']; ?>)">
              <span class="">
               $ <?php echo isset($value['total'])?$value['total']:0; ?> &nbsp;&nbsp;(total)
              </span>
            </a>

            <?php } else {?>

                  $<?php echo isset($value['total'])?$value['total']:0; ?> 
                  
                

                <?php } ?>
            </td>

            <td>  
              
                
                <?php if(isset($value['last_payment']) && $value['last_payment']){?>
                  
                <a href="javascript:void(0);" onclick="payment_received(<?php echo $value['u_id']; ?>)">
                  $<?php echo isset($value['last_payment'])?$value['last_payment']:0; ?> 
                  (<?php echo isset($value['last_payment_date'])?date('m-d-Y',strtotime($value['last_payment_date'])):''; ?>)
                </a>
                <?php } else {?>

                  $<?php echo isset($value['last_payment'])?$value['last_payment']:0; ?> 
                  
                

                <?php } ?>
                
              
            </td>
            
            <td>  
              <a href="javascript:void(0);" status = "<?php echo $value['u_status']; ?>" onclick="get_paid_data(<?php echo $value['u_id']; ?>)">
                <span class="<?php echo $status_class; ?>"><?php echo $status; ?></span>
              </a>
            </td>
            
          </tr>
        <?php $i++; } ?>
            
          </tr>

        
      </tbody>
    </table>

    <!-- Pagination -->
    <div class="row">
      <div class="col-sm-5">
        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php if(!$page)$page=1; echo (($page - 1)*10)+1; ?> to <?php echo (($page - 1)*10)+count($transaction); ?> of <?php echo $count; ?> entries
        </div>
      </div>
      <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
          <ul class="pagination">
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/client_payment_info/'.(1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">First</a></li>
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/client_payment_info/'.($page - 1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0"><<</a></li>
            
            <?php
              $page_tab_options = 10;
              if($page == 1){
                $start = 1;
              }
              elseif((int)($count/10) == 1){
                $start = $page - 1;
              }
              elseif($page == ((int)($count/10)+1)){
                $start = $page - 2;
              }
              else{
                $start = $page-1;
              }
              for($i = 1; $i <= $page_tab_options; $i++){ 

                if($start > ceil(($count/10))){
                  continue;
                }
              ?>
                <li class="paginate_button <?php echo ($page == $start)?'active':""; ?>"><a href="<?php echo base_url('admin/payment_info_mua/'.$start).$query; ?>" aria-controls="datatable" data-dt-idx="1" tabindex="0"><?php echo $start; ?></a></li>
            <?php $start++; } ?>

            <li class="paginate_button next <?php echo ($page == ceil(($count/10)))?"disabled":''; ?>" id="datatable_next"><a href="<?php echo ($page != ceil(($count/10)))?(base_url('admin/client_payment_info/'.($page + 1)).$query):'javascript:void(0);'; ?>" aria-controls="datatable" data-dt-idx="2" tabindex="0">>></a></li>
            <li class="paginate_button previous <?php echo ($page == ceil($count/$page_tab_options))?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != ceil($count/$page_tab_options))?(base_url('admin/client_payment_info/'.(ceil($count/$page_tab_options))).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">Last</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- /Pagination -->

  </div><!-- /.box-body -->
</div><!-- /.box -->


<!-- Profile Model -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog js-modal-width" role="document">
    <div class="modal-content js_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>


