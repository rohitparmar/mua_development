<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Transaction</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Transaction</li>
      <li class="active">Client payment</li>
    </ol>
</section>
<div class="box">
  <div class="box-body">

    <!-- Search -->
    <div class="row">
      <div class="col-sm-6">
      </div>


      
    </div>
    <!-- /Search -->
    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Artist</th>
                <th>Subscription name</th>
                <th>Service charge</th>
                <th>Dated</th>
                <th>Next Billing Date</th>
            </tr>
        </thead>
        <tbody>
          <?php
            if(isset($transaction) && $transaction){
              foreach ($transaction as $key => $value){
          ?>
            <tr>
              <td><?php echo $value['artist_profile_name']?$value['artist_profile_name']:$value['artist_name']; ?></td>
              <td><?php echo ucfirst($value['subscription_name']); ?></td>
              <td><?php echo "<strong>$</strong>".($value['api_service_amout']?$value['api_service_amout']:"0"); ?></td>
              <td><?php echo date('Y-m-d',strtotime($value['api_created'])); ?></td>
              <td>  

              <?php
                $time = strtotime(date('Y-m-d',strtotime($value['api_created'])));
                $final = date("Y-m-d", strtotime("+1 month", $time));
              ?>
              <?php echo isset($final)?$final:''; ?>
              </td>
            </tr>

            <?php  } } ?>
            
        </tbody>
</table>



  </div><!-- /.box-body -->
</div><!-- /.box -->


<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    
 




<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>