<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Transaction</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Transaction</li>
      <li class="active">Client payment</li>
    </ol>
</section>
<div class="box">
  <div class="box-body">

    <!-- Search -->
    <div class="row">
      <div class="col-sm-6">
      </div>


     

    <!-- <div class="col-sm-6">
        <div id="custom_datatable_filter" class="dataTables_filter">
          <form action="<?php echo base_url('admin/client_payment_info'); ?>">
            <label>Start Date:<input type="search" name="start_date" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['start_date'])?$_GET['start_date']:""; ?>" aria-controls="custom_datatable"></label>

            <label>End Date:<input type="search" name="end_date" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['end_date'])?$_GET['end_date']:""; ?>" aria-controls="custom_datatable"></label>
          </form>
        </div>
      </div> -->




      <div class="col-sm-7">
        <div id="custom_datatable_filter" class="dataTables_filter">
          <form action="<?php echo base_url('admin/client_payment_info'); ?>">

          <label>Search:<input type="search" name="search" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['search'])?$_GET['search']:""; ?>" aria-controls="custom_datatable"></label>

          <label>Start Date:<input type="text" name="start_date" placeholder="y-m-d" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['start_date'])?$_GET['start_date']:""; ?>"></label>

          <label>End Date:<input type="text" name="end_date" placeholder="y-m-d" class="form-control input-sm" placeholder="" value="<?php echo isset($_GET['end_date'])?$_GET['end_date']:""; ?>"></label>
            
            

            <input type="submit" class="hide">
          </form>
        </div>
      </div>
    </div>
    <!-- /Search -->
    <table id="custom_datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Client</th>
          <th>Artist</th>
          <th>Price</th>
          <th>Service name</th>
          <th>Service duration</th>
          <th>Payment time</th>
          <th>Payment date</th>
          <th>Commision</th>
          <th>Payment</th>
          <th>Pay to artist</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; foreach($transaction as $value){ ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $value['client_name']; ?></td>
            <td><?php echo $value['artist_name']; ?></td>
            <td><strong>$</strong><?php echo $value['cpi_service_amout']; ?></td>
            <td><?php echo $value['category_name']; ?></td>
            <td><?php if($value['duration']){ ?><?php echo $value['duration']; ?> min <?php } ?></td>
            <td><?php echo date('h:i a',strtotime($value['cpi_created'])); ?></td>
            <td><?php echo date('Y-m-d',strtotime($value['cpi_created'])); ?></td>
            <td><?php echo $value['cpi_commission']; ?>%</td>
            <td><strong>$</strong><?php echo $value['cpi_paid_amount']; ?></td>
            <td><strong>$</strong><?php echo round($value['cpi_pay_to_artist'],2); //round(($value['cpi_paid_amount']-(($value['cpi_service_amout']/2)*$value['cpi_commission']/100)),2); ?></td>
          </tr>
        <?php $i++; } ?>
      </tbody>
    </table>

<!-- Pagination -->
    <div class="row">
      <div class="col-sm-5">
        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing <?php echo (($page - 1)*10)+1; ?> to <?php echo (($page - 1)*10)+count($transaction); ?> of <?php echo $count; ?> entries
        </div>
      </div>
      <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
          <ul class="pagination">
            
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/client_payment_info/'.(1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">First</a></li>
            <li class="paginate_button previous <?php echo ($page == 1)?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != 1)?(base_url('admin/client_payment_info/'.($page - 1)).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0"><<</a></li>

            <?php
              $page_tab_options = 10;
              if($page == 1){
                $start = 1;
              }
              elseif((int)($count/10) == 1){
                $start = $page - 1;
              }
              elseif($page == ((int)($count/10)+1)){
                $start = $page - 2;
              }
              else{
                $start = $page-1;
              }
              for($i = 1; $i <=$page_tab_options; $i++){ 

                if($start > ceil(($count/10))){
                  continue;
                }
              ?>
                <li class="paginate_button <?php echo ($page == $start)?'active':""; ?>"><a href="<?php echo base_url('admin/client_payment_info/'.$start).$query; ?>" aria-controls="datatable" data-dt-idx="1" tabindex="0"><?php echo $start; ?></a></li>
            <?php $start++; } ?>

            <li class="paginate_button next <?php echo ($page == ceil(($count/10)))?"disabled":''; ?>" id="datatable_next"><a href="<?php echo ($page != ceil(($count/10)))?(base_url('admin/client_payment_info/'.($page + 1)).$query):'javascript:void(0);'; ?>" aria-controls="datatable" data-dt-idx="2" tabindex="0">>></a></li>
            <li class="paginate_button previous <?php echo ($page == ceil($count/$page_tab_options))?'disabled':''; ?>" id="datatable_previous"><a href="<?php echo ($page != ceil($count/$page_tab_options))?(base_url('admin/client_payment_info/'.(ceil($count/$page_tab_options))).$query):"javascript:void(0);"; ?>" aria-controls="datatable" data-dt-idx="0" tabindex="0">Last</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- /Pagination -->

  </div><!-- /.box-body -->
</div><!-- /.box -->


