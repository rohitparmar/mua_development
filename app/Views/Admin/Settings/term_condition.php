<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Settings</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Settings</li>
      <li class="active">Term Condition</li>
    </ol>
</section>


<div class="box" style="margin: 20px 0 0 0;">

    <div class="box-header" style="padding: 20px 0px;">

    <h3 style="padding-left: 10px !important; display: inline-block !important; margin: 20px 0px 0px 0px !important;"><bold>Term Condition</bold></h3>
              
      <!-- tools box -->
      <div class="pull-right box-tools" style="margin: 5px 0 0 0;">
        <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        
      </div><!-- /. tools -->
    </div><!-- /.box-header -->
    

    <div class="box-body pad">
          <form>
            <textarea id="editor1" name="term_condition" rows="10" cols="80">
                    <?php 
                      echo $file_eng;
                     ?>
            </textarea>
            <button type="submit" class="btn btn-info pull-left" style="padding: 8px 20px;">Save</button>
          </form>
        </div>
    
</div>




<div class="box" style="margin: 20px 0 0 0;">

    <div class="box-header" style="padding: 20px 0px;">

    <h3 style="padding-left: 10px !important; display: inline-block !important; margin: 20px 0px 0px 0px !important;"><bold>Término y condición</bold></h3>

                   
      <!-- tools box -->
      <div class="pull-right box-tools" style="margin: 5px 0 0 0;">
        <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        
      </div><!-- /. tools -->
    </div><!-- /.box-header -->
    


    <div class="box-body pad">
          <form>
            <textarea id="editor2" name="term_condition" rows="10" cols="80">
                    <?php 
                      echo $file_spn;
                      
                     ?>
            </textarea>
            <input type="hidden" name="lang" value="spn">
      
            <button type="submit" class="btn btn-info pull-left" style="padding: 8px 20px;">Save</button>
          </form>
        </div>

</div>

<script src="<?php echo base_url('themes/admin/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('themes/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editor2');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>