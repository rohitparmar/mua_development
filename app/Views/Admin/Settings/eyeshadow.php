<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Settings</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Settings</li>
      <li class="active">Eyeshadow list</li>
    </ol>
</section>
<div class="box">
    <div class="box-header with-border"> <h3 class="box-title">Eyeshadow List</h3> (English) </div>
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($result as $row) {
                    if(isset($row['language']) && $row['language']==1){
               	$i++; 
                ?>
                    <tr class="">
                        <td><?php echo $i; ?></td>
                        <td><?php echo isset($row['me_name'])?$row['me_name']:''; ?></td>
                        <td>
                            <?php if (isset($row['me_status']) && $row['me_status'] == 'true') { ?>

                            <a href="javascript:void(0);" title="Click to Deactivate" onclick="change_eyeshadow_status(<?php echo $row['me_id'];?>, $(this))" status="true"><span class="action label label-success">Active</span></a>

                            <?php } else { ?>

                            <a href="javascript:void(0);" title="Click to Activate" onclick="change_eyeshadow_status(<?php echo $row['me_id'];?>, $(this))" status="true"><span class="action label label-danger">Inactive</span></a>

                            <?php } ?>
                        </td>

                        <td>
                            <a href="javascript:void(0);" title="Edit" onclick="update_eyeshadow(<?php echo $row['me_id']; ?>)"><button class="fa fa-pencil-square-o btn-primary"></button></a>
                        </td>
                    </tr>
               <?php }  }?>
            </tbody>
        </table>
    </div>
</div>



<div class="box">
    <div class="box-header with-border"> <h3 class="box-title">Eyeshadow List</h3> (Spanish) </div>
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($result as $row) {
                    if(isset($row['language']) && $row['language']==2){
                $i++; 
                ?>
                    <tr class="">
                        <td><?php echo $i; ?></td>
                        <td><?php echo isset($row['me_name'])?$row['me_name']:''; ?></td>
                        <td>
                            <?php if (isset($row['me_status']) && $row['me_status'] == 'true') { ?>

                            <a href="javascript:void(0);" title="Click to Deactivate" onclick="change_eyeshadow_status(<?php echo $row['me_id'];?>, $(this))" status="true"><span class="action label label-success">Active</span></a>

                            <?php } else { ?>

                            <a href="javascript:void(0);" title="Click to Activate" onclick="change_eyeshadow_status(<?php echo $row['me_id'];?>, $(this))" status="true"><span class="action label label-danger">Inactive</span></a>

                            <?php } ?>
                        </td>

                        <td>
                            <a href="javascript:void(0);" title="Edit" onclick="update_eyeshadow(<?php echo $row['me_id']; ?>)"><button class="fa fa-pencil-square-o btn-primary"></button></a>
                        </td>
                    </tr>
               <?php }  }?>
            </tbody>
        </table>
    </div>
</div>





<!-- Profile Model -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog js-modal-width" role="document">
    <div class="modal-content js_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>


