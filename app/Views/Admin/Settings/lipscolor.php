<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Settings</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Settings</li>
      <li class="active">Lips Color list</li>
    </ol>
</section>

<div class="box">
    <div class="box-header with-border"> <h3 class="box-title">Lips Color List</h3> (English) </div>
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
        
            <tbody>
            <?php
                $i = 0;
                foreach ($result as $row) {
                    if(isset($row['language']) && $row['language']==1){
                $i++; 
            ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo isset($row['mlc_name'])?$row['mlc_name']:''; ?></td>
                    <td>
                        <?php if (isset($row['mlc_status']) && $row['mlc_status'] == 'true') { ?>

                        <a href="javascript:void(0);" title="Click to Deactivate" onclick="change_lipscolor_status(<?php echo $row['mlc_id'];?>, $(this))" status="true"><span class="action label label-success">Active</span></a>

                        <?php } else { ?>

                        <a href="javascript:void(0);" title="Click to Activate" onclick="change_lipscolor_status(<?php echo $row['mlc_id'];?>, $(this))" status="true"><span class="action label label-danger">Inactive</span></a>

                        <?php } ?>
                    </td>

                    <td>
                        <a href="javascript:void(0);" title="Edit" onclick="update_lipscolor(<?php echo $row['mlc_id']; ?>)"><button class="fa fa-pencil-square-o btn-primary"></button></a>
                    </td>

                 </tr>
                 <?php } } ?>
            </tbody>
        </table>
    </div>
</div>




<div class="box">

    <div class="box-header with-border"> <h3 class="box-title">Lips Color List</h3> (Spanish) </div>

    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
            <?php
            $i = 0;
             foreach ($result as $row) {
                if(isset($row['language']) && $row['language']==2){
                $i++;
             ?>
                <tr class="">
                    <td><?php echo $i; ?></td>
                    <td><?php echo isset($row['mlc_name'])?$row['mlc_name']:''; ?></td>
                    <td>
                        <?php if (isset($row['mlc_status']) && $row['mlc_status'] == 'true') { ?>

                        <a href="javascript:void(0);" title="Click to Deactivate" onclick="change_lipscolor_status(<?php echo $row['mlc_id'];?>, $(this))" status="true"><span class="action label label-success">Active</span></a>

                        <?php } else { ?>

                        <a href="javascript:void(0);" title="Click to Activate" onclick="change_lipscolor_status(<?php echo $row['mlc_id'];?>, $(this))" status="true"><span class="action label label-danger">Inactive</span></a>

                        <?php } ?>
                    </td>

                    <td>
                        <a href="javascript:void(0);" title="Edit" onclick="update_lipscolor(<?php echo $row['mlc_id']; ?>)"><button class="fa fa-pencil-square-o btn-primary"></button></a>
                    </td>
                </tr>
                <?php } } ?>
            </tbody>

        </table>

    </div>

</div>







<!-- Profile Model -->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#js_model" style="display: none;" id='js_model_button'>
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="js_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog js-modal-width" role="document">
    <div class="modal-content js_model_html">
      <!-- popup contents goes here -->
    </div>
  </div>
</div>


