<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Settings</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Settings</li>
      <li class="active">Commission list</li>
    </ol>
</section>
<div class="box box-info hide">
  <!-- form start -->
  <form class="form-horizontal" action="" method="post">
      <h3 class="box-title">Commission Setting</h3>
    <div class="box-body">

    

      <div class="pull-right">
        <label>Last updated : <?php echo @$settings['commission']['s_updated']; ?></label> 
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="form-group">
          <label class="col-sm-2 control-label">Commission in percent</label>
          <div class="col-sm-2">
            <input type="number" step='.1' name="commission" class="form-control" placeholder="Commision" value="<?php echo @$settings['commission']['s_value']; ?>">
            <span class="error"> <?php //echo $validation->error('commision'); ?> </span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Status</label>
          <div class="col-sm-2">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="status" <?php if(isset($settings['commission']['s_status']) && ($settings['commission']['s_status'] == 'true')){ echo 'checked'; } ?>> Active
              </label>
            </div>
          </div>
        </div>
      </div>

    </div><!-- /.box-body -->
    <div class="box-footer">
        <div class="row">
            <div class="col-sm-3 offset2">
                <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
        </div>
    </div><!-- /.box-footer -->
  </form>
</div>


<div class="box box-info">
  <!-- form start -->
  <form class="form-horizontal" action="" method="post">
      <h3 class="box-title">Rate per mile Setting</h3>
    <div class="box-body">
      <div class="pull-right">
        <label>Last updated : <?php echo @$settings['rate_per_mile']['s_updated']; ?></label> 
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="form-group">
          <label class="col-sm-2 control-label">Rate/mile</label>
          <div class="col-sm-2">
            <input type="number" step='.1' name="ratepermile" class="form-control" placeholder="Rate per mils" value="<?php echo @$settings['rate_per_mile']['s_value']; ?>">
            <span class="error"> <?php //echo $validation->error('ratepermile'); ?> </span>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Status</label>
          <div class="col-sm-2">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="status" <?php if(isset($settings['rate_per_mile']['s_status']) && ($settings['rate_per_mile']['s_status'] == 'true')){ echo 'checked'; } ?>> Active
              </label>
            </div>
          </div>
        </div>
      </div>

    </div><!-- /.box-body -->
    <div class="box-footer">
        <div class="row">
            <div class="col-sm-3 offset2">
                <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
        </div>
    </div><!-- /.box-footer -->
  </form>
</div><!-- /.box -->


<div class="box box-info">
  <!-- form start -->
  <form class="form-horizontal" action="" method="post">
      <h3 class="box-title">Users Referral Setting</h3>
    <div class="box-body">

      <div class="pull-right">
        <label>Last updated : <?php echo @$settings['referral_expiry']['s_updated']; ?></label> 
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="form-group">
          <label class="col-sm-2 control-label">Referral Expiry Duration (in days)</label>
          <div class="col-sm-2">
            <input type="number" step='1' min="0" name="referral_expiry" class="form-control" value="<?php echo isset($settings['referral_expiry']['s_value'])?$settings['referral_expiry']['s_value']:20; ?>">
            
            <span class="error"> <?php //echo $validation->error('commision'); ?> </span>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Referral Rebate Amout (in $)</label>
          <div class="col-sm-2">
            <input type="number" step='1' min="0" max="20" name="referral_amount" class="form-control" value="<?php echo isset($settings['referral_amount']['s_value'])?$settings['referral_amount']['s_value']:0; ?>">
            <span class="error"> <?php //echo $validation->error('commision'); ?> </span>
          </div>
        </div>
        
      </div>

    </div><!-- /.box-body -->
    <div class="box-footer">
        <div class="row">
            <div class="col-sm-3 offset2">
                <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
        </div>
    </div><!-- /.box-footer -->
  </form>
</div>


<div class="box box-info">
  <!-- form start -->
  <form class="form-horizontal" action="" method="post">
      <h3 class="box-title">App Price</h3>
    <div class="box-body">


      <div class="pull-right">
        <label>Last updated : <?php echo @$settings['referral_expiry']['s_updated']; ?></label> 
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="form-group">
          <label class="col-sm-2 control-label">App Price(in $)</label>
          <div class="col-sm-2">
            <input type="number" min="0" step="0.01" name="app_price" class="form-control" value="<?php echo isset($settings['app_prices']['s_value'])?$settings['app_prices']['s_value']:20; ?>"> 
            
            <span class="error"> <?php //echo $validation->error('commision'); ?> </span>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Price Discount(in %)</label>
          <div class="col-sm-2">
            <input type="number" min="0" max="100" step="0.01" name="app_discount" class="form-control" value="<?php echo isset($settings['app_commission']['s_value'])?$settings['app_commission']['s_value']:0; ?>">
            <span class="error"> <?php //echo $validation->error('commision'); ?> </span>
          </div>
        </div>
        
      </div>

    </div><!-- /.box-body -->
    <div class="box-footer">
        <div class="row">
            <div class="col-sm-3 offset2">
                <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
        </div>
    </div><!-- /.box-footer -->
  </form>
</div>