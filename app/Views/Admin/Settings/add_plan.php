<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Settings</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Settings</li>
      <li class="active">Plan</li>
    </ol>
</section>
<!-- general form elements -->
<div class="box box-primary">
  <div class="box-header with-border">
    <a class="btn btn-primary pull-right" href="<?php echo base_url('admin/plan'); ?>">Back</a>
  </div>
  <!-- form start -->
  <form role="form" action="" method="post" enctype="multipart/form-data" id="add_plan_frm">
    <div class="box-body">

    <div class="box-body">
      
      <div style="width: 45%; float: left;">
         <div class="form-group">
              <?php 
                

                $data = "";
                if($validation->get_data('name')){
                  $data = $validation->get_data('name');
                }
                elseif(isset($plan['asp_name'])){
                  $data = $plan['asp_name'];
                }
              ?>
              <label>Name</label>
              <input type="hidden" name="id" value="<?php echo isset($plan['asp_id'])?$plan['asp_id']:'';?>">

              <select class="form-control" name="name">
                <option value="normal" <?php echo ($data == 'normal')?"selected":""; ?>>Normal</option>
                <option value="free" <?php echo ($data == 'free')?"selected":""; ?>>Free</option>
                <option value="palladium" <?php echo ($data == 'palladium')?"selected":""; ?>>Palladium</option>
                <option value="diamond" <?php echo ($data == 'diamond')?"selected":""; ?>>Diamond</option>
                <option value="deluxe" <?php echo ($data == 'deluxe')?"selected":""; ?>>Deluxe</option>
              </select>
        </div>

        <div class="form-group">
            <?php 
                $data = "";
                if($validation->get_data('description')){
                  $data = $validation->get_data('description');
                }
                elseif(isset($plan['asp_description'])){
                  $data = $plan['asp_description'];
                }
              ?>
              <label>Description</label>
              <textarea class="form-control" name="description" placeholder="Description"><?php echo $data; ?></textarea>
        </div>
      </div>

      
  <div style="width: 45%; float: right;">

       <div class="form-group">
          <?php 
            $data = "";
            if($validation->get_data('name_spn')){
              $data = $validation->get_data('name_spn');
            }
            elseif(isset($plan['asp_name_spn'])){
              $data = $plan['asp_name_spn'];
            }
          ?>
          
          <label>Spanish Name</label>
          <select class="form-control" name="name_spn">
            <option value="normal"   <?php echo ($data == 'normal')?"selected":""; ?>>Normal</option>
            <option value="gratis"   <?php echo ($data == 'gratis')?"selected":""; ?>>Gratis</option>
            <option value="paladio"  <?php echo ($data == 'paladio')?"selected":""; ?>>Paladio</option>
            <option value="diamante" <?php echo ($data == 'diamante')?"selected":""; ?>>Diamante</option>
            <option value="de lujo"  <?php echo ($data == 'de lujo')?"selected":""; ?>>De lujo</option>
          </select>
      </div>

      <div class="form-group">
        <?php 
            $data = "";
            if($validation->get_data('description_spn')){
              $data = $validation->get_data('description_spn');
            }
            elseif(isset($plan['asp_description_spn'])){
              $data = $plan['asp_description_spn'];
            }
          ?>
          <label>Spanish Description</label>
          <textarea class="form-control" name="description_spn" placeholder="Español Descripción"><?php echo $data; ?></textarea>
        </div>
     </div>

  </div>    

      
      <div class="form-group">
        <?php 
          $data = "";
          if($validation->get_data('amount')){
            $data = $validation->get_data('amount');
          }
          elseif(isset($plan['asp_amount'])){
            $data = $plan['asp_amount'];
          }
        ?>
        <label>Amount</label>
        <input type="number" step=".1" name="amount" class="form-control" placeholder="Amount" value="<?php echo $data; ?>">
      </div>

      <div class="form-group hidden">
        <?php 
          $data = "";
          /*echo'<pre>';
          print_r($plan); die;*/
          if($validation->get_data('duration')){
            $data = $validation->get_data('duration');
          }
          elseif(isset($plan['asp_duration'])){
            $data = $plan['asp_duration'];
          }
        ?>
        <label>Duration (In months)</label>
        <input type="number" name="duration" class="form-control" placeholder="Duration" value="<?php echo $data; ?>">
      </div>

      
      

      

      <div class="form-group">
        <label>Image(JPG,PNG,GIF,BMP)</label>
        <input type="file" name="image">
      </div>

      <div class="form-group">
        <?php 
        	
        	$all_plan = isset($all_plan)?end($all_plan):'';
        	$last_id  = isset($all_plan['asp_id'])?$all_plan['asp_id']:'';
        	
          $data = "";
          if($validation->get_data('status')){
            $data = $validation->get_data('status');
          }
          elseif(isset($plan['asp_status'])){
            $data = $plan['asp_status'];
          }
        ?>
        
        <label>Status</label>
        <div class="row">
          <div class="col-xs-2 col-sm-2 col-md-2 col-md-2">
            
              <div class="radio">
                <label>
                  <input type="radio" name="status" value="true" <?php echo (($data == "") || ($data == 'true'))?"checked":""; ?>>
                  Active
                </label>
              </div>
          </div>
        
          <div class="col-xs-2 col-sm-2 col-md-2 col-md-2">
            <div class="radio">
                <label>
                  <input type="radio" name="status" value="false" <?php echo ($data == 'false')?'checked':""; ?>>
                  Inactive
                </label>
            </div>
          </div>
        </div>
        
      </div>


      <div class="col-sm-4">
      <?php
     /* echo'<pre>';
      print_r($plan);
      print_r($last_id);*/
      
      ?>
        
        <input name="old_number" value="<?php echo isset($plan['asp_orderby'])?$plan['asp_orderby']:'';?>" type="hidden">

		<input type="number" name="order_number" 
    value="<?php echo isset($plan['asp_orderby'])?$plan['asp_orderby']:'';?>" 
    min="1" 
    max="<?php echo isset($last_id)?$last_id:''; ?>">


        <!-- <input class="required form-control" name="order_number" 
		        value="<?php echo isset($plan['asp_orderby'])?$plan['asp_orderby']:'';?>" 
		        min="1" 
		        max="<?php echo isset($last_id)?$last_id:''; ?>" type="number"> -->

      </div>

      


    </div><!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary pull-right">Submit</button>
      
    </div>
  </form>
</div><!-- /.box -->