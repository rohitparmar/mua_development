<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Settings</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Settings</li>
      <li class="active">Notification</li>
    </ol>
</section>
<div class="box" style="padding: 5px;">

  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <?php foreach ($notification as $key => $value) { ?>
    
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="heading<?php echo isset($value['n_id'])?$value['n_id']:''; ?>">
        <h4 class="panel-title">
          
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo isset($value['n_id'])?$value['n_id']:''; ?>" aria-expanded="true" aria-controls="collapseOne">
            <?php echo isset($value['n_heading'])?$value['n_heading']:''; ?>
          </a>
        </h4>
      </div>
      <div id="collapse<?php echo isset($value['n_id'])?$value['n_id']:''; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo isset($value['n_id'])?$value['n_id']:''; ?>">
        <div class="panel-body">

          <form class="form-horizontal" action="" method="post">

          <div class="row">
              <div class="form-group">
                <label class="col-sm-2 control-label" style="color:#f00;">English</label>
                <div class="col-sm-8">
                  
                </div>
              </div>
            
            <div class="row">
              <div class="form-group">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-8">
                  <input type="text" name="title" class="form-control" placeholder="Title" value="<?php echo isset($value['n_title_eng'])?$value['n_title_eng']:''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Message</label>
                <div class="col-sm-8">
                    <input type="text" name="message" class="form-control" placeholder="Message" value="<?php echo isset($value['n_message_eng'])?$value['n_message_eng']:''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" style="color:#f00;">Spanish</label>
                <div class="col-sm-8">
                  
                </div>
              </div>

               <div class="form-group">
                <label class="col-sm-2 control-label">Título</label>
                <div class="col-sm-8">
                  <input type="text" name="title_spn" class="form-control" placeholder="Título" value="<?php echo isset($value['n_title_spn'])?$value['n_title_spn']:''; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Mensaje</label>
                <div class="col-sm-8">
                    <input type="text" name="message_spn" class="form-control" placeholder="Mensaje" value="<?php echo isset($value['n_message_spn'])?$value['n_message_spn']:''; ?>">
                </div>
              </div>

              <input type="hidden" name="id" value="<?php echo isset($value['n_id'])?$value['n_id']:''; ?>">
              <input type="hidden" name="lang" value="eng">

              <button type="submit" class="btn btn-info pull-left" style="margin-left:50%;">Save</button>
          
          </div>
          </form>


          

        </div>
      </div>
   </div>    
    
  <?php } ?>
  </div>
  

</div>

