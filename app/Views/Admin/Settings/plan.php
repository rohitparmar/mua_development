<section class="content-header">
    <h1>
        <i class="fa fa-home"></i>
        <small>Settings</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="">Settings</li>
      <li class="active">Add plan</li>
    </ol>
</section>
<div class="box">
  <div class="box-header">
    <!-- <a href="<?php echo base_url('admin/add_plan'); ?>" class="btn btn-primary pull-right">Add plan</a> -->
  </div><!-- /.box-header -->
  <div class="box-body">
    <table id="datatable" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Image</th>
          <th>Name</th>
          <th>Amount</th>
          <th>Duration</th>
          <th>Order By</th>
          <th class="no_sort">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; foreach($plan as $value){ ?>
          <?php
            $status = ($value['asp_status'] == 'true')?'Active':'Inactive';
            $status_class = ($value['asp_status'] == 'true')?'label label-success':'label label-danger';
          ?>
          <tr class='tr_<?php echo $value['asp_id']; ?>'>
            <td width='5%'><?php echo $i; ?></td>
            <td>
              <?php 
                if($value['asp_image'] && file_exists(SUBSCRIPTION_PLAN_IMG.$value['asp_image'])){ 
                  $img_path = base_url(SUBSCRIPTION_PLAN_IMG.$value['asp_image']);
                }
                else{
                  $img_path = base_url(SUBSCRIPTION_PLAN_IMG.'default.jpg');
                } 
              ?>
              <img style="width:60px;" src='<?php echo $img_path; ?>'>
            </td>
            <td><?php echo ucfirst($value['asp_name']); ?></td>
            <td>$<b><?php echo $value['asp_amount']; ?></b></td>
            <td><b><?php echo (double)$value['asp_duration']; ?></b> <?php echo ($value['asp_duration'] > 1)?"months":"month"; ?></td>
            <td><?php echo $value['asp_orderby']; ?></td>
            <td>
              <a href='javascript:void(0);' status="<?php echo $value['asp_status']; ?>" onclick="change_plan_status(<?php echo $value['asp_id']; ?>,$(this))"><span class='<?php echo $status_class ?> action'><?php echo $status; ?></span></a>

              <img style="display: none; width: 16px;" class="js_loading_img" src="http://localhost/admin_slim/themes/admin/custom/images/loading.gif"><!-- Status -->

              <a href="<?php echo base_url('admin/edit_plan/'.$value['asp_id']); ?>"><button class="fa fa-pencil-square-o btn-primary"></button></a><!-- Edit -->
              
              <!--<a href="<?php echo base_url('admin/delete_plan/'.$value['asp_id']); ?>" class='delete_plan'><button class="fa fa-remove btn-danger"></button></a>--><!-- Delete -->
            </td>
          </tr>
        <?php $i++; } ?>
      </tbody>
    </table>
  </div><!-- /.box-body -->
</div><!-- /.box -->