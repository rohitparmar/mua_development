<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sample</title>
</head> 
<body>
  <div style="width: 650px;height: auto;margin: auto;background: #f1f1f1;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td bgcolor="black" style="width: 220px;background: #000000;text-align: center;">
          <div style="padding: 0 10px 30px;">
            <img src="http://dev.bebebellamuas.com/artist-client-upload-file/email/logo.png" />
            <!-- <p style="color: #ffffff;font-family: Verdana;font-size: 10px;">111, Land Mark, Street Name, State, Country 90001</p>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #ffffff;font-family: Verdana;font-size: 10px;">
              <tr>
                <td>Ph: 999-999-9999</td>
                <td>Fax: 55-5555</td>
              </tr>
            </table> -->
          </div>
        </td>
        <td valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #333333;font-family: Verdana;font-size: 12px;">
              <tr>
                <td valign="top" style="height: 267px;">
                  <h3 style="font-family: verdana;font-size: 30px;font-weight: normal;text-align: center;">
                  <!-- <img src="http://bebebellamuas.com/artist-client-upload-file/email/giftboxes.png" />  -->
                   New Booking.
                  </h3>
                  <table border="0" cellpadding="5" width="90%" style="margin: auto;">


          <tr>
            <td><strong>Services Info-</strong> </td>
            <td></td>
          </tr>


          <tr>
            <td>Services Name</td>
            <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">         
              <?php echo isset($ca_name)?$ca_name:''; ?> 
            </div></td>
          </tr>
          <tr>
            <td>Services Description </td>
            <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">         
              <?php echo isset($ca_description)?$ca_description:''; ?>
            </div></td>
          </tr>

          
          <tr>
            <td><strong>Client Info-</strong> </td>
            <td></td>
          </tr>
               
          
          <tr>
            <td>Name</td>
            <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">
                <?php echo isset($client_name)?$client_name:''; ?>
            </div></td>
          </tr>
                    
          <tr>
            <td>Phone</td>
            <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">
              <?php echo isset($client_phone)?$client_phone:''; ?>,
              <?php echo isset($client_phone2)?$client_phone2:''; ?>
            </div></td>
          </tr>
          
          <tr>
            <td>City</td>
            <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">
                <?php echo isset($cp_city)?$cp_city:''; ?>
            </div></td>
          </tr>

          <tr>
            <td>State</td>
            <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">
                <?php echo isset($cp_state)?$cp_state:''; ?>
            </div></td>
          </tr>

          <tr>
            <td>Address</td>
            <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">
                <?php echo isset($cp_address)?$cp_address:''; ?>
            </div></td>
          </tr>

          <tr>
            <td>Zipcode</td>
            <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">
                <?php echo isset($cp_zipcode)?$cp_zipcode:''; ?>
            </div></td>
          </tr>
                    
                  





                  </table>
                  <br>
                  <!-- <p style="text-align: center;">Not Redeemable for Cash</p> -->
                </td>
              </tr>
              <tr><td><div style="background: #D8136B;height: 40px;width: 100%; color: #fff; text-align: center; line-height: 35px;" > © 2016-2017 bebebellamuas.com</div></td></tr>
          </table>
        </td>
      </tr>
    </table>
  </div><!-- container -->
</body>
</html>
 
 

  

