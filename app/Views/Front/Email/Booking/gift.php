<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sample</title>
</head> 
<body>
  <p style="width: 650px;height: 100px;margin: auto;background: #f1f1f1; padding: 5px;">
    Your friend <?php echo isset($client_name)?$client_name:''; ?> has requested a service with  an artist on the Bébé Bella App. This service is a complimentary gift to you. Once the service has been approved by the artist, you will be notified on the logistics. If the service is not approved by the artist, your friend will rebook the service with a different artist. Please contact clientsupport@bebebellamuas.com with any questions! Enjoy your service!

  </p>
  <div style="width: 650px;height: 308px;margin: auto;background: #f1f1f1; padding: 5px;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td bgcolor="black" style="width: 220px;background: #000000;text-align: center;">
          <div style="padding: 0 10px 30px;">
          
            <img src="http://dev.bebebellamuas.com/artist-client-upload-file/email/logo.png" />
            <!-- <p style="color: #ffffff;font-family: Verdana;font-size: 10px;">111, Land Mark, Street Name, State, Country 90001</p>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #ffffff;font-family: Verdana;font-size: 10px;">
              <tr>
                <td>Ph: 999-999-9999</td>
                <td>Fax: 55-5555</td>
              </tr>
            </table> -->
          </div>
        </td>
        <td valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #333333;font-family: Verdana;font-size: 12px;">
              <tr>
                <td valign="top" style="height: 267px;">
                  <h3 style="font-family: verdana;font-size: 30px;font-weight: normal;text-align: center;">
                  <img src="http://dev.bebebellamuas.com/artist-client-upload-file/email/giftboxes.png" /> 
                   You are receiving a Gift!
                  </h3>
                  <table border="0" cellpadding="5" width="90%" style="margin: auto;">
                    <tr>
                      <td>Name</td>
                      <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">
                          <?php echo isset($name)?$name:''; ?>
                      </div></td>
                    </tr>
                    <tr>
                      <td>Phone</td>
                      <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">
                        <?php echo isset($phone)?$phone:''; ?>
                      </div></td>
                    </tr>
                    <tr>
                      <td>Address</td>
                      <td><div style="min-width: 150px;border-bottom: 1px solid #333333;">
                          <?php echo isset($address)?$address:''; ?>
                      </div></td>
                    </tr>
                    
                  </table>
                  <br>
                  <!-- <p style="text-align: center;">Not Redeemable for Cash</p> -->
                </td>
              </tr>
              <tr><td><div style="background: #D8136B;height: 40px;width: 100%; color: #fff; text-align: center; line-height: 35px;" > © 2016-2017 bebebellamuas.com</div></td></tr>
          </table>
        </td>
      </tr>
    </table>
  </div><!-- container -->
</body>
</html>
 
 

  
