<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sample</title>
</head> 
<body>
  <div style="width: 650px;height: auto;margin: auto;background: #f1f1f1;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td bgcolor="black" style="width: 220px;background: #000000;text-align: center;">
          <div style="padding: 0 10px 30px;">
            <img src="http://bebebellamuas.com/artist-client-upload-file/email/logo.png" />
            <!-- <p style="color: #ffffff;font-family: Verdana;font-size: 10px;">111, Land Mark, Street Name, State, Country 90001</p>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #ffffff;font-family: Verdana;font-size: 10px;">
              <tr>
                <td>Ph: 999-999-9999</td>
                <td>Fax: 55-5555</td>
              </tr>
            </table> -->
          </div>
        </td>
        <td valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #333333;font-family: Verdana;font-size: 12px;">
              <tr>
                <td valign="top" style="height: 267px;">
                  <h4 style="font-family: verdana;font-size: 22px;font-weight: normal;text-align: center;">
                     Your Bébé Bella account password has been changed
                  </h4>
                  <table border="0" cellpadding="5" width="90%" style="margin: auto;">

                  
                  <tr>
                      <p style="padding: 0px 10px;">
                      <strong>Hi <?php echo isset($user_name)?$user_name:'';?></strong>,</p>
                  </tr>

                  <tr>
                      <p style="padding: 0px 10px;">
                      The password for the Bébé Bella account: <strong><?php echo isset($email)?$email:'';?></strong> was recently changed.</p>
                  </tr>

                  <tr>
                    <p style="padding: 0px 10px;">
                      New password <strong><?php echo isset($opt)?$opt:''; ?></strong>.</p>
                  </tr>
                            
                </table>
                  
                  <!-- <p style="text-align: center;">Not Redeemable for Cash</p> -->
                </td>
              </tr>
              <tr><td><div style="background: #D8136B;height: 40px;width: 100%; color: #fff; text-align: center; line-height: 35px;" > © 2016-2017 bebebellamuas.com</div></td></tr>
          </table>
        </td>
      </tr>
    </table>
  </div><!-- container -->
</body>
</html>