<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sample</title>
</head> 
<body>
  <div style="width: 650px;height: auto;margin: auto;background: #f1f1f1;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td bgcolor="black" style="width: 220px;background: #000000;text-align: center;">
          <div style="padding: 0 10px 30px;">
            <img src="http://bebebellamuas.com/artist-client-upload-file/email/logo.png" />
            <!-- <p style="color: #ffffff;font-family: Verdana;font-size: 10px;">111, Land Mark, Street Name, State, Country 90001</p>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #ffffff;font-family: Verdana;font-size: 10px;">
              <tr>
                <td>Ph: 999-999-9999</td>
                <td>Fax: 55-5555</td>
              </tr>
            </table> -->
          </div>
        </td>

  <?php if(isset($type) && $type == 'user'){?>

        <td valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #333333;font-family: Verdana;font-size: 12px;">
              <tr>
                <td valign="top" style="height: auto;">
                  <h4 style="font-family: verdana;font-size: 22px;font-weight: normal;text-align: center;">
                     Bébé Bella Support
                  </h4>
                  <table border="0" cellpadding="5" width="90%" style="margin: auto;">
                
                  <tr>
                    <p style="padding: 0px 0px;">
                    <strong>Name <?php echo isset($name)?$name:'';?></strong>,</p>
                  </tr>

                  <tr>
                    <p style="padding: 0px 0px;">
                    <strong>Email <?php echo isset($email)?$email:'';?></strong>,</p>
                  </tr>

                  <tr>
                    <p style="padding: 0px 0px;">
                    <strong>Phone <?php echo isset($Phone)?$Phone:'';?></strong>,</p>
                  </tr>

                  <tr>
                    <p style="padding: 0px 0px;">
                    <strong>Subject :</strong><?php echo isset($subject)?$subject:'';?>.</p>
                  </tr>

                  <tr>
                    <p style="padding: 0px 0px;">
                    <strong>Message : </strong>  <?php echo isset($message)?$message:'';?></p>
                  </tr>
          </table>
        </td>
        
  <?php } elseif(isset($type) && $type=='admin'){?>

          <td valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #333333;font-family: Verdana;font-size: 12px;">
              <tr>
                <td valign="top" style="height: auto;">
                  <h4 style="font-family: verdana;font-size: 22px;font-weight: normal;text-align: center;">
                    Bébé Bella Support
                  </h4>
                  <table border="0" cellpadding="5" width="90%" style="margin: auto;">
                
                  <tr>
                    <p style="padding: 0px 0px;">
                    <strong>Hi <?php echo isset($name)?$name:'';?></strong></p>
                  </tr>

                  <tr>
                    <p style="padding: 0px 0px;">
                    <strong>Question : <?php echo isset($question)?$question:'';?></strong>,</p>
                  </tr>

                  <tr>
                    <p style="padding: 0px 0px;">
                    <strong>Answer : </strong><?php echo isset($comment)?$comment:'';?>.</p>
                  </tr>
          </table>
        </td>

  <?php } ?>      


              </tr>
              <tr><td><div style="background: #D8136B;height: 40px;width: 100%; color: #fff; text-align: center; line-height: 35px;" > © <?php echo date("Y");?> bebebellamuas.com</div></td></tr>
          </table>
        </td>
      </tr>
    </table>
  </div><!-- container -->
</body>
</html>