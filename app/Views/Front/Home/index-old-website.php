<!DOCTYPE html>
<html>
<head>
<title>Bebebella</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="<?php echo base_url('themes/admin/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Capriola' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url('themes/front/css/style.css'); ?>" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85224664-1', 'auto');
  ga('send', 'pageview');
  
</script>

</head>
<body>

<header class="main__header">
  <div class="container">
    <nav class="navbar navbar-default" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="navbar-header">
        <h1 class="navbar-brand"><a href="index.html">Bebebella Makeup Artist</a></h1>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1,#bs-example-navbar-collapse-2"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>
      <div class="navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="http://bebebellamuas.com">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">contact us</a></li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </nav>
  </div>
</header>
<section class="slider">
  <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">
      <div class="item"> <img data-src="<?php echo base_url('themes/front/images/slider/slider.jpg'); ?>" alt="First slide" src="<?php echo base_url('themes/front/images/slider/slider.jpg');?>">
        <div class="container">
          <div class="carousel-caption">
            <h1>Lorem Ipsum is simply dummy.</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <p><a class="btn btn-default btn-lg" href="#" role="button">demo</a></p>
          </div>
        </div>
      </div>
      <div class="item active"> <img data-src="<?php echo base_url('themes/front/images/slider/slider1.jpg');?>" alt="Second slide" src="<?php echo base_url('themes/front/images/slider/slider1.jpg');?>">
        <div class="container">
          <div class="carousel-caption">
            <h1>Lorem Ipsum is simply dummy.</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p><a class="btn btn-default btn-lg" href="#" role="button">Get Started</a></p>
          </div>
        </div>
      </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon carousel-control-left"></span></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon carousel-control-right"></span></a> </div>
</section>
<!--end of slider section-->
<section class="main__middle__container">
  <div class="row text-center no-margin nothing green__line">
    <div class="container headings">
      <h1 class="page_title">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h1>
      <p class="small-paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      <p><a class="btn btn-info btn-lg" href="#">Buy Now</a><a class="btn btn-info btn-lg" href="#" role="button">Read More</a></p>
    </div>
  </div>
  <div class="row heading__block text-center no-margin nothing">
    <h2>WHAT WE DO</h2>
    <p class="small-paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
  </div>
  <div class="row no-margin grey-info-block text-center">
    <div class="container">
      <h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
      <p class="small-paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
      <div class="col-md-12"> <img src="<?php echo base_url('themes/front/images/content__images/long_img.jpg');?>" alt="img" class="img-rounded img-responsive">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <p><a href="#" class="btn btn-primary">Learn more</a></p>
      </div>
      <div class="col-md-6">
        <h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h3>
        <p class="small-paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry..</p>
        <img src="<?php echo base_url('themes/front/images/content__images/pic1.jpg');?>" alt="pic" class="img-rounded img-responsive">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
      <div class="col-md-6">
        <h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
        <p class="small-paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <img src="<?php echo base_url('themes/front/images/content__images/pic2.jpg');?>" alt="pic" class="img-rounded img-responsive">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </div>
  </div>
  <div class="row heading__block services text-center no-margin nothing">
    <h2>OUR CREATIVE SERVICES</h2>
    <p class="small-paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
  </div>
  <div class="row  three__blocks no_padding no-margin">
    <div class="container">
      <div class="col-md-4">
        <h3><small><a href="#">Lorem Ipsum is simply dummy.</a></small></h3>
        <img src="<?php echo base_url('themes/front/images/content__images/img1.jpg');?>" alt="image" class="img-responsive pull-left img-rounded">
        <div class="clr"></div>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
        <p><a class="btn btn-primary" href="#" role="button">read more</a> 
      </div>
      <div class="col-md-4 middle">
        <h3><small><a href="#">Lorem Ipsum is simply dummy.</a></small></h3>
        <img src="<?php echo base_url('themes/front/images/content__images/img2.jpg');?>" alt="image" class="img-responsive pull-left img-rounded">
        <div class="clr"></div>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <p><a class="btn btn-primary" href="#" role="button">read more</a> 
      </div>
      <div class="col-md-4">
        <h3><small><a href="#">Great performance</a></small></h3>
        <img src="<?php echo base_url('themes/front/images/content__images/img3.jpg');?>" alt="image" class="img-responsive pull-left img-rounded">
        <div class="clr"></div>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <p><a class="btn btn-primary" href="#" role="button">read more</a> 
      </div>
    </div>
  </div>
  <div class="row text-center no-margin nothing green__line testimonials">
    <div class="container headings">
      <h2 class="page_title">TESTIMONIALS</h2>
      <p class="small-paragraph"><small>Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</small></p>
      <div id="myCarousel2" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel2" data-slide-to="1"></li>
          <li data-target="#myCarousel2" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="item active">
            <p class="small-paragraph">"Quisque tristique sem ut lacus pulvinar sodales. Praesent ultricies sed suscipit tincidunt. Pellentesque eros dui, interdum vel elit vel. Nunc semper mauris sed eros accumsan, quis viverra. Cras sed leo non sem dictum."</p>
            <p class="small-paragraph"><small>John Smith, Developer</small></p>
          </div>
          <div class="item">
            <p class="small-paragraph">"Donec non faucibus quam. Proin sagittis lectus vitae dapibus pellentesque. Morbi at magna pellentesque, vulputate nisl nec. Donec non faucibus quam. Integer non venenatis nunc."</p>
            <p class="small-paragraph"><small>Daniel Stone, Manager</small></p>
          </div>
          <div class="item">
            <p class="small-paragraph">"Cras sed leo non sem dictum vulputate nec a nisi. Aliquam erat volutpat. Vivamus facilisis purus eu cursus dictum. Aliquam rutrum est sed purus elementum. Nunc condimentum est in nisi dictum."</p>
            <p class="small-paragraph"><small>Alison Parks, Sales</small></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h3>About</h3>
        <p>We strive to deliver a level of service that exceeds the expectations of our customers. <br />
          <br />
          If you have any questions about our products or services, please do not hesitate to contact us. We have friendly, knowledgeable representatives available seven days a week to assist you.</p>
      </div>
      <div class="col-md-3">
        <h3>Tweets</h3>
        <p><span>Tweet</span> <a href="#">@You</a><br />
          Etiam egestas, ipsum posuere accumsan sollicitudin, nulla mauris volutpat sem, sit amet rutrum risus. </p>
        <p><span>Tweet</span> <a href="#">@You</a><br />
          Quisque porta tellus vitae adipiscing molestie. Mauris et lacus blandit, malesuada.</p>
      </div>
      <div class="col-md-3">
        <h3>Mailing list</h3>
        <p>Subscribe to our mailing list for offers, news updates and more!</p>
        <br />
        <form action="#" method="post" id="signup-form" class="form-inline" role="form">
          <div class="form-group">
            <label class="sr-only" for="exampleInputEmail2">your email:</label>
            <input type="email" class="form-control form-control-lg" name="email" id="exampleInputEmail2" placeholder="your email:">
          </div>
          <button type="submit" id="thisbutton" class="btn btn-primary" onClick="_gaq.push(['_trackEvent', 'subscribe', 'email-me-for-subscribe', 'email for subscribe us', 5, true]);" >subscribe</button>
        </form>
      </div>
      <div class="col-md-3">
        <h3>Social</h3>
        <p> </p>
        <div class="social__icons"> <a href="#" class="socialicon socialicon-twitter"></a> <a href="#" class="socialicon socialicon-facebook"></a> <a href="#" class="socialicon socialicon-google"></a> </div>
      </div>
    </div>
    <hr>
    <p class="text-center">&copy; Copyright Bebebellamus 2016 . All Rights Reserved.</p>
  </div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="<?php echo base_url('themes/front/js/jquery.min.js');?>"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('themes/front/js/bootstrap.min.js');?>"></script>
<script type="text/javascript">



$('.carousel').carousel({

  interval: 3500, // in milliseconds

  pause: 'none' // set to 'true' to pause slider on mouse hover

})

</script>
</body>
</html>
