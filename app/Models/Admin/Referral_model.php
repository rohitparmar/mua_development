<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Referral_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }

    /**
     * Creating new referral
     * @param $param
     * @return null
     */
    public function add_referral($param) {
        $sql = "INSERT INTO referral (r_name,r_rebate,r_created_by,r_created_date,r_start_date,r_end_date,r_code_type) VALUES(:r_name,:r_rebate,:r_created_by,:r_created_date,:r_start_date,:r_end_date,1)";

        $stmt = $this->conn->prepare($sql);

        // $stmt->bindParam(':r_usertype',$param['user_type']);
        $stmt->bindParam(':r_name',$param['celebrity_name']);
        $stmt->bindParam(':r_rebate',$param['rebate_price']);
        
        $created_by = 'admin';
        $stmt->bindParam(':r_created_by',$created_by);
        $created_date = date('Y-m-d',time());
        $stmt->bindParam(':r_created_date',$created_date);
        $stmt->bindParam(":r_start_date",$param['start_date']);
        $stmt->bindParam(":r_end_date",$param['end_date']);

        $result = $stmt->execute();
        $stmt->closeCursor();

        //$result = $this->conn->lastInsertId();

        $last_id = '';
        if($result){

            $last_id = $this->conn->lastInsertId();
            //$refrral_code = str_replace(' ', '', $param['celebrity_name']).'-'.$last_id.rand(1000,9999);

            //$refrral_code = create_referral_code($param['celebrity_name'], $last_id);
			$refrral_code = str_replace(' ','', preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $param['celebrity_name']));
            $sql = "UPDATE referral SET r_code = :r_code WHERE r_id = :r_id";
            $stmt = $this->conn->prepare($sql);

            $stmt->bindParam(':r_code',$refrral_code);
            $stmt->bindParam(':r_id',$last_id);

            $stmt->execute();
            $stmt->closeCursor();
        }
    }

    /**
    * Get referral list
    * @param $param
    * @return $result
    */
    public function get_referral_list($param){
        $page = isset($param['page'])?$param['page']:0;

        $sql = "SELECT *, (SELECT count(ur_id) FROM used_referral WHERE ur_r_id = r_id LIMIT 1) AS count FROM referral WHERE r_fk_u_id = 0 ";

        if(isset($param['search']) && $param['search']){
            $sql .= " AND (r_name LIKE :search
                    OR 
                    r_rebate LIKE :search 
                    OR r_duration LIKE :search
                    OR r_code LIKE :search
                    )";
        }

        $sql .= " ORDER BY `r_id` DESC LIMIT $page , 10";

        $stmt = $this->conn->prepare($sql);
        if(isset($param['search']) && $param['search']){
            $search = '%'.$param['search'].'%';
            $stmt->bindParam(':search',$search);
        }
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }


    public function get_celebrity_used_referrel($id){
        $sql = "SELECT *,
        (SELECT u_email FROM user WHERE u_id = ur_fk_u_id LIMIT 1) AS u_email,
        (SELECT cr_id FROM client_request WHERE cr_id = ur_services_id AND cr_status = 'finished' LIMIT 1) AS booking

         FROM used_referral WHERE ur_r_id = '".$id."'";
        
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
    * Get referral count
    * @param $param
    * @return $result
    */
    public function get_referral_count($param){

        $page = isset($param['page'])?$param['page']:0;

        $sql = "SELECT * FROM referral WHERE r_fk_u_id = 0 ";

        if(isset($param['search']) && $param['search']){
            $sql .= " AND (r_name LIKE :search
                    OR 
                    r_rebate LIKE :search 
                    OR r_duration LIKE :search
                    OR r_code LIKE :search
                    )";
        }

        //$sql .= " ORDER BY `r_id` DESC LIMIT $page , 10";

        $stmt = $this->conn->prepare($sql);
        if(isset($param['search']) && $param['search']){
            $search = '%'.$param['search'].'%';
            $stmt->bindParam(':search',$search);
        }
        $stmt->execute();
        $result = $stmt->rowCount();
        $stmt->closeCursor();
        return $result;
    }

    /**
    * Get referral list
    * @param $param
    * @return $result
    */
    public function get_user_referral_list($param){
        $page = isset($param['page'])?$param['page']:0;

        /*$sql = "SELECT *,
        
               (SELECT COUNT(*) FROM used_referral WHERE ur_r_id  = (SELECT r_id FROM referral WHERE r_fk_u_id = user.u_id LIMIT 1) LIMIT 1) AS used_referral,
                
                (SELECT SUM(IF(u_type='mua',(SELECT 1 FROM client_request WHERE cr_fk_u_mua_id = uu.u_id AND cr_status = 'finished' LIMIT 1),(SELECT 1 FROM client_request WHERE cr_fk_u_client_id = uu.u_id AND cr_status = 'finished' LIMIT 1))) AS earned
                FROM user AS uu WHERE uu.u_id IN (SELECT ur_fk_u_id FROM used_referral WHERE ur_r_id  = (SELECT r_id FROM referral WHERE r_fk_u_id = user.u_id LIMIT 1))) AS earned_referral,

                (SELECT r_id FROM referral WHERE r_fk_u_id = user.u_id LIMIT 1) AS r_id


                FROM user
                INNER JOIN referral
                r_fk_u_id = user.u_id";*/

        $sql = "SELECT *,
                (SELECT COUNT(*) FROM used_referral WHERE ur_r_id  = (SELECT r_id FROM referral WHERE r_fk_u_id = user.u_id LIMIT 1) LIMIT 1) AS used_referral,
                
                (SELECT SUM(IF(u_type='mua',(SELECT 1 FROM client_request WHERE cr_fk_u_mua_id = uu.u_id AND cr_status = 'finished' LIMIT 1),(SELECT 1 FROM client_request WHERE cr_fk_u_client_id = uu.u_id AND cr_status = 'finished' LIMIT 1))) AS earned
                FROM user AS uu WHERE uu.u_id IN (SELECT ur_fk_u_id FROM used_referral WHERE ur_r_id  = (SELECT r_id FROM referral WHERE r_fk_u_id = user.u_id LIMIT 1))) AS earned_referral,

                (SELECT COUNT(*) FROM used_referral WHERE ur_fk_u_id  = user.u_id AND ur_celebrity_code = 'true' LIMIT 1) AS celebrity_referral
        
                FROM user
                INNER JOIN referral
                ON
                r_fk_u_id = user.u_id";

        if(isset($param['search']) && $param['search']){
            $sql .= " AND (r_name LIKE :search
                    OR 
                    r_rebate LIKE :search 
                    OR r_duration LIKE :search
                    OR r_code LIKE :search
                    )";
        }

        $sql .= " ORDER BY `r_id` DESC LIMIT $page , 10";

        $stmt = $this->conn->prepare($sql);
        if(isset($param['search']) && $param['search']){
            $search = '%'.$param['search'].'%';
            $stmt->bindParam(':search',$search);
        }
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
    * Get referral count
    * @param $param
    * @return $result
    */
    public function get_user_referral_count($param){

        $page = isset($param['page'])?$param['page']:0;

        $sql = "SELECT * FROM referral WHERE r_fk_u_id <> 0 ";

        if(isset($param['search']) && $param['search']){
            $sql .= " AND (r_name LIKE :search
                    OR 
                    r_rebate LIKE :search 
                    OR r_duration LIKE :search
                    OR r_code LIKE :search
                    )";
        }

        //$sql .= " ORDER BY `r_id` DESC LIMIT $page , 10";

        $stmt = $this->conn->prepare($sql);
        if(isset($param['search']) && $param['search']){
            $search = '%'.$param['search'].'%';
            $stmt->bindParam(':search',$search);
        }
        $stmt->execute();
        $result = $stmt->rowCount();
        $stmt->closeCursor();
        return $result;
    }

    /**
    * Get referral 
    * @param $id
    * @return $result
    */
    public function get_referral($id){
        $sql = "SELECT * FROM referral WHERE r_id = :r_id";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':r_id',$id);

        $stmt->execute();

        $result = $stmt->fetch();

        $stmt->closeCursor();

        return $result;
    }

    /**
    * Change referral status
    * @param $param
    * @return $result
    */
    public function change_referral_status($param){
        $sql = "UPDATE referral SET r_status = :r_status WHERE r_id = :r_id";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':r_status',$param['status']);

        $stmt->bindParam(':r_id',$param['id']);

        $stmt->execute();
        
        $stmt->closeCursor();
    }

    /**
     * Update referral
     * @param $param
     * @return null
     */
    public function update_referral($param) {
        $sql = "UPDATE referral SET r_name = :r_name,r_rebate = :r_rebate,r_start_date = :r_start_date,r_end_date = :r_end_date WHERE r_id = :r_id";

        $stmt = $this->conn->prepare($sql);

        // $stmt->bindParam(':r_usertype',$param['user_type']);
        $stmt->bindParam(':r_name',$param['celebrity_name']);
        $stmt->bindParam(':r_rebate',$param['rebate_price']);
        //$stmt->bindParam(':r_duration',$param['time_duration']);
        $stmt->bindParam(':r_id',$param['id']);
        $stmt->bindParam(':r_start_date',$param['start_date']);
        $stmt->bindParam(':r_end_date',$param['end_date']);

        $result = $stmt->execute();
        $stmt->closeCursor();

        //$result = $this->conn->lastInsertId();
    }
}