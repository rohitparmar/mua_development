<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Payment_info_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }

    /**
    * Get payment info list
    * @param $param
    * @param $result
    */
    public function get_payment_info_list($param){
        $sql = "SELECT * FROM ( SELECT *,(SELECT u_name FROM user WHERE u_id = uai_fk_u_id LIMIT 1) AS name FROM user_account_info ) AS payment ";

        if(isset($param['search']) && $param['search']){
            
            $sql .= " WHERE 
                    payment.name LIKE :search 
                    OR uai_account_no LIKE :search
                    OR uai_rounting_no LIKE :search
                    OR uai_account_name LIKE :search
                    OR uai_paypal_id LIKE :search
                    OR uai_payment_preference LIKE :search
                    OR uai_created LIKE :search
                    ";
        }

        $page = isset($param['page'])?$param['page']:0;

        $sql .= " ORDER BY `uai_id` DESC LIMIT $page , 10";

        $stmt = $this->conn->prepare($sql);

        if(isset($param['search']) && $param['search']){
            $search = "%".$param['search']."%";

            $stmt->bindParam(':search',$search);
        }

        $stmt->execute();

        $result = $stmt->fetchAll();

        $stmt->closeCursor();

        return $result;
    }

    /**
    * Get payment info count
    * @param $param
    * @param $result
    */
    public function get_payment_info_count($param){
        $sql = "SELECT * FROM ( SELECT *,(SELECT u_name FROM user WHERE u_id = uai_fk_u_id LIMIT 1) AS name FROM user_account_info ) AS payment ";

        if(isset($param['search']) && $param['search']){
            
            $sql .= " WHERE 
                    payment.name LIKE :search 
                    OR uai_account_no LIKE :search
                    OR uai_rounting_no LIKE :search
                    OR uai_account_name LIKE :search
                    OR uai_paypal_id LIKE :search
                    OR uai_payment_preference LIKE :search
                    OR uai_created LIKE :search
                    ";
        }

        //$page = isset($param['page'])?$param['page']:0;
        
        //$sql .= " ORDER BY `u_id` DESC LIMIT $page , 10";

        $stmt = $this->conn->prepare($sql);

        if(isset($param['search']) && $param['search']){
            $search = "%".$param['search']."%";

            $stmt->bindParam(':search',$search);
        }

        $stmt->execute();

        $result = $stmt->rowCount();

        $stmt->closeCursor();

        return $result;
    }

    /**
    * Get payment info
    * @param $param
    * @param $result
    */
    public function get_payment_info($param){
        $sql = "SELECT * FROM user_account_info WHERE uai_id = :uai_id";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':uai_id',$param['id']);

        $stmt->execute();

        $result = $stmt->fetch();

        $stmt->closeCursor();

        return $result;
    }

    /**
    * Update payment info
    * @param $param
    * @param null
    */
    public function update_payment_info($param){

        $sql = "UPDATE user_account_info SET uai_account_no = :uai_account_no, uai_rounting_no = :uai_rounting_no, uai_account_name = :uai_account_name, uai_paypal_id = :uai_paypal_id, uai_payment_preference = :uai_payment_preference WHERE uai_id = :uai_id";
        
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':uai_account_no',$param['account_no']);
        $stmt->bindParam(':uai_rounting_no',$param['rounting_no']);
        $stmt->bindParam(':uai_account_name',$param['account_name']);
        $stmt->bindParam(':uai_paypal_id',$param['paypal_id']);
        $stmt->bindParam(':uai_payment_preference',$param['payment_preference']);
        $stmt->bindParam(':uai_id',$param['id']);

        $stmt->execute();

        $stmt->closeCursor();
    }

    /**
    * Pay to user
    * @param $param
    * @param null
    */
    public function pay_to_user($param){
        $user = $this->get_user_by_payment($param['id']);

        $sql = "INSERT INTO admin_payment_history_to_user (aph_fk_u_id , aph_user_type,
                aph_payment_type, aph_payment_via, aph_payment_amount, aph_payment_date, aph_payment_comment) VALUES 
                (:aph_fk_u_id , :aph_user_type,
                :aph_payment_type, :aph_payment_via, :aph_payment_amount, :aph_payment_date, :aph_payment_comment)";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(":aph_fk_u_id",$user['u_id']);
        $stmt->bindParam(":aph_user_type",$user['u_type']);
        $stmt->bindParam(":aph_payment_type",$param['payment_type']);
        $stmt->bindParam(":aph_payment_via",$param['payment_via']);
        $stmt->bindParam(":aph_payment_amount",$param['payment_amount']);

        $payment_date = date('Y-m-d',strtotime($param['payment_date']));

        $stmt->bindParam(":aph_payment_date",$payment_date);

        $stmt->bindParam(":aph_payment_comment",$param['payment_comment']);

        $stmt->execute();

        $stmt->closeCursor();
    }

    /**
    * Get user
    * @param $id
    * @param null
    */
    protected function get_user_by_payment($id){
        $sql = 'SELECT u_id,u_type FROM user WHERE u_id = (SELECT uai_fk_u_id FROM user_account_info WHERE uai_id = :uai_id LIMIT 1)';
        
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':uai_id',$id);

        $stmt->execute();

        $result = $stmt->fetch();

        $stmt->closeCursor();

        return $result;
    }
}