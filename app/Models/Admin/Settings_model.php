<?php

class Settings_model {

    protected $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }
    
    function upate_skintone_status_ajax($id){
        $sql = "UPDATE master_skintone SET ms_status = IF(ms_status='true','false','true') WHERE ms_id=".$id;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        return 'true';
    }

    function upate_lipscolor_status_ajax($id){
        $sql = "UPDATE master_lips_color SET mlc_status = IF(mlc_status='true','false','true') WHERE mlc_id=".$id;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        return 'true';
    }

    function upate_eyeshadow_status_ajax($id){
        $sql = "UPDATE master_eyeshadow SET me_status = IF(me_status='true','false','true') WHERE me_id=".$id;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        return 'true';
    }
    
    function get_skintone_list(){
        $sql = "SELECT * FROM master_skintone";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * Get commision
     * @param null
     * @return $result
     */
    public function get_settings() {
        $sql = "SELECT * FROM settings";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * Update commision
     * @param $param
     * @return $result
     */
    public function update_settings($param) {
        $sql = "UPDATE settings SET s_value = '" . $param['value'] . "', s_status = '" . $param['status'] . "', s_updated = NOW() WHERE s_types = '" . $param['type'] . "'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }

    /**
     * Get plans
     * @param null
     * @return $result
     */
    public function get_plans() {
        $sql = "SELECT * FROM `artist_subscription_plan` WHERE `asp_deleted` = 'false' ORDER BY asp_id ASC";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * Insert plan
     * @param $param
     * @return $result
     */
    public function insert_plan($param) {
        $sql = "INSERT INTO artist_subscription_plan (asp_name,asp_duration,asp_description,asp_amount,asp_status,asp_created) VALUES (:asp_name,:asp_duration,:asp_description,:asp_amount,:asp_status,:asp_created)";
        $stmt = $this->conn->prepare($sql);

        $date = date('Y-m-d', time());

        $stmt->bindParam(':asp_name', $param['name']);
        $stmt->bindParam(':asp_duration', $param['duration']);
        $stmt->bindParam(':asp_description', $param['description']);
        $stmt->bindParam(':asp_amount', $param['amount']);
        $stmt->bindParam(':asp_status', $param['status']);
        $stmt->bindParam(':asp_created', $date);
        $stmt->execute();
        $stmt->closeCursor();
        $result = $this->conn->lastInsertId();
        return $result;
    }

    /**
     * Update image
     * @param $param
     * @return null
     */
    public function update_image($param) {
        $sql = "UPDATE artist_subscription_plan SET asp_image = :asp_image WHERE asp_id = :asp_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':asp_image', $param['image_name']);
        $stmt->bindParam(':asp_id', $param['id']);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }

    /**
     * Change plan status
     * @param $param
     * @return null
     */
    public function change_plan_status($param) {
        $sql = "UPDATE artist_subscription_plan SET asp_status = :asp_status WHERE asp_id = :asp_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':asp_status', $param['status']);
        $stmt->bindParam(':asp_id', $param['id']);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }

    /**
     * Delete plan
     * @param $param
     * @return null
     */
    public function delete_plan($param) {
        $sql = "UPDATE artist_subscription_plan SET asp_deleted = 'true' WHERE asp_id = :asp_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':asp_id', $param['id']);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }

    /**
     * Delete plan
     * @param $param
     * @return null
     */
    public function get_plan_by_id($param) {
        $sql = "SELECT * FROM artist_subscription_plan WHERE asp_id = :asp_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':asp_id', $param['id']);
        $stmt->execute();
        $result = $stmt->fetch();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * Update plan
     * @param $param
     * @return null
     */
    public function update_plan($param) {

        $sql = "UPDATE artist_subscription_plan SET 
                                                    asp_name            = :asp_name,
                                                    asp_name_spn        = :asp_name_spn,
                                                    asp_duration        = :asp_duration ,
                                                    asp_description     = :asp_description,
                                                    asp_description_spn = :asp_description_spn,
                                                    asp_amount          = :asp_amount,
                                                    asp_status          = :asp_status,
                                                    asp_orderby         = :asp_orderby  
                                                    WHERE asp_id = :asp_id";
        $stmt = $this->conn->prepare($sql);

        $date = date('Y-m-d', time());

        $stmt->bindParam(':asp_name', $param['name']);
        $stmt->bindParam(':asp_name_spn', $param['name_spn']);
        $stmt->bindParam(':asp_duration', $param['duration']);
        $stmt->bindParam(':asp_description', $param['description']);
        $stmt->bindParam(':asp_description_spn', $param['description_spn']);
        $stmt->bindParam(':asp_amount', $param['amount']);
        $stmt->bindParam(':asp_status', $param['status']);
        $stmt->bindParam(':asp_orderby', $param['new_value']);
        $stmt->bindParam(':asp_id', $param['old_id']);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }

    public function update_plan_orderby($param) {
        $sql = "UPDATE artist_subscription_plan SET asp_orderby = :asp_orderby WHERE asp_id = :asp_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':asp_orderby', $param['old_value']);
        $stmt->bindParam(':asp_id', $param['new_id']);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }

    public function get_all_where($id){
        $sql = "SELECT * FROM artist_subscription_plan WHERE asp_orderby = :asp_orderby";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':asp_orderby', $id);
        $stmt->execute();
        $result = $stmt->fetch();
        $stmt->closeCursor();
        return $result;
    }

    public function notification(){
        $sql = "SELECT * FROM notification WHERE n_status = 'true'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }


    public function update_notification($id, $title, $message, $title_spn, $message_spn) {
        

       $sql = "UPDATE notification SET n_title_eng = :n_title_eng, n_message_eng = :n_message_eng, n_title_spn = :n_title_spn, n_message_spn = :n_message_spn WHERE n_id = :n_id";
        
     //  $sql = "UPDATE notification SET n_title_eng = '".title."', n_message_eng = '".message."', n_title_spn = '".title_spn."', n_message_spn = '".message_spn."' WHERE n_id = '".id."'";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':n_id', $id);
        $stmt->bindParam(':n_title_eng', $title);
        $stmt->bindParam(':n_message_eng', $message);
        $stmt->bindParam(':n_title_spn', $title_spn);
        $stmt->bindParam(':n_message_spn', $message_spn);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }


    public function get_skintone($id){

        $sql = "SELECT * FROM master_skintone WHERE ms_id = '".$id."'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $stmt->closeCursor();
        return $result;
    }

    public function edit_skinton($id, $name, $type){

        if(isset($type) && $type=='skintone'){
        
            $sql = "UPDATE master_skintone SET ms_name = :ms_name WHERE ms_id = :ms_id";
        
        }elseif(isset($type) && $type=='lipscolor'){
        
            $sql = "UPDATE master_lips_color SET mlc_name = :ms_name WHERE mlc_id = :ms_id";   
        
        }elseif(isset($type) && $type=='eyeshadow'){
            
            $sql = "UPDATE master_eyeshadow SET me_name = :ms_name WHERE me_id = :ms_id";
        }

        
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ms_id', $id);
        $stmt->bindParam(':ms_name', $name);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }


    public function get_lipscolor($id=''){
        if(isset($id) && $id){
            $sql = "SELECT * FROM master_lips_color WHERE mlc_id = :mlc_id";                
        }else{
            $sql = "SELECT * FROM master_lips_color WHERE mlc_status = 'true'";
        }
        
        $stmt = $this->conn->prepare($sql);
        if(isset($id) && $id){
            $stmt->bindParam(':mlc_id', $id);    
        }
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }


    public function get_eyeshadow($id=''){
        if(isset($id) && $id){
            $sql = "SELECT * FROM master_eyeshadow WHERE me_id = :me_id";
        }else{
            $sql = "SELECT * FROM master_eyeshadow WHERE me_status = 'true'";    
        }
        $stmt = $this->conn->prepare($sql);
        if(isset($id) && $id){
            $stmt->bindParam(':me_id', $id);    
        }
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }


    public function user_email_list($param){
        $page = isset($param['page'])?$param['page']:0;
        $sql = "SELECT *,

        IF(user.u_name='', (
            IF(user.u_type='mua', (SELECT ap_profile_name FROM artist_profile WHERE artist_profile.ap_fk_u_id = user.u_id),'')), user.u_name) AS u_name

         FROM user_mail
                INNER JOIN user
                ON user.u_id = user_mail.um_fk_u_id";    
        $sql .= " ORDER BY `um_id` DESC LIMIT $page , 10";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }


   /**
    * Get user_email_list_count
    * @param null
    * @return $result
    */
    public function user_email_list_count($param){

        $sql = "SELECT * FROM user_mail";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->rowCount();
        $stmt->closeCursor();
        return $result;
    }


    
    public function email_cooment_model($id){
        $sql = "SELECT * FROM user_mail WHERE um_id = :um_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':um_id', $id);    
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    public function update_email_cooment($param){
        
        $sql = "UPDATE user_mail SET um_comment = :um_comment, um_admin_status = :um_admin_status WHERE um_id = :um_id;
        INSERT INTO user_mail_admin (uma_fk_um_id, uma_message, uma_admin_status) VALUES (:um_id, :um_comment, :um_admin_status);";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':um_comment', $param['js_comment']);
        $stmt->bindParam(':um_admin_status', $param['js_status']);
        $stmt->bindParam(':um_id', $param['js_id']);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }

    public function get_data_by_id($id){
        $sql = "SELECT * FROM user WHERE u_id = ".$id."";
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if($stmt->execute()) {
           $result = $stmt->fetch();
        }
        $stmt->closeCursor();
        return $result;
    }

    public function get_admin_email_data($id){
        $sql = "SELECT * FROM user_mail_admin WHERE uma_fk_um_id = ".$id." ORDER BY uma_id DESC";
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if($stmt->execute()) {
           $result = $stmt->fetchAll();
        }
        $stmt->closeCursor();
        return $result;
    }

}