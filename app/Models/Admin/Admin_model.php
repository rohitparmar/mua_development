<?php
class Admin_model{

	protected $conn;

	function __construct($app){
		$this->conn = $app->get('db');
	}

	public function get_user($param){
		$stmt = $this->conn->prepare("SELECT * FROM `admin` WHERE `ad_email` = :email and `ad_password` = :password");
		$stmt->bindParam(':email',$param['email']);
		$stmt->bindParam(':password',$param['pass']);
		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

}