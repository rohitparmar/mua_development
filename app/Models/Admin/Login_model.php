<?php
class Login_model{

	protected $conn;

	function __construct($app){
		$this->conn = $app->get('db');
	}

	/**
	* Get user 
	* @param $param
	* @return $result
	*/
	public function get_user($param){
		$stmt = $this->conn->prepare("SELECT * FROM `admin` WHERE `ad_email` = :email and `ad_password` = :password");
		$stmt->bindParam(':email',$param['email']);
		$stmt->bindParam(':password',$param['pass']);
		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Check email
	* @param $param
	* @return $result
	*/
	public function check_admin($param){
		$sql = "SELECT ad_email FROM admin WHERE ad_email = :ad_email";
		$stmt = $this->conn->prepare($sql);

		$stmt->bindParam(':ad_email',$param['email']);

		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Update password
	* @param $param
	* @return null
	*/
	public function update_password($param){
		$sql = "UPDATE admin SET ad_password = :ad_password WHERE ad_email = :ad_email";
		$stmt = $this->conn->prepare($sql);

		$new_pass = md5($param['new_pass']);
		$stmt->bindParam(':ad_email',$param['email']);
		$stmt->bindParam(':ad_password',$new_pass);

		$stmt->execute();
		$stmt->closeCursor();
		return;
	}
}