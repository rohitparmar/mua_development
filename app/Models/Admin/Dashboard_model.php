<?php
class Dashboard_model{

	protected $conn;

	function __construct($app){
		$this->conn = $app->get('db');
	}

	/**
	* Validate passwrod
	* @param $param
	* @return $result
	*/
	public function validate_password($param){
		$sql = "SELECT ad_id FROM admin WHERE ad_id = :ad_id AND ad_password = :ad_password";
		$stmt = $this->conn->prepare($sql);

		$pass = md5($param['old_pass']);
		$id = $_SESSION['admin_info']['id'];

		$stmt->bindParam(':ad_password',$pass);
		$stmt->bindParam(':ad_id',$id);

		$stmt->execute();
		$result = $stmt->rowCount();
		$stmt->closeCursor();

		return $result;
	}

	/**
	* Update password
	* @param $param
	* @return $result
	*/
	public function update_password($param){
		$sql = "UPDATE `admin` SET ad_password = :ad_password WHERE ad_id = :ad_id and `ad_password` = :old_pass";
		$stmt = $this->conn->prepare($sql);

		$new_pass = md5($param['new_pass']);
		$old_pass = md5($param['old_pass']);
		$id = $_SESSION['admin_info']['id'];

		$stmt->bindParam(':ad_password',$new_pass);
		$stmt->bindParam(':ad_id',$id);
		$stmt->bindParam(':old_pass',$old_pass);

		$result = $stmt->execute();
		$stmt->closeCursor();

		return $result;
	}

	/**
	* Get profile
	* @param $param
	* @return $result
	*/
	public function get_profile($param){
		$sql = "SELECT * FROM admin WHERE ad_id = :ad_id";
		$stmt = $this->conn->prepare($sql);

		$stmt->bindParam(":ad_id",$param['id']);

		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Update admin profile
	* @param $param
	* @return null
	*/
	public function update_admin_profile($param){
		$sql = "UPDATE admin SET ad_name = :ad_name , ad_email = :ad_email , ad_phone = :ad_phone WHERE ad_id = :ad_id";
		$stmt = $this->conn->prepare($sql);

		$stmt->bindParam(":ad_name",$param['name']);
		$stmt->bindParam(":ad_email",$param['email']);
		$stmt->bindParam(":ad_phone",$param['phone']);
		$stmt->bindParam(":ad_id",$param['id']);

		$stmt->execute();
		$stmt->closeCursor();
		return;
	}

	/**
	* Check email exites
	* @param $param
	* @return $result
	*/
	public function check_email_exites($param){
		$sql = "SELECT * FROM admin WHERE ad_email = :ad_email AND ad_id <> :ad_id";
		$stmt = $this->conn->prepare($sql);

		$stmt->bindParam(":ad_id",$param['id']);
		$stmt->bindParam(":ad_email",$param['email']);

		$stmt->execute();
		$result = $stmt->rowCount();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Update admin profile image
	* @param $param
	* @return null
	*/
	public function update_admin_profile_image($param){
		$sql = "UPDATE admin SET ad_image = :ad_image WHERE ad_id = :ad_id";
		$stmt = $this->conn->prepare($sql);

		$stmt->bindParam(":ad_image",$param['image']);
		$stmt->bindParam(":ad_id",$param['id']);

		$stmt->execute();
		$stmt->closeCursor();
		return;
	}

	/**
	* Get artist count
	* @param null
	* @return $result
	*/
	public function get_artist_count(){
		$sql = "SELECT count(*) AS count FROM user WHERE u_type = 'mua' AND u_varified = 'accepted'";
		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get client count
	* @param null
	* @return $result
	*/
	public function get_client_count(){
		$sql = "SELECT count(*) AS count FROM user WHERE u_type = 'client'";
		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get artist request count
	* @param null
	* @return $result
	*/
	public function get_artist_request_count(){
		$sql = "SELECT count(*) AS count FROM user WHERE u_type = 'mua' AND (u_varified = 'new' OR u_varified = 'on-hold')";
		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get artist payment count
	* @param null
	* @return $result
	*/
	public function get_artist_payment_count(){
		$sql = "SELECT count(*) AS count FROM artist_payment_information";
		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get artist payment count
	* @param null
	* @return $result
	*/
	public function get_booking_count(){
		$sql = "SELECT count(*) AS count FROM client_artist_booking";
		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get recent 5 artist
	* @param null
	* @return $result
	*/
	public function get_recent_5_artist(){
		$sql = "SELECT u_id, u_email,u_status,
				(SELECT ap_profile_name FROM artist_profile WHERE ap_fk_u_id = u_id LIMIT 1) AS name
				FROM user WHERE u_type = 'mua' ORDER BY u_id DESC LIMIT 5";
		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get recent 5 client
	* @param null
	* @return $result
	*/
	public function get_recent_5_client(){
		$sql = "SELECT u_id, u_email,u_name,u_status
				FROM user WHERE u_type = 'client' ORDER BY u_id DESC LIMIT 5";
		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}
}