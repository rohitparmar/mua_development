<?php
class Service_model{

	protected $conn;

	function __construct($app){
		$this->conn = $app->get('db');
	}

	/**
	* Get service list
	* @param null
	* @return $result
	*/
	public function get_service_list($param){
		$page = isset($param['page'])?$param['page']:0;

		$sql = "SELECT * FROM (SELECT 
				cab_id,cr_status,
				(SELECT u_name FROM user WHERE u_id = cr_fk_u_mua_id LIMIT 1) AS artist_name,
				(SELECT u_name FROM user WHERE u_id = cr_fk_u_client_id LIMIT 1) AS client_name,
				(SELECT cm_name FROM category_master WHERE cm_id = (SELECT ac_fk_cm_id FROM artist_category WHERE ac_id = cr_fk_ac_id LIMIT 1) LIMIT 1) AS category_name
				FROM client_artist_booking JOIN client_request ON cr_id = cab_fk_cr_id) AS booking";

		if(isset($param['search']) && $param['search']){
			$sql .= "WHERE (artist_name LIKE '%".$param['search']."%'
					OR 
					client_name LIKE '%".$param['search']."%'
					)";
		}

		$sql .= " ORDER BY `cab_id` DESC LIMIT $page , 10";

		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get service list
	* @param null
	* @return $result
	*/
	public function get_service_count($param){

		$sql = "SELECT count(*) AS count FROM (SELECT 
				cab_id,cr_status,
				(SELECT u_name FROM user WHERE u_id = cr_fk_u_mua_id LIMIT 1) AS artist_name,
				(SELECT u_name FROM user WHERE u_id = cr_fk_u_client_id LIMIT 1) AS client_name,
				(SELECT cm_name FROM category_master WHERE cm_id = (SELECT ac_fk_cm_id FROM artist_category WHERE ac_id = cr_fk_ac_id LIMIT 1) LIMIT 1) AS category_name
				FROM client_artist_booking JOIN client_request ON cr_id = cab_fk_cr_id) AS booking";

		if(isset($param['search']) && $param['search']){
			$sql .= "WHERE (artist_name LIKE '%".$param['search']."%'
					OR 
					client_name LIKE '%".$param['search']."%'
					)";
		}

		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}
}