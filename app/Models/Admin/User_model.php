<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class User_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($name, $email, $password, $phone, $fb_id, $user_type, $language, $fb_login, $gcm_id) {

        $response = array();

        if (!$this->isUserExists($email)) {

            $stmt = $this->conn->prepare("INSERT INTO user(u_name, u_email, u_password, u_phone, u_fb_id, u_type, u_lang, u_fb_login, u_gcm_id) values(?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $md5_password = md5($password);

            $stmt->bindParam(1,$name);
            $stmt->bindParam(2,$email);
            $stmt->bindParam(3,$md5_password);
            $stmt->bindParam(4,$phone);
            $stmt->bindParam(5,$fb_id);
            $stmt->bindParam(6,$user_type);
            $stmt->bindParam(7,$language);
            $stmt->bindParam(8,$fb_login);
            $stmt->bindParam(9,$gcm_id);

            $result = $stmt->execute();

            $id = "";

            if($result){
                $id = $this->conn->lastInsertId();
            }

            $stmt->closeCursor();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                //return USER_CREATED_SUCCESSFULLY;
                return array('res' => 1, 'id' => $id);
            } else {
                // Failed to create user
                //return USER_CREATE_FAILED;
                return array('res' => 0, 'id' => '');
            }
        } else {
            // User with same email already existed in the db
            //return USER_ALREADY_EXISTED;
            return array('res' => 2, 'id' => '');
        }

        return $response;
    }

    /**
     * Checking user email
     * @param String $email
     */
    public function getUserEmail($email)  {
        //print_r($email); die;
        $stmt = $this->conn->prepare("SELECT u_id, u_name, u_email, u_phone, u_fb_id, u_type, u_lang, u_fb_login, u_status, u_gcm_id FROM user WHERE u_email = ? ");
        $stmt->bindParam(1,$email);

        if ($stmt->execute()) {

           $user = $stmt->fetch();
           $stmt->closeCursor();

            return $user;

        } else {
            return NULL;
        }
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email, $password) {
        //print_r($email); die;
        $stmt = $this->conn->prepare("SELECT u_id, u_name, u_email, u_phone, u_fb_id, u_type, u_lang, u_fb_login, u_status, u_gcm_id FROM user WHERE u_email = ? AND u_password = ? ");

        $md5_password = md5($password);

        $stmt->bindParam(1,$email);
        $stmt->bindParam(2,$md5_password);

        if ($stmt->execute()) {

            $user = $stmt->fetch();
            $stmt->closeCursor();

            return $user;
        } else {
            return NULL;
        }
    }



    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function get_social_login($fbID) {
        //print_r($email); die;
        $stmt = $this->conn->prepare("SELECT u_id, u_name, u_email, u_phone, u_fb_id, u_type, u_lang, u_fb_login, u_status, u_gcm_id FROM user WHERE u_fb_id = ? ");

        $stmt->bindParam(1,$fbID);
       

        if ($stmt->execute()) {

            $user = $stmt->fetch();
            $stmt->closeCursor();

            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {

        $stmt = $this->conn->prepare("SELECT u_id from user WHERE u_email = ?");

        $stmt->bindParam("1", $email);
        $stmt->execute();
        $num_rows = $stmt->fetch();
        $stmt->closeCursor();
        return $num_rows;
    }

 

/**
     * Creating new update_Password
     * @param String $email, $password, $id 
     */
    public function update_Password($email, $password, $id) {
        $stmt = $this->conn->prepare("UPDATE user us set us.u_password = ? WHERE us.u_email = ? AND us.u_id = ?");
        $md5_password = md5($password);
        $stmt->bindParam("1", $md5_password);
        $stmt->bindParam("2", $email);
        $stmt->bindParam("3", $id);
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }


    public function update_profile($email, $name, $password, $phone) {

        $stmt = $this->conn->prepare("UPDATE user us set us.u_name = ?, us.u_phone = ?, us.u_password = ? WHERE us.u_email = ? ");
        $md5_password = md5($password);
        $stmt->bindParam("1", $name);
        $stmt->bindParam("2", $md5_password);
        $stmt->bindParam("3", $phone);
        $stmt->bindParam("4", $email);
        
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }




  /**
     * Creating new update_gcmID
     * @param String $email, $password, $id 
     */
    public function update_gcmID($user_id, $gcmID) {
        //print_r($_REQUEST); die;
        $stmt = $this->conn->prepare("UPDATE user us set us.u_gcm_id = ? WHERE us.u_id = ?");
        $stmt->bindParam(1,$gcmID);
        $stmt->bindParam(2,$user_id);
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }

}