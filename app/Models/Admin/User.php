<?php

/**
 * Class to handle all apis
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class User extends Layout {
  protected $app;

  private $api_key;
  private $api_endpoint = 'https://<dc>.api.mailchimp.com/3.0';
  private $verify_ssl   = true;
 
  
  // (iOS) Private key's passphrase.
  private static $passphrase = 'joashp';
  
  
  

  public function __construct($app) {
    $this->app = $app;
    $this->load_library('Api/Validation');
	  $this->load_library('Api/Email', 'email');
    $this->load_model('Api/User_model');
    $this->load_model('Api/Client_model');
    $this->load_model('Api/Artist_model');
    $this->load_model('Api/Referral_model');
    $this->load_model('Api/Booking_model');

      $this->api_key = "f2d206a119a8214b27a86934ceb21b1f-us1";
      list(, $datacentre) = explode('-', $this->api_key);
      $this->api_endpoint = str_replace('<dc>', $datacentre, $this->api_endpoint);
  }

  public function nearby($request, $response, $args){

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
        echo json_encode($response);
        die; 
    }

       $response = array();

       $lang      = isset($_POST['lang'])?$_POST['lang']:"eng";
       $user_id   = isset($_POST['user_id'])?$_POST['user_id']:"";
       $latitude  = isset($_POST['lat'])?$_POST['lat']:0;
       $longitude = isset($_POST['long'])?$_POST['long']:0;
       $distance  = isset($_POST['distance'])?$_POST['distance']:"";


       $cat_array = isset($_POST['cat_array'])?$_POST['cat_array']:''; //1,2,3,4
       $cat_array = @json_decode($cat_array);
       $cat_array = is_array($cat_array)?implode(',', $cat_array):false;
       



       $search = isset($_POST['search'])?$_POST['search']:false;

       $skintype = isset($_POST['skintype'])?$_POST['skintype']:false;
       $skintype = $skintype?@json_decode($skintype):false;

       if(is_array($skintype) && count($skintype)){
          $skintype = "'".implode("','", $skintype)."'";
       }
       

       $speakinglang = isset($_POST['speakinglang'])?$_POST['speakinglang']:false;
       $speakinglang = $speakinglang?@json_decode($speakinglang):false;
      
       if(is_array($speakinglang) && count($speakinglang)){
          $speakinglang = "'".implode("','", $speakinglang)."'";
       }
       
       if($this->validation->error){

        $response["error"] = true;
        $response["message"] = array_pop($this->validation->error);

       }
       else{

        $user_id = $this->decode_base64($user_id);
        
        $user_type = 'client';
        //now first of all update the loction
       // $this->Client_model->update_user_lat_long($user_id, $user_type, $latitude, $longitude);
        if($latitude && $longitude){
          $this->Client_model->update_client_lat_long($user_id, $user_type, $latitude, $longitude);
        }
        //biw get list of mua nearby
        $result = $this->User_model->nearby($user_id, $distance, $cat_array, $search, $skintype, $speakinglang);
        //get_nearby($user_id, $distance, $cat_array= fasle, $search = false, $skintype=false, $speakinglang=false, $lang='eng')
        

        $data = array();
        //print_r($result);die;
        foreach ($result as $key => $value) {
          $result[$key]['avgRating'] = (double)$value['avgRating'];
          $result[$key]['favorite'] = (bool)$value['favorite'];
          $result[$key]['artist_id'] = encode_base64($value['artist_id']);
          $result[$key]['artist_profile_image'] = $value['artist_profile_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$value['artist_profile_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$value['artist_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
        }

        if(!empty($result)){
          //successfully
          $response = array(
                            'response_code' => 200,
                            'status'        => 'OK',
                            'message'       => 'success',
                            'data'          => $result
                           );
        }
        else{
          //failed
         $response = array(
                            'response_code' => 200,
                            'status'        => 'OK',
                            'message'       => 'success',
                            'data'          => $result
                           );
        }

       }

       //echo json response
       echo json_encode($response);
    
  }

  /**
  * User register
  * url - /register
  * method - POST
  */
  public function register($request, $response, $args) {
  
  	die;
   
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }

    $response = array();    
            
    // reading post params
    $name          = isset($_POST['name'])?$_POST['name']:"";
    $username      = isset($_POST['username'])?$_POST['username']:"";
    $email         = isset($_POST['email'])?$_POST['email']:"";
    $password      = isset($_POST['password'])?$_POST['password']:"";
    $phone         = isset($_POST['phoneNumber'])?$_POST['phoneNumber']:"";
    $social_type   = isset($_POST['social_type'])?$_POST['social_type']:"normal";
    $fbID          = isset($_POST['fbID'])?$_POST['fbID']:"";
    $user_type     = isset($_POST['userType'])?$_POST['userType']:"";
    $language      = isset($_POST['isLangSpanish'])?$_POST['isLangSpanish']:"";
    $fb_login      = isset($_POST['isFbLogin'])?$_POST['isFbLogin']:"";
    $gcm_id        = isset($_POST['gcmID'])?$_POST['gcmID']:"";
    $time_zone     = isset($_POST['time_zone'])?$_POST['time_zone']:"";
    $referral_code = isset($_POST['referral_code'])?$_POST['referral_code']:"";
    $type_of_arts  = isset($_POST['type_of_arts'])?$_POST['type_of_arts']:"";
    $lat           = isset($_POST['lat'])?$_POST['lat']:"";
    $long          = isset($_POST['long'])?$_POST['long']:"";
    

    //file_put_contents('nonce/a.php', serialize($_POST));
    

    /*Validation*/
    //$this->validation->rules('Email',$email,'email|required');
    $this->validation->rules('name',$name,'required');
    $this->validation->rules('time_zone',$time_zone,'required');
    $this->validation->rules('user_type',$user_type,'required');

    if($username == '' && $email == ''){
      $this->validation->rules('username',$username,'required');
    }elseif($email == ''){
      $this->validation->rules('username',$username,'required');
    }elseif($username == ''){
      $this->validation->rules('email',$email,'required');
    }

            
    if($fb_login == 'false'){
      $this->validation->rules('password',$password,'required');
    }


    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{ 

          $code = '' ; //create_referral_code($name, $password);
          
          $params = (object)array(
                                  'name'         => $name,
                                  'username'     => ($username)?$username:$code,
                                  'email'        => $email,
                                  'password'     => $password,
                                  'phone'        => $phone,
                                  'social_type'  => $social_type,
                                  'fbID'         => $fbID,
                                  'user_type'    => $user_type,
                                  'language'     => $language,
                                  'fb_login'     => $fb_login,
                                  'gcm_id'       => $gcm_id,
                                  'time_zone'    => $time_zone,
                                  'type_of_arts' => $type_of_arts
                              );

          if(isset($username) && $username){

              $where = array(
                             'u_username' => $username
                              );

              $where = $this->User_model->_where($where);

              $userData = $this->User_model->get_all_where('user', $where);

              if($userData){

                  $response = array(
                                  'response_code' =>  400,
                                  'status'        =>  'error',
                                  'message'       =>  'Sorry, this username already exists'
                                );
                  echo json_encode($response);
                  die;
                  

              }
          }
		  elseif (isset($email) && $email) {
              $where = array(
                             'u_email' => $email
                              );

              $where = $this->User_model->_where($where);

              $userData = $this->User_model->get_all_where('user', $where);

              if($userData){

                  $response = array(
                                  'response_code' =>  400,
                                  'status'        =>  'error',
                                  'message'       =>  'email_exist_error'
                                );
                  echo json_encode($response);
                  die;
              }
          }

          

            $user_id = $this->User_model->createUser($params);
            
            if ($user_id) {
              $where = array(
                             's_types' => 'referral_expiry'
                              );

              $where = $this->User_model->_where($where);

              $duro = $this->User_model->get_all_where('settings', $where);

              $duro = isset($duro)?current($duro):'';

              $duration = isset($duro)?$duro['s_value']:'';
              
              

              $refrral_code = '';
              $rebate = 5; //later it will be dynamic...
              

              $exp_days = $this->Referral_model->getReferelExpriyDate(); //default is 30 days;

              $refrral_param = (object)array(
                                             'user_id'       => $user_id,
                                             'user_type'     => $user_type,
                                             'name'          => $name,
                                             'rebate'        => $rebate,
                                             'duration'      => $duration,
                                             'refrral_code'  => $refrral_code,
                                             'gcm_id'        => $gcm_id,
                                             'date'          => date("Y-m-d"),
                                             'start_date'    => date("Y-m-d"),
                                             'end_date'      => date('Y-m-d', strtotime("+".$exp_days." days"))
                                            );

              $refrral_code = $this->User_model->update_refrral_code($refrral_param);


              $params4 = (object)array(
                                      'user_id'       => $user_id,
                                      'user_type'     => $user_type,
                                      'referral_code' => $referral_code,
                                      'device_id'     => $gcm_id,
                                      'date'          => date("Y-m-d h:i:sa")
                                     );

              $this->Referral_model->add_referral_code($params4);

              
                 $data =  array(
                                'id'           => $this->encode_base64($user_id),
                                'name'         => $name,
                                'email'        => $email,
                                'phoneNumber'  => $phone,
                                'fbID'         => $fbID,
                                'userType'     => $user_type, 
                                'language'     => $language, 
                                'isFbLogin'    => (bool)$fb_login,
                                'gcmID'        => $gcm_id,
                                'refrral_code' => $refrral_code,
                                'type_of_arts' => $type_of_arts
                              );
                 if($user_type=='mua'){
                    $data['u_varified'] = 'new';
                  }
                
                $this->User_model->add_client_artist_user_id($user_id, $user_type, $lat, $long);

                //$this->user_model->update_user_lat_long($user_id, $user_type, $lat, $long);
                $response = array(
                                  'response_code' => 200,
                                  'status'        => 'ok',
                                  'message'       => 'success',
                                  'data'          => $data
                                  );
          
            } else {
                $response = array(
                                'response_code' =>  400,
                                'status'        =>  'error',
                                'message'       =>  'email_exist_error'
                              );
            }
      }
    
    echo json_encode($response);

  }

  /**
  * User register by get
  * url - /register_get
  * method - GET
  */
  public function register_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


  /**
  * User login
  * url - /login
  * method - POST
  */
  public function login($request, $response, $args) {
    
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }

    $response = array();

    $email     = isset($_POST['email'])?$_POST['email']:"";
    $username  = isset($_POST['username'])?$_POST['username']:"";
    $password  = isset($_POST['password'])?$_POST['password']:"";
    $gcmID     = isset($_POST['gcmID'])?$_POST['gcmID']:"";
    $user_long = isset($_POST['user_long'])?$_POST['user_long']:"";
    $user_lat  = isset($_POST['user_lat'])?$_POST['user_lat']:0;
    $user_type = isset($_POST['user_type'])?$_POST['user_type']:0;
    $time_zone = isset($_POST['time_zone'])?$_POST['time_zone']:"";
    
    /*Validation*/
    
    $this->validation->rules('password',$password,'required');
    if($username == '' && $email == ''){
      $this->validation->rules('username',$username,'required');
    }


    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else {

      $params = (object)array(
                              'username'  => $username,
                              'email'     => $email,
                              'password'  => $password,
                              'gcmID'     => $gcmID,
                              'time_zone' => $time_zone
                              );

      // check for correct email and password
      $userData = $this->User_model->getUserByEmail($params);


      
      

     
      
          if ($userData['u_id'] != 0 || $userData['u_id'] != ""){

            $userData['u_id']            = encode_base64($userData['u_id']);
            $userData['u_status']        = (bool)$userData['u_status'];
            $userData['u_fb_login']      = (bool)$userData['u_fb_login'];
			$userData['subscription_id'] = encode_base64($userData['subscription_id']);
			$userData['subscription_check'] = $userData['subscription_check']=='free'?(bool)0:(bool)$userData['subscription_check'];
            $userData['u_phone']         = (isset($userData['u_phone']) || $userData['u_phone'] == 'null' || $userData['u_phone'] == 'NULL')?$userData['u_phone']:'';

            
           

            if($userData['u_action'] == 'artist'){
              
              $userData['u_image'] = $userData['u_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$userData['u_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$userData['u_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
            
            }else if($userData['u_action'] == 'client'){
              $userData['u_image'] = $userData['u_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$userData['u_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$userData['u_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

            }

            if(isset($userData['u_type']) && $userData['u_type'] == 'mua'){
              $name = $this->User_model->get_mua_name(decode_base64($userData['u_id']));
              $userData['u_name'] = isset($name['ap_profile_name'])?$name['ap_profile_name']:'';
            }



            
            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success',
                              'data'          => $userData
                              );
            
            /*$params = (object)array(
                                    'user_id'   => $user_id,
                                    'gcmID'     => $gcmID,
                                    'user_long' => $user_long,
                                    'user_lat'  => $user_lat,
                                    'user_type' => $user_type,
                                   );*/

            // insert data in user_action table
            //$this->User_model->insert_userAction($params);
          } 
          else{
                // unknown error occurred
                $response = array(
                                  'response_code' => 400,
                                  'status'        => 'error',
                                  'message'       => 'Login credentials incorrect'
                                  );
          }
    } 
    //echo json response
    echo json_encode($response);
  }

 
  /**
  * User login by get
  * url - /login_get
  * method - GET
  */
  public function login_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


/**
  * User fbloginn
  * url - / fblogin
  * method - POST
  */
  public function fblogin($request, $response, $args){

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    

    /* start */
    $name = false;
    $fbID        = isset($_POST['fbID'])?$_POST['fbID']:"";
    $email       = isset($_POST['email'])?$_POST['email']:"";
    $phone       = isset($_POST['phone'])?$_POST['phone']:"";
    $login_with  = isset($_POST['login_with'])?$_POST['login_with']:"";  //fb, gmail
    $gcmID       = isset($_POST['gcmID'])?$_POST['gcmID']:"";
    $user_long   = isset($_POST['user_long'])?$_POST['user_long']:"";
    $user_lat    = isset($_POST['user_lat'])?$_POST['user_lat']:"";
    $user_type   = isset($_POST['user_type'])?$_POST['user_type']:"";
    $time_zone   = isset($_POST['time_zone'])?$_POST['time_zone']:"";

    /* end */
    
    $this->validation->rules('login_with',$login_with,'required');
    $this->validation->rules('user_type',$user_type,'required');
    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{

      $params = (object)array(
                              'fbID'        => $fbID,
                              'email'       => $email,
                              'phone'       => $phone,
                              'login_with'  => $login_with,
                              'gcmID'       => $gcmID,
                              'user_long'   => $user_long,
                              'user_lat'    => $user_lat,
                              'user_type'   => $user_type,
                              'time_zone'   => $time_zone
                            );


      // check for correct fbID
      if($params->fbID !='' || $params->email !=''){
        

        $user = $this->User_model->get_social_login($params);  
       
        // update gcm_ID 
        $user_id = $user['u_id'];
        $this->User_model->update_gcmID($user_id, $gcmID, $time_zone);

        $userData = $this->User_model->get_social_login($params);
      


        if ($userData['u_id'] != 0 || $userData['u_id'] != "") {

         
                      $userData['u_id'] = encode_base64($userData['u_id']);
                      $userData['u_status'] = (bool)$userData['u_status'];
                      $userData['u_fb_login'] = (bool)$userData['u_fb_login'];
                      if($userData['u_action'] == 'artist'){
                            
                          $userData['u_image'] = $userData['u_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$userData['u_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$userData['u_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
                          
                      }else if($userData['u_action'] == 'client'){
                            $userData['u_image'] = $userData['u_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$userData['u_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$userData['u_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

                      }

                      if(isset($userData['u_type']) && $userData['u_type'] == 'mua'){
                        $name = $this->User_model->get_mua_name(decode_base64($userData['u_id']));
                        $userData['u_name'] = isset($name['ap_profile_name'])?$name['ap_profile_name']:'';
                      }

                      $response = array(
                                        'response_code' => '200',
                                        'status'        => 'ok',
                                        'message'       => 'successfully login',
                                        'data'          => $userData
                                        );
                                           
                      
                      $params = (object)array(
                                              'user_id'   => $user_id,
                                              'gcmID'     => $gcmID,
                                              'user_long' => $user_long,
                                              'user_lat'  => $user_lat,
                                              'user_type' => $user_type,
                                             );

                          // insert data in user_action table
                          //$this->User_model->insert_userAction($params);
        }
        else {

                //IF not found emailid and facebookid 

                $aa = isset($email)?$email:'';
                
                $name = $name?$name:current(explode("@",$aa));

                $params = (object)array(
                                      'name'         => $name,
                                      'email'        => $email,
                                      'password'     => "",
                                      'phone'        => $phone,
                                      'social_type'  => $login_with,
                                      'fbID'         => $fbID,
                                      'user_type'    => $user_type,
                                      'language'     => "eng",
                                      'fb_login'     => "true",
                                      'gcm_id'       => $gcmID,
                                      'time_zone'    => $time_zone,
                                      'type_of_arts' => ""
                                  );


                if($params->email != '' && $params->fbID != ''){
                    
                      $user_id = $this->User_model->createUser($params);

                      //Entry user_type wise client_profile & artist_profile table
                      $this->User_model->add_client_artist_user_id($user_id, $user_type, $lat, $long);


                      if((isset($user_lat) && $user_lat) && (isset($user_long) && $user_long)){
                          $this->Client_model->update_client_lat_long($user_id, $user_type, $user_lat, $user_long);
                      }

                      if($user_id){

                    /*------ refrral process start-------*/

                          $refrral_code = create_referral_code($name, $user_id); //str_replace(' ', '', $name).'-'.$user_id.rand(1000,9999);

                          $rebate = 5; //later it will be dynamic...
                          $duration = 20; //later it will be dynamic...
                          $exp_days = 30;

                          $refrral_param = (object)array(
                                                         'user_id'       => $user_id,
                                                         'user_type'     => $user_type,
                                                         'name'          => $name,
                                                         'rebate'        => $rebate,
                                                         'duration'      => $duration,
                                                         'refrral_code'  => $refrral_code,
                                                         'gcm_id'        => $gcmID,
                                                         'date'          => date("Y-m-d"),
                                                         'start_date'    => date("Y-m-d"),
                                                         'end_date'      => date('Y-m-d', strtotime("+".$exp_days." days"))
                                                        );

                          $this->User_model->update_refrral_code($refrral_param);


                          $params4 = (object)array(
                                                  'user_id'       => $user_id,
                                                  'user_type'     => $user_type,
                                                  'referral_code' => $refrral_code,
                                                  'device_id'     => $gcmID,
                                                  'date'          => date("Y-m-d h:i:sa")
                                                 );

                          $this->Referral_model->add_referral_code($params4);


                    /*------ refrral process ends-------*/



                          $data = $this->User_model->get_data_by_id($user_id);

                          $response = array(
                                            'response_code' => 200,
                                            'status'        => 'ok',
                                            'message'       => 'successfully login new account',
                                            'data'          => $data
                                            );
                      }else{

                        // unknown error occurred
                        $response = array(
                                          'response_code' => 203,
                                          'status'        => 'success',
                                          'message'       => 'Not registered with this account'
                                          );
                      }

                }else{
                        $response = array(
                                          'response_code' => 203,
                                          'status'        => 'success',
                                          'message'       => 'please fill field email, fbID'
                                          );
                }

          }

    
    }else{
         $response = array(
                            'response_code' => 203,
                            'status'        => 'error',
                            'message'       => 'please fill field email, fbID'
                            );
        }
    }
    
    //echo json response
    echo json_encode($response);
  
  }





  private function login_register($params){

    

      if ($user_id) {
                  $refrral_code = create_referral_code($name, $user_id); //str_replace(' ', '', $name).'-'.$user_id.rand(1000,9999);
                  $rebate = 5; //later it will be dynamic...
                  $duration = 20; //later it will be dynamic...

                  $exp_days = 30;

                  $refrral_param = (object)array(
                                                 'user_id'       => $user_id,
                                                 'user_type'     => $user_type,
                                                 'name'          => $name,
                                                 'rebate'        => $rebate,
                                                 'duration'      => $duration,
                                                 'refrral_code'  => $refrral_code,
                                                 'gcm_id'        => $gcm_id,
                                                 'date'          => date("Y-m-d"),
                                                 'start_date'    => date("Y-m-d"),
                                                 'end_date'      => date('Y-m-d', strtotime("+".$exp_days." days"))
                                                );

                  $this->User_model->update_refrral_code($refrral_param);


                  $params4 = (object)array(
                                          'user_id'       => $user_id,
                                          'user_type'     => $user_type,
                                          'referral_code' => $referral_code,
                                          'device_id'     => $gcm_id,
                                          'date'          => date("Y-m-d h:i:sa")
                                         );

                  $this->Referral_model->add_referral_code($params4);
    }

  }










  /**
  * User fblogin by get
  * url - /login_get
  * method - GET
  */
  public function fblogin_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


   /**
   * User forget_password
   * URL - http://bebebella.cloudapp.net/v1/api/forget_password?email=amityadav314@gmail.com
   * method - POST
   */
  public function forget_password($request, $response, $args) {

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    $email =  isset($_POST['email'])?$_POST['email']:"";
    
    $this->validation->rules('email',$email,'required');
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = $this->validation->error;
    }
    else{

      // get the detail by email
      $user = $this->User_model->getUserEmail($email);


      
      if ($user['u_id']){
        
        $u_name   = $user['u_name'];
        $email    = $user['u_email'];
        $id       = $user['u_id'];
        $password = rand(100000, 999999);
        $result   = $this->User_model->update_Password($email,$password,$id);

        if ($result) {
            
            $subject           = FORGET_PASSWORD_MSG;
            
            $data['opt']        = isset($password)?$password:'';
            $data['user_name']  = isset($u_name)?$u_name:'';
            $data['email']      = isset($email)?$email:'';
            $msg = $this->_view('Front/Email/Users/forget_password.php', $data, true);

            $this->email->to($email);
      			$this->email->subject($subject);
      			$this->email->messageType('html');
      			$this->email->message($msg);
      			$this->email->send();
			       
            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success'
                              );
              
        } else {
              // password failed to update
                $response = array(
                                  'response_code' =>  400,
                                  'status'        =>  'error',
                                  'message'       =>  'bad request'
                                  );
        } 
    } else{
            // user credentials are wrong
            $response = array(
                              'response_code' =>  400,
                              'status'        =>  'error',
                              'message'       =>  'bad request'
                              );
        } 
    }
    //echo json response
    echo json_encode($response);
  }

  /**
   * function forget_password_get
   * method - GET
   */
  public function forget_password_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
   * function - reset_password
   * URL - http://bebebella.cloudapp.net/v1/api/reset/password?
   * method - POST
   */
  public function reset_password($request, $response, $args) {

    if(!$_POST)
       {
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'some fields required'
                            );
          echo json_encode($response);
          die;
       } 
    
      $response = array();
      $user_id          =  isset($_POST['user_id'])?$_POST['user_id']:"";
      $currunt_password =  isset($_POST['currunt_password'])?$_POST['currunt_password']:"";
      $new_password     =  isset($_POST['new_password'])?$_POST['new_password']:"";
      
      $this->validation->rules('user_id',$user_id,'required');
      $this->validation->rules('currunt_password',$currunt_password,'required');
      $this->validation->rules('new_password',$new_password,'required');

      


    if($this->validation->error)
    {
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{

        $user_id = decode_base64($user_id);  

        $result = $this->User_model->get_user_id($user_id);

        $u_password = $result['u_password'];
        $email = $result['u_email'];

        if($result){

            if(md5($currunt_password) == $u_password){

              $result = $this->User_model->update_Password($email, $new_password, $user_id);
              
              if ($result) {
                              // password updated successfully
                              $response = array(
                                                'response_code' =>  200,
                                                'status'        =>  'OK',
                                                'message'       =>  'success'
                                                );
                  
                            } 
              else {
                    // password failed to update
                      $response = array(
                                        'response_code' => 400,
                                        'status'        => 'error',
                                        'message'       => 'bad request'
                                        );
                    } 

            }else{
                  $response = array(
                                  'response_code'  => 400,
                                  'status'         => 'error',
                                  'message'        => 'current password is incorrect'
                                  );
            }

            


             
         

        }
        else{
            $response = array(
                              'response_code'  => 400,
                              'status'         => 'error',
                              'message'        => 'Login credentials incorrect'
                              );
        }
       


             
    }
    echo json_encode($response);
  }

/**
   * function edit_profile_get
   * method - GET
   */
  public function edit_profile_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }

/*function get_mua
  url get_mua
*/
  public function get_mua($request, $response, $args){

        


    



    $msg_payload = array (
                          'mtitle' => 'Test push notification title',
                          'mdesc' => 'Test push notification body',
                         );

    $deviceToken = '2f478577e8709cafeded191d0a042c95111591a947693f6471f72c0952a7c3f9';

    

    $ctx = stream_context_create();
    // ck.pem is your certificate file
    stream_context_set_option($ctx, 'ssl', 'local_cert', 'BellaAPNSProd.pem');
    stream_context_set_option($ctx, 'ssl', 'passphrase', self::$passphrase);

    // Open a connection to the APNS server
    $fp = stream_socket_client(
                               'ssl://gateway.push.apple.com:2195', $err,
                               $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx
                              );

    if (!$fp)
      exit("Failed to connect: $err $errstr" . PHP_EOL);

    // Create the payload body

    //$data['mtitle'] = 'bebebella';
    //$data['mdesc'] = 'Description';
    $body['aps'] = array(
                         'alert' => array(
                                          'title' => 'bebebella',
                                          'body' => 'bebebella',
                                          ),
                         'sound' => 'default'
                        );

    // Encode the payload as JSON
    $payload = json_encode($body);

    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));
    
    // Close the connection to the server
    fclose($fp);

    if (!$result)

     echo 'Message not delivered' . PHP_EOL;
    else
      echo 'Message successfully delivered' . PHP_EOL;


  }

  



/**
   * function get_mua_get
   * method - GET
   */
  public function get_mua_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


 /**
    * Send mail api
    * @param null
    * @return $result
    */
    private function send_mail_api($data){

      
        $url = 'http://synergytop.com/api/email';
        
        $params = http_build_query($data, NULL, '&');
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url); //Remote Location URL
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser   
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
            
        //We add these 2 lines to create POST request
        curl_setopt($ch, CURLOPT_POST, count($data)); //number of parameters sent
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params); //parameters data
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }






  /*--- user id decode function on base64_decode ---*/
  protected function decode_base64($code){
    $int = base64_decode($code);

    $id = $int/(45421*45421);

    return $id;
  }


/*--- user id encode function on base64_encode ---*/
  protected function encode_base64($requestData){
    $sting = base64_encode($requestData*45421*45421);

    return $sting;
  }




  public function send_mail($request, $response, $args){

      // Content-Type = application/x-www-form-urlencoded

              $data =   $_POST;

              $a = json_decode($data['vardata']);

              //print_r($a->user_id); die;


              $result = $this->User_model->get_user_id(decode_base64($a->user_id));

              print_r($result);

              //var_export($_REQUEST);

              die;


   /* $email = isset($_POST['email'])?$_POST['email']:'';

    listid = b3020b47fa;
    APIkey = f2d206a119a8214b27a86934ceb21b1f-us1;

    if($email)
    {
    $list_id = "b3020b47fa";
    $api_key = "f2d206a119a8214b27a86934ceb21b1f-us1";
    
    $subemail = $email;
    
    require('app/Third-Party/Mailchimp/Mailchimp.php');
    
    $Mailchimp       = new Mailchimp( $api_key );
    $Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );
    $subscriber = $Mailchimp_Lists->subscribe( $list_id,array('email' => htmlentities($subemail)));
    
    if ( ! empty( $subscriber['leid'] ) ) {
      echo 'message to show when the subscriber has successfully subscribed';
    }
    else
    {
     echo 'message to show when the subscription failed';
    }
    }
    else {
    echo 'Unauthorized access';*/


          //require_once('mailchimpint/mcapi/inc/MCAPI.class.php');
         // $apikey = "f2d206a119a8214b27a86934ceb21b1f-us1";

         // $list_id = "b3020b47fa";
          // $api_key = "f2d206a119a8214b27a86934ceb21b1f-us1";

          /*$to_emails = array('amityadav314@gmail.com', 'amityadav314@gmail.com');
          $to_names = array('amityadav314@gmail.com', 'amityadav314@gmail.com');

          $message = array(
              'html'=>'Yo, this is the <b>html</b> portion',
              'text'=>'Yo, this is the *text* portion',
              'subject'=>'This is the subject',
              'from_name'=>'Me!',
              'from_email'=>'',
              'to_email'=>$to_emails,
              'to_name'=>$to_names
          );

          $tags = array('WelcomeEmail');

          $params = array(
              'apikey'=>$apikey,
              'message'=>$message,
              'track_opens'=>true,
              'track_clicks'=>false,
              'tags'=>$tags
          );

          $url = "http://us5.sts.mailchimp.com/1.0/SendEmail";
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($params));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

          $result = curl_exec($ch);
          echo $result;
          curl_close ($ch);
           var_dump($result);
          $data = json_decode($result);

          */

          //echo "Status = ".$data->status."\n";


          $timeout = 10;
          $postString = '{
                          "message": {
                              "html": "this is the emails html content",
                              "text": "this is the emails text content",
                              "subject": "this is the subject",
                              "from_email": "amityadav314@gmail.com",
                              "from_name": "John",
                              "to_email": "amityadav314@gmail.com",
                              "to_name": "Anton",
                              "track_opens": false,
                              "track_clicks": false
                          }}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_endpoint);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
        curl_setopt($ch, CURLOPT_USERPWD, 'drewm:'.$this->api_key);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/vnd.api+json', 'Content-Type: application/vnd.api+json'));
        curl_setopt($ch, CURLOPT_USERAGENT, 'DrewM/MailChimp-API/3.0 (github.com/drewm/mailchimp-api)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        

        $result = curl_exec($ch);

        echo $result;




        // $apikey = 'f2d206a119a8214b27a86934ceb21b1f-us1';

        //     $to_emails = array('amityadav314@gmail.com');
        //     $to_names = array('You', 'Your Mom');

        //     $message = array(
        //         'html'=>'Yo, this is the <b>html</b> portion',
        //         'text'=>'Yo, this is the *text* portion',
        //         'subject'=>'This is the subject',
        //         'from_name'=>'Me!',
        //         'from_email'=>'verifed@example.com',
        //         'to_email'=>$to_emails,
        //         'to_name'=>$to_names
        //     );

        //     $tags = array('WelcomeEmail');

        //     $params = array(
        //         'apikey'=>$apikey,
        //         'message'=>$message,
        //         'track_opens'=>true,
        //         'track_clicks'=>false,
        //         'tags'=>$tags
        //     );

        //     $url = "http://us1.sts.mailchimp.com/3.0/SendEmail";

        //     $ch = curl_init();
        //     curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($params));
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //     $result = curl_exec($ch);
        //     echo $result;
        //     curl_close ($ch);

        //     $data = json_decode($result);

        //     print_r($data);
            //echo "Status = ".$data->status."\n"; 
  }



/**
  * User fb_gmail_login
  * url - / fb_gmail_login
  * method - POST
  */
  public function fb_gmail_login($request, $response, $args){

  	if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    

    /* start */
    $name 		   = isset($_POST['name'])?$_POST['name']:"";
    $username    = isset($_POST['username'])?$_POST['username']:"";

    $fbID        = isset($_POST['fbID'])?$_POST['fbID']:"";
    $email       = isset($_POST['email'])?$_POST['email']:"";
    $phone       = isset($_POST['phone'])?$_POST['phone']:"";
    $login_with  = isset($_POST['login_with'])?$_POST['login_with']:"";  //fb, gmail
    $gcmID       = isset($_POST['gcmID'])?$_POST['gcmID']:"";
    $user_long   = isset($_POST['long'])?$_POST['long']:"";
    $user_lat    = isset($_POST['lat'])?$_POST['lat']:"";
    $user_type   = isset($_POST['user_type'])?$_POST['user_type']:"";
    $time_zone   = isset($_POST['time_zone'])?$_POST['time_zone']:"";

    $language     = isset($_POST['language'])?$_POST['language']:"eng";
    $fb_login     = isset($_POST['fb_login'])?$_POST['fb_login']:"true";
    $type_of_arts = isset($_POST['type_of_arts'])?$_POST['type_of_arts']:"";


    
    
	 /* end */
    
    $this->validation->rules('fbID',$fbID,'required');
    $this->validation->rules('login_with',$login_with,'required');

    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{
  	   $params = (object)array(
                                'name'         => $name,
                                'username'     => $username,
                                'email'        => $email,
                                'password'     => '',
                                'phone'        => $phone,
                                'social_type'  => $login_with,
                                'fbID'         => $fbID,
                                'user_type'    => $user_type,
                                'language'     => $language,
                                'fb_login'     => $fb_login,
                                'gcm_id'       => $gcmID,
                                'time_zone'    => $time_zone,
                                'type_of_arts' => $type_of_arts,
                                'user_long'    => $user_long,
                                'user_lat'     => $user_lat,
                              );


          $userData = $this->register_with_social_login($params);



          if ($userData=='new_user'){
            $response = array(
                                'response_code' => 203,
                                'status'        => 'success',
                                'message'       => 'Not registered with this account'
                                );
          } else if($userData){
            
            $response = array(
                              'response_code' => '200',
                              'status'        => 'ok',
                              'message'       => 'successfully login',
                              'data'          => $userData
                              );
          }else{

              $response = array(
                                'response_code' => 203,
                                'status'        => 'success',
                                'message'       => 'Not registered with this account'
                                );
          }
          
         


       /* if(isset($params->user_type) && $params->user_type){

                $userData = $this->register_with_social_login($params);

                if ($userData) {
                  
                  $response = array(
                                    'response_code' => '200',
                                    'status'        => 'ok',
                                    'message'       => 'successfully login',
                                    'data'          => $userData
                                    );
                }else{

                    $response = array(
                                      'response_code' => 203,
                                      'status'        => 'success',
                                      'message'       => 'Not registered with this account'
                                      );
                }
        }else{
              */
        /*Start login process with face and gmail account with user*/
                /*$userData = $this->get_data_social_login($params);

                if ($userData) {
                  
                  $response = array(
                                    'response_code' => '200',
                                    'status'        => 'ok',
                                    'message'       => 'successfully login',
                                    'data'          => $userData
                                    );
                }else{

                    $response = array(
                                      'response_code' => 203,
                                      'status'        => 'success',
                                      'message'       => 'Not registered with this account'
                                      );
                }*/
        /*Ends login process with face and gmail account with user*/
       /* }*/

  }/*validation else close*/
    
  //echo json response
  echo json_encode($response);
  
}


   /*
  ||======================================================
  ||  private function to register data with soical login
  ||======================================================
  */

  private function register_with_social_login($params){

      //check if already exist
      $userData = $this->get_data_social_login($params);

      if(isset($userData) && $userData){
        //user exist so now let user login
        return $userData;

      }else{
        //user not exist
        

        if($params->user_type==''){
          //point here user is new
          return 'new_user';
        }else{
          //now do registration or let user login
          
          $user_id = $this->User_model->createUser($params);

          /*print_r($user_id);
          die;*/
         
          if ($user_id) {

                $where = array(
                               's_types' => 'referral_expiry'
                                );

                $where = $this->User_model->_where($where);

                $duro = $this->User_model->get_all_where('settings', $where);

                $duro = isset($duro)?current($duro):'';

                $duration = isset($duro)?$duro['s_value']:'';
                
                $name = isset($params->name)?$params->name:'Bebe';

                $refrral_code = create_referral_code('Bebe', $user_id); //str_replace(' ', '', $name).'-'.$user_id.rand(1000,9999);
                $rebate = 5; //later it will be dynamic...
                //$duration = 20; //later it will be dynamic...

                $exp_days = 30;

                $refrral_param = (object)array(
                                               'user_id'       => $user_id,
                                               'user_type'     => $params->user_type,
                                               'name'          => $name,
                                               'rebate'        => $rebate,
                                               'duration'      => $duration,
                                               'refrral_code'  => $refrral_code,
                                               'gcm_id'        => $params->gcm_id,
                                               'date'          => date("Y-m-d"),
                                               'start_date'    => date("Y-m-d"),
                                               'end_date'      => date('Y-m-d', strtotime("+".$exp_days." days"))
                                              );

                $this->User_model->update_refrral_code($refrral_param);


                $params4 = (object)array(
                                        'user_id'       => $user_id,
                                        'user_type'     => $params->user_type,
                                        'referral_code' => $refrral_code,
                                        'device_id'     => $params->gcm_id,
                                        'date'          => date("Y-m-d h:i:sa")
                                       );

                

                $this->Referral_model->add_referral_code($params4);



                $this->User_model->add_client_artist_user_id($user_id, $params->user_type, $params->user_lat, $params->user_long);

                $userData = $this->get_data_social_login($params);

                 return $userData;
                 
            
          } else {
              return array();
          }
        }
      }

  }


  /*
  ||======================================================
  ||  private function to get data soical login
  ||======================================================
  */
  

  private function get_data_social_login($params){

        $userData = $this->User_model->get_social_login($params);

        if ($userData['u_id'] != 0 || $userData['u_id'] != "") {

          $userData['u_id']       = encode_base64($userData['u_id']);
          $userData['u_status']   = (bool)$userData['u_status'];
          $userData['u_fb_login'] = (bool)$userData['u_fb_login'];
          $userData['subscription_id'] = encode_base64($userData['subscription_id']);
		  $userData['subscription_check'] = $userData['subscription_check']=='free'?(bool)0:(bool)$userData['subscription_check'];
			
          if($userData['u_action'] == 'artist'){
              
              $userData['u_image'] = $userData['u_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$userData['u_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$userData['u_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
                
          }else if($userData['u_action'] == 'client'){
                  $userData['u_image'] = $userData['u_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$userData['u_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$userData['u_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

          }

          if(isset($userData['u_type']) && $userData['u_type'] == 'mua'){
              $name = $this->User_model->get_mua_name(decode_base64($userData['u_id']));
              $userData['u_name'] = isset($name['ap_profile_name'])?$name['ap_profile_name']:$userData['u_name'];
          }
          

          return $userData;
      }else{

          return array();
      }

  }


  public function term_condition($request, $response, $args){

    $lang = isset($_GET['lang'])?$_GET['lang']:'eng';


    if(isset($lang) && ($lang == 'spn')){
      $this->_view('Front/term_condition/term_condition_spn.html');    
      
    }else{
      $this->_view('Front/term_condition/term_condition.html');
      
    }

    
  }


  public function get_username($request, $response, $args){

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $username    = isset($_POST['username'])?$_POST['username']:"";

    $this->validation->rules('username',$username,'required');

    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{

          $where = array(
                         'u_username' => $username
                          );

          $where = $this->User_model->_where($where);

          $result = $this->User_model->get_all_where('user', $where);

          if($result){

                $response = array(
                                  'valid' => "true"
                                  );

          }else{
            $response = array(
                              'valid' => "false"
                             );
          }

         

     }
      echo json_encode($response);
      die;
  }



  /*
  ||=================================================================
  || function get_earned_data in the mua
  ||=================================================================
  */
  public function get_earned_data($request, $response, $args){

    $response = array();

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    $this->validation->rules('user_id',$user_id,'required');
    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{
            $my_earning          = $this->User_model->get_earned_data(decode_base64($user_id),  $lang);
            $payment_received        = $this->User_model->get_earned_data_admin(decode_base64($user_id), $lang);

            $subscription_data = $this->User_model->get_subscription_data(decode_base64($user_id), $lang);

            if(isset($my_earning) && $my_earning){
	             foreach ($my_earning as $key => $value) {
	        		$my_earning[$key]['client_id'] = encode_base64($value['client_id']);
	        		$my_earning[$key]['services_id'] = encode_base64($value['services_id']);
	        		$my_earning[$key]['client_image'] = $value['client_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$value['client_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$value['client_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
	        	}
        	}


 		if(isset($subscription_data) && $subscription_data){
        	foreach ($subscription_data as $key => $value) {

		      if(isset($lang) && $lang == 'spn'){

		        $subscription_data[$key]['asp_name'] = $value['asp_name_spn'];
		        $subscription_data[$key]['asp_description'] = $value['asp_description_spn'];

		        

		      }
		      unset($subscription_data[$key]['asp_name_spn']);
		        unset($subscription_data[$key]['asp_description_spn']);

		      //$subscription_data[$key]['plan_id'] = encode_base64($value['plan_id']);
		      $subscription_data[$key]['asp_image'] = $value['asp_image']?(file_exists(APP_DIR.'/'.SUBSCRIPTION_PLAN_IMG.$value['asp_image'])?(BASE_URL.SUBSCRIPTION_PLAN_IMG.$value['asp_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
		    }

		}

    
            $total = array(
                            'my_earning_total'   => isset($my_earning[0]['mua_total'])?$my_earning[0]['mua_total']:'',
                            'payment_received_total' => isset($payment_received[0]['admin_total'])?$payment_received[0]['admin_total']:'',
                            'subscription_total' => isset($subscription_data[0]['subscription_total'])?$subscription_data[0]['subscription_total']:''
                            );

            $result = array(
                            'my_earning'   => $my_earning,
                            'payment_received' => $payment_received,
                            'subscription_data' => $subscription_data,
                            'total'   => $total
                            );

            if ($result) {

              $response = array(
                                'response_code' => '200',
                                'status'        => 'ok',
                                'message'       => 'success',
                                'data'          => $result
                                );
            }else{

              $response = array(
                                'response_code' => 200,
                                'status'        => 'ok',
                                'message'       => 'success',
                                'data'          => $result
                                );
            }
    }
  //echo json response
  echo json_encode($response);
  }



  /*
  ||================================================
  || Function add user location
  ||================================================
  */
  public function add_user_location($request, $response, $args){

    $response = array();

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $address = isset($_POST['address'])?$_POST['address']:"";
    $lat     = isset($_POST['lat'])?$_POST['lat']:"";
    $long    = isset($_POST['long'])?$_POST['long']:"";


    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('lat',$lat,'required');
    $this->validation->rules('long',$long,'required');
    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{

      $user_id = decode_base64($user_id);

      $result = $this->User_model->get_user_id($user_id);

      if(isset($result) && $result){

        $insert_id =  $this->User_model->add_user_location($user_id, $address, $lat, $long);

        
        if(isset($insert_id) && $insert_id) {

          $loc_data =  $this->User_model->get_user_location($user_id);

          if(isset($loc_data) && count($loc_data)==1) {
          @$this->User_model->update_user_lat_long($user_id, $result['u_type'], $lat, $long);
          }

          $response = array(
                        'response_code' => 200,
                        'status'        => 'success',
                        'message'       => 'Add info successfully.'
                        );  
        }else{
          $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'please try again.'
                        );
        }

      }else{
        $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'user_id not valid'
                        );
      }
    }
    
    //echo JSON response
    echo json_encode($response);
  }



  /*
  ||================================================
  || Function get_user_location
  ||================================================
  */
  public function get_user_location($request, $response, $args){

    $response = array();

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";

    $this->validation->rules('user_id',$user_id,'required');
   
    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{

      $user_id = decode_base64($user_id);

      $result = $this->User_model->get_user_location($user_id);

      foreach ($result as $key => $value) {
        $result[$key]['ul_id'] = encode_base64($value['ul_id']);
        $result[$key]['ul_fk_u_id'] = encode_base64($value['ul_fk_u_id']);
      }

      if(isset($result) && $result){
          $response = array(
                        'response_code' => 200,
                        'status'        => 'success',
                        'data'          => $result
                        );  
      }else{
        $response = array(
                      'response_code' => 400,
                      'status'        => 'error',
                      'message'       => $result
                      );
      }

    }
    
    //echo JSON response
    echo json_encode($response);
  }



  /*
  ||================================================
  || Function get_user_location
  ||================================================
  */
  public function edit_user_location($request, $response, $args){

    $response = array();

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $ul_id   = isset($_POST['ul_id'])?$_POST['ul_id']:"";
    $status  = isset($_POST['status'])?$_POST['status']:"";

    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('ul_id',$ul_id,'required');
    $this->validation->rules('status',$status,'required');
   
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{

      $user_id = decode_base64($user_id);
      $ul_id = decode_base64($ul_id);

      $user_data = $this->User_model->get_user_id($user_id);

      if(isset($user_data) && $user_data){

          $result = $this->User_model->edit_user_location($user_id, $ul_id, $status);

          if(isset($result) && $result){

            $loc_data =  $this->User_model->get_user_location_id($user_id);



            if(isset($loc_data) && $loc_data) {
              $u_type = isset($user_data['u_type'])?$user_data['u_type']:'';
              @$this->user_model->update_user_lat_long($user_id, $u_type, $loc_data['ul_lat'], $loc_data['ul_long']);
            }

              $response = array(
                            'response_code' => 200,
                            'status'        => 'success',
                            'data'          => $result
                            );  
          }else{
            $response = array(
                          'response_code' => 400,
                          'status'        => 'error',
                          'message'       => $result
                          );
          }

      }else{
            $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'user_id not valid'
                            );
      }

    }
    
    //echo JSON response
    echo json_encode($response);
  }



  /*
  ||================================================
  || Function deleted_user_location
  ||================================================
  */
  public function deleted_user_location($request, $response, $args){

    $response = array();

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $ul_id   = isset($_POST['ul_id'])?$_POST['ul_id']:"";

    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('ul_id',$ul_id,'required');
   
    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{

        $user_id = decode_base64($user_id);
        $ul_id   = decode_base64($ul_id);

        $location_data = $this->User_model->get_user_location($user_id);

        if(isset($location_data) && count($location_data)>1){
          
            $result = $this->User_model->deleted_user_location($user_id, $ul_id);
            
            if(isset($result) && $result){
                  $response = array(
                                'response_code' => 200,
                                'status'        => 'success',
                                'data'          => $result
                                );  
            }else{
                $response = array(
                              'response_code' => 400,
                              'status'        => 'error',
                              'message'       => $result
                              );
            }  
        
        }else{

          $location_data = current($location_data);
         
          if(isset($location_data) && $location_data){
            @$this->User_model->edit_user_location($location_data['ul_fk_u_id'], $location_data['ul_id'], $status='true');  
          }
          
          

          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'please add new location than delete'
                            );
            
        }
    }
  //echo JSON response
  echo json_encode($response);
  }


  /*
  ||================================================
  || Function user_mail_list
  ||================================================
  */
  public function user_mail_list($request, $response, $args){

    $response = array();

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";

    $this->validation->rules('user_id',$user_id,'required');
    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{

        $user_id = decode_base64($user_id);
        
        $result = $this->User_model->user_mail_list($user_id);
        foreach ($result as $key => $value) {
          $result[$key]['um_id'] = encode_base64($value['um_id']);
          $result[$key]['ticket_no'] = str_pad($value['um_id'],4,0);

          $result[$key]['um_created'] = date('l jS F Y', strtotime($value['um_created']));
          $result[$key]['um_updated'] = date('l jS F Y', strtotime($value['um_updated']));

          
          //date("Y-m-d",strtotime($value->ucr_created));

          
          
          $result[$key]['um_fk_u_id'] = encode_base64($value['um_fk_u_id']);
        }
            
        if(isset($result) && $result){
              $response = array(
                            'response_code' => 200,
                            'status'        => 'success',
                            'data'          => $result
                            );  
        }else{
            $response = array(
                           'response_code' => 200,
                           'status'        => 'success',
                           'data'          => $result
                          );
        }  
    
    }
  //echo JSON response
  echo json_encode($response);
  }


  /*
  ||================================================
  || Function user_send_mail
  ||================================================
  */
  public function user_send_mail($request, $response, $args){

    $response = array();

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $subject = isset($_POST['subject'])?$_POST['subject']:"";
    $message = isset($_POST['message'])?$_POST['message']:"";
    

    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('subject',$subject,'required');
    $this->validation->rules('message',$message,'required');
    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{

        $user_id = decode_base64($user_id);
        
        $result = $this->User_model->user_send_mail($user_id, $subject, $message);
            
        if(isset($result) && $result){
            
            $user_data = $this->User_model->get_data_by_id($user_id);

            $data['subject'] = $subject;
            $data['message'] = $message;
            $data['type']    = 'user';
            $data['name']    = isset($user_data)?$user_data['u_name']:'';
            $data['phone']   = isset($user_data)?$user_data['u_phone']:'';
            $data['email']   = isset($user_data)?$user_data['u_email']:'';
            $msg = $this->_view('Front/Email/Users/email_communication.php', $data, true);
            $email = 'support@bebebellamuas.com';
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->messageType('html');
            $this->email->message($msg);
            $this->email->send();
              
            $response = array(
                          'response_code' => 200,
                          'status'        => 'success'
                          );  
        }else{
            $response = array(
                          'response_code' => 400,
                          'status'        => 'error'
                          );
        }  
    
    }
  //echo JSON response
  echo json_encode($response);
  }


  
  /*
  ||================================================
  || Function user_help
  ||================================================
  */
  public function user_help($request, $response, $args){

    $response = array();

    /*if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } */

    /*$user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $subject = isset($_POST['subject'])?$_POST['subject']:"";
    $message = isset($_POST['message'])?$_POST['message']:"";
    

    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('subject',$subject,'required');
    $this->validation->rules('message',$message,'required');
    
    if($this->validation->error){
        $response["error"] = true;
        $response["message"] = $this->validation->error;
    }
    else{*/

        /*$user_id = decode_base64($user_id);
        
        $result = $this->User_model->user_send_mail($user_id, $subject, $message);
            
        if(isset($result) && $result){
            
            $user_data = $this->User_model->get_data_by_id($user_id);

            $data['subject'] = $subject;
            $data['message'] = $message;
            $data['type']    = 'user';
            $data['name']    = isset($user_data)?$user_data['u_name']:'';
            $data['phone']   = isset($user_data)?$user_data['u_phone']:'';
            $data['email']   = isset($user_data)?$user_data['u_email']:'';
            $msg = $this->_view('Front/Email/Users/email_communication.php', $data, true);
            $email = 'support@bebebellamuas.com';
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->messageType('html');
            $this->email->message($msg);
            $this->email->send();*/
              
            $response = array(
                              'response_code' => 200,
                              'status'        => 'success',
                              'data'          => array(
                                                       'email'   => 'Email : bebebellamuas@gmail.com',
                                                       'phone'   => '',
                                                       'message' => ''
                                                       )

                          );  
        /*}else{
            $response = array(
                          'response_code' => 400,
                          'status'        => 'error'
                          );
        }  
    
    }*/
  //echo JSON response
    echo json_encode($response);
  }
  
  
  //for getting encode for numeric no (only for testing purpose and not for API purpose)
  public function user_encode(){
      $user_id = isset($_POST['user_id'])?$_POST['user_id']:0;
      $encode_user_id = encode_base64($user_id);
      $res = array(
          'user_id'     => $user_id,
          'encoded_id'  => $encode_user_id
      );
      echo json_encode($res);
  }



}