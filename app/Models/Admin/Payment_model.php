<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */

class Payment_model{

	protected $conn;

	function __construct($app){
		$this->conn = $app->get('db');
	}

	/**
	* Get transaction list
	* @param null
	* @return $result
	*/
	public function get_transaction($param){

		$page = isset($param['page'])?$param['page']:0;

		$sql = "SELECT *
				FROM (SELECT * ,
				
				(SELECT ap_profile_name FROM artist_profile WHERE ap_fk_u_id = api_fk_artist_id LIMIT 1)
				AS artist_profile_name, 
				(SELECT u_name FROM user WHERE u_id = api_fk_artist_id LIMIT 1)
				AS artist_name,
				(SELECT asp_name FROM artist_subscription_plan WHERE asp_id = api_fk_asp_id LIMIT 1) AS subscription_name,
				
				IF(TIMESTAMPDIFF(SECOND, artist_payment_information.api_created, now())<=86400,1,0) AS spent_time,

				(SELECT cr_id FROM client_request WHERE api_fk_artist_id = cr_fk_u_mua_id LIMIT 1) AS booking
				
				FROM artist_payment_information) AS artist_payment_information 
				WHERE api_status = 'true' AND api_transaction_details != 'free'
				AND TIMESTAMPDIFF( MONTH , artist_payment_information.api_created, DATE( NOW( ) ) ) <1 ";

		if(isset($param['search']) && $param['search']){
			$sql .= "AND (
					 artist_payment_information.api_paid_amount LIKE '%".$param['search']."%'
					 OR
					 artist_payment_information.api_service_amout LIKE '%".$param['search']."%'
					 OR
					 artist_payment_information.artist_name LIKE '%".$param['search']."%'
					)";
		}

		$sql .= " GROUP BY api_fk_artist_id";

		$sql .= " ORDER BY `api_id` DESC LIMIT $page , 10";

		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get transaction list
	* @param null
	* @return $result
	*/
	public function get_transaction_count($param){
		$page = isset($param['page'])?$param['page']:0;

		$sql = "SELECT * FROM 
				(SELECT * ,
				(SELECT ap_profile_name FROM artist_profile WHERE ap_fk_u_id = api_fk_artist_id LIMIT 1)
				AS artist_name, 
				(SELECT asp_name FROM artist_subscription_plan WHERE asp_id = api_fk_asp_id LIMIT 1) AS subscription_name,
				
				IF(TIMESTAMPDIFF(SECOND, artist_payment_information.api_created, now())<=86400,1,0) AS spent_time,

				(SELECT cr_id FROM client_request WHERE api_fk_artist_id = cr_fk_u_mua_id LIMIT 1) AS booking
				FROM artist_payment_information) AS artist_payment_information 
				WHERE api_status = 'true' AND api_transaction_details != 'free'
				AND TIMESTAMPDIFF( MONTH , artist_payment_information.api_created, DATE( NOW( ) ) ) <1 ";

		if(isset($param['search']) && $param['search']){
			$sql .= "AND (
					 artist_payment_information.api_paid_amount LIKE '%".$param['search']."%'
					 OR
					 artist_payment_information.api_service_amout LIKE '%".$param['search']."%'
					 OR
					 artist_payment_information.artist_name LIKE '%".$param['search']."%'
					)";
		}

		$sql .= " GROUP BY api_fk_artist_id";

		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->rowCount();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get transaction details
	* @param $param
	* @return $result
	*/
	public function get_transaction_details($param){
		$sql = "SELECT api_transaction_details FROM artist_payment_information WHERE api_id = :api_id";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(':api_id',$param['id']);
		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get transaction list
	* @param null
	* @return $result
	*/
	public function get_client_transaction($param){
		
		$page = isset($param['page'])?$param['page']:0;

		$sql = "SELECT *, ROUND((client_payment_information.cpi_pay_to_artist), 2) AS cpi_pay_to_artist  FROM (SELECT *, 

				(SELECT ap_profile_name FROM artist_profile WHERE ap_fk_u_id = cpi_fk_artist_id LIMIT 1) AS artist_name,

				(SELECT u_name FROM user WHERE u_id = cpi_fk_client_id LIMIT 1) AS client_name,

				(SELECT cm_duration as ca_duration FROM category_master WHERE cm_id = (SELECT ac_fk_cm_id FROM artist_category WHERE ac_id = (SELECT cr_fk_ac_id FROM client_request WHERE cr_id = (SELECT cab_fk_cr_id FROM client_artist_booking WHERE cab_id = cpi_fk_cab_id LIMIT 1) LIMIT 1) LIMIT 1) LIMIT 1) AS duration,

				(SELECT cm_name FROM category_master WHERE cm_id = (SELECT ac_fk_cm_id FROM artist_category WHERE ac_id = (SELECT cr_fk_ac_id FROM client_request WHERE cr_id = (SELECT cab_fk_cr_id FROM client_artist_booking WHERE cab_id = cpi_fk_cab_id LIMIT 1) LIMIT 1) LIMIT 1) LIMIT 1) AS category_name

				FROM client_payment_information ) AS client_payment_information ";

		$where = '';
					
		if(isset($param['search']) && $param['search']){

			$where = "WHERE (
					 client_payment_information.cpi_service_amout LIKE :search
					 OR
					 client_payment_information.artist_name LIKE :search
					 OR
					 client_payment_information.client_name LIKE :search
					 OR
					 client_payment_information.duration LIKE :search
					 OR
					 client_payment_information.category_name LIKE :search
					 OR
					 client_payment_information.cpi_created LIKE :search
					 OR
					 client_payment_information.cpi_commission LIKE :search
					)";
		}

		if(isset($param['start_date']) && $param['start_date'] && isset($param['end_date']) && $param['end_date']){
						
			if($where == ''){
				$where .= " WHERE ";
			}
			else{
				$where .= " AND";
			}
			$where .= " (client_payment_information.cpi_created BETWEEN :start_date AND :end_date)";
		}
		$sql .=$where;

		$sql .= " ORDER BY `cpi_id` DESC LIMIT $page , 10";

		$stmt = $this->conn->prepare($sql);

		if(isset($param['search']) && $param['search']){
			$search = "%".$param['search']."%";
			$stmt->bindParam(':search',$search);
		}


		if(isset($param['start_date']) && $param['start_date'] && isset($param['end_date']) && $param['end_date']){
			$stmt->bindParam(':start_date',$param['start_date']);
			$stmt->bindParam(':end_date',$param['end_date']);
		}

		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get transaction list
	* @param null
	* @return $result
	*/
	public function get_client_transaction_count($param){

		$sql = "SELECT *  FROM (SELECT *,

				(SELECT ap_profile_name FROM artist_profile WHERE ap_fk_u_id = cpi_fk_artist_id LIMIT 1) AS artist_name,

				(SELECT u_name FROM user WHERE u_id = cpi_fk_client_id LIMIT 1) AS client_name,

				(SELECT cm_duration as ca_duration FROM category_master WHERE cm_id = (SELECT ac_fk_cm_id FROM artist_category WHERE ac_id = (SELECT cr_fk_ac_id FROM client_request WHERE cr_id = (SELECT cab_fk_cr_id FROM client_artist_booking WHERE cab_id = cpi_fk_cab_id LIMIT 1) LIMIT 1) LIMIT 1) LIMIT 1) AS duration,

				(SELECT cm_name FROM category_master WHERE cm_id = (SELECT ac_fk_cm_id FROM artist_category WHERE ac_id = (SELECT cr_fk_ac_id FROM client_request WHERE cr_id = (SELECT cab_fk_cr_id FROM client_artist_booking WHERE cab_id = cpi_fk_cab_id LIMIT 1) LIMIT 1) LIMIT 1) LIMIT 1) AS category_name

				FROM client_payment_information ) AS client_payment_information ";

		$where = '';
					
		if(isset($param['search']) && $param['search']){

			$where = "WHERE (
					 client_payment_information.cpi_service_amout LIKE :search
					 OR
					 client_payment_information.artist_name LIKE :search
					 OR
					 client_payment_information.client_name LIKE :search
					 OR
					 client_payment_information.duration LIKE :search
					 OR
					 client_payment_information.category_name LIKE :search
					 OR
					 client_payment_information.cpi_created LIKE :search
					 OR
					 client_payment_information.cpi_commission LIKE :search
					)";
		}

		if(isset($param['start_date']) && $param['start_date'] && isset($param['end_date']) && $param['end_date']){
						
			if($where == ''){
				$where .= " WHERE ";
			}
			else{
				$where .= " AND";
			}
			$where .= " (client_payment_information.cpi_created BETWEEN :start_date AND :end_date)";
		}


		$sql .=$where;



		$stmt = $this->conn->prepare($sql);

		if(isset($param['search']) && $param['search']){
			$search = "%".$param['search']."%";
			$stmt->bindParam(':search',$search);
		}


		if(isset($param['start_date']) && $param['start_date'] && isset($param['end_date']) && $param['end_date']){
			$stmt->bindParam(':start_date',$param['start_date']);
			$stmt->bindParam(':end_date',$param['end_date']);
		}

		$stmt->execute();
		$result = $stmt->rowCount();
		$stmt->closeCursor();
		return $result;
	}


	/**
	* Get get_mua_payment_info list
	* @param null
	* @return $result
	*/
	public function get_mua_payment_info($param){

		$page = isset($param['page'])?$param['page']:0;


		$a_date = date("Y-m-d H:i:s");   
		$a = date("Y-m-1", strtotime($a_date));
	
		$b = date("Y-m-t", strtotime($a_date));
		
		$date  = date('Y-m-d');
		$like = "";
		if(isset($param['search']) && $param['search']){
				$like .= " AND (
						 artist_profile.ap_profile_name LIKE '%".$param['search']."%'
						 OR
						 user.u_name LIKE '%".$param['search']."%'
						 OR
						 user.u_email LIKE '%".$param['search']."%'
						) ";
			}

			$sql =  "SELECT * ,

				(SELECT SUM(cpi_pay_to_artist) FROM client_payment_information WHERE cpi_fk_artist_id = user.u_id AND cpi_status = 'true' LIMIT 1) AS total,

				(SELECT SUM(aph_payment_amount) FROM admin_payment_history_to_user WHERE aph_fk_u_id = user.u_id LIMIT 1) AS last_payment,

				(SELECT aph_updated FROM admin_payment_history_to_user WHERE aph_fk_u_id = user.u_id ORDER BY aph_id DESC LIMIT 1) AS last_payment_date,

				(SELECT SUM(cpi_paid_amount) FROM client_payment_information WHERE cpi_fk_artist_id = user.u_id AND cpi_created BETWEEN '".$a."' AND '".$b."' LIMIT 1) AS monthly,

				(SELECT SUM(cpi_paid_amount) FROM client_payment_information WHERE cpi_fk_artist_id = user.u_id AND cpi_created BETWEEN '".$param['start_date']."' AND '".$param['end_date']."' LIMIT 1) AS weekly,

				(SELECT SUM(cpi_paid_amount) FROM client_payment_information WHERE cpi_created BETWEEN '".$a."' AND '".$b."' LIMIT 1) AS grand_monthly,

				(SELECT SUM(cpi_paid_amount) FROM client_payment_information WHERE cpi_created BETWEEN '".$param['start_date']."' AND '".$param['end_date']."' LIMIT 1) AS grand_weekly,

				(SELECT SUM(cpi_pay_to_artist) FROM client_payment_information WHERE cpi_status = 'true' LIMIT 1) AS grand_total
		
				
			 	FROM user
				
				INNER JOIN artist_profile
		        ON
		        artist_profile.ap_fk_u_id = user.u_id

		        INNER JOIN user_account_info
		        ON
		        user_account_info.uai_fk_u_id = user.u_id

				WHERE u_type = 'mua' AND u_status = 'true'";

				$sql.= $like;
	

			$sql .= " ORDER BY `u_id` DESC LIMIT $page , 10";

		

		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}



	/**
	* Get transaction list
	* @param null
	* @return $result
	*/
	public function get_mua_payment_info_count($param){

		$like = "";
		if(isset($param['search']) && $param['search']){
				$like .= " AND (
						 artist_profile.ap_profile_name LIKE '%".$param['search']."%'
						 OR
						 user.u_name LIKE '%".$param['search']."%'
						 OR
						 user.u_email LIKE '%".$param['search']."%'
						) ";
			}

		$sql =  "SELECT *

			 	FROM user
				
				INNER JOIN artist_profile
		        ON
		        artist_profile.ap_fk_u_id = user.u_id

		        INNER JOIN user_account_info
		        ON
		        user_account_info.uai_fk_u_id = user.u_id

				WHERE u_type = 'mua' AND u_status = 'true' ";



			$sql .= $like;

		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->rowCount();
		$stmt->closeCursor();
		return $result;
	}


	/**
	* Get artist profile
	* @param $param
	* @return $result
	*/
	public function get_paid_data($param){
		$sql = "SELECT * FROM user
				INNER JOIN artist_profile
				ON
				artist_profile.ap_fk_u_id = user.u_id


			 WHERE `u_id` = :u_id";

		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":u_id",$param['id']);
		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}



	public function add_paid_data($param) {

		$param = (object)$param;
		$date = date("Y-m-d");

		$sql = "INSERT INTO admin_payment_history_to_user (aph_fk_u_id, aph_payment_type, aph_payment_via, aph_payment_amount, aph_payment_comment, aph_created, aph_bank_account) values(:aph_fk_u_id, :aph_payment_type, :aph_payment_via, :aph_payment_amount, :aph_payment_comment, :aph_created, :aph_bank_account)";
            $stmt = $this->conn->prepare($sql);

    	$stmt->bindParam(':aph_fk_u_id', $param->mua_id);
        $stmt->bindParam(':aph_payment_type', $param->js_payment_type);
        $stmt->bindParam(':aph_payment_via', $param->js_payment_via);
        $stmt->bindParam(':aph_payment_amount', $param->js_amount);
        $stmt->bindParam(':aph_payment_comment', $param->js_comment);
        $stmt->bindParam(':aph_created', $date);
        $stmt->bindParam(':aph_bank_account', $param->js_account);
        
        $result = $stmt->execute();
        $id = '';
        if ($result) {
            $id = $this->conn->lastInsertId();
        }
		$stmt->closeCursor();
		return $id;

    }


    public function payment_received($user_id){

    	$sql = "SELECT * FROM admin_payment_history_to_user WHERE aph_fk_u_id = :aph_fk_u_id";

		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":aph_fk_u_id",$user_id);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;

    }


    public function total_payment_received($user_id ,$lang){

    	$sql = "SELECT 

            client_payment_information.cpi_fk_client_id AS client_id,

            user.u_name AS client_name,

            client_payment_information.cpi_booking_time_rate_per_mile AS rpm,

            client_payment_information.cpi_paid_amount AS ammount_recvied,
            client_payment_information.cpi_service_amout AS service_amout,
            client_payment_information.cpi_rebate_amount AS rebate_amount,
            client_payment_information.cpi_total_rebate_amount AS total_rebate_amount,
            client_payment_information.cpi_payment_mode AS payment_mode,
            client_payment_information.cpi_created AS ammount_recvied_date,
            
            client_request.cr_id AS services_id,

            category_attributes.ca_name AS services_name, 
            category_attributes.ca_description AS services_description,

            (
             SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = client_payment_information.cpi_fk_client_id AND (image_master.im_action = 'client' OR image_master.im_action = 'client') ORDER BY image_master.im_id DESC LIMIT 1
            ) AS client_image,

            (SELECT SUM(client_payment_information.cpi_paid_amount) FROM client_payment_information WHERE cpi_fk_artist_id = '".$user_id."' LIMIT 1) 
        AS mua_total


         FROM client_payment_information

         INNER JOIN client_profile
         ON
         client_profile.cp_fk_u_id = client_payment_information.cpi_fk_client_id

         INNER JOIN user
         ON
         user.u_id = client_payment_information.cpi_fk_client_id

         INNER JOIN client_artist_booking
         ON
         client_artist_booking.cab_id = client_payment_information.cpi_fk_cab_id

         INNER JOIN client_request
         ON
         client_request.cr_id = client_artist_booking.cab_fk_cr_id

         INNER JOIN artist_category
        ON
        artist_category.ac_id = client_request.cr_fk_ac_id
        INNER JOIN category_master
        ON
        category_master.cm_id = artist_category.ac_fk_cm_id
        INNER JOIN category_attributes
        ON
        category_attributes.ca_fk_cm_id = category_master.cm_id
        AND
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name='".$lang."')

         WHERE cpi_fk_artist_id = '".$user_id."' AND cpi_status = 'true' ORDER BY cpi_id DESC";    
        
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cpi_fk_artist_id', $user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;

    }


    public function deleted_subscription($id){
    	$sql = "UPDATE artist_payment_information SET api_status = 'false', api_updated = NOW(), api_admin_status = 'true' WHERE api_id = :api_id";
            $stmt = $this->conn->prepare($sql);

    	$stmt->bindParam(':api_id', $id);
        $result = $stmt->execute();
        $stmt->closeCursor();
		return;
    }


    /**
	* @Function get_artist_transaction
	* @param null
	* @return $result
	*/
	public function get_artist_transaction($param){
		
		$sql = "SELECT *,

		(SELECT cr_commission FROM client_request WHERE client_request.cr_id = client_payment_information.cpi_fk_cab_id LIMIT 1) AS 
		cr_commission,
		(SELECT cr_com_grand_price FROM client_request WHERE client_request.cr_id = client_payment_information.cpi_fk_cab_id LIMIT 1) AS 
		cr_com_grand_price,
		CONCAT('', FORMAT((client_payment_information.cpi_pay_to_artist), 2)) AS q 
		FROM client_payment_information WHERE cpi_fk_artist_id = '".$param['id']."' AND cpi_status = 'true'";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

}