<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Client_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }

    /**
     * Get client list
     * @param $param 
     * @return $result
     */
    public function get_client_list($param){
        $page = $param['page'];

        $sql = "SELECT *,
			(SELECT count(*) FROM used_referral WHERE ur_r_id=(SELECT ur_id FROM used_referral WHERE ur_fk_u_id=u_id)) AS earned_referral,
			(SELECT count(*) FROM used_referral WHERE ur_used_status='true' AND ur_r_id=(SELECT ur_id FROM used_referral WHERE ur_fk_u_id=u_id)) AS used_referral,
			(SELECT cp_city FROM client_profile WHERE cp_fk_u_id = u_id LIMIT 1) AS city FROM `user` WHERE `u_type` = 'client'";
		
		
        $sql = "SELECT referral.r_id, user.u_id, user. u_status, referral.r_id, referral.r_rebate, referral.r_duration, referral.r_code, user.u_name, user.u_username, user.u_email, user.u_phone,

            /*(SELECT count(*) FROM used_referral WHERE used_referral.ur_r_id = referral.r_id) AS used_referral,*/
            /*(SELECT count(*) FROM used_referral WHERE ur_paid = 'true' AND ur_r_id = referral.r_id) AS earned_referral,*/
            (SELECT cp_city FROM client_profile WHERE cp_fk_u_id = u_id LIMIT 1) AS city,

             (SELECT COUNT(*) FROM used_referral WHERE ur_r_id  = (SELECT r_id FROM referral WHERE r_fk_u_id = user.u_id LIMIT 1) LIMIT 1) AS used_referral,

            (SELECT SUM(IF(u_type='mua',(SELECT 1 FROM client_request WHERE cr_fk_u_mua_id = uu.u_id AND cr_status = 'finished' LIMIT 1),(SELECT 1 FROM client_request WHERE cr_fk_u_client_id = uu.u_id AND cr_status = 'finished' LIMIT 1))) AS earned
                FROM user AS uu WHERE uu.u_id IN (SELECT ur_fk_u_id FROM used_referral WHERE ur_r_id  = (SELECT r_id FROM referral WHERE r_fk_u_id = user.u_id LIMIT 1))) AS earned_referral

            FROM user 
            INNER JOIN referral
            ON
            referral.r_fk_u_id = user.u_id
            WHERE user.u_type='client' ";
		
        if(isset($param['search']) && $param['search']){
            $sql .= "AND (u_name LIKE '%".$param['search']."%'
                    OR 
                    u_email LIKE '%".$param['search']."%' 
                    OR 
                    u_phone LIKE '%".$param['search']."%'
                    )";
        }

        $sql .= " ORDER BY `u_id` DESC LIMIT $page , 25";
		
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }


    public function get_client_used_referrel_modal($r_id){
        
        /*$sql = "SELECT *,
        (SELECT r_rebate FROM referral WHERE r_id = ur_r_id LIMIT 1) AS r_rebate,
        (SELECT u_email FROM user WHERE u_id = ur_fk_u_id LIMIT 1) AS u_email,
        (SELECT client_request.cr_id FROM client_request WHERE client_request.cr_fk_u_client_id = used_referral.ur_fk_u_id AND cr_status = 'finished' LIMIT 1) AS booking

         FROM used_referral WHERE ur_r_id = '".$id."'";*/


        $sql = "SELECT *,

        IF(u_type='mua',(SELECT COUNT(*) FROM client_request WHERE cr_fk_u_mua_id = u_id AND cr_status = 'finished'),(SELECT COUNT(*) FROM client_request WHERE cr_fk_u_client_id = u_id AND cr_status = 'finished')) AS booking,
        (SELECT r_rebate FROM referral WHERE r_id = used_referral.ur_r_id LIMIT 1) AS r_rebate

        FROM used_referral
        INNER JOIN user
        ON 
        user.u_id = used_referral.ur_fk_u_id


        WHERE ur_r_id = '".$r_id."'";


        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }
    

    /**
     * Get client list
     * @param $param 
     * @return $result
     */
    public function get_client_count($param){

        $sql = "SELECT * FROM `user` WHERE `u_type` = 'client'";

        if(isset($param['search']) && $param['search']){
            $sql .= "AND (u_name LIKE '%".$param['search']."%'
                    OR 
                    u_email LIKE '%".$param['search']."%' 
                    OR 
                    u_phone LIKE '%".$param['search']."%'
                    )";
        }

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->rowCount();
        $stmt->closeCursor();

        return $result;
    }

    /**
     * Client change status
     * @param $param
     * @return null
     */
    public function client_change_status($param){
        $sql = "UPDATE user SET u_status = :u_status WHERE u_id = :u_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':u_id',$param['id']);
        $stmt->bindParam(':u_status',$param['status']);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }

    /**
     * Get client profile
     * @param $param
     * @return $result
     */
    public function get_client_profile($param){
        $sql = "SELECT cp_skin_color,cp_phone,cp_about,cp_email,cp_city,cp_address,cp_language,cp_status,(SELECT u_name FROM user WHERE u_id = :cp_fk_u_id LIMIT 1) AS u_name,(SELECT im_image_name FROM image_master WHERE im_fk_u_id = :cp_fk_u_id AND im_action = 'client') AS image FROM client_profile WHERE cp_fk_u_id = :cp_fk_u_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cp_fk_u_id',$param['id']);
        $stmt->execute();
        $result = $stmt->fetch();
        $stmt->closeCursor();
        return $result;
    }
}