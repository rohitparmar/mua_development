<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Client_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }
	
	public function get_client_location($user_id){
		$sql = "SELECT cp_lat AS client_lat, cp_long AS client_long FROM client_profile WHERE cp_fk_u_id = :cp_fk_u_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cp_fk_u_id',$user_id);
        if($stmt->execute()){
             $user = $stmt->fetch();
             $stmt->closeCursor();
             return (object)array('lat'=>$user['client_lat'], 'long'=>$user['client_long']);
        }else{
            return (object)array();
        }
	}
    /**
     * Creating new general information
     * @param String $name, $phone, $website, $about, $email, $city, $address, $rate, $language, $zip 
     */
    public function create_client_profile($param){


        
        if(!$this->get_client_profile($param->user_id)){
            
            $sql = "INSERT INTO client_profile (cp_fk_u_id, cp_skin_color, cp_image, cp_phone, cp_about, cp_email, cp_city, cp_address, cp_language, cp_zipcode) VALUES(:cp_fk_u_id, :cp_skin_color, :cp_image, :cp_phone, :cp_about, :cp_email, :cp_city, :cp_address, :cp_language, :cp_zipcode)";
            $stmt = $this->conn->prepare($sql);
            
            $stmt->bindParam(':cp_fk_u_id',$param->user_id);
            $stmt->bindParam(':cp_skin_color',$param->skin_color);
            $stmt->bindParam(':cp_image',$param->cp_image);
            $stmt->bindParam(':cp_phone',$param->phone);
            $stmt->bindParam(':cp_about',$param->about);
            $stmt->bindParam(':cp_email',$param->email);
            $stmt->bindParam(':cp_city',$param->city);
            $stmt->bindParam(':cp_address',$param->address);
            $stmt->bindParam(':cp_language',$param->language);
            $stmt->bindParam(':cp_zipcode',$param->zip);
            
            $result = $stmt->execute();

             $stmt->closeCursor();

            // Check for successful insertion
            if ($result) {
                // information successfully inserted
                return 1;
            } else {
                // Failed to create user
                return 0;
            }
        }
        else{
            return 2;
        }   
   }


    
/**get_client_profile
* @param String $user_id
*/
    public function get_client_profile($user_id){
   
        $sql = "SELECT im.im_id, cp_id, cp_skin_color, user.u_name AS cp_name, cp_phone, cp_about, cp_email, cp_city, cp_state, cp_address, cp_language, im.im_image_name,im.im_action, cp_zipcode, cp_status, cp_long, cp_lat,
          (
            SELECT ROUND(AVG(rr_rating), 2) FROM rating_remarks WHERE rr_review_by = 'client' AND rr_status = 'true' AND rr_fk_client_id = :cp_fk_u_id
          ) 
          AS avg_rating, u_email, u_phone, cp_profile_completed

         FROM client_profile AS CP
        INNER JOIN user
        ON
        user.u_id = CP.cp_fk_u_id

         LEFT JOIN image_master AS im
         ON
         im.im_fk_u_id = CP.cp_fk_u_id
         AND
         im.im_status = 'true'
         AND
         im.im_deleted = 'false'
       
        WHERE cp_fk_u_id = :cp_fk_u_id AND cp_status = 'true' ORDER BY im.im_updated DESC LIMIT 1";



        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cp_fk_u_id',$user_id);
        if($stmt->execute())
        {
             $user = $stmt->fetch();
             $stmt->closeCursor();
             //print_r($user); die;
             return $user;
        }else{
            return NULL;
        }
       
    }


/**update_client_profile
  * @param String $param
*/

  public function update_client_profile($param) {
   // print_r($param); die;

	if($param->lat!="" && $param->long!=""){
    	$sql = "UPDATE client_profile SET cp_skin_color = :cp_skin_color, cp_image = :cp_image, cp_phone = :cp_phone, cp_about = :cp_about, cp_email = :cp_email, cp_city = :cp_city, cp_state = :cp_state, cp_address = :cp_address, cp_language = :cp_language, cp_zipcode = :cp_zipcode, cp_profile_completed = :cp_profile_completed  WHERE cp_fk_u_id = :cp_fk_u_id";

      
	}else{
		$sql = "UPDATE client_profile SET cp_skin_color = :cp_skin_color, cp_image = :cp_image, cp_phone = :cp_phone, cp_about = :cp_about, cp_email = :cp_email, cp_city = :cp_city, cp_state = :cp_state, cp_address = :cp_address, cp_language = :cp_language, cp_zipcode = :cp_zipcode, cp_profile_completed = :cp_profile_completed  WHERE cp_fk_u_id = :cp_fk_u_id";
    
	}

  
    
    $stmt = $this->conn->prepare($sql);
            
    $stmt->bindParam(':cp_skin_color',$param->skin_color);
    $stmt->bindParam(':cp_image',$param->cp_image);
    $stmt->bindParam(':cp_phone',$param->phone);
    $stmt->bindParam(':cp_about',$param->about);
    $stmt->bindParam(':cp_email',$param->email);
    $stmt->bindParam(':cp_city',$param->city);
    $stmt->bindParam(':cp_state',$param->state);
    $stmt->bindParam(':cp_address',$param->address);
    $stmt->bindParam(':cp_language',$param->language);
    $stmt->bindParam(':cp_zipcode',$param->zip);
    $stmt->bindParam(':cp_fk_u_id',$param->user_id);

  	if($param->lat!="" && $param->long!=""){
  		//$stmt->bindParam(':cp_lat',$param->lat);
  		//$stmt->bindParam(':cp_long',$param->long);
  	}
    $stmt->bindParam(':cp_profile_completed',$param->status);
    
        
    $stmt->execute();

    $num_affected_rows = $stmt->rowCount();
    $stmt->closeCursor();
    return $num_affected_rows > 0;
  }   



/** insert the data rating_remarks table in create_rating
  * @param 
*/

   public function create_rating($param){

    if(!$this->check_rating($param->user_id, $param->mua_id, $param->request_id, $param->user_type)){
        $param->user_type = strtolower($param->user_type);

        $sql = "INSERT INTO rating_remarks(rr_fk_cab_id, rr_fk_client_id, rr_fk_mua_id, rr_rating, rr_remarks, rr_review_by) values(:rr_fk_cab_id, :rr_fk_client_id, :rr_fk_mua_id, :rr_rating, :rr_remarks, :rr_review_by) ";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':rr_fk_cab_id',$param->request_id);
        $stmt->bindParam(':rr_fk_client_id',$param->user_id);
        $stmt->bindParam(':rr_fk_mua_id',$param->mua_id);
        $stmt->bindParam(':rr_rating',$param->rating);
        $stmt->bindParam(':rr_remarks',$param->review);
        $stmt->bindParam(':rr_review_by',$param->user_type);

        $result = $stmt->execute();
        $stmt->closeCursor();

        if($result){
            return 1;
        }
        else{
            return 0;
        }
      }else{
         return 2;
      }
   }
    


 public function check_rating($user_id, $mua_id, $request_id, $user_type){
  $user_type = strtolower($user_type);
  $sql = "SELECT * FROM rating_remarks WHERE rr_status = 'true' AND rr_fk_client_id = :rr_fk_client_id AND rr_fk_mua_id = :rr_fk_mua_id AND rr_fk_cab_id = :rr_fk_cab_id AND rr_review_by = :rr_review_by";

  $stmt = $this->conn->prepare($sql);
  $stmt->bindParam(':rr_fk_client_id',$user_id);
  $stmt->bindParam(':rr_fk_mua_id',$mua_id);
  $stmt->bindParam(':rr_fk_cab_id',$request_id);
  $stmt->bindParam(':rr_review_by',$user_type);
  
  if($stmt->execute()){
    $result = $stmt->fetchAll();
    $stmt->closeCursor();
    return $result;
  }else{
    return NULL;
  }

 }



/** insert the data rating_remarks table in create_rating
  * @param $user_id
*/
    public function get_rating($user_id, $lang){
       

          $sql = "SELECT category_attributes.ca_name AS category_name, ap_fk_u_id AS mua_id, ap_profile_name AS client_name, im_image_name AS artist_profile_image, rr_rating AS rating, rr_remarks AS reviews FROM rating_remarks

          INNER JOIN artist_profile
          ON
          artist_profile.ap_fk_u_id = rating_remarks.rr_fk_mua_id

          INNER JOIN client_artist_booking
          ON
          client_artist_booking.cab_id = rating_remarks.rr_fk_cab_id

          INNER JOIN client_request
          ON
          client_artist_booking.cab_fk_cr_id = client_request.cr_id

          INNER JOIN artist_category
          ON
          artist_category.ac_id = client_request.cr_fk_ac_id

          INNER JOIN category_master
          ON
          category_master.cm_id = artist_category.ac_fk_cm_id
          INNER JOIN category_attributes
          ON
          category_attributes.ca_fk_cm_id = category_master.cm_id
          AND
          category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lang)

          LEFT JOIN image_master 
          ON
          image_master.im_fk_u_id = rating_remarks.rr_fk_mua_id
          AND
          image_master.im_action='artist'
          AND
          image_master.im_status='true'
          AND
          image_master.im_deleted='false'

           WHERE rating_remarks.rr_fk_client_id = :rr_fk_client_id AND rating_remarks.rr_status = 'true' AND rr_review_by = 'mua'";

      
      $stmt = $this->conn->prepare($sql);
      $stmt->bindParam(":rr_fk_client_id", $user_id);
      $stmt->bindParam(":lang", $lang);
      

      if($stmt->execute()){

          $result = $stmt->fetchAll();
          //print_r($result); die;
          $stmt->closeCursor();
          return $result;

      }else{
          return NULL;
      }
    } 

    public function get_mua_clients_rating($mua_id, $cab_id){
       $sql = "SELECT a.cm_name AS category_name, a.u_id AS client_id, a.u_name AS client_name, a.im_image_name AS client_profile_image, a.rr_rating AS rating, a.rr_remarks AS reviews FROM (SELECT user.u_id, image_master.im_id, rating_remarks.rr_rating, rating_remarks.rr_remarks, user.u_name, image_master.im_image_name, category_master.cm_name
          FROM rating_remarks
          
          INNER JOIN user
          ON
          user.u_id = rating_remarks.rr_fk_client_id

          INNER JOIN client_artist_booking
          ON
          client_artist_booking.cab_id = rating_remarks.rr_fk_cab_id

          INNER JOIN client_request
          ON
          client_artist_booking.cab_fk_cr_id = client_request.cr_id

          INNER JOIN artist_category
          ON
          artist_category.ac_id = client_request.cr_fk_ac_id

          INNER JOIN category_master
          ON
          category_master.cm_id = artist_category.ac_fk_cm_id

          LEFT JOIN image_master 
          ON
          image_master.im_fk_u_id = rating_remarks.rr_fk_client_id
          AND
          image_master.im_action='client'
          AND
          image_master.im_status='true'
          AND
          image_master.im_deleted='false'
           
          WHERE rating_remarks.".(strtolower($user_type)=='mua'?'rr_fk_mua_id':'rr_fk_client_id')."rr_user_id = :rr_fk_mua_id AND rating_remarks.rr_status = 'true' 

          ORDER BY image_master.im_updated DESC) AS a 
          GROUP BY a.u_id
          ORDER BY a.im_id ASC";
      
      $stmt = $this->conn->prepare($sql);
      $stmt->bindParam(":rr_user_id", $user_id);

      if($stmt->execute()){

          $result = $stmt->fetchAll();
          $stmt->closeCursor();
          return $result;

      }else{
          return NULL;
      }
    } 

/*
 * function get_nearby
  * @param $user_id
*/
    public function get_nearby($user_id, $distance=false, $cat_array= fasle, $search = false, $skintype=false, $speakinglang=false, $lang='eng', $_start = 0, $_limit = false){
      	$if_verified = ""; //"AND user.u_varified='accepted'";
        $WHERE = '';
        $WHERE_NAME = '';
        $WHERE_SKINTYPE = '';
        $WHERE_LANGUAGE = '';

        $GWHERE = 1;
		    $LIMIT = ($_limit) ? ' LIMIT '.$_start.', '.$_limit : '';
        
        if($cat_array){
          $WHERE = "INNER JOIN artist_category 
                    ON
                    artist_category.ac_fk_u_id = artist_profile.ap_fk_u_id AND artist_category.ac_fk_cm_id IN(".$cat_array.") ";
        }

        /*
        if($skintype && $cat_array){
          $WHERE_SKINTYPE = " INNER JOIN category_master
                             ON
                             artist_category.ac_fk_cm_id=category_master.cm_id
                             INNER JOIN category_attributes
                             ON
                             category_attributes.ca_fk_cm_id=category_master.cm_id
                             AND
                             category_attributes.ca_skintone IN (".$skintype.")
                             AND
                             category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name='".$lang."')
                             ";
        }
        */

        
        if($search){
          $GWHERE = " artist_profile.ap_profile_name LIKE '%".$search."%'";
          $GWHERE .= " OR user.u_name LIKE '%".$search."%' AND user.u_type='mua'";
		  $GWHERE = " (artist_profile.ap_profile_name LIKE '%".$search."%' OR user.u_name LIKE '%".$search."%' OR user.u_username LIKE '%".$search."%' OR user.u_email LIKE '%".$search."%') AND user.u_type='mua' ";
        }

        if($speakinglang){

          $WHERE_LANGUAGE = " artist_profile.ap_language IN (".$speakinglang.")"; 
          
          if($GWHERE<>1){
            $GWHERE = ' ('.$GWHERE. ' AND '. $WHERE_LANGUAGE. ')';
          }else{
            $GWHERE = $WHERE_LANGUAGE;
          }
        }
        

        
        $ARIAL_DISTANCE = $distance; // ($distance)?$distance : ARIAL_DISTANCE;
        $sql = "SELECT a.ap_language, a.ap_fk_u_id AS artist_id, a.ap_profile_name AS userName, IF(a.ap_lat='', 0, a.ap_lat) AS ap_lat, IF(a.ap_long='', 0, a.ap_long) AS ap_long, a.avgRating AS avgRating, a.reviews, a.distance, IF(a.favorite='true', 1, 0) AS favorite, a.artist_profile_image, a.u_name

              FROM 
                
                (
                  SELECT ap_fk_u_id, ap_profile_name, ap_lat, ap_long, ap_language, u_name,
                  (
                    ROUND(ARIAL_DISTANCE((SELECT cp_lat FROM client_profile WHERE cp_fk_u_id = :cp_fk_u_id), ap_lat,   
                    (SELECT cp_long FROM client_profile WHERE cp_fk_u_id = :cp_fk_u_id LIMIT 1), ap_long, 'M'), 2)

                  ) AS distance,
                  (
                   SELECT ROUND(AVG(rating_remarks.rr_rating),2) FROM rating_remarks 
                   WHERE rating_remarks.rr_fk_mua_id = ap_fk_u_id AND rating_remarks.rr_status='true' AND rating_remarks.rr_review_by='client' 
                   )  AS avgRating,

                  (
                   SELECT COUNT(rating_remarks.rr_rating) FROM rating_remarks 
                   WHERE rating_remarks.rr_fk_mua_id = ap_fk_u_id AND rating_remarks.rr_status='true' AND rating_remarks.rr_review_by='client'
                  )  AS reviews,

                  (
                   SELECT IF(cf_is_favorite='true', 'true','false') AS cf_is_favorite FROM client_favorite 
                   WHERE client_favorite.cf_fk_client_id = :cp_fk_u_id AND client_favorite.cf_fk_mua_id = ap_fk_u_id ORDER BY cf_id DESC LIMIT 1
                  ) AS favorite,

                  (
                    SELECT im_image_name FROM image_master 
                    WHERE image_master.im_fk_u_id = artist_profile.ap_fk_u_id 
                    AND im_action = 'artist' AND im_status = 'true' AND im_deleted = 'false' 
                    ORDER BY im_id DESC LIMIT 1
                  ) AS artist_profile_image

               
                FROM artist_profile 

                INNER JOIN user 
                ON
                user.u_id = artist_profile.ap_fk_u_id
        				AND
        				user.u_varified='accepted' AND user.u_status = 'true'

                ###catsearch### 

                ###mua-skintype###

                WHERE artist_profile.ap_status ='true' ".$if_verified." AND ###mua-name-language-search###
                ) AS a ";
                
                //$sql = " WHERE distance <= :ARIAL_DISTANCE GROUP BY a.ap_fk_u_id ORDER BY distance ASC";
				if($ARIAL_DISTANCE){$sql .= " WHERE distance <= ".$ARIAL_DISTANCE;}
				$sql .= " ORDER BY CASE  WHEN (distance != 0) THEN 0 ELSE 1 END ASC, distance ASC ".$LIMIT;
				
				 

        $sql = str_replace('###catsearch###', $WHERE, $sql);
        $sql = str_replace('###mua-name-language-search###', $GWHERE, $sql);
        //$sql = str_replace('###mua-skintype###', $WHERE_SKINTYPE, $sql);
		
		
		
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cp_fk_u_id', $user_id);



        if($stmt->execute()){
			     $result = $stmt->fetchAll();
           $stmt->closeCursor();
           return $result;
        }
        else{
           return NULL;  
        }
    } 


/*
 * function update_user_lat_long
 * @param $user_id, $user_type, $latitude, $longitude
*/
    public function update_user_lat_long($user_id, $user_type, $latitude, $longitude){
       // print_r($latitude); die;
       /* print_r($latitude); 
        print_r($longitude); 
        die;*/
        /*
        $table = $user_type=='mua'?'artist_profile':'client_profile';
        $latCol = $user_type=='mua'?'ap_lat':'cp_lat';
        $longCol = $user_type=='mua'?'ap_long':'cp_long';
        $user_id_Col = $user_type=='mua'?'ap_fk_u_id':'cp_fk_u_id';

         $sql = "UPDATE ".$table." SET ".$latCol." = '".$latitude."', ".$longCol." = '".$longitude."' WHERE ".
                $user_id_Col." = '".$user_id."'";
                
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
        */
        return 1;

    }


/*
 * function add_favorite
 * @param $user_id, $mua_id, $favorite
*/
    public function add_favorite($user_id, $mua_id, $favorite){
        $sql = "SELECT UPDATE_FAVORITE (".$user_id.", ".$mua_id.", '".$favorite."')";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
    }





/*
 * function add_favorite
 * @param $user_id, $mua_id, $favorite
*/
   public function get_favorite_list($user_id){
        $sql = "SELECT a.ap_fk_u_id AS mua_id, a.cf_is_favorite, a.ap_profile_name, a.ap_image, IF(ceil(a.avgRating) = a.avgRating, a.avgRating, 0.0) AS avgRating, a.reviews   FROM (
                SELECT AP.ap_fk_u_id, CF.cf_is_favorite, AP.ap_profile_name, AP.ap_image,

                 (
                   SELECT AVG(rating_remarks.rr_rating) FROM rating_remarks 
                   WHERE rating_remarks.rr_fk_mua_id = CF.cf_fk_mua_id AND rating_remarks.rr_status='true'
                   )  AS avgRating,

                (
                   SELECT COUNT(rating_remarks.rr_rating) FROM rating_remarks 
                   WHERE rating_remarks.rr_fk_mua_id = CF.cf_fk_mua_id AND rating_remarks.rr_status='true'
                   )  AS reviews

                  FROM client_favorite AS CF

                LEFT JOIN artist_profile AS AP
                ON
                AP.ap_fk_u_id = CF.cf_fk_mua_id

                WHERE cf_fk_client_id = ".$user_id." AND cf_status = 'true') AS a";
        
        $stmt = $this->conn->prepare($sql);
        
        if($stmt->execute()){
            
            $result = $stmt->fetchAll();
            $stmt->closeCursor();    
            //print_r($result); die;
        }
        else{
             $result = NULL;
        }

        return $result;

        
    }


    public function get_mua_info($mua_id, $user_id){

      $sql = "SELECT *FROM (SELECT im.im_id, ap_profile_name, ap_phone, ap_website, ap_about, ap_email, ap_city, im.im_image_name, im.im_action, ap_address, CONCAT('', FORMAT(ap_rpm, 2)) AS ap_rpm, ap_language, ap_zipcode, ap_created, ap_certificate_status, ap_comment,

         (SELECT CONCAT('', FORMAT(SUM(client_payment_information.cpi_paid_amount), 2)) FROM client_payment_information WHERE cpi_fk_artist_id = :ap_fk_u_id LIMIT 1 ) AS total_earn,

         (SELECT count(client_request.cr_id) FROM client_request WHERE cr_fk_u_mua_id = :ap_fk_u_id AND cr_status = 'finished'  ORDER BY cr_id LIMIT 1 ) AS total_jobs,

         (SELECT cf_is_favorite FROM client_favorite WHERE cf_fk_client_id = :cf_fk_client_id AND cf_fk_mua_id = AP.ap_fk_u_id  ORDER BY cf_id LIMIT 1 ) AS favorite



         FROM artist_profile AS AP
         
         LEFT JOIN image_master AS im
         ON
         im.im_fk_u_id = AP.ap_fk_u_id
         AND
         im.im_status = 'true'
         AND
         im.im_deleted = 'false'
         
       
        WHERE ap_fk_u_id = :ap_fk_u_id AND ap_status = 'true' ORDER BY im.im_updated DESC) AS a GROUP BY a.im_action";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ap_fk_u_id',$mua_id);
        $stmt->bindParam(':cf_fk_client_id',$user_id);
        
        if($stmt->execute()){
            
            $result = $stmt->fetchAll();
            //print_r($result); die;
            

            $stmt->closeCursor();    
            
        }
        else{
             $result = NULL;
        }

        return $result;
    }


    public function get_artist_details($mua_id){
      $sql = "SELECT ap_about, ap_language FROM artist_profile AS AP WHERE ap_fk_u_id = :ap_fk_u_id";


      $stmt = $this->conn->prepare($sql);
      $stmt->bindParam(':ap_fk_u_id',$mua_id);
      //$stmt->bindParam(':cf_fk_client_id',$user_id);
        
        if($stmt->execute()){
            
            $result = $stmt->fetch();
            $stmt->closeCursor();    
            
        }
        else{
             $result = NULL;
        }

        return $result;        
    }


   public function get_availability($mua_id){
    $sql = "SELECT aa_id, aa_days, aa_time_from, aa_time_to, aa_active_status FROM artist_availability WHERE aa_fk_u_id = :aa_fk_u_id AND aa_active_status = 'true' ";
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(":aa_fk_u_id", $mua_id);

    if($stmt->execute()){
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
    }else{
        $result = NULL;
    }
    return $result;
}


public function get_mua_rating($mua_id, $lang){
        
      $sql = "SELECT category_attributes.ca_name AS category_name, u_id AS client_id, u_name AS client_name, im_image_name AS client_profile_image, rr_rating AS rating, rr_remarks AS reviews FROM rating_remarks
        INNER JOIN user
          ON
          user.u_id = rating_remarks.rr_fk_client_id

          INNER JOIN client_artist_booking
          ON
          client_artist_booking.cab_id = rating_remarks.rr_fk_cab_id

          INNER JOIN client_request
          ON
          client_artist_booking.cab_fk_cr_id = client_request.cr_id

          INNER JOIN artist_category
          ON
          artist_category.ac_id = client_request.cr_fk_ac_id
          INNER JOIN category_master
          ON
          category_master.cm_id = artist_category.ac_fk_cm_id
          INNER JOIN category_attributes
          ON
          category_attributes.ca_fk_cm_id = category_master.cm_id
          AND
          category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lm_name)

          LEFT JOIN image_master 
          ON
          image_master.im_fk_u_id = rating_remarks.rr_fk_client_id
          AND
          image_master.im_action='client'
          AND
          image_master.im_status='true'
          AND
          image_master.im_deleted='false'

       WHERE rating_remarks.rr_fk_mua_id = :rr_fk_mua_id AND rating_remarks.rr_status = 'true' AND rr_review_by = 'client'";
      
      $stmt = $this->conn->prepare($sql);
      $stmt->bindParam(":rr_fk_mua_id", $mua_id);
      $stmt->bindParam(":lm_name", $lang);

      if($stmt->execute()){

          $result = $stmt->fetchAll();
          $stmt->closeCursor();
          return $result;

      }else{
          return NULL;
      }

}


  public function update_client_lat_long($user_id, $user_type, $latitude, $longitude){
    /*
    $sql = "UPDATE client_profile SET cp_lat = :cp_lat, cp_long = :cp_long  WHERE cp_fk_u_id = :cp_fk_u_id";
    
    $stmt = $this->conn->prepare($sql);
            
    $stmt->bindParam(':cp_fk_u_id',$user_id);
    $stmt->bindParam(':cp_lat',$latitude);
    $stmt->bindParam(':cp_long',$longitude);
        
    $stmt->execute();
    $stmt->closeCursor();
    */
    return 1;
  }


/*
* function : add_notification
* @param
*/

  public function add_notification($param){

   $sql = "INSERT INTO user_notification (un_fk_u_id, un_fk_from_id, un_user_type, un_fk_cr_id, un_title, un_message, un_message_spn, un_notification_type, un_created) VALUES (:un_fk_u_id, :un_fk_from_id, :un_user_type, :un_fk_cr_id, :un_title, :un_message, :un_message_spn, :un_notification_type, :un_created)";
    $stmt = $this->conn->prepare($sql);

    $stmt->bindParam(':un_fk_u_id', $param->user_id);
    $stmt->bindParam(':un_fk_from_id', $param->from_id);
    $stmt->bindParam(':un_user_type', $param->user_type);
    $stmt->bindParam(':un_fk_cr_id', $param->requsetId);
    $stmt->bindParam(':un_title', $param->title);
    $stmt->bindParam(':un_message', $param->message);
    $stmt->bindParam(':un_message_spn', $param->message_spn);
    $stmt->bindParam(':un_notification_type', $param->type);
    $stmt->bindParam(':un_created', $param->date);

    if($stmt->execute()){
      $id = '';
      $id = $this->conn->lastInsertId();

      $stmt->closeCursor();
      return $id;
    }else{
      return $id = 0;
    }


  }
  
  public function get_skintone_list($lang) {
  	  $sql = "SELECT ms_fk_ms_id AS skintone_id, ms_name as skintone_name FROM master_skintone WHERE language=(SELECT lm_id FROM language_master WHERE lm_name='".$lang."') AND ms_status='true'";
	  $stmt = $this->conn->prepare($sql);
	  if($stmt->execute()){
		  $result = $stmt->fetchAll();
		  $stmt->closeCursor();
		  return $result;
	  }else{
		  return array();
	  }
  }

 

 public function get_client_name($param) {
   
    $sql = "UPDATE user SEt u_name = :u_name WHERE u_id = :u_id";
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':u_id',$param->user_id);
    $stmt->bindParam(':u_name',$param->name);

    
    $stmt->execute();

    $num_affected_rows = $stmt->rowCount();
    $stmt->closeCursor();
    
  }


  public function get_category_name($id, $lang) {

    $sql = "SELECT * FROM 
          client_request

          INNER JOIN artist_category
          ON
          artist_category.ac_id = client_request.cr_fk_ac_id
          INNER JOIN category_master
          ON
          category_master.cm_id = artist_category.ac_fk_cm_id
          LEFT JOIN category_attributes
          ON
          category_attributes.ca_fk_cm_id = category_master.cm_id
          AND
          language = '".$lang."'
          

          WHERE cr_id = '".$id."'";
   
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':cr_id',$id);
    $stmt->bindParam(':language',$lang);

    if($stmt->execute()){
      $result = $stmt->fetchAll();
      $stmt->closeCursor();
      return $result;
    }else{
      return array();
    }
    
  }   


   


}