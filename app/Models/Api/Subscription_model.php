<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Subscription_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }
    
    public function renew_subscription($user_id,$plan_id,$amount,$result,$token,$free=false,$retry=0){
        if($retry==1){
            $sql="update artist_payment_information set api_status='false' where api_fk_artist_id=".$user_id." and api_fk_asp_id=4 and api_status='true'";
            $stmt=$this->conn->prepare($sql);
            $stmt->execute();
            $stmt->closeCursor();
        }
        $sql = "INSERT INTO artist_payment_information
                (api_fk_artist_id, api_fk_asp_id, api_paid_amount, api_service_amout, api_transaction_details, 
               api_status, api_created,api_nextBillAmount,api_paymentMethodToken, api_planId, api_nextBillingDate,api_admin_status
               ) 
               
                VALUES (:api_fk_artist_id, :api_fk_asp_id, :api_paid_amount, :api_service_amout, :api_transaction_details, 
               :api_status, :api_created, :api_nextBillAmount, :api_paymentMethodToken, :api_planId, :api_nextBillingDate, :api_admin_status
               )";

        $stmt = $this->conn->prepare($sql);
        $status = 'true';
        $admin_status = 'false';
        $nextBillingDate = date('Y-m-d',strtotime('+1 month',strtotime(date('y-m-d'))));
        $current_time = date('Y-m-d H:i:s');
        $stmt->bindParam(':api_fk_artist_id', $user_id); 
        $stmt->bindParam(':api_fk_asp_id', $plan_id);
        $stmt->bindParam(':api_transaction_details', $result);
        $stmt->bindParam(':api_status', $status);
        $stmt->bindParam(':api_admin_status', $admin_status);
        $stmt->bindParam(':api_created', $current_time);     
        $stmt->bindParam(':api_paymentMethodToken', $token);
        $stmt->bindParam(':api_planId', $plan_id);
        $stmt->bindParam(':api_nextBillingDate', $nextBillingDate);
        $stmt->bindParam(':api_paid_amount', $amount);
        $stmt->bindParam(':api_service_amout', $amount);
        $stmt->bindParam(':api_nextBillAmount', $amount);
        $result = $stmt->execute();
        $stmt->closeCursor();
    }
    
    public function select_subscription($notification=false){
        $where=" and api_nextBillingDate='".date('Y-m-d')."') or (api_retry='1' and DATEDIFF('".date('Y-m-d')."',api_nextBillingDate)=2)";
        if($notification){
            $where=" and DATEDIFF(api_nextBillingDate,'".date('Y-m-d')."') between 1 and 3)";
        }
        $sql = "SELECT api_id,api_fk_artist_id,api_fk_asp_id,api_nextBillingDate,api_retry,(select asp_name from artist_subscription_plan where asp_id=api_fk_asp_id) as plan_name,(select asp_amount from artist_subscription_plan where asp_id=api_fk_asp_id) as plan_amount,(select asp_name from artist_subscription_plan where asp_id=(select api_fk_asp_id from artist_payment_information as a where a.api_fk_artist_id=artist_payment_information.api_fk_artist_id and api_status='update')) as update_plan,(select u_email from user where u_id=api_fk_artist_id) as email,(select u_gcm_id from user where u_id=api_fk_artist_id) as u_gcm_id,(select u_type from user where u_id=api_fk_artist_id) as u_type,(select u_device_type from user where u_id=api_fk_artist_id) as u_device_type,(select api_fk_asp_id from artist_payment_information as a where a.api_fk_artist_id=artist_payment_information.api_fk_artist_id and api_status='update') as plan,(select api_id from artist_payment_information as a where a.api_fk_artist_id=artist_payment_information.api_fk_artist_id and api_status='update') as plan_id FROM artist_payment_information WHERE (api_status = true ".$where;
        $stmt = $this->conn->prepare($sql);
        $result = $stmt->execute();
        $result= $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }
    
    public function disable_subscription($id,$id1='',$retry=false){
        $sql="update artist_payment_information set api_status='false' where api_id=".$id;
        if($retry){
            $sql="update artist_payment_information set api_status='false',api_retry='1' where api_id=".$id;
        }
        $stmt=$this->conn->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
        if($id1){
            $sql="update artist_payment_information set api_status='true',api_nextBillingDate='".date('Y-m-d',strtotime('+1 month',strtotime(date('y-m-d'))))."' where api_id=".$id1;
            $stmt=$this->conn->prepare($sql);
            $stmt->execute();
            $stmt->closeCursor();
        }
    }
    public function update_subscription($api_subscription_id, $webhookNotification){
        //update current
        $sql = "UPDATE artist_payment_information SET api_status= 'true', api_created=NOW() WHERE api_subscription_id = :api_subscription_id AND api_status = 'update'";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':api_subscription_id', $api_subscription_id);
        $result = $stmt->execute();
        $stmt->closeCursor();
        
        $sql = "SELECT * FROM artist_payment_information WHERE api_subscription_id = :api_subscription_id ORDER BY api_id DESC LIMIT 1";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_subscription_id", $api_subscription_id);
        
        if($stmt->execute()) {
            $result = $stmt->fetch();
            $user_id = $result['api_fk_artist_id'];
            $sql = "UPDATE artist_payment_information SET api_status= 'false' WHERE api_fk_artist_id=:api_fk_artist_id AND api_subscription_id != :api_subscription_id";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(":api_fk_artist_id", $user_id);
            $stmt->bindParam(":api_subscription_id", $api_subscription_id);
        }
        
		$stmt->closeCursor();
    }

     public function get_status_mua_subscription($user_id){
        $sql = "SELECT * FROM artist_payment_information WHERE api_fk_artist_id = :api_fk_artist_id AND api_fk_asp_id =:api_fk_artist_id AND api_status = 'true'";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_fk_artist_id", $user_id);
        
        if($stmt->execute()) {
            $result = $stmt->fetch();
            $stmt->closeCursor();
            return $result;
        }else{
            return false;
        }
    }

    public function update_free_plan($user_id){
        $sql = "UPDATE artist_payment_information SET api_status= 'false' WHERE api_fk_artist_id = :api_fk_artist_id AND api_fk_asp_id = 4";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':api_fk_artist_id', $user_id);
        $result = $stmt->execute();
        $stmt->closeCursor();
        
    }


    public function update_subscription_info($data){
        
        $current_time = date('Y-m-d H:i:s');
        /*UPDATE artist_payment_information SET api_status= 'false' WHERE api_fk_artist_id =:api_fk_artist_id WHERE api_fk_asp_id = '4';*/
        $sql = "INSERT INTO artist_payment_information
                (api_fk_artist_id, api_fk_asp_id, api_paid_amount, api_service_amout, api_transaction_id, api_transaction_details, 
               api_status, api_created, api_subscription_id, api_nextBillAmount, api_paymentMethodToken, api_planId, api_nextBillingDate,
               api_transactions_id, api_customer_id ) 
               
                VALUES (:api_fk_artist_id, :api_fk_asp_id, :api_paid_amount, :api_service_amout, :api_transaction_id, :api_transaction_details, 
               :api_status, :api_created, :api_subscription_id, :api_nextBillAmount, :api_paymentMethodToken, :api_planId, :api_nextBillingDate,
               :api_transactions_id, :api_customer_id)";

        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindParam(':api_fk_artist_id', $data->user_id);
        $stmt->bindParam(':api_fk_asp_id', $data->plan_id);
        $stmt->bindParam(':api_paid_amount', $data->paid_amount);
        $stmt->bindParam(':api_service_amout', $data->api_service_amout);
        $stmt->bindParam(':api_transaction_id', $data->transactions_id);
        $stmt->bindParam(':api_transaction_details', $data->api_transaction_details);
        $stmt->bindParam(':api_status', $data->status);
        $stmt->bindParam(':api_created', $current_time);
        $stmt->bindParam(':api_subscription_id', $data->subscription_id);
        $stmt->bindParam(':api_nextBillAmount', $data->nextBillAmount);
        $stmt->bindParam(':api_paymentMethodToken', $data->paymentMethodToken);
        $stmt->bindParam(':api_planId', $data->plan_id);
        $stmt->bindParam(':api_nextBillingDate', $data->nextBillingDate);
        $stmt->bindParam(':api_transactions_id', $data->transactions_id);
        $stmt->bindParam(':api_customer_id', $data->customer_id);
        
        
        
        $result = $stmt->execute();
        $stmt->closeCursor();
        if ($result) {
            $sql = "SELECT * FROM artist_payment_information WHERE api_fk_artist_id = :api_fk_artist_id AND api_status='true'";
			$stmt = $this->conn->prepare($sql);
			$stmt->bindParam(":api_fk_artist_id", $data->user_id);
			if($stmt->execute()) {
				$result = $stmt->fetchAll();
				if(count($result)>1){
					$sql = "UPDATE artist_payment_information SET api_status= 'false' WHERE api_service_amout=0 AND api_status='true' AND api_fk_artist_id=:api_fk_artist_id";
					$stmt = $this->conn->prepare($sql);
					$stmt->bindParam(":api_fk_artist_id", $data->user_id);
					$stmt->execute();
					$stmt->closeCursor();
				}
			}
			return true;
			
        } else {
            return false;
        }
        
        
        
        
    }
    
    public function get_subscription_plan_id_data($plan_id){
        $sql = "SELECT * FROM artist_subscription_plan WHERE asp_id = :asp_id ORDER BY asp_id DESC LIMIT 1";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":asp_id", $plan_id);
        if ($stmt->execute()) {
            $result = $stmt->fetch();
            $stmt->closeCursor();
            if(isset($result['asp_plan_id'])){
                return $result;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }
    
    
    
    
    public function get_subscription_update_plan_id($user_id){
        $sql = "SELECT * FROM artist_payment_information WHERE api_fk_artist_id = :api_fk_artist_id AND api_status = 'update'";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_fk_artist_id", $user_id);
        if ($stmt->execute()) {
            $result = $stmt->fetch();
            $stmt->closeCursor();
            
            return $result;
            
        } else {
            return false;
        }
    }



    
    
    
    
    public function get_mua_subscription_token($user_id){
        $sql = "SELECT * FROM braintree_tokens WHERE bt_fk_u_id = :api_fk_artist_id AND bt_status = 'true' AND bt_is_default = 'true' ORDER BY bt_id DESC LIMIT 1";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_fk_artist_id", $user_id);
        if ($stmt->execute()) {
            $result = $stmt->fetch();
            $stmt->closeCursor();
            return $result;
        } else {
            return false;
        }
    }
    public function get_subscription_data($user_id, $plan_id){
        $sql = "SELECT * FROM artist_payment_information WHERE api_fk_artist_id = :api_fk_artist_id AND api_fk_asp_id = :api_fk_asp_id";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_fk_artist_id", $user_id);
        $stmt->bindParam(":api_fk_asp_id", $plan_id);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
            $stmt->closeCursor();
            return $result;
        } else {
            return NULL;
        }
    }

    public function get_subscription_status($user_id){
        $sql = "SELECT * FROM artist_payment_information WHERE api_fk_artist_id = :api_fk_artist_id AND api_status = 'true' ORDER BY api_id DESC";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_fk_artist_id", $user_id);
        
        if ($stmt->execute()) {
            $result = $stmt->fetch();
            $stmt->closeCursor();
            return $result;
        } else {
            return NULL;
        }
    }

    public function cancel_subscription($user_id, $plan_id){
        $sql = "UPDATE artist_payment_information SET api_status = 'cancel' WHERE api_fk_artist_id = :api_fk_artist_id AND api_fk_asp_id = :api_fk_asp_id";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_fk_artist_id", $user_id);
        $stmt->bindParam(":api_fk_asp_id", $plan_id);
        
        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows;
    }


    public function get_subscription_plan_amount($plan_id) {
        $sql = "SELECT *FROM artist_subscription_plan WHERE asp_id=" . $plan_id . " AND asp_status='true' AND asp_deleted='false'";

        $result = false;
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = (object) $stmt->fetch();
            $result = isset($result->asp_amount) ? $result->asp_amount : false;
        }
        $stmt->closeCursor();
        return $result;
    }


    public function create_payment_history_for_mua($info) {
        $current_time = date('Y-m-d H:i:s');
        
        $sql = "INSERT INTO artist_payment_information(api_fk_artist_id, api_fk_asp_id, api_paid_amount, api_service_amout, api_transaction_id, api_transaction_details, api_status, api_created) VALUES ('" . $info->artist_id . "', '" . $info->plan_id . "', '" . $info->paid_amount . "', '" . $info->service_amount . "', '" . $info->transaction_id . "', '" . $info->transaction_details . "', '" . $info->status . "', '".$current_time."')";

        $stmt = $this->conn->prepare($sql);
        $result = $stmt->execute();
        $stmt->closeCursor();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }


    public function get_mua_subscription($user_id){
        
        $sql = "SELECT asp_id AS plan_id,
            UPPER(asp_name) AS asp_name, UPPER(asp_name_spn) AS asp_name_spn,
            asp_image, asp_duration,
            asp_description, 
            asp_description_spn,
            CONCAT('', FORMAT(asp_amount, 2)) AS asp_amount,
            asp_client_limit, asp_commission_after_limit,
            (SELECT api_status FROM artist_payment_information WHERE api_fk_artist_id = :api_fk_artist_id AND artist_subscription_plan.asp_id = api_fk_asp_id ORDER BY api_id DESC LIMIT 1) AS status,
            (SELECT api_created FROM artist_payment_information WHERE api_fk_artist_id = :api_fk_artist_id AND api_status = 'true' LIMIT 1) AS api_created
             FROM artist_subscription_plan WHERE asp_status = 'true' AND asp_deleted = 'false' ORDER BY asp_orderby ASC";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_fk_artist_id", $user_id);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
            
            $stmt->closeCursor();
        } else {
            $result = NULL;
        }
        return $result;
    }

    public function get_mua_update_plan_data($user_id){
        $sql = "SELECT * FROM artist_payment_information
        INNER JOIN artist_subscription_plan
        ON
        artist_payment_information.api_fk_asp_id = artist_subscription_plan.asp_id

         WHERE api_status = 'update' AND api_fk_artist_id = :api_fk_artist_id ORDER BY api_id DESC";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_fk_artist_id", $user_id);

        if ($stmt->execute()) {
            $result = $stmt->fetch();
            $stmt->closeCursor();
        } else {
            $result = NULL;
        }
        return $result;

    }

    public function is_free_data($user_id){
        $sql = "SELECT * FROM artist_payment_information WHERE api_fk_artist_id = :api_fk_artist_id AND api_status = 'true' AND api_fk_asp_id = '4' ORDER BY api_id DESC";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":api_fk_artist_id", $user_id);

        if ($stmt->execute()) {
            $result = $stmt->fetch();
            $stmt->closeCursor();
        } else {
            $result = NULL;
        }
        return $result;

    }


    public function is_mua_subscription_active($mua_id) {
        $sql = "SELECT * FROM artist_payment_information WHERE api_status='true' AND api_fk_artist_id = " . $mua_id . " AND TIMESTAMPDIFF(MONTH, api_created, date(NOW())) < 1 ORDER BY api_id DESC LIMIT 1";
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = $stmt->fetch();
        }
        $stmt->closeCursor();
        return $result;
    }

    public function insert_subscription($param){

        $current_time = date('Y-m-d H:i:s');

        $sql = "INSERT INTO artist_payment_information
                (api_fk_artist_id, api_fk_asp_id, api_paid_amount, api_service_amout, api_transaction_details, 
               api_status, api_created, api_nextBillAmount, api_paymentMethodToken, api_planId, api_nextBillingDate ) 
               
                VALUES (:api_fk_artist_id, :api_fk_asp_id, :api_paid_amount, :api_service_amout, :api_transaction_details, 
               :api_status, :api_created, :api_nextBillAmount, :api_paymentMethodToken, :api_planId, :api_nextBillingDate)";

        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindParam(':api_fk_artist_id', $param->user_id); 
        $stmt->bindParam(':api_fk_asp_id', $param->plan_id);
        $stmt->bindParam(':api_paid_amount', $param->amount);
        $stmt->bindParam(':api_service_amout', $param->amount);
        $stmt->bindParam(':api_transaction_details', $param->result);
        $stmt->bindParam(':api_status', $param->status);
        $stmt->bindParam(':api_created', $current_time);
        $stmt->bindParam(':api_nextBillAmount', $param->amount);
        $stmt->bindParam(':api_paymentMethodToken', $param->paymentMethodToken);
        $stmt->bindParam(':api_planId', $param->plan_id);
        $stmt->bindParam(':api_nextBillingDate', $param->nextBillingDate);
        $result = $stmt->execute();
        $stmt->closeCursor();
    }

}
