<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Payment_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }
    
    public function get_promocode_info($u_id){
        
        return array();
    }
    
    public function get_user_default_token($user_id) {
        $sql = "SELECT bt_id, bt_token FROM braintree_tokens WHERE bt_fk_u_id = " . $user_id . " AND bt_status='true' AND bt_is_default = 'true'";
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = $stmt->fetch();
        }
        $stmt->closeCursor();
        return $result;
    }

    public function create_payment_history_for_mua($info) {
        $current_time = date('Y-m-d H:i:s');
        
        $sql = "UPDATE artist_payment_information SET api_status='false' WHERE api_fk_artist_id = " . $info->artist_id . "; 
    			INSERT INTO artist_payment_information(api_fk_artist_id, api_fk_asp_id, api_paid_amount, api_service_amout, api_transaction_id, api_transaction_details, api_status, api_created) VALUES ('" . $info->artist_id . "', '" . $info->plan_id . "', '" . $info->paid_amount . "', '" . $info->service_amount . "', '" . $info->transaction_id . "', '" . $info->transaction_details . "', 'true', '".$current_time."')";

        $stmt = $this->conn->prepare($sql);
        $result = $stmt->execute();
        $stmt->closeCursor();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function get_subscription_plan_amount($plan_id) {
        $sql = "SELECT *FROM artist_subscription_plan WHERE asp_id=" . $plan_id . " AND asp_status='true' AND asp_deleted='false'";

        $result = false;
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = (object) $stmt->fetch();
            $result = isset($result->asp_amount) ? $result->asp_amount : false;
        }
        $stmt->closeCursor();
        return $result;
    }

    public function is_already_paid_for_current_booking($u_id, $mua_id, $booking_id, $payment_mode = 1, $payment_status = 1) {
        //if($payment_mode==$payment_status){
        $sql = "SELECT *FROM client_payment_information WHERE cpi_fk_client_id=" . $u_id . " AND cpi_fk_artist_id=" . $mua_id . " AND cpi_fk_cab_id=" . $booking_id . " AND cpi_status='true' AND cpi_payment_mode =" . $payment_mode . " AND cpi_payment_action = " . $payment_status;
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = count($stmt->fetchAll()) ? true : false;
        }
        $stmt->closeCursor();
        return $result;
        //}
    }

    public function create_payment_history_for_client($info) {


        

    /*$sql = "SELECT artist_payment_information.api_fk_asp_id AS plan_id,
            (SELECT COUNT(*) FROM client_request WHERE client_request.cr_fk_u_mua_id = artist_payment_information.api_fk_artist_id AND cr_status = 'finished'  LIMIT 1) as booking_count,
            (SELECT asp_commission_after_limit FROM artist_subscription_plan WHERE artist_subscription_plan.asp_id = artist_payment_information.api_fk_asp_id LIMIT 1) AS commission,
            (SELECT asp_client_limit FROM artist_subscription_plan WHERE artist_subscription_plan.asp_id = artist_payment_information.api_fk_asp_id LIMIT 1) AS client_limit

            FROM artist_payment_information WHERE api_fk_artist_id = '".$info->artist_id."' AND api_status = 'true' ORDER BY api_id LIMIT 1";*/

        $sql = "SELECT * FROM user";
       	
        $stmt = $this->conn->prepare($sql);

        if ($stmt->execute()) {
            $result = $stmt->fetch();

            $commission = '';

           /* if(isset($result) && $result){
                if($result['plan_id'] == 4 && $info->cpi_payment_action == 2){
                    $commission = $result['commission'];
                }else{
                   
                    if(($result['booking_count'] >= $result['client_limit']) && ($info->cpi_payment_action == 2)){
                        $commission = $result['commission'];
                    }
                }
            }else{
                if($info->cpi_payment_action == 2){
                    $commission = 25;
                }else{
                    $commission = 0;
                }
                
			}*/

            $cpi_pay_to_artist = $info->paid_amount - (($info->booking_amount/2) * $commission / 100);

            $date = date("Y-m-d H:i:s");
            $status = 'true';


         $sql = "INSERT INTO client_payment_information(
                             cpi_total_rebate_amount, 
                             cpi_rebate_amount, 
                             cpi_fk_ur_ids, 
                             cpi_payment_mode, 
                             cpi_payment_action, 
                             cpi_commission, 
                             cpi_pay_to_artist, 
                             cpi_fk_client_id, 
                             cpi_fk_artist_id, 
                             cpi_fk_cab_id, 
                             cpi_paid_amount, 
                             cpi_service_amout, 
                             cpi_transaction_id, 
                             cpi_transaction_details, 
                             cpi_token, 
                             cpi_masked_number, 
                             cpi_booking_time_distance, 
                             cpi_booking_time_rate_per_mile, 
                             cpi_expiration_date, 
                             cpi_status, 
                             cpi_created) VALUES (
                                '" . $info->cpi_total_rebate_amount . "',
                                '" . $info->cpi_rebate_amount . "',
                                '" . $info->cpi_fk_ur_ids . "',
                                '" . $info->cpi_payment_mode . "',
                                '" . $info->cpi_payment_action . "',
                                '" . $commission . "',
                                '" . $cpi_pay_to_artist . "',
                                '" . $info->client_id . "',
                                '" . $info->artist_id . "',
                                '" . $info->booking_id . "',
                                '" . $info->paid_amount . "',
                                '" . $info->booking_amount . "',
                                '" . $info->transaction_id . "',
                                '" . $info->transaction_details . "',
                                '" . $info->token . "',
                                '" . $info->maskedNumber . "',
                                '" . $info->booking_time_distance . "',
                                '" . $info->booking_time_rate_per_mile . "',
                                '" . $info->expirationDate . "',
                                '" . $status . "',
                                '" . $date . "')";
                    
            $stmt = $this->conn->prepare($sql);
            $result = $stmt->execute();
            $stmt->closeCursor();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } else {
            $stmt->closeCursor();
            return false;
        }
    }

    public function get_booking_price($booking_id) {
        $sql = "SELECT artist_category.ac_price FROM client_artist_booking 
				INNER JOIN client_request
				ON
				client_request.cr_id = client_artist_booking.cab_fk_cr_id
				INNER JOIN artist_category
				ON
				artist_category.ac_id = client_request.cr_fk_ac_id
				WHERE cab_id = " . $booking_id;
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = $stmt->fetch();
            $result = isset($result['ac_price']) ? $result['ac_price'] : false;
        }
        $stmt->closeCursor();
        return $result;
    }

    public function get_booking_info($request_id) {
        $sql = "SELECT * FROM client_request WHERE cr_id=" . $request_id . " LIMIT 1";
        $result = array();
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = $stmt->fetch();
        }
        $stmt->closeCursor();
        return $result;
    }

    public function is_mua_subscription_active($mua_id) {
        $sql = "SELECT * FROM artist_payment_information WHERE api_status='true' AND api_service_amout>0 AND api_fk_artist_id = " . $mua_id . " AND TIMESTAMPDIFF(MONTH, api_created, date(NOW())) < 1 LIMIT 1";
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = count($stmt->fetchAll()) ? true : false;
        }
        $stmt->closeCursor();
        return $result;
    }

    public function braintree() {
        return true;
    }

    public function get_user_card_details_list($user_id) {
        
        $sql = "SELECT bt_id, bt_maskedNumber, bt_cardTypeImg, bt_is_default FROM braintree_tokens WHERE bt_fk_u_id = " . $user_id . " AND bt_status = 'true' ORDER BY bt_is_default ASC";
        $result = array();
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
        } else {
            $result = null;
        }

        $stmt->closeCursor();
        return $result;
    }

    public function set_user_default_card($user_id, $card_id, $status) {
        $sql = "UPDATE `braintree_tokens` SET bt_is_default = 'false' WHERE bt_fk_u_id =:bt_fk_u_id;";
        $sql .= "UPDATE braintree_tokens SET bt_is_default = :bt_is_default WHERE bt_id = :bt_id AND bt_fk_u_id = :bt_fk_u_id";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':bt_fk_u_id', $user_id);
        $stmt->bindParam(':bt_id', $card_id);
        $stmt->bindParam(':bt_is_default', $status);
        $stmt->execute();

        $stmt->closeCursor();
        return 1;
    }

    public function insert_user_card_details($param) {

        $sql = "SELECT * FROM braintree_tokens WHERE bt_fk_u_id = :bt_fk_u_id AND bt_maskedNumber = :bt_maskedNumber";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':bt_fk_u_id', $param->user_id);
        $stmt->bindParam(':bt_maskedNumber', $param->maskedNumber);
        $stmt->execute();
        $result = $stmt->fetch();

        if (isset($result) && ($result)) {
            return 1;
        }else{
            $sql = "UPDATE `braintree_tokens` SET bt_is_default = 'false' WHERE bt_fk_u_id =:bt_fk_u_id;";
            $sql .= "INSERT INTO braintree_tokens (bt_fk_u_id, bt_token, bt_maskedNumber, bt_cardTypeImg, bt_response_data, bt_created, bt_is_default) VALUES (:bt_fk_u_id, :bt_token, :bt_maskedNumber, :bt_cardTypeImg, :bt_response_data, :bt_created, :bt_is_default)";

            $stmt = $this->conn->prepare($sql);

            $stmt->bindParam(':bt_fk_u_id', $param->user_id);
            $stmt->bindParam(':bt_token', $param->token);
            $stmt->bindParam(':bt_maskedNumber', $param->maskedNumber);
			$stmt->bindParam(':bt_cardTypeImg', $param->cardTypeImg);
            $stmt->bindParam(':bt_response_data', $param->response_data);
            $stmt->bindParam(':bt_created', $param->date);
            $stmt->bindParam(':bt_is_default', $param->bt_is_default);

            $result = $stmt->execute();

            $stmt->closeCursor();

            if ($result) {
                // information successfully inserted
                return 1;
            } else {
                // Failed to create user
                return 0;
            }
        }
    }



    public function update_user_card_details($param) {
        //print_r($param); die;

        $sql = "UPDATE braintree_tokens SET bt_maskedNumber = :bt_maskedNumber, bt_response_data = :bt_response_data WHERE bt_fk_u_id = :bt_fk_u_id AND bt_token = :bt_token";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':bt_fk_u_id', $param->user_id);
        $stmt->bindParam(':bt_token', $param->card_token);
        $stmt->bindParam(':bt_maskedNumber', $param->maskedNumber);
        $stmt->bindParam(':bt_response_data', $param->response_data);
        //$stmt->bindParam(':bt_id',$param->card_id);

        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }

    public function delete_user_card_details($user_id, $card_token) {
        $sql = "UPDATE braintree_tokens SET bt_status = 'false', bt_is_default='false' WHERE bt_fk_u_id = :bt_fk_u_id AND bt_token = :bt_token";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':bt_fk_u_id', $user_id);
        $stmt->bindParam(':bt_token', $card_token);
        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }

    public function mua_action($param) {
        $sql = "UPDATE client_request SET cr_status ='" . $param->action . "' WHERE cr_fk_u_client_id = '" . $param->user_id . "' AND cr_id = " . $param->requsetId;
        $stmt = $this->conn->prepare($sql);

        /* $stmt->bindParam(':cr_status', $param->action);
          $stmt->bindParam(':cr_fk_u_client_id', $param->user_id);
          $stmt->bindParam(':cr_id', $param->requsetId); */

        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows;
    }

    public function user_payment_info($user_id) {

        $sql = "SELECT IF(card_status=0,'false','true') AS card_status, "
                . "IF(is_subscription=0, 'false', 'true') AS is_subscription, "
                . "IF(is_subscription=0, 0, subscription_id) AS subscription_id  "
                . "FROM ("
                . "SELECT ("
                . "SELECT count(*) FROM `braintree_tokens` "
                . "WHERE "
                . "bt_fk_u_id = " . $user_id . " AND bt_status = 'true' AND bt_is_default = 'true' "
                . "ORDER BY bt_id DESC LIMIT 1) "
                . "AS card_status, "
                . "(SELECT api_fk_asp_id FROM artist_payment_information "
                . "WHERE api_fk_artist_id = " . $user_id . " AND api_status = 'true' "
                . "ORDER BY api_id DESC LIMIT 1) AS subscription_id, "
                . "(SELECT COUNT(*) FROM artist_payment_information "
                . "WHERE api_fk_artist_id = " . $user_id . " AND api_status = 'true' "
                . "ORDER BY api_id DESC LIMIT 1) AS is_subscription) AS a";

        $stmt = $this->conn->prepare($sql);
        //$stmt->bindParam(':u_id', $user_id);
        $result = array();
        if ($stmt->execute()) {
            $result = $stmt->fetch();
        } else {
            $result = null;
        }

        $stmt->closeCursor();
        return $result;
    }


    public function deleted_card($user_id, $bt_id){

        $sql = "DELETE FROM braintree_tokens WHERE bt_fk_u_id = '" . $user_id . "' AND bt_id =  '".$bt_id."' ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows;

    }


    public function get_last_payment_time($u_id){
        $sql = "SELECT * FROM client_payment_information WHERE cpi_fk_client_id = :cpi_fk_client_id ORDER BY cpi_id DESC";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cpi_fk_client_id', $u_id);
        $result = array();
        if ($stmt->execute()) {
            $result = $stmt->fetch();
        } else {
            $result = null;
        }

        $stmt->closeCursor();
        return $result;
    }


}
