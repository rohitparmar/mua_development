<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Referral_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }
	
	function getReferelExpriyDate(){
		$days = 30;
        $sql = "SELECT * FROM settings WHERE s_types='referral_expiry' and s_status='true'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        if ($result) {
			$days=isset($result['s_value'])?$result['s_value']:$days;
        }
        $stmt->closeCursor();
		return $days;
	}

    function get_rebate_amount(){
        
        $sql = "SELECT * FROM settings WHERE s_types='referral_amount' and s_status='true'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        if ($result) {
            $days=isset($result['s_value'])?$result['s_value']:$days;
        }
        $stmt->closeCursor();
        return $days;
    }


	
    function make_used_promocode_used($service_id, $referral_ids = array()) {
        if (count($referral_ids)) {
            $referral_ids = implode(',', $referral_ids);
            $sql = "UPDATE used_referral SET ur_services_id='" . $service_id . "', ur_used_status='true', ur_paid='true' WHERE ur_id IN(" . $referral_ids . ")";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
        }
        return true;
    }

    function get_promocode_info_used_by_other($uid) {
        $sql22 = "SELECT *FROM (SELECT ur.ur_id, ur.ur_r_id, ur.ur_fk_u_id, ur.ur_usertype, CONCAT('', FORMAT(r.r_rebate, 2)) AS r_rebate, r.r_rebate as rr_rebate FROM referral as r
            INNER JOIN used_referral as ur
            ON
            ur.ur_r_id = r.r_id
			AND
			IF(ur_usertype='client', (CLIENT_USED_REFERREL(ur.ur_fk_u_id)>0), (ARTIST_USED_REFERREL(ur.ur_fk_u_id)>0)) AS active_status
            AND
            ur.ur_services_id=0
            AND 
            ur.ur_used_status = 'false'
            WHERE r.r_fk_u_id = " . $uid.") AS a WHERE a.active_status>0";
			
		$sql = "SELECT *FROM (SELECT ur.ur_id, ur.ur_r_id, ur.ur_fk_u_id, ur.ur_usertype, CONCAT('', FORMAT(r.r_rebate, 2)) AS r_rebate, r.r_rebate as rr_rebate, 
				IF(ur.ur_usertype='client', (CLIENT_USED_REFERREL(ur.ur_fk_u_id)>0), (ARTIST_USED_REFERREL(ur.ur_fk_u_id)>0)) AS active_status FROM referral as r
				INNER JOIN used_referral as ur
				ON
				ur.ur_r_id = r.r_id
				AND
				ur.ur_services_id=0
				AND 
				ur.ur_used_status = 'false'
				WHERE r.r_fk_u_id = ".$uid.") AS a WHERE a.active_status";
					
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    /*
     * FUNCTION : add_referral_code
     * @param : $param
     */

    public function add_referral_code($param) {

        
        $sql = "SELECT * FROM referral WHERE r_code = :r_code";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':r_code', $param->referral_code);

        if ($stmt->execute()) {
            $result = $stmt->fetch();

            if ($result) {

                $sql = "SELECT * FROM used_referral WHERE ur_fk_u_id = :ur_fk_u_id";
                $stmt = $this->conn->prepare($sql);


                $stmt->bindParam(':ur_fk_u_id', $param->user_id);

                if ($stmt->execute()) {
                    $user = array();
                    $user = $stmt->fetch();

                    if (empty($user)) {
                        $sql = "INSERT INTO used_referral (ur_r_id, ur_fk_u_id, ur_usertype, ur_deviced_id, ur_created_date) VALUES (:ur_r_id, :ur_fk_u_id, :ur_usertype, :ur_deviced_id, :ur_created_date)";
                        $stmt = $this->conn->prepare($sql);

                        $stmt->bindParam(':ur_r_id', $result['r_id']);
                        $stmt->bindParam(':ur_fk_u_id', $param->user_id);
                        $stmt->bindParam(':ur_usertype', $param->user_type);
                        $stmt->bindParam(':ur_deviced_id', $param->device_id);
                        $stmt->bindParam(':ur_created_date', $param->date);
                        if ($stmt->execute()) {
                            return 'success';
                        }
                    } else {
                        return 'this code allready used';
                    }
                } else {
                    return NULL;
                }
            } else {

                return "this code be exits in table";
            }
        } else {

            return "this code be exits in table";
        }
        $stmt->closeCursor();
    }

    public function get_referral_code($referral_code,$type) {
        $date = date("Y-m-d");

        $sql = "SELECT * FROM referral WHERE r_code = :r_code AND r_code_type = :r_code_type AND :r_end_date between r_start_date AND r_end_date";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':r_code', $referral_code);
        $stmt->bindParam(':r_end_date', $date);
        $stmt->bindParam(':r_code_type', $type);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return $result;
        } else {
            return $result;
        }

        $stmt->closeCursor();
    }

    /*
     * FUNCTION : add_referral_code
     * @param : $param
     */

    public function edit_user_account_info($param) {

        //print_r($param); die;

        $sql = "UPDATE user_account_info SET uai_account_no = :uai_account_no, uai_rounting_no = :uai_rounting_no, uai_account_name = :uai_account_name, uai_paypal_id = :uai_paypal_id WHERE uai_fk_u_id = :uai_fk_u_id;";

        $sql .= "INSERT INTO user_account_info_history (uaih_fk_u_id, uaih_account_no, uaih_rounting_no, uaih_account_name, uaih_paypal_id) VALUES (:uai_fk_u_id, :uai_account_no, :uai_rounting_no, :uai_account_name, :uai_paypal_id)";


        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':uai_fk_u_id', $param->user_id);
        $stmt->bindParam(':uai_account_no', $param->account_no);
        $stmt->bindParam(':uai_rounting_no', $param->rounting_no);
        $stmt->bindParam(':uai_account_name', $param->account_name);
        $stmt->bindParam(':uai_paypal_id', $param->paypal_id);
        //$stmt->bindParam(':uai_paypal_id',$param->date);

        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return 1;
    }
    public function get_user_account_info($u_id){
        $sql = "SELECT * FROM  user_account_info WHERE uai_fk_u_id = ".$u_id;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        if($result){
            return $result;
        }else{
            return (object)array();
        }
        
        $stmt->closeCursor();
    }

    /*
     * function get_notification
     * @user_id
     */

    public function get_notification($user_id, $lang) {


        //$where = "INNER JOIN artist_profile ON artist_profile.ap_fk_u_id = user_notification.un_fk_from_id";

       $sql = "SELECT un_id, un_fk_from_id, un_user_type, un_is_read, category_attributes.ca_name AS category_name,



            IF('".$lang."'='spn', user_notification.un_message_spn, user_notification.un_message) AS un_message

            FROM user_notification

            INNER JOIN client_request
            ON 
            client_request.cr_id = user_notification.un_fk_cr_id

            INNER JOIN artist_category
            ON
            artist_category.ac_id = client_request.cr_fk_ac_id
            
            INNER JOIN category_master
            ON
            category_master.cm_id = artist_category.ac_fk_cm_id
            INNER JOIN category_attributes
            ON
            category_attributes.ca_fk_cm_id = category_master.cm_id
            AND
            category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lm_name)

            WHERE un_fk_u_id = :un_fk_u_id AND un_is_read = 'false' ORDER BY un_id DESC";

        $stmt = $this->conn->prepare($sql);
 
        $stmt->bindParam(':un_fk_u_id', $user_id);
        $stmt->bindParam(':lm_name', $lang);


        $result = array();

        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
        } else {
            $result = array();
        }
        $stmt->closeCursor();
        return $result;
    }

    public function get_notification_name($user_id, $user_type) {

        if (isset($user_type) && $user_type == 'mua') {
            $sql = "SELECT
              (
                SELECT im_image_name FROM image_master WHERE 
                image_master.im_fk_u_id = :ap_fk_u_id
                AND
                image_master.im_action='artist'
                AND
                image_master.im_status='true'
                AND
                image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
              ) AS image,

              ap_profile_name AS name 

              FROM artist_profile WHERE ap_fk_u_id = :ap_fk_u_id";
        } else if (isset($user_type) && $user_type == 'client') {
            $sql = "SELECT (
        SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = :ap_fk_u_id
        AND image_master.im_action='client' AND image_master.im_status='true' AND image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
        ) AS image, u_name AS name FROM user WHERE u_id = :ap_fk_u_id";
        }

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ap_fk_u_id', $user_id);
        $result = array();

        if ($stmt->execute()) {
            $result = $stmt->fetch();
        } else {
            $result = array();
        }
        return $result;
        $stmt->closeCursor();
    }

    public function update_notification($user_id) {
        $sql = "UPDATE user_notification SET un_is_read = 'true' WHERE un_fk_u_id = :un_fk_u_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':un_fk_u_id', $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return 1;
    }

    public function add_gift_info($param) {

        $sql = "INSERT INTO user_gift (uaih_fk_u_id, uaih_account_no, uaih_rounting_no, uaih_account_name, uaih_paypal_id) VALUES (:uai_fk_u_id, :uai_account_no, :uai_rounting_no, :uai_account_name, :uai_paypal_id)";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':uai_fk_u_id', $param->user_id);
        $stmt->bindParam(':uai_account_no', $param->request_id);
        $stmt->bindParam(':uai_rounting_no', $param->gender);
        $stmt->bindParam(':uai_account_name', $param->email);
        $stmt->bindParam(':uai_paypal_id', $param->age);
        $stmt->bindParam(':uai_paypal_id', $param->mobile);

        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return 1;
    }




    public function user_promocode_info($user_id){
        
        $sql = "SELECT * FROM (SELECT *, 
                    (
                     SELECT category_master.cm_name FROM client_request 
                        INNER JOIN artist_category
                        ON
                        artist_category.ac_id = client_request.cr_fk_ac_id
                        INNER JOIN category_master
                        ON
                        category_master.cm_id = artist_category.ac_fk_cm_id
                        INNER JOIN category_attributes
                        ON
                        category_attributes.ca_fk_cm_id = category_master.cm_id
                        WHERE cr_id=a.ur_services_id 
                        LIMIT 1
                    ) AS services_name,

                    IF(ur_usertype='client', (CLIENT_USED_REFERREL(user_id)>0), (ARTIST_USED_REFERREL(user_id)>0)) AS active_status

                    FROM (
                    SELECT r.r_id AS referral_id, ur.ur_id, ur.ur_fk_u_id AS user_id, ur.ur_usertype, CONCAT('', FORMAT(r.r_rebate, 2)) AS  r_rebate, ur.ur_services_id, ur.ur_used_status AS is_used, ur.ur_paid AS ur_paid,



                    (SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = ur.ur_fk_u_id
                    AND image_master.im_action='client' AND image_master.im_status='true' AND image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
                    ) AS user_image,
                    (
                    SELECT IF((u_name!=null OR u_name!=''), u_name, IF((u_username!=null OR u_username!=''), u_username, u_email)) FROM user WHERE user.u_id = ur.ur_fk_u_id LIMIT 1
                    ) AS user_name

                    FROM used_referral as ur
                    INNER JOIN referral as r
                    ON
                    ur.ur_r_id = r.r_id
                    
                    WHERE ur.ur_r_id = (SELECT r_id FROM referral WHERE r_fk_u_id = :r_fk_u_id LIMIT 1)
                    ) AS a) AS b ORDER BY active_status DESC";

                    

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':r_fk_u_id', $user_id);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
        } else {
            $result = array();
        }
        $stmt->closeCursor();
        return $result;
        

    }


    public function mua_promocode_info($user_id){
        
        /*$sql = "SELECT r.r_id AS referral_id, ur.ur_id, ur.ur_fk_u_id AS user_id, ur.ur_usertype, CONCAT('', FORMAT(r.r_rebate, 2)) AS r_rebate, ur.ur_used_status AS is_used,

                    (SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = ur.ur_fk_u_id
                    AND image_master.im_action='artist' AND image_master.im_status='true' AND image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
                    ) AS user_image,
                    (
                    SELECT ap_profile_name FROM artist_profile WHERE artist_profile.ap_fk_u_id = ur.ur_fk_u_id LIMIT 1
                    ) AS user_name

                    FROM referral as r
                    INNER JOIN used_referral as ur
                    ON
                    ur.ur_r_id = r.r_id
                    AND
                    ur.ur_usertype = 'mua'
                    WHERE r.r_fk_u_id = :r_fk_u_id";*/


         $sql = "SELECT * FROM(SELECT r.r_id AS referral_id, ur.ur_id, ur.ur_fk_u_id AS user_id, ur.ur_usertype, CONCAT('', FORMAT(r.r_rebate, 2)) AS r_rebate, ur.ur_used_status AS is_used, ur.ur_paid AS ur_paid,

                    (SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = ur.ur_fk_u_id
                    AND image_master.im_action='artist' AND image_master.im_status='true' AND image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
                    ) AS user_image,
                    (
                    SELECT IF((u_name!=null OR u_name!=''), u_name, IF((u_username!=null OR u_username!=''), u_username, u_email)) FROM user WHERE user.u_id = ur.ur_fk_u_id LIMIT 1
                    ) AS user_name,

                    IF(ur.ur_usertype='client', (CLIENT_USED_REFERREL(ur.ur_fk_u_id)>0), (ARTIST_USED_REFERREL(ur.ur_fk_u_id)>0)) AS active_status

                    FROM used_referral AS ur
                    INNER JOIN referral AS r
                    ON
                    ur.ur_r_id=r.r_id
                    WHERE  ur_r_id = (SELECT r_id FROM referral WHERE r_fk_u_id = :r_fk_u_id LIMIT 1)) AS a ORDER BY active_status DESC";


        
            

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':r_fk_u_id', $user_id);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
        } else {
            $result = array();
        }
        return $result;
        $stmt->closeCursor();

    }


    public function get_used_referral_list($user_id){
        

        $sql = "SELECT * FROM referral
                LEFT JOIN used_referral
                ON 
                used_referral.ur_r_id = referral.r_id  AND ur_used_status = 'true'
                WHERE r_fk_u_id = '".$user_id."' AND r_status = 'true'";
				
		$sql = "SELECT * FROM referral
                LEFT JOIN used_referral
                ON 
                used_referral.ur_r_id = referral.r_id 
                WHERE r_fk_u_id = '".$user_id."' AND r_status = 'true'";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':r_fk_u_id', $user_id);
        
        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
        } else {
            $result = array();
        }
        return $result;
        $stmt->closeCursor();

    }


    /*
    ||==================================================   
    || Function get_current_notification
    || @$user_id, $lang
    ||==================================================
    */

    public function get_current_notification($user_id, $lang, $type) {

        $where = '';

        if($type == "current"){
            
            $where =  " un_fk_u_id = :un_fk_u_id AND un_fk_cr_id IN (SELECT cr_id  FROM client_request WHERE 
            !(cr_status = 'finished' OR cr_status = 'cancelled' OR cr_status = 'declined') AND cr_id IN (SELECT un_fk_cr_id  FROM user_notification WHERE un_fk_u_id = :un_fk_u_id GROUP BY un_fk_cr_id)) ORDER BY un_id DESC";
        }else{
            $where =  " un_fk_u_id = :un_fk_u_id AND un_fk_cr_id IN (SELECT cr_id  FROM client_request WHERE cr_status IN ('finished','cancelled', 'declined') AND cr_id IN (SELECT un_fk_cr_id  FROM user_notification WHERE un_fk_u_id = :un_fk_u_id GROUP BY un_fk_cr_id)) ORDER BY un_id DESC";
        }


        $sql = "SELECT 
        un_fk_u_id,cr_status, un_notification_type, un_id, un_fk_from_id, un_user_type, un_is_read, category_attributes.ca_name AS category_name,

        IF('".$lang."'='spn', user_notification.un_message_spn, user_notification.un_message) AS un_message,

        cr_booking_time_distance AS distance,
        cr_mua_rate_per_mile AS rpm,
        cr_services_ammount AS services_ammount


        FROM user_notification

        INNER JOIN client_request
        ON 
        client_request.cr_id = user_notification.un_fk_cr_id

        INNER JOIN artist_category
        ON
        artist_category.ac_id = client_request.cr_fk_ac_id

        INNER JOIN category_master
        ON
        category_master.cm_id = artist_category.ac_fk_cm_id
        INNER JOIN category_attributes
        ON
        category_attributes.ca_fk_cm_id = category_master.cm_id
        AND
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lm_name)

        WHERE $where";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':un_fk_u_id', $user_id);
        $stmt->bindParam(':lm_name', $lang);

        $result = array();

        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
        } else {
            $result = array();
        }
        $stmt->closeCursor();
        return $result;
    }


    public function get_ios_notification_count($user_id){
        $sql = "SELECT count(*) AS notification_count FROM user_notification WHERE un_fk_u_id = :un_fk_u_id AND un_is_read = 'false'";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':un_fk_u_id', $user_id);
        $result = array();

        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
        } else {
            $result = array();
        }
        $stmt->closeCursor();
        return $result;
    }

    public function check_valid_celebrity_code($param){
       
        $sql = "SELECT * FROM used_referral WHERE ur_r_id = :ur_r_id AND (ur_fk_u_id = :ur_fk_u_id OR ur_deviced_id = :ur_deviced_id)";


        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ur_r_id', $param->r_id);
        $stmt->bindParam(':ur_fk_u_id', $param->user_id);
        $stmt->bindParam(':ur_deviced_id', $param->device_id);
        $result = array();

        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
        } else {
            $result = array();
        }
        $stmt->closeCursor();

        return $result;
    }

    public function get_user_used_celebrity_code_data($param){
       
        $sql = "SELECT * FROM used_referral WHERE ur_r_id = :ur_r_id AND ur_celebrity_code = 'true'";


        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ur_r_id', $param->r_id);
        $result = array();

        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
        } else {
            $result = array();
        }
        $stmt->closeCursor();

        return $result;
    }

    
    public function insert_celebrity_code($param){

        $date = date("Y-m-d H:i:s");

        $sql = "INSERT INTO used_referral (ur_r_id, ur_fk_u_id, ur_usertype, ur_deviced_id , ur_created_date, ur_celebrity_code) VALUES (:ur_r_id, :ur_fk_u_id, :ur_usertype, :ur_deviced_id, :ur_created_date, 'true')";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':ur_r_id', $param->r_id);
        $stmt->bindParam(':ur_fk_u_id', $param->user_id);
        $stmt->bindParam(':ur_usertype', $param->usertype);
        $stmt->bindParam(':ur_deviced_id', $param->device_id);
        $stmt->bindParam(':ur_created_date', $date);
        
        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return 1;
    }

}
