<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Artist_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }

    public function get_artist_booked_time_list($mua_id, $booking_time, $category_id) {
        $sql = "SELECT cr_id, cr_fk_u_client_id, cr_fk_u_mua_id, ROUND(cr_start_time/1000,0) AS cr_start_time, ROUND(cr_end_time/1000,0) AS cr_end_time, cr_booking_time_zone, cr_status FROM client_request WHERE cr_status='accepted' AND cr_fk_u_mua_id = " . $mua_id . " AND date(from_unixtime(cr_booking_date/1000))= date(from_unixtime(" . $booking_time . "/1000))"; //  AND cr_fk_ac_id = ".$category_id;

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $times = $stmt->fetchAll();
        } else {
            $times = false;
        }
        $stmt->closeCursor();
        return $times;
    }

    public function get_mua_time_info($mua_id, $day_name) {
        $sql = "SELECT aa_id,  (SELECT u_time_zone FROM user WHERE u_id = " . $mua_id . " LIMIT 1) AS mua_time_zone, ROUND(aa_time_from/1000,0) AS aa_time_from, ROUND(aa_time_to/1000,0) AS aa_time_to FROM artist_availability WHERE aa_days='" . $day_name . "' AND aa_fk_u_id=" . $mua_id . " AND aa_active_status=1";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $times = $stmt->fetch();
        } else {
            $times = false;
        }
        $stmt->closeCursor();
        return $times;
    }

    public function get_artist_availabile_time_slot($param) {


        $sql = "SELECT ROUND(cr_start_time/1000,0)+19800 AS service_start_time, ROUND(cr_end_time/1000,0) AS service_end_time, 
              (
              SELECT ROUND(aa_time_from/1000,0)+19800 FROM artist_availability WHERE aa_days = DAYNAME ('2016-07-06') AND aa_active_status='true' AND aa_fk_u_id = 1 ORDER BY aa_id LIMIT 1
              ) AS mua_service_start_time, 
              (
              SELECT ROUND(aa_time_to/1000,0)+19800 FROM artist_availability WHERE aa_days = DAYNAME ('2016-07-06') AND aa_active_status='true' AND aa_fk_u_id = 1 ORDER BY aa_id LIMIT 1
              ) AS mua_service_end_time 
              FROM `client_request` WHERE cr_booking_date = '2016-07-16' AND cr_status = 'accepted' AND cr_fk_u_mua_id = " . $param->user_id;

        /* $sql = "SELECT aa_days,  ROUND(aa_time_from/1000,0)+19800 AS aa_time_from, ROUND(aa_time_to/1000,0)+19800 AS aa_time_to FROM artist_availability WHERE aa_fk_u_id = 1 AND aa_active_status = 'true' AND aa_days = DAYNAME ('2016-07-06') "; */
        //$sql = "SELECT * FROM client_request";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $services = $stmt->fetchAll();

            $stmt->closeCursor();
        } else {
            $services = false;
        }
        //print_r($services); die;
        return $services;
    }

    public function get_artist_booking_time_slot() {
        $sql = "SELECT cr_booking_date FROM client_request WHERE cr_booking_date = '2016-07-06' AND cr_status = 'accepted' AND cr_fk_u_mua_id = 1";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $result = $stmt->fetch();
        } else {
            $result = false;
        }
        return $result;
    }

    public function get_services($mua_id, $user_id='', $lang = 'eng') {

        $sql = "SELECT CONCAT('', FORMAT(category_master.cm_price, 2)) AS base_price, artist_category.ac_id, CONCAT('', FORMAT(artist_category.ac_price, 2)) AS ac_price, artist_category.ac_products, artist_category.ac_duration,
                category_attributes.ca_name AS cm_name, category_attributes.ca_description AS cm_description, category_master.cm_image,

                (SELECT mcm_img FROM master_catgegory_mapping WHERE mcm_service_name='master_skintone' AND mcm_fk_ca_id = 
        (SELECT ca_id FROM category_attributes WHERE language = 1 AND ca_fk_cm_id = category_master.cm_id) AND mcm_service_id = 
        (SELECT ms_fk_ms_id AS skintone_id FROM `master_skintone` WHERE ms_id = 
        (SELECT cp_skin_color FROM client_profile WHERE cp_fk_u_id='".$user_id."'))) AS skintone_img

                FROM artist_category 
                
                INNER JOIN category_master
                ON
                artist_category.ac_fk_cm_id = category_master.cm_id
                AND
                category_master.cm_action_taken<>'deleted'
                
                INNER JOIN category_attributes
                ON
                category_attributes.ca_fk_cm_id = category_master.cm_id
                AND
                category_attributes.language=(SELECT lm_id FROM language_master WHERE lm_name='" . $lang . "')

                

                WHERE artist_category.ac_fk_u_id = " . $mua_id . " AND artist_category.ac_status='true'";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $services = $stmt->fetchAll();
            $stmt->closeCursor();
        } else {
            $services = NULL;
        }
        return $services;
    }

    /**
     * Creating new general information
     * @param $param 
     */
    public function artist_profile($param) {


        if (!$this->get_artist_profile($param->user_id)) {


            $sql = "INSERT INTO artist_profile (ap_fk_u_id, ap_profile_name, ap_phone, ap_website, ap_image, ap_about, ap_email, ap_city, ap_certificate, ap_address, ap_rpm, ap_language, ap_zipcode) values(:ap_fk_u_id, :ap_profile_name, :ap_phone, :ap_website, :ap_image, :ap_about, :ap_email, :ap_city, :ap_certificate, :ap_address, :ap_rpm, :ap_language, :ap_zipcode)";
            $stmt = $this->conn->prepare($sql);

            $stmt->bindParam(':ap_fk_u_id', $param->user_id);
            $stmt->bindParam(':ap_profile_name', $param->name);
            $stmt->bindParam(':ap_phone', $param->phone);
            $stmt->bindParam(':ap_website', $param->website);
            $stmt->bindParam(':ap_image', $param->artist_image);
            $stmt->bindParam(':ap_about', $param->about);
            $stmt->bindParam(':ap_email', $param->email);
            $stmt->bindParam(':ap_city', $param->city);
            $stmt->bindParam(':ap_certificate', $param->certificate);
            $stmt->bindParam(':ap_address', $param->address);
            $stmt->bindParam(':ap_rpm', $param->rate);
            $stmt->bindParam(':ap_language', $param->language);
            $stmt->bindParam(':ap_zipcode', $param->zip);

            $result = $stmt->execute();

            $stmt->closeCursor();

            if ($result) {
                // information successfully inserted
                return 1;
            } else {
                // Failed to create user
                return 0;
            }
        } else {
            return 2;
        }
    }

    /** get_artist_profile
     *  @param String $user_id
     */
    public function get_artist_profile($user_id) {
        //print_r($user_id); die;

        $sql = "SELECT ap_id, ap_profile_name, ap_fk_u_id FROM artist_profile WHERE ap_fk_u_id = :ap_fk_u_id AND ap_status = 'true'";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ap_fk_u_id', $user_id);

        if ($stmt->execute()) {

            $user = $stmt->fetch();
            $stmt->closeCursor();
            
        } else {
            $user = array();
        }
        return $user;
    }

    public function get_validate_user_id($user_id) {
        $sql = "SELECT u_id, u_name FROM user WHERE u_id = :u_id AND u_status = 'true'";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':u_id', $user_id);
        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
            $stmt->closeCursor();
        } else {

            $result = NULL;
        }
        return $result;
    }

    /**
     * update_artist_profile
     * @param 
     */
    public function update_artist_profile($param) {

        if($param->lat!="" && $param->long!=""){

            $sql = "UPDATE artist_profile SET ap_profile_name = :ap_profile_name, ap_phone = :ap_phone, ap_website = :ap_website,  ap_about = :ap_about, ap_email = :ap_email, ap_city = :ap_city, ap_certificate = :ap_certificate, ap_address = :ap_address, ap_rpm = :ap_rpm, ap_language = :ap_language, ap_zipcode = :ap_zipcode, ap_state = :ap_state WHERE ap_fk_u_id = :ap_fk_u_id; 

              UPDATE user SET u_profile_status= :u_profile_status WHERE u_id=:ap_fk_u_id;
              UPDATE user SET u_phone= :ap_phone WHERE u_id=:ap_fk_u_id;
            ";

        }else{

            $sql = "UPDATE artist_profile SET ap_profile_name = :ap_profile_name, ap_phone = :ap_phone, ap_website = :ap_website,  ap_about = :ap_about, ap_email = :ap_email, ap_city = :ap_city, ap_certificate = :ap_certificate, ap_address = :ap_address, ap_rpm = :ap_rpm, ap_language = :ap_language, ap_zipcode = :ap_zipcode, ap_state = :ap_state WHERE ap_fk_u_id = :ap_fk_u_id; 

              UPDATE user SET u_profile_status= :u_profile_status WHERE u_id=:ap_fk_u_id;
              UPDATE user SET u_phone= :ap_phone WHERE u_id=:ap_fk_u_id;
            ";
        }    

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':ap_profile_name', $param->name);
        $stmt->bindParam(':ap_phone', $param->phone);
        $stmt->bindParam(':ap_website', $param->website);
        $stmt->bindParam(':ap_about', $param->about);
        $stmt->bindParam(':ap_email', $param->email);
        $stmt->bindParam(':ap_city', $param->city);
        $stmt->bindParam(':ap_certificate', $param->certificate);
        $stmt->bindParam(':ap_address', $param->address);
        $stmt->bindParam(':ap_rpm', $param->rate);
        $stmt->bindParam(':ap_language', $param->language);
        $stmt->bindParam(':ap_zipcode', $param->zip);
        $stmt->bindParam(':ap_fk_u_id', $param->user_id);

        $stmt->bindParam(':ap_state', $param->state);
       
        $stmt->bindParam(':u_profile_status', $param->profile_status);


        if($param->lat!="" && $param->long!=""){
             //$stmt->bindParam(':ap_lat', $param->lat);
            //$stmt->bindParam(':ap_long', $param->long);
        }




        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }

    /**
     * Creating create_portfolio
     * @param String 
     */
    public function create_portfolio($param) {
        //print_r($param); die;

        if (!$this->get_portfolio($param->user_id, $param->category_id)) {


            $sql = "INSERT INTO artist_portfolio (ap_fk_u_id, ap_fk_ac_id, ap_name, ap_description) VALUES(:ap_fk_u_id, :ap_fk_ac_id, :ap_name, :ap_description);

                UPDATE user SET user.u_portfolio_status = :u_portfolio_status WHERE u_id = :ap_fk_u_id;
               ";

            $stmt = $this->conn->prepare($sql);

            $stmt->bindParam(':ap_fk_u_id', $param->user_id);
            $stmt->bindParam(':ap_fk_ac_id', $param->category_id);
            $stmt->bindParam(':ap_name', $param->p_name);
            $stmt->bindParam(':ap_description', $param->description);
            $stmt->bindParam(':u_portfolio_status', $param->portfolio_status);

            $result = $stmt->execute();

            $portfolio_id = '';
            if ($result) {
                $portfolio_id = $this->conn->lastInsertId();
            }
            $stmt->closeCursor();

            if ($result) {

                // information successfully inserted
                return array('res' => 1, 'portfolio_id' => $portfolio_id);
            } else {
                // Failed to create user
                return array('res' => 0, 'portfolio_id' => '');
            }
        } else {
            return array('res' => 2, 'portfolio_id' => '');
        }
    }

    /*     * --- update_portfolio --- */

    public function update_portfolio($param) {
        // print_r($param); die;
        $sql = "UPDATE artist_portfolio SET ap_name = :ap_name, ap_description = :ap_description WHERE ap_fk_u_id = :ap_fk_u_id AND ap_id = :ap_id;

        UPDATE user SET user.u_portfolio_status = :u_portfolio_status WHERE user.u_id = :ap_fk_u_id; ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':ap_name', $param->p_name);
        $stmt->bindParam(':ap_description', $param->description);
        $stmt->bindParam(':ap_fk_u_id', $param->user_id);
        $stmt->bindParam(':ap_id', $param->portfolio_id);
        //$stmt->bindParam(':ap_fk_ac_id', $param->category_id);
        $stmt->bindParam(':u_portfolio_status', $param->portfolio_status);

        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }

    /** get_portfolio
     *  @param $user_id, $category_id
     */
    public function get_portfolio($user_id, $category_id) {
        $sql = "SELECT  * FROM artist_portfolio WHERE ap_fk_u_id = :ap_fk_u_id AND ap_fk_ac_id = :ap_fk_ac_id ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':ap_fk_u_id', $user_id);
        $stmt->bindParam(':ap_fk_ac_id', $category_id);

        if ($stmt->execute()) {
            $user = $stmt->fetchAll();
            $stmt->closeCursor();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * get_portfolio_userlist  
     * @param $user_id
     */
    public function get_portfolio_userlist($user_id, $lang = 'eng') {

        $sql = "SELECT artist_portfolio.ap_id AS portfolio_id, image_master.im_portfolio_action AS image_type, artist_portfolio.ap_fk_ac_id AS category_id, image_master.im_id, artist_portfolio.ap_name AS portfolio_name, artist_portfolio.ap_description AS portfolio_desc, artist_portfolio.ap_fk_ac_id, image_master.im_image_name, category_attributes.ca_name AS category_name, image_master.im_action, category_attributes.ca_description AS category_description  FROM artist_portfolio
        
        INNER JOIN artist_category
        ON
        artist_category.ac_id = artist_portfolio.ap_fk_ac_id

        /*INNER JOIN category_master
        ON
        category_master.cm_id = artist_portfolio.ap_fk_ac_id*/
        INNER JOIN category_attributes
        ON
        category_attributes.ca_fk_cm_id = artist_category.ac_fk_cm_id
        AND
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name='" . $lang . "')




          
          LEFT JOIN image_master
          ON 
          image_master.im_fk_u_id = artist_portfolio.ap_fk_u_id
          AND
          image_master.im_action='portfolio'
          AND
          image_master.im_deleted='false'
          AND
          image_master.im_status = 'true'
          AND 
          image_master.im_fk_ac_id = artist_portfolio.ap_id


          WHERE artist_portfolio.ap_fk_u_id=".$user_id." AND artist_portfolio.ap_status='true' ORDER BY artist_portfolio.ap_id  DESC";


        $stmt = $this->conn->prepare($sql);

        //$stmt->bindParam(':ap_fk_u_id', $user_id);
        if ($stmt->execute()) {
            $user = $stmt->fetchAll();
            $stmt->closeCursor();
            
            return $user;
        } else {
            return NULL;
        }
    }

    /** get_portfolio_ID
     *  @param String $id
     */
    public function get_portfolio_ID($portfolio_id) {
        $sql = "select * from artist_portfolio where ap_id = ?";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1, $portfolio_id);
        if ($stmt->execute()) {
            $user = $stmt->fetch();
            $stmt->closeCursor();
            return $user;
        } else {
            return NULL;
        }
    }

    /*
     * FUNCTION deleted_portfolio
     * @param String $user_id, $portfolio_id
     */

    public function deleted_portfolio($user_id, $portfolio_id) {
        // print_r($user_id); print_r($portfolio_id); die;
        $sql = "DELETE FROM artist_portfolio WHERE ap_fk_u_id = :ap_fk_u_id AND ap_id = :ap_id";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':ap_fk_u_id', $user_id);
        $stmt->bindParam(':ap_id', $portfolio_id);

        if ($stmt->execute()) {

            $result = $stmt->rowCount();
            $stmt->closeCursor();
        } else {
            $result = NULL;
        }

        return $result;
    }

    /** function get_artist_detailes
     *  @param String $user_id
     */
    public function get_artist_detailes($mua_id) {

        $sql = "SELECT *FROM (SELECT im.im_id, ap_profile_name, IF(user.u_phone='0','',user.u_phone) AS ap_phone, user.u_email, ap_website, ap_about, ap_email, ap_city, im.im_image_name, im.im_action, ap_address, IF(ap_rpm=0,'',ap_rpm) AS ap_rpm, ap_language, ap_zipcode, ap_created,ta_name, ta_name_spn, ta_id,
        ap_certificate_status, ap_comment,

         (SELECT SUM(cpi_paid_amount-(cpi_service_amout/2)*cpi_commission/100) FROM client_payment_information WHERE cpi_fk_artist_id = :ap_fk_u_id LIMIT 1 ) AS total_earn,

         (SELECT count(client_request.cr_id) FROM client_request WHERE cr_fk_u_mua_id = :ap_fk_u_id AND cr_status = 'finished'  ORDER BY cr_id LIMIT 1 ) AS total_jobs,
         ap_state

         FROM artist_profile AS AP
         
         LEFT JOIN image_master AS im
         ON
         im.im_fk_u_id = AP.ap_fk_u_id
         AND
         im.im_status = 'true'
         AND
         im.im_deleted = 'false'

         LEFT JOIN user
         ON
         user.u_id = AP.ap_fk_u_id

         LEFT JOIN typr_of_arts
         ON
         typr_of_arts.ta_id = user.u_type_of_arts

         
       
        WHERE ap_fk_u_id = :ap_fk_u_id AND ap_status = 'true' ORDER BY im.im_updated DESC) AS a GROUP BY a.im_action";


        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ap_fk_u_id', $mua_id);
        if ($stmt->execute()) {
            $user = $stmt->fetchALL();
            $stmt->closeCursor();
        } else {
            $user = NULL;
        }
        return $user;
    }

    /** function get_availability
     *  @param String $user_id
     */
    public function get_availability($user_id) {
        $sql = "SELECT aa_id, aa_days, aa_time_from, aa_time_to, IF(aa_active_status='true',1,0) AS aa_active_status FROM artist_availability WHERE aa_fk_u_id = :aa_fk_u_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':aa_fk_u_id', $user_id);



        if ($stmt->execute()) {

            $result = $stmt->fetchAll();
            $stmt->closeCursor();
            return $result;
        } else {
            return NULL;
        }
    }

    /**
     * update_artist_profile
     * @param 
     */
    public function edit_artist_availability($param) {

        if($param->time_from!="" || $param->time_to!=""){

            $sql = "UPDATE artist_availability SET aa_time_from = :aa_time_from, aa_time_to = :aa_time_to, aa_active_status = :aa_active_status WHERE aa_fk_u_id = :aa_fk_u_id AND aa_id = :aa_id ";

            

        }else{
                $sql = "UPDATE artist_availability SET aa_active_status = :aa_active_status WHERE aa_fk_u_id = :aa_fk_u_id AND aa_id = :aa_id ";
            
        } 




        $stmt = $this->conn->prepare($sql);


        
        $stmt->bindParam(':aa_active_status', $param->status);
        $stmt->bindParam(':aa_fk_u_id', $param->user_id);
        $stmt->bindParam(':aa_id', $param->aa_id);

        if($param->time_from || $param->time_to){
            $stmt->bindParam(':aa_time_from', $param->time_from);
            $stmt->bindParam(':aa_time_to', $param->time_to);
        }





        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }

    /** function get_mua_client
     *  @param $user_id
     */
    public function get_mua_client($user_id, $lang) {

        $sql = "SELECT client_request.cr_id, client_request.cr_fk_u_client_id AS client_id, client_request.cr_booking_date AS booking_date,
            (
            SELECT category_attributes.ca_name FROM client_request 
            INNER JOIN artist_category 
            ON
            artist_category.ac_id = client_request.cr_fk_ac_id
             
            INNER JOIN category_master 
            ON
            category_master.cm_id = artist_category.ac_fk_cm_id


            INNER JOIN category_attributes
            ON
            category_attributes.ca_fk_cm_id = category_master.cm_id
            AND
            category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lang)
                
             WHERE cr_fk_u_client_id = client_request.cr_fk_u_client_id ORDER BY cr_id DESC LIMIT 1
            )  

            AS latest_service,

            (SELECT u_name FROM user 
                WHERE user.u_id = client_request.cr_fk_u_client_id) AS client_name,

            (
             SELECT im_image_name FROM image_master 
             WHERE image_master.im_fk_u_id = client_request.cr_fk_u_client_id 
             AND im_action = 'client' AND im_status = 'true' AND im_deleted = 'false' 
             ORDER BY im_id DESC LIMIT 1
            ) AS client_profile_image,

            (SELECT ROUND(AVG(rr_rating),2) FROM rating_remarks 
                WHERE rr_fk_client_id = client_request.cr_fk_u_client_id  AND rr_status='true' AND rr_review_by='mua') AS avg_rating,

            (SELECT count(rr_remarks) FROM rating_remarks 
                WHERE rr_fk_client_id = client_request.cr_fk_u_client_id  AND rr_status='true' AND rr_review_by='mua') AS avg_reviews

            FROM client_request WHERE cr_id IN (SELECT MAX(cr_id) as cr_id FROM client_request WHERE cr_fk_u_mua_id = :cr_fk_u_mua_id AND cr_status = 'finished' GROUP BY cr_fk_u_client_id)";

            //FROM client_request WHERE cr_id = (SELECT MAX(cr_id) as cr_id FROM client_request WHERE cr_fk_u_mua_id=:cr_fk_u_mua_id AND cr_status = 'finished' GROUP BY cr_fk_u_client_id)";

            //IN (SELECT MAX(dj_id) as dj_id FROM driver_journey WHERE dj_fk_ucr_id = $id AND dj_status NOT IN ('true','false') GROUP BY dj_date) ORDER BY dj_id DESC



        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cr_fk_u_mua_id', $user_id);
        $stmt->bindParam(':lang', $lang);


        if ($stmt->execute()) {

            $result = $stmt->fetchAll();
            $stmt->closeCursor();
            //print_r($result); die;
            return $result;
        } else {
            return NULL;
        }
    }

    /** function get_mua_timeline
     *  @param $user_id
     */
    public function get_mua_timeline($user_id, $lang) {
        $result = array();
        $sql = "SELECT CR.cr_id AS bookingId, CR.cr_fk_u_client_id AS clientId, 

            category_attributes.ca_name AS makeUpName,
            category_attributes.ca_description AS makeUpdesc, 

            /*(
            SELECT cm_name FROM category_master WHERE cm_id = AC.ac_fk_cm_id LIMIT 1
            ) AS makeUpName,
            (
            SELECT cm_description FROM category_master WHERE cm_id = AC.ac_fk_cm_id LIMIT 1
            ) AS makeUpdesc,*/
            
            US.u_name AS clientName, CONCAT('', FORMAT(AC.ac_price, 2)) AS amount, CONCAT('', FORMAT(CR.cr_grand_price , 2)) AS grand_price, CR.cr_booking_date AS bookingDate, CR.cr_start_time AS start_time, CR.cr_end_time AS end_time, CR.cr_status AS bookingStatus 
            
            FROM client_request AS CR 

            INNER JOIN artist_category
            ON
            artist_category.ac_id = CR.cr_fk_ac_id
            INNER JOIN category_master
            ON
            category_master.cm_id = artist_category.ac_fk_cm_id
            INNER JOIN category_attributes
            ON
            category_attributes.ca_fk_cm_id = category_master.cm_id
            AND
            category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lm_name)

            LEFT JOIN artist_category AS AC
            ON 
            AC.ac_id = CR.cr_fk_ac_id

            LEFT JOIN user AS US
            ON
            US.u_id = CR.cr_fk_u_client_id

            LEFT JOIN client_profile AS CP
            ON
            CP.cp_fk_u_id = CR.cr_fk_u_client_id

            WHERE cr_fk_u_mua_id = :cr_fk_u_mua_id AND cr_status = 'pending'";


        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cr_fk_u_mua_id', $user_id);
        $stmt->bindParam(':lm_name', $lang);
        if ($stmt->execute()) {

            $result = $stmt->fetchAll();
            //print_r($result); die;
            $stmt->closeCursor();
        } else {
            $result = NULL;
        }

        return $result;
    }

    /**
     * FUNCTION mua_action
     * @param 
     */
    public function mua_action($param) {
        $by = $param->mua_id ? $param->mua_id : $param->user_id;
        $action_taken_by = $by . ',' . $param->action . ',' . time();

        $where = $param->mua_id ? ('cr_fk_u_mua_id =' . $param->mua_id) : ('cr_fk_u_client_id =' . $param->user_id);

        $sql = "UPDATE client_request SET cr_status = :cr_status, cr_action_taken_by = CONCAT(cr_action_taken_by, ';', :cr_action_taken_by) WHERE " . $where . " AND cr_id = :cr_id";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cr_status', $param->action);
        $stmt->bindParam(':cr_action_taken_by', $action_taken_by);
        $stmt->bindParam(':cr_id', $param->requsetId);

        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows;
    }

    /**
     * FUNCTION insert_canceled_bookings
     * @param 
     */
    public function insert_canceled_bookings($params) {

        $sql = "INSERT INTO canceled_bookings (cb_fk_cab_id, )";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cr_status', $param->action);
        $stmt->bindParam(':cr_fk_u_mua_id', $param->mua_id);
        $stmt->bindParam(':cr_fk_u_client_id', $param->user_id);
        $stmt->bindParam(':cr_id', $param->requsetId);

        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows;
    }

    /**
     * FUNCTION get_client_request
     * @param $user_id, $requestData
     */
    public function get_client_request($param) {

        $sql = "SELECT cr_fk_u_client_id, cr_status FROM client_request WHERE cr_id = :cr_id";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cr_id', $param->requsetId);

        if ($stmt->execute()) {
            $result = $stmt->fetch();
            $stmt->closeCursor();
        } else {
            $result = NULL;
        }
        return $result;
    }

    /**
     * FUNCTION get_subscription
     * @param
     */
    public function get_subscription() {
        $sql = "SELECT asp_id AS plan_id, asp_name, asp_name_spn, asp_image, asp_duration, asp_description, asp_description_spn, CONCAT('', FORMAT(asp_amount, 2)) AS asp_amount, asp_client_limit, asp_commission_after_limit FROM artist_subscription_plan WHERE asp_status = 'true' AND asp_deleted = 'false' ORDER BY asp_orderby ASC";
        $stmt = $this->conn->prepare($sql);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll();
            
            $stmt->closeCursor();
        } else {
            $result = NULL;
        }
        return $result;
    }

    /*
     * * insert the data rating_remarks table in create_rating
     * @param $user_id
     */

    public function get_rating($user_id, $lang) {
        
        $sql = "SELECT category_attributes.ca_name AS category_name, u_id AS client_id, u_name AS client_name, im_image_name AS client_profile_image, rr_rating AS rating, rr_remarks AS reviews FROM rating_remarks
          
          INNER JOIN user
          ON
          user.u_id = rating_remarks.rr_fk_client_id

          INNER JOIN client_artist_booking
          ON
          client_artist_booking.cab_id = rating_remarks.rr_fk_cab_id

          INNER JOIN client_request
          ON
          client_artist_booking.cab_fk_cr_id = client_request.cr_id

          INNER JOIN artist_category
          ON
          artist_category.ac_id = client_request.cr_fk_ac_id

          INNER JOIN category_master
          ON
          category_master.cm_id = artist_category.ac_fk_cm_id
          INNER JOIN category_attributes
          ON
          category_attributes.ca_fk_cm_id = category_master.cm_id
          AND
          category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lang)

          LEFT JOIN image_master 
          ON
          image_master.im_fk_u_id = rating_remarks.rr_fk_client_id
          AND
          image_master.im_action='client'
          AND
          image_master.im_status='true'
          AND
          image_master.im_deleted='false'

       WHERE rating_remarks.rr_fk_mua_id = :rr_fk_mua_id AND rating_remarks.rr_status = 'true' AND rr_review_by = 'client'";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(":rr_fk_mua_id", $user_id);
        $stmt->bindParam(":lang", $lang);


        if ($stmt->execute()) {

            $result = $stmt->fetchAll();
            $stmt->closeCursor();
            return $result;
        } else {
            return NULL;
        }
    }



    /** function get_mua_client
     *  @param $user_id
     */
    public function chek_mua_client($param) {

        $sql = "SELECT * FROM client_request WHERE cr_fk_u_mua_id = :cr_fk_u_mua_id AND cr_id = :cr_id AND cr_status != 'finished'";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cr_fk_u_mua_id', $param->mua_id);
        $stmt->bindParam(':cr_id', $param->requsetId);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        if (count($result)>0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    public function update_type_of_arts($type_of_arts, $u_id){

        $sql = "UPDATE user SET u_type_of_arts = :u_type_of_arts WHERE u_id = :u_id";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':u_id', $u_id);
        $stmt->bindParam(':u_type_of_arts', $type_of_arts);
        

        $stmt->execute();
        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows;
    }

    public function get_type_of_arts(){
        $sql = "SELECT * FROM typr_of_arts WHERE ta_status = 'true'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
        $stmt->closeCursor();
    }

}