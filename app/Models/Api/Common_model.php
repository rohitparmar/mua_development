<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Common_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }

    /* ------------- `users` table method ------------------ */

    
  public function update_image($param){
    $sql = "INSERT INTO image_master(im_fk_u_id, im_portfolio_action, im_image_name, im_action, im_created, im_lat, im_long, im_fk_ac_id) VALUES ('".$param->user_id."', '".$param->im_portfolio_action."',  '".$param->image."', '".$param->action."', '".$param->date."', '".$param->user_lat."', '".$param->user_long."', '".$param->portfolio_id."')";


    $stmt = $this->conn->prepare($sql);

    $result = $stmt->execute();

    $id = '';
    if($result){
      $id = $this->conn->lastInsertId();
    }
    $stmt->closeCursor();

            
    if($result){
      return array('res' => 1, 'id' => $id);
    }
    else{
      return array('res' => 0, 'id' => '');
    }
}


    public function check_upload_image($param){


        if(isset($param->action) && $param->action == 'portfolio'){
            $sql = "SELECT * FROM image_master WHERE (im_fk_u_id = :im_fk_u_id AND im_action = :im_action AND im_fk_ac_id = :im_fk_ac_id AND im_portfolio_action = :im_portfolio_action AND im_status = 'true' AND im_deleted = 'false')"; 
            $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':im_fk_u_id', $param->user_id);
        $stmt->bindParam(':im_action', $param->action);
        $stmt->bindParam(':im_fk_ac_id', $param->portfolio_id);
        $stmt->bindParam(':im_portfolio_action', $param->im_portfolio_action);   
        }else{
            $sql = "SELECT * FROM image_master WHERE im_fk_u_id = :im_fk_u_id AND im_action = :im_action";
            $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':im_fk_u_id', $param->user_id);
        $stmt->bindParam(':im_action', $param->action);
        
        }

        
        

        if($stmt->execute()){
            $result = $stmt->fetchAll();

            $stmt->closeCursor();
        }else
        {
            $result = NULL;
        }
        return current($result);


    }







  public function get_image_name($id){

    $sql = "SELECT im_id, im_image_name, im_action FROM image_master WHERE im_id = :im_id AND im_status = 'true' AND im_deleted = 'false'";

    $stmt = $this->conn->prepare($sql);

    $stmt->bindParam(':im_id', $id);

    if($stmt->execute()){
        $result = $stmt->fetch();
        $stmt->closeCursor();
    }else
    {
        $result = NULL;
    }
    return $result;

  }



  public function update_upload_image($param){ 

    //print_r($param); die;
    $sql = "UPDATE image_master SET im_image_name = :im_image_name WHERE im_id = :im_id";

    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':im_id', $param->image_id);
    $stmt->bindParam(':im_image_name', $param->image);
    
    $stmt->execute();

    $num_affected_rows = $stmt->rowCount();
    $stmt->closeCursor();
    return $num_affected_rows > 0;

            
    

    


  }

}