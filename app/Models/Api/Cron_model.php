<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Cron_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }


    public function get_pending_booking(){
        /*$sql = "SELECT *,

            (SELECT u_gcm_id FROM user WHERE u_id = cr_fk_u_client_id) AS client_gcm_id,
            (SELECT u_gcm_id FROM user WHERE u_id = cr_fk_u_mua_id) AS mua_gcm_id


         FROM client_request WHERE cr_status='pending' ORDER BY cr_id DESC";*/


    echo    $sql = "SELECT *, (UNIX_TIMESTAMP(UTC_TIMESTAMP()) - 15*60 -b.actual_booking_time) as diff FROM 
                (
                 SELECT *, (get_zero_time(a.cr_start_time, a.u_time_zone )) as actual_booking_time
                    FROM
                    (
                        SELECT *,
                    
                                (SELECT u_gcm_id FROM user WHERE u_id = cr_fk_u_client_id) AS client_gcm_id,
                                (SELECT u_gcm_id FROM user WHERE u_id = cr_fk_u_mua_id) AS mua_gcm_id,
                                (SELECT u_time_zone FROM user WHERE u_id = cr_fk_u_mua_id) AS u_time_zone
                                
                    
                    
                             FROM client_request WHERE cr_status='pending' 
                            
                             ORDER BY cr_id DESC
                     ) AS a
                ) AS b WHERE (UNIX_TIMESTAMP(UTC_TIMESTAMP()) - 15*60 -b.actual_booking_time)<15*60";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $times = $stmt->fetchAll();
        } else {
            $times = false;
        }
        $stmt->closeCursor();
        return $times;
    }


    public function get_artist_booked_time_list($mua_id, $booking_time, $category_id) {
        $sql = "SELECT cr_id, cr_fk_u_client_id, cr_fk_u_mua_id, ROUND(cr_start_time/1000,0) AS cr_start_time, ROUND(cr_end_time/1000,0) AS cr_end_time, cr_booking_time_zone, cr_status FROM client_request WHERE cr_status='accepted' AND cr_fk_u_mua_id = " . $mua_id . " AND date(from_unixtime(cr_booking_date/1000))= date(from_unixtime(" . $booking_time . "/1000))"; //  AND cr_fk_ac_id = ".$category_id;

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $times = $stmt->fetchAll();
        } else {
            $times = false;
        }
        $stmt->closeCursor();
        return $times;
    }

    public function get_mua_time_info($mua_id, $day_name) {
        $sql = "SELECT aa_id,  (SELECT u_time_zone FROM user WHERE u_id = " . $mua_id . " LIMIT 1) AS mua_time_zone, ROUND(aa_time_from/1000,0) AS aa_time_from, ROUND(aa_time_to/1000,0) AS aa_time_to FROM artist_availability WHERE aa_days='" . $day_name . "' AND aa_fk_u_id=" . $mua_id . " AND aa_active_status=1";

        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $times = $stmt->fetch();
        } else {
            $times = false;
        }
        $stmt->closeCursor();
        return $times;
    }

    public function get_artist_availabile_time_slot($param) {


        $sql = "SELECT ROUND(cr_start_time/1000,0)+19800 AS service_start_time, ROUND(cr_end_time/1000,0) AS service_end_time, 
              (
              SELECT ROUND(aa_time_from/1000,0)+19800 FROM artist_availability WHERE aa_days = DAYNAME ('2016-07-06') AND aa_active_status='true' AND aa_fk_u_id = 1 ORDER BY aa_id LIMIT 1
              ) AS mua_service_start_time, 
              (
              SELECT ROUND(aa_time_to/1000,0)+19800 FROM artist_availability WHERE aa_days = DAYNAME ('2016-07-06') AND aa_active_status='true' AND aa_fk_u_id = 1 ORDER BY aa_id LIMIT 1
              ) AS mua_service_end_time 
              FROM `client_request` WHERE cr_booking_date = '2016-07-16' AND cr_status = 'accepted' AND cr_fk_u_mua_id = " . $param->user_id;

        /* $sql = "SELECT aa_days,  ROUND(aa_time_from/1000,0)+19800 AS aa_time_from, ROUND(aa_time_to/1000,0)+19800 AS aa_time_to FROM artist_availability WHERE aa_fk_u_id = 1 AND aa_active_status = 'true' AND aa_days = DAYNAME ('2016-07-06') "; */
        //$sql = "SELECT * FROM client_request";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute()) {
            $services = $stmt->fetchAll();

            $stmt->closeCursor();
        } else {
            $services = false;
        }
        //print_r($services); die;
        return $services;
    }

    
}
