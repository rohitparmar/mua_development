<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Category_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }

    /* ------------- `category` table method ------------------ */

/**
   * Creating get_category
*/
    public function get_category($lang='eng'){
         
        $sql = "SELECT category_master.cm_id, category_attributes.ca_name as cm_name, category_attributes.ca_description as cm_description, category_master.cm_status, category_master.cm_image, category_master.cm_fk_parent_id ,
        CONCAT('', FORMAT(category_attributes.ca_price, 2)) AS ca_price, category_attributes.ca_skintone, category_attributes.ca_eye_shadow, category_attributes.ca_lip_color,category_attributes.ca_duration
        FROM category_master 
        INNER JOIN category_attributes
        ON
        category_attributes.ca_fk_cm_id = category_master.cm_id
        AND
        category_attributes.status = 'true'
        AND 
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name='".$lang."' AND lm_status='true') 

        WHERE category_master.cm_status = 'true' AND category_master.cm_action_taken = 'default'";
        $stmt = $this->conn->prepare($sql);
        if($stmt->execute()){

            $result = $stmt->fetchAll();
            $stmt->closeCursor();
            return $result;

        }else{
            return NULL;
        }
    }

public function manage_portfolio_during_artist_category_create($info){
    //print_r($info); die;
    $sql = "SELECT * FROM `artist_portfolio` WHERE ap_fk_u_id=".$info->user_id." AND ap_fk_ac_id=".$info->category_id;
    $stmt = $this->conn->prepare($sql);
    $stmt->execute();
    if($stmt->rowCount() == 0){
        //now get data from category_master and insert into artist_portfolio table
        $userId            = $info->user_id;
        $catId             = $info->category_id;
        $catInfo           = $this->get_category_image($info->category_id);
        $cm_name           = $catInfo['cm_name'];
        $cm_description    = $catInfo['cm_description'];
        $cm_before_image   = false;
        $cm_after_image    = false;
        $ext = current(array_reverse(explode('.',$catInfo['cm_before_image'])));
        if(file_exists(CATEGORY_PORTFOLIO_BEFORE_IMG.$catInfo['cm_before_image'])){
            $cm_before_image   = $userId.'-before-img-'.time().'-'.rand(1000,9999).'.'.$ext;
        }

        $ext = current(array_reverse(explode('.',$catInfo['cm_after_image'])));
        if(file_exists(CATEGORY_PORTFOLIO_AFTER_IMG.$catInfo['cm_after_image'])){
            $cm_after_image   = $userId.'-after-img-'.time().'-'.rand(1000,9999).'.'.$ext;
        }

       
        //now insert into artist_portfolio table
        $sql = "INSERT INTO artist_portfolio(ap_fk_u_id, ap_fk_ac_id, ap_name, ap_description )VALUES('".$userId."', '".$catId."', '".$cm_name."', '".$cm_description."')";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $portfolioId = $this->conn->lastInsertId();

        //now insert into category_master table

        $lat    = '0';
        $long   = '0';

        $sql    = "INSERT INTO image_master(im_fk_u_id, im_fk_ac_id, im_image_name, im_action, im_portfolio_action, im_lat, im_long )VALUES('".$userId."', '".$portfolioId."', '".$cm_before_image."', 'portfolio', 'before', '".$lat."', '".$long."');";
        $sql    .= "INSERT INTO image_master(im_fk_u_id, im_fk_ac_id, im_image_name, im_action, im_portfolio_action, im_lat, im_long )VALUES('".$userId."', '".$portfolioId."', '".$cm_after_image."', 'portfolio', 'after', '".$lat."', '".$long."')";

        $stmt   = $this->conn->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()){
            //now copy above category images to /var/www/public_html/v1/artist-client-upload-file/artist/portfolio
            if($cm_before_image){
                //copy before image
                $sourceImage = CATEGORY_PORTFOLIO_BEFORE_IMG.'/'.$catInfo['cm_before_image'];
                $destinationImage = ARTIST_PORTFOLIO_IMG.'/'.$cm_before_image;
                @copy($sourceImage, $destinationImage);
            }

            if($cm_after_image){
                //copy after image
                $sourceImage = CATEGORY_PORTFOLIO_AFTER_IMG.'/'.$catInfo['cm_after_image'];
                $destinationImage = ARTIST_PORTFOLIO_IMG.'/'.$cm_after_image;
                @copy($sourceImage, $destinationImage);
            }
        }
    }
    $stmt->closeCursor();
}
/*
** functon add_category
*/
public function manage_category($param){
    if(!$this->find_artist_profile($param->user_id,$param->category_id)){
        //manage artist category here
        $sql = "INSERT INTO artist_category(ac_fk_u_id, ac_fk_cm_id, ac_price , ac_products , ac_duration ) VALUES (:ac_fk_u_id, :ac_fk_cm_id, :ac_price, :ac_products, :ac_duration)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ac_fk_u_id', $param->user_id);
        $stmt->bindParam(':ac_fk_cm_id', $param->category_id);
         $stmt->bindParam(':ac_price', $param->price);
        $stmt->bindParam(':ac_products', $param->products);
        $stmt->bindParam(':ac_duration', $param->duration);
        $result = $stmt->execute();

         $id = '';
        if($result){
          $id = $this->conn->lastInsertId();
          if($id){
            //now manage artist portfolio from category master for before and after images as well as name and description
            $this->manage_portfolio_during_artist_category_create($param);
          }
        }

        $stmt->closeCursor();


        if($result){
          return array('res' => 1, 'id' => $id);
        }
        else{
            return array('res' => 0, 'id' => '');
        }
    }else{
          return array('res' => 2, 'id' => '');
    }  
}


    public function manage_category_xxxx($param){
    if(!$this->find_artist_profile($param->user_id,$param->category_id)){

        //manage artist category here
        $sql = "INSERT INTO artist_category(ac_fk_u_id, ac_fk_cm_id, ac_price , ac_products , ac_duration ) VALUES (:ac_fk_u_id, :ac_fk_cm_id, :ac_price, :ac_products, :ac_duration)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ac_fk_u_id', $param->user_id);
        $stmt->bindParam(':ac_fk_cm_id', $param->category_id);
         $stmt->bindParam(':ac_price', $param->price);
        $stmt->bindParam(':ac_products', $param->products);
        $stmt->bindParam(':ac_duration', $param->duration);
        $result = $stmt->execute();

         $id = '';
        if($result){
          $id = $this->conn->lastInsertId();
        }

        $stmt->closeCursor();



        
        if($result){
          return array('res' => 1, 'id' => $id);
        }
        else{
            return array('res' => 0, 'id' => '');
        }
    }
      else{
          return array('res' => 2, 'id' => '');
      }  
    }



public function find_artist_profile($user_id, $category_id){
        //print_r($user_id); die;

        $sql = "SELECT * FROM artist_category WHERE ac_fk_u_id = :ac_fk_u_id AND ac_fk_cm_id = :ac_fk_cm_id AND ac_status = 'true'";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ac_fk_u_id', $user_id);
        $stmt->bindParam(':ac_fk_cm_id', $category_id);
           
        if($stmt->execute()){

            $user = $stmt->fetch();
            $stmt->closeCursor();
           // print_r($user); die;
        }
        else{
            $user = NULL;
        }
        return $user;
    }














/*==== function mua_category=== 
** @params $user_id
*/    
    public function mua_category($user_id){
        $sql = "SELECT ac_id, ac_fk_cm_id, CONCAT('', FORMAT(ac_price, 2)) AS ac_price, ac_products, ac_duration FROM artist_category  WHERE ac_fk_u_id = :ac_fk_u_id AND ac_status = 'true' ";
        /*$sql = "SELECT cm_id, cm_name, cm_description, cm_status, cm_fk_parent_id, 
                (SELECT `ac_id` FROM `artist_category` WHERE ac_status = 'true' AND ac_fk_u_id = :ac_fk_u_id  LIMIT 1) AS ac_id , 
                (SELECT `ac_fk_cm_id` FROM `artist_category` WHERE ac_status = 'true' AND ac_fk_u_id = :ac_fk_u_id LIMIT 1) AS ac_fk_cm_id , 
                (SELECT `ac_price` FROM `artist_category` WHERE ac_status = 'true' AND ac_fk_u_id = :ac_fk_u_id LIMIT 1) AS ac_price, 
                (SELECT `ac_products` FROM `artist_category` WHERE ac_status = 'true' AND ac_fk_u_id = :ac_fk_u_id LIMIT 1) AS ac_products  FROM category_master WHERE cm_status = 'true' AND cm_action_taken = 'default'";*/
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ac_fk_u_id', $user_id);

        if($stmt->execute()){
             
             $result = $stmt->fetchAll();
             $stmt->closeCursor();

             return $result;
            }else{
                return NULL;
            }
    }


/*==== function mua_category=== 
 ** @params $user_id
*/ 

    public function delete_category($user_id, $category_id){
        
        $sql = "UPDATE artist_category SET ac_status = 'false' WHERE ac_id = :ac_id AND ac_fk_u_id = :ac_fk_u_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ac_id',$category_id);
        $stmt->bindParam(':ac_fk_u_id',$user_id);

        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;

    }



/*==== function edit_category=== 
 ** @params $param
*/
    public function edit_category($param){
        $sql = "SELECT cm_price FROM category_master WHERE cm_id = (SELECT ac_fk_cm_id FROM artist_category WHERE ac_fk_u_id = :ac_fk_u_id AND ac_id = :ac_id ORDER BY ac_id DESC LIMIT 1)";
		$stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ac_fk_u_id', $param->user_id);
        $stmt->bindParam(':ac_id', $param->ac_id);
		if($stmt->execute()){
			$result = $stmt->fetch();
            $cm_price = $result['cm_price'];
			if($param->price>=$cm_price){
				$sql = "UPDATE artist_category SET ac_price = :ac_price, ac_products = :ac_products, ac_duration = :ac_duration, ac_status =:ac_status WHERE
				 ac_fk_u_id = :ac_fk_u_id AND ac_id = :ac_id";
				$stmt = $this->conn->prepare($sql);
				$stmt->bindParam(':ac_price', $param->price);
				$stmt->bindParam(':ac_products', $param->products);
				$stmt->bindParam(':ac_duration', $param->duration);
				$stmt->bindParam(':ac_fk_u_id', $param->user_id);
				$stmt->bindParam(':ac_id', $param->ac_id);
				$stmt->bindParam(':ac_status', $param->status);
				$stmt->execute();
				$num_affected_rows = $stmt->rowCount();
				$stmt->closeCursor();
				return $num_affected_rows > 0;
			}else{
				return 0;
			}
        }else{
			return 0;
		}
    }

    public function create_portfolio($param){
        //print_r($param); die;
        $sql = "INSERT INTO artist_portfolio (ap_fk_u_id, ap_fk_ac_id, ap_before_image, ap_after_image) VALUES(:ap_fk_u_id, :ap_fk_ac_id, :ap_before_image, :ap_after_image)";

        $stmt = $this->conn->prepare($sql);
                
        $stmt->bindParam(':ap_fk_u_id',$param->user_id);
        $stmt->bindParam(':ap_fk_ac_id',$param->category_id);
        $stmt->bindParam(':ap_before_image',$param->cm_before_image);
        $stmt->bindParam(':ap_after_image',$param->cm_after_image);
        
        $result = $stmt->execute();

        $portfolio_id = '';
        if($result){
            $portfolio_id = $this->conn->lastInsertId();
        }
        $stmt->closeCursor();

        if($result){
            // information successfully inserted
            return array('res' => 1, 'portfolio_id' => $portfolio_id);
        }
        else{
            // Failed to create user
            return array('res' => 0, 'portfolio_id' => '');
        }
    }  


    public function get_category_image($id){
        $sql = "SELECT cm_name, cm_description, cm_image, cm_before_image, cm_after_image FROM category_master WHERE cm_id = :cm_id";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':cm_id', $id);
        
        if($stmt->execute()){
            $result = $stmt->fetch();
            $stmt->closeCursor();
        }else{
            $result = false;
        }
        return $result;
    }  


     public function get_portfolio($user_id, $category_id){
       $sql = "SELECT  * FROM artist_portfolio WHERE ap_fk_u_id = :ap_fk_u_id AND ap_fk_ac_id = :ap_fk_ac_id ";
       
       $stmt = $this->conn->prepare($sql);
       
       $stmt->bindParam(':ap_fk_u_id', $user_id);
       $stmt->bindParam(':ap_fk_ac_id', $category_id);
       
       if($stmt->execute()){
        $user = $stmt->fetchAll();
        $stmt->closeCursor();
        return $user;
       }
       else{
            return NULL;
       }
    }


     public function get_skinton($lang){
       $sql = "SELECT  * FROM master_skintone WHERE ms_status = 'true' AND language = (SELECT lm_id FROM language_master WHERE lm_name='".$lang."' AND lm_status='true')";
       
       $stmt = $this->conn->prepare($sql);

       $user = array();
       
     if($stmt->execute()){
        $user = $stmt->fetchAll();
       
        return $user;
       }
       else{
        return $user;
       }

        $stmt->closeCursor();
    }



public function select_category($lang='eng', $client_id){

    $lng = $lang=='eng'?1:2;

    $sql = "SELECT *, 
		(SELECT mcm_img FROM master_catgegory_mapping WHERE mcm_service_name='master_skintone' AND mcm_fk_ca_id = 
        (SELECT ca_id FROM category_attributes WHERE language = 1 AND ca_fk_cm_id = a.cm_id) AND mcm_service_id = 
        (SELECT ms_fk_ms_id AS skintone_id FROM `master_skintone` WHERE ms_id = 
        (SELECT cp_skin_color FROM client_profile WHERE cp_fk_u_id='".$client_id."'))) AS skintone_img,
		
        IF(a.mcm_service_name='master_skintone', (SELECT ms_name FROM master_skintone WHERE master_skintone.ms_id = a.mcm_service_id), 

        IF(a.mcm_service_name='master_eyeshadow', 
        (
            
            


            SELECT IF(language='".$lng."',me_name,

            (


                SELECT me_name FROM master_eyeshadow WHERE language='".$lng."' AND me_fk_me_id = (SELECT me_fk_me_id FROM master_eyeshadow WHERE me_id = a.mcm_service_id )
            )

             )  FROM master_eyeshadow WHERE me_id = (a.mcm_service_id)


        ), 
        IF(a.mcm_service_name='master_lips_color', 

        (

            SELECT IF(language='".$lng."',mlc_name,

            (


                SELECT mlc_name FROM master_lips_color WHERE language='".$lng."' AND mlc_fk_mlc_id = (SELECT mlc_fk_mlc_id FROM master_lips_color WHERE mlc_id = a.mcm_service_id )
            )

             )  FROM master_lips_color WHERE mlc_id = (a.mcm_service_id)


        )


        , '')
            

            )
                ) AS service_name,


    IF(a.mcm_service_name='master_skintone', (SELECT ms_image FROM master_skintone  WHERE master_skintone.ms_id = a.mcm_service_id), '') AS service_image


    FROM (
    SELECT 
        category_master.cm_id,
        category_attributes.ca_name AS cm_name, 
        category_attributes.ca_description AS cm_description,
        category_master.cm_status,
        category_master.cm_image,
        category_master.cm_fk_parent_id,
        CONCAT('', FORMAT(category_master.cm_price, 2)) AS ca_price,
		CONCAT('', FORMAT(category_master.cm_price, 2)) AS base_price,
        category_master.cm_duration AS ca_duration,
        
        
        
        category_attributes.ca_id,
        
        
        master_catgegory_mapping.mcm_service_name,
        master_catgegory_mapping.mcm_service_id
        
    FROM category_master
        LEFT JOIN 
        category_attributes 
        ON
        category_attributes.ca_fk_cm_id = category_master.cm_id
        AND
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lm_name AND lm_status='true')
        
        LEFT JOIN 
        master_catgegory_mapping
        ON
        master_catgegory_mapping.mcm_fk_ca_id = category_attributes.ca_id 
        WHERE category_master.cm_action_taken!='deleted' 
		ORDER BY category_master.cm_id ASC
    ) AS a 
            WHERE a.ca_id IS NOT NULL";
    
    $stmt = $this->conn->prepare($sql);
     $stmt->bindParam(':lm_name', $lang);
    if($stmt->execute()){

        $result = $stmt->fetchAll();
        $stmt->closeCursor();
       
        return $result;

    }else{
            return NULL;
        }
    }


    public function add_category($param){
    if(!$this->find_artist_profile($param->user_id,$param->category_id)){

        $sql = "INSERT INTO artist_category(ac_fk_u_id, ac_fk_cm_id, ac_price , ac_products , ac_duration ) VALUES (:ac_fk_u_id, :ac_fk_cm_id, :ac_price, :ac_products, :ac_duration)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ac_fk_u_id', $param->user_id);
        $stmt->bindParam(':ac_fk_cm_id', $param->category_id);
         $stmt->bindParam(':ac_price', $param->price);
        $stmt->bindParam(':ac_products', $param->products);
        $stmt->bindParam(':ac_duration', $param->duration);
        $result = $stmt->execute();

         $id = '';
        if($result){
          $id = $this->conn->lastInsertId();
        }

        $stmt->closeCursor();
                if($result){
                  return array('res' => 1, 'id' => $id);
                }
                else{
                    return array('res' => 0, 'id' => '');
                }
    }
      else{
          return array('res' => 2, 'id' => '');
      }  
    }



}