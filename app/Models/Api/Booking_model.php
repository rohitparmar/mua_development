<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Booking_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }
    



    public function get_booking_info($mua_id, $client_id, $service_id){
        $sql = "SELECT CONCAT('', FORMAT(artist_category.ac_price, 2)) AS price, ac_price, ac_products AS product, ac_duration AS service_duration FROM artist_category WHERE ac_fk_u_id=".$mua_id." AND ac_id =".$service_id." LIMIT 1";
		
		
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetch();
        $info = array();
        if($results){
            $sql = "SELECT *, (SELECT IF(ap_rpm IS NULL, (SELECT s_value FROM settings WHERE s_types = 'rate_per_mile' LIMIT 1), ap_rpm) 
                AS rate_per_mile FROM artist_profile WHERE ap_fk_u_id = ".$mua_id.") AS rate_per_mile FROM "
                    . "(SELECT 'mua' AS user_type, ap_lat AS lat, ap_long as lng FROM artist_profile WHERE ap_fk_u_id=".$mua_id."
                    union
                    SELECT 'client', cp_lat, cp_long FROM client_profile WHERE cp_fk_u_id=".$client_id.") AS a";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            foreach($result as $row){
                $info[$row['user_type']]['lat'] =  $row['lat'];
                $info[$row['user_type']]['long'] = $row['lng'];
                $info['rate_per_mile'] = (double)$row['rate_per_mile']; //money_format('%.2n',$row['rate_per_mile']);
            }
            $info['price'] = $results['price']; //money_format('%.2n', $results['price']);
			$info['ac_price'] = (double)$results['ac_price'];
            $info['product'] = $results['product'];
            $info['duration'] = $results['service_duration'];
            
            $info['distance'] = 0;
        	
			
			
            $sql = "SELECT "
			."(SELECT bt_maskedNumber FROM braintree_tokens WHERE bt_fk_u_id = '".$client_id."' AND bt_status = 'true' AND bt_is_default='true' LIMIT 1) as cardNumber, "
			."ARIAL_DISTANCE(".($info['client']['lat']?$info['client']['lat']:0).", ".($info['mua']['lat']?$info['mua']['lat']:0).", ".($info['client']['long']?$info['client']['long']:0).", ".($info['mua']['long']?$info['mua']['long']:0).", 'M') AS distance";
            
			$stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetch();
            if($result){
                $info['distance'] = round($result['distance'], 2);
				
				//setlocale(LC_MONETARY,"en_US");
				setlocale(LC_MONETARY, 'en_US.UTF-8');
				
                $info['grand_price'] = str_replace('$', '', money_format('%.2n', ($info['ac_price'] + $info['distance']*$info['rate_per_mile'])));
				$info['cardNumber'] = $result['cardNumber'];
            }
        }
        
        return ($info);
   
    }
    
    public function update_current_location($user_id, $mua_id, $request_id){
        
        $sql = "SELECT *, (SELECT IF(ap_rpm IS NULL, (SELECT s_value FROM settings WHERE s_types = 'rate_per_mile' LIMIT 1), ap_rpm) 
            AS rate_per_mile FROM artist_profile WHERE ap_fk_u_id = ".$mua_id.") AS rate_per_mile FROM "
                . "(SELECT 'mua' AS user_type, ap_lat AS lat, ap_long as lng FROM artist_profile WHERE ap_fk_u_id=".$mua_id."
                union
                SELECT 'client', cp_lat, cp_long FROM client_profile WHERE cp_fk_u_id=".$user_id.") AS a";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $info = array();
        foreach($result as $row){
            $info[$row['user_type']]['lat'] =  $row['lat'];
            $info[$row['user_type']]['long'] = $row['lng'];
            $info['rate_per_mile'] = $row['rate_per_mile'];
        }
        $info['distance'] = 0;
        
        //$sql = "SELECT ARIAL_DISTANCE(".$info['client']['lat'].", ".$info['mua']['lat'].", ".$info['client']['long'].", ".$info['mua']['long'].", 'M') AS distance";
		$sql = "SELECT ARIAL_DISTANCE(".($info['client']['lat']?$info['client']['lat']:0).", ".($info['mua']['lat']?$info['mua']['lat']:0).", ".($info['client']['long']?$info['client']['long']:0).", ".($info['mua']['long']?$info['mua']['long']:0).", 'M') AS distance";
		
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        if($result){
            $info['distance'] = round($result['distance'], 2);
        }
        
        //now insert into table for later use
        $sql = "UPDATE client_request SET cr_mua_lat = '".$info['mua']['lat']."', cr_mua_long = '".$info['mua']['long']."', "
                . "cr_client_lat = '".$info['client']['lat']."', cr_client_long = '".$info['client']['long']."', cr_booking_time_distance = '".$info['distance']."', "
                . "cr_mua_rate_per_mile = '".$info['rate_per_mile']."' WHERE cr_id=".$request_id;

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

/**
* Creating new booking information
* @param
*/
    public function booking($param){
        
    if (!$this->check_booking($param)) {
     
        $sql = "INSERT INTO client_request(cr_fk_u_client_id, cr_fk_u_mua_id, cr_booking_date, cr_request_date, cr_start_time, cr_end_time, "
                 . "cr_booking_message, cr_fk_ac_id, cr_created, cr_gift, cr_grand_price, cr_com_grand_price, cr_mua_plan_id, cr_commission, cr_booking_now) "
                 . "VALUES (:cr_fk_u_client_id, :cr_fk_u_mua_id, :cr_booking_date, :cr_request_date, :cr_start_time, :cr_end_time, "
                 . ":cr_booking_message, :cr_fk_ac_id, :cr_request_date, :cr_gift, :cr_grand_price, :cr_com_grand_price, :cr_mua_plan_id, :cr_commission, :cr_booking_now)";
         
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':cr_fk_u_client_id',  $param['user_id']);
        $stmt->bindParam(':cr_fk_u_mua_id',     $param['mua_id']);
        $stmt->bindParam(':cr_booking_date',    $param['book_date']);
        $stmt->bindParam(':cr_request_date',    $param['request_date']);
        $stmt->bindParam(':cr_start_time',      $param['start_time']);
        $stmt->bindParam(':cr_end_time',        $param['end_time']);
        //$stmt->bindParam(':cr_booking_time',  $param['date']);
        $stmt->bindParam(':cr_booking_message', $param['message']);
        $stmt->bindParam(':cr_fk_ac_id',        $param['category_id']);
        $stmt->bindParam(':cr_gift',            $param['gift']);
        $stmt->bindParam(':cr_grand_price',     $param['grand_price']);
        $stmt->bindParam(':cr_com_grand_price', $param['com_grand_price']);
        $stmt->bindParam(':cr_mua_plan_id',     $param['mua_plan_id']);
        $stmt->bindParam(':cr_commission',      $param['commission']);
        $stmt->bindParam(':cr_booking_now',     $param['plan_booking_date']);

        $result = $stmt->execute();

        $request_id = "";

        if($result){
            $request_id = $this->conn->lastInsertId();
        }
         
        $stmt->closeCursor();
         
        if ($result) {
            return array('res' => 1, 'request_id' => $request_id);
        }
        else{
            return array('res' => 0, 'request_id' => '');
        }

    }
    else{
        return array('res' => 2, 'id' => '');
    }        
}


/**
* Creating check_booking function
* @param
*/
public function check_booking($param){
    //print_r($param); die;
    $sql = "SELECT * FROM client_request WHERE cr_fk_u_mua_id = :cr_fk_u_mua_id AND cr_booking_date = :cr_booking_date AND cr_start_time = :cr_start_time AND cr_end_time = :cr_end_time AND cr_fk_ac_id = :cr_fk_ac_id ";

    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':cr_fk_u_mua_id', $param['mua_id']);
    $stmt->bindParam(':cr_booking_date', $param['book_date']);
    $stmt->bindParam(':cr_start_time', $param['start_time']);
    $stmt->bindParam(':cr_end_time', $param['end_time']);
    $stmt->bindParam(':cr_fk_ac_id', $param['category_id']);

    $stmt->execute();

     $num_rows = $stmt->fetch();
     //print_r($num_rows); die;
     $stmt->closeCursor();
        return $num_rows;
}


/**
* Creating insert_cab_table function
* @param $user_id
*/
    public function insert_cab_table($request_id){
        //print_r($mua_id);
        $sql = "INSERT INTO client_artist_booking(cab_fk_cr_id) VALUES (:cab_fk_cr_id)";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cab_fk_cr_id', $request_id);
        $stmt->execute();
        $stmt->closeCursor();

}




/**
* Creating get_client_booking function
* @param $user_id
*/
    public function get_client_booking($user_id, $lang='eng'){
        
   
    $sql = "SELECT 
        client_request.cr_id AS request_id,
        artist_profile.ap_profile_name AS artist_name, 
        artist_profile.ap_phone AS ap_phone, 
        client_request.cr_fk_u_mua_id AS artist_id, 
        client_request.cr_booking_date AS booking_date, 
        client_request.cr_request_date AS booking_request_date,
        client_request.cr_start_time AS service_start_time, 
        client_request.cr_end_time AS service_end_time, 
        CONCAT('', FORMAT(artist_category.ac_price, 2)) AS service_price,
        CONCAT('', FORMAT(client_request.cr_grand_price , 2)) AS grand_price,
        client_request.cr_status AS service_status, 
        category_attributes.ca_name AS category_name, category_attributes.ca_description AS category_description
        ,
        (
        SELECT im_image_name FROM image_master WHERE 
        image_master.im_fk_u_id = client_request.cr_fk_u_mua_id
        AND
        image_master.im_action='artist'
        AND
        image_master.im_status='true'
        AND
        image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
        )
        AS artist_profile_image,
        client_request.cr_gift AS address

        FROM client_request 
        LEFT JOIN artist_category
        ON
        artist_category.ac_id = client_request.cr_fk_ac_id
        
        INNER JOIN category_master
        ON
        category_master.cm_id = artist_category.ac_fk_cm_id
        INNER JOIN category_attributes
        ON
        category_attributes.ca_fk_cm_id = category_master.cm_id
        AND
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lang)

        LEFT JOIN artist_profile
        ON
        artist_profile.ap_fk_u_id = client_request.cr_fk_u_mua_id
        
       WHERE client_request.cr_fk_u_client_id = :cr_fk_u_client_id  AND (client_request.cr_status = 'pending' OR client_request.cr_status = 'accepted' OR client_request.cr_status = 'start')  GROUP BY cr_id ORDER BY client_request.cr_id DESC";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cr_fk_u_client_id', $user_id);
        $stmt->bindParam(':lang', $lang);
        
        
        if($stmt->execute()){

            $result = $stmt->fetchAll();
            $stmt->closeCursor();
            //print_r($result); die;
            return $result;

        }else{
            return NULL;
        }
    }


/**
* Creating privious_booking function
* @param $user_id
*/
   public function client_privious_booking($user_id, $lang='eng'){
    
    $sql = "SELECT  
            
            (
            SELECT ROUND((rating_remarks.rr_rating),2) FROM rating_remarks WHERE rating_remarks.rr_fk_mua_id=client_request.cr_fk_u_mua_id AND rating_remarks.rr_status='true' AND rating_remarks.rr_review_by='mua' LIMIT 1)  AS rating,


            IF(rating_remarks.rr_status!='null', rating_remarks.rr_status, 'false') AS review_status,
            
            (SELECT ROUND(AVG(rating_remarks.rr_rating),2) FROM rating_remarks WHERE rating_remarks.rr_fk_mua_id=client_request.cr_fk_u_mua_id AND rating_remarks.rr_status='true' AND rating_remarks.rr_review_by='mua' LIMIT 1)  AS avg_rating,

        client_request.cr_id AS request_id, artist_profile.ap_profile_name AS artist_name, artist_profile.ap_phone AS ap_phone, client_request.cr_fk_u_mua_id AS artist_id, client_request.cr_booking_date AS booking_date, client_request.cr_request_date AS booking_request_date, client_request.cr_start_time AS service_start_time, client_request.cr_end_time AS service_end_time, CONCAT('', FORMAT(artist_category.ac_price, 2)) AS service_price, client_request.cr_status AS service_status, category_attributes.ca_name AS category_name, category_attributes.ca_description AS category_description
            , /*image_master.im_image_name AS artist_profile_image*/

        (
            SELECT im_image_name FROM image_master WHERE 
            image_master.im_fk_u_id = client_request.cr_fk_u_mua_id
            AND
            image_master.im_action='artist'
            AND
            image_master.im_status='true'
            AND
            image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
        )
        AS artist_profile_image    
        
        FROM client_request 
        
        INNER JOIN artist_category
        ON
        artist_category.ac_id = client_request.cr_fk_ac_id
        INNER JOIN category_master
        ON
        category_master.cm_id = artist_category.ac_fk_cm_id
        INNER JOIN category_attributes
        ON
        category_attributes.ca_fk_cm_id = category_master.cm_id
        AND
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lm_name)
        
        INNER JOIN artist_profile
        ON
        artist_profile.ap_fk_u_id = client_request.cr_fk_u_mua_id
        
        LEFT JOIN client_artist_booking
        ON
        client_artist_booking.cab_fk_cr_id = client_request.cr_id
        LEFT JOIN rating_remarks
        ON
        rating_remarks.rr_fk_cab_id=client_artist_booking.cab_id
        AND
        rating_remarks.rr_fk_client_id=client_request.cr_fk_u_client_id
        AND
        rating_remarks.rr_fk_mua_id=client_request.cr_fk_u_mua_id


        WHERE client_request.cr_fk_u_client_id = ".$user_id." AND (client_request.cr_status = 'finished' OR client_request.cr_status = 'declined' OR client_request.cr_status = 'cancelled') GROUP BY cr_id ORDER BY client_request.cr_id DESC";

    $stmt = $this->conn->prepare($sql);
    //$stmt->bindParam(':cr_fk_u_client_id', $user_id);
    $stmt->bindParam(':lm_name', $lang);

    

    if($stmt->execute()){

        $result = $stmt->fetchAll();
       //print_r($result); die;
        $stmt->closeCursor();
        return $result;
        
       }else{
              return NULL;
            }

   }



/**
* Creating get_mua_booking function
* @param $mua_id
*/

   public function get_mua_booking($user_id, $lang='eng'){
    $sql = "SELECT

        client_request.cr_id AS request_id, user.u_name AS client_name, user.u_phone AS client_phone, client_request.cr_fk_u_client_id AS client_id, client_request.cr_booking_date AS booking_date, client_request.cr_request_date AS booking_request_date, client_request.cr_start_time AS service_start_time, client_request.cr_end_time AS service_end_time, CONCAT('', FORMAT(artist_category.ac_price, 2)) AS service_price,
        CONCAT('', FORMAT(client_request.cr_grand_price , 2)) AS grand_price,
       client_request.cr_com_grand_price AS cr_com_grand_price,
         client_request.cr_status AS service_status, category_attributes.ca_name AS category_name, category_attributes.ca_description AS category_description, 

        (
        SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = client_request.cr_fk_u_client_id
        AND image_master.im_action='client' AND image_master.im_status='true' AND image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
        ) AS client_profile_image,

        client_profile.cp_lat     AS client_lat,
        client_profile.cp_long    AS client_long,
        client_profile.cp_address AS client_address,
        client_profile.cp_city    AS cp_city,
        client_profile.cp_state   AS cp_state,
        client_profile.cp_state   AS cp_zipcode,
        
        client_request.cr_gift AS address


       
        /*image_master.im_image_name AS client_profile_image*/
        /*,
        (
        SELECT ss_status FROM service_status WHERE service_status.ss_fk_cr_id = client_request.cr_id ORDER BY ss_id DESC LIMIT 1
        )
        AS booking_status */
        
        FROM client_request 
        INNER JOIN artist_category
        ON
        artist_category.ac_id = client_request.cr_fk_ac_id
        INNER JOIN category_master
        ON
        category_master.cm_id = artist_category.ac_fk_cm_id
        INNER JOIN category_attributes
        ON
        category_attributes.ca_fk_cm_id = category_master.cm_id
        AND
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lang)
        INNER JOIN user
        ON
        user.u_id = client_request.cr_fk_u_client_id
        INNER JOIN client_profile
        ON
        client_profile.cp_fk_u_id = client_request.cr_fk_u_client_id
        
        
        /*LEFT JOIN image_master
        ON
        image_master.im_fk_u_id = client_request.cr_fk_u_client_id
        AND
        image_master.im_action='client'
        AND
        image_master.im_status='true'
        AND
        image_master.im_deleted='false'*/


WHERE client_request.cr_fk_u_mua_id = :cr_fk_u_mua_id  AND 
(client_request.cr_status = 'pending' OR client_request.cr_status = 'accepted' OR client_request.cr_status = 'start') 
GROUP BY client_request.cr_id ORDER BY client_request.cr_id DESC";

    
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':cr_fk_u_mua_id', $user_id);
    $stmt->bindParam(':lang', $lang);

    if($stmt->execute()){
        $result = $stmt->fetchAll();
        //print_r($result); die;
        $stmt->closeCursor();
        return $result;
    }else{
            return array();
         }

   }


/**
* Creating get_mua_privious_booking function
* @param $mua_id
*/
 public function get_mua_privious_booking($user_id, $lang='eng'){

    $sql = "SELECT  
            /*IF(rating_remarks.rr_rating!='null', rating_remarks.rr_rating, 0)*/
          (
           SELECT ROUND((rating_remarks.rr_rating),2) FROM rating_remarks WHERE rating_remarks.rr_fk_mua_id=client_request.cr_fk_u_mua_id AND rating_remarks.rr_status='true' AND rating_remarks.rr_review_by='client' LIMIT 1
          )  AS rating, 

          IF(rating_remarks.rr_status!='null', rating_remarks.rr_status, 'false') AS review_status,
           (
           SELECT ROUND(AVG(rating_remarks.rr_rating),2) FROM rating_remarks WHERE rating_remarks.rr_fk_mua_id=client_request.cr_fk_u_mua_id AND rating_remarks.rr_status='true' AND rating_remarks.rr_review_by='client' LIMIT 1
           )  AS avg_rating,

client_request.cr_id AS request_id, user.u_name AS client_name, client_request.cr_fk_u_client_id AS client_id, user.u_phone AS client_phone, client_request.cr_booking_date AS booking_date, client_request.cr_request_date AS booking_request_date, client_request.cr_start_time AS service_start_time, client_request.cr_end_time AS service_end_time, CONCAT('', FORMAT(artist_category.ac_price, 2)) AS service_price, client_request.cr_status AS service_status, category_attributes.ca_name AS category_name, category_attributes.ca_description AS category_description, /*image_master.im_image_name AS client_profile_image*/
        (
        SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = client_request.cr_fk_u_client_id
        AND image_master.im_action='client' AND image_master.im_status='true' AND image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
        ) AS client_profile_image

    FROM client_request 

    INNER JOIN artist_category
    ON
    artist_category.ac_id = client_request.cr_fk_ac_id

    INNER JOIN category_master
    ON
    category_master.cm_id = artist_category.ac_fk_cm_id

    INNER JOIN category_attributes
    ON
    category_attributes.ca_fk_cm_id = category_master.cm_id
    AND
    category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lang)

    INNER JOIN user
    ON
    user.u_id = client_request.cr_fk_u_client_id

    /*LEFT JOIN image_master
    ON
    image_master.im_fk_u_id = client_request.cr_fk_u_client_id
    AND
    image_master.im_action='client'
    AND
    image_master.im_status='true'
    AND
    image_master.im_deleted='false'*/

    LEFT JOIN client_artist_booking
    ON
    client_artist_booking.cab_fk_cr_id = client_request.cr_id
    LEFT JOIN rating_remarks
    ON
    rating_remarks.rr_fk_cab_id=client_artist_booking.cab_id
    AND
    rating_remarks.rr_fk_client_id=client_request.cr_fk_u_client_id
    AND
    rating_remarks.rr_fk_mua_id=client_request.cr_fk_u_mua_id


    WHERE client_request.cr_fk_u_mua_id = :cr_fk_u_mua_id  AND (client_request.cr_status = 'finished' OR client_request.cr_status = 'declined' OR client_request.cr_status = 'cancelled') GROUP BY user.u_id ORDER BY client_request.cr_id DESC";       
    
    $stmt = $this->conn->prepare($sql);

    $stmt->bindParam(':cr_fk_u_mua_id', $user_id);
    $stmt->bindParam(':lang', $lang);


    if($stmt->execute()){

        $result = $stmt->fetchAll();
        //print_r($result); die;
        $stmt->closeCursor();
        return $result;
    }else{
            return NULL;
         }

 }


public function get_mua_gcmID($mua_id){
    $result = array();
    $sql = "SELECT u_id, u_type, u_gcm_id, u_device_type,

    IF(user.u_type='mua',(SELECT ap_language FROM artist_profile WHERE artist_profile.ap_fk_u_id = user.u_id),(SELECT cp_language FROM client_profile WHERE client_profile.cp_fk_u_id = user.u_id)) AS u_lang            


     FROM user WHERE u_id = :u_id";
    $stmt = $this->conn->prepare($sql);

    $stmt->bindParam(':u_id', $mua_id);

    if($stmt->execute()){
        $result = $stmt->fetch();
        $stmt->closeCursor();
    } 
    else{
        $result = NULL;
    }

    return $result;
}

/*
  function : set_service_status($params)
  @ param
*/
    public function get_service_status($param){
        
        $sql = "SELECT * FROM service_status WHERE ss_fk_cr_id = :ss_fk_cr_id ORDER BY ss_id DESC LIMIT 1";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':ss_fk_cr_id', $param->requsetId);

        if($stmt->execute()){
            $result = $stmt->fetch();
            $stmt->closeCursor();
        } 
        else{
            $result = NULL;
        }

        return $result;

    }
    


    public function set_service_status($param){

       $sql = "INSERT INTO service_status (ss_fk_cr_id, ss_fk_mua_id, ss_status, ss_device_info, ss_created) VALUES (:ss_fk_cr_id, :ss_fk_mua_id, :ss_status, :ss_device_info, :ss_created)";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':ss_fk_cr_id', $param->requsetId);
        $stmt->bindParam(':ss_fk_mua_id', $param->user_id);
        $stmt->bindParam(':ss_status', $param->action);
        $stmt->bindParam(':ss_device_info', $param->device_info);
        $stmt->bindParam(':ss_created', $param->date);

        $stmt->execute();
        $stmt->closeCursor();



    }

    public function get_cr_status($param){
       $sql = "UPDATE client_request SET cr_status = 'finished', cr_created = :cr_created WHERE cr_id = :cr_id AND cr_fk_u_mua_id = :cr_fk_u_mua_id";
       $stmt = $this->conn->prepare($sql);
       $stmt->bindParam(':cr_id', $param->requsetId);
       $stmt->bindParam(':cr_fk_u_mua_id', $param->user_id);
       //$stmt->bindParam(':cr_status', $param->action);
       $stmt->bindParam(':cr_created', $param->date);
       $stmt->execute();
       $stmt->closeCursor();
    }

/**
* Creating mua_history function
* @param $mua_id
*/
 public function mua_history($user_id, $lang='eng'){

   $sql = "SELECT
            ROUND(rating_remarks.rr_rating,2) AS rating,
            IF((
            SELECT rr_status FROM rating_remarks WHERE rating_remarks.rr_fk_mua_id = client_request.cr_fk_u_mua_id AND rating_remarks.rr_review_by = 'mua' AND rr_fk_cab_id = cab_fk_cr_id
            )!='null', rating_remarks.rr_status, 'false') AS review_status,

            IF(rating_remarks.rr_status!='null', rating_remarks.rr_status, 'false') AS review_status1,
            ROUND(rating_remarks.rr_rating,2) AS avg_rating,
            client_request.cr_id AS request_id,
            user.u_name AS client_name,
            client_request.cr_fk_u_client_id AS client_id,
            user.u_phone AS client_phone,
            client_request.cr_booking_date AS booking_date,
            client_request.cr_request_date AS booking_request_date,
            client_request.cr_start_time AS service_start_time,
            client_request.cr_end_time AS service_end_time,
            CONCAT('', FORMAT(artist_category.ac_price, 2)) AS service_price,
            CONCAT('', FORMAT(client_request.cr_grand_price , 2)) AS grand_price, 
            client_request.cr_com_grand_price AS cr_com_grand_price, 
            client_request.cr_status AS service_status, 
            category_attributes.ca_name AS category_name, 
            category_attributes.ca_description AS category_description,
        (
        SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = client_request.cr_fk_u_client_id
        AND image_master.im_action='client' AND image_master.im_status='true' AND image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
        ) AS client_profile_image

    
    FROM client_request 

    INNER JOIN artist_category
    ON
    artist_category.ac_id = client_request.cr_fk_ac_id

    INNER JOIN category_master
    ON
    category_master.cm_id = artist_category.ac_fk_cm_id

    INNER JOIN category_attributes
    ON
    category_attributes.ca_fk_cm_id = category_master.cm_id
    AND
    category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lang)

    INNER JOIN user
    ON
    user.u_id = client_request.cr_fk_u_client_id

    LEFT JOIN client_artist_booking
    ON
    client_artist_booking.cab_fk_cr_id = client_request.cr_id
    
    LEFT JOIN rating_remarks
    ON
    rating_remarks.rr_fk_cab_id = client_artist_booking.cab_id
    AND
    rating_remarks.rr_fk_client_id = client_request.cr_fk_u_client_id
    AND
    rating_remarks.rr_fk_mua_id = client_request.cr_fk_u_mua_id
    AND 
    rating_remarks.rr_review_by = 'client'


    WHERE client_request.cr_fk_u_mua_id = :cr_fk_u_mua_id  AND (client_request.cr_status = 'finished' OR client_request.cr_status = 'declined' OR client_request.cr_status = 'cancelled') ORDER BY client_request.cr_id DESC";       
    
    $stmt = $this->conn->prepare($sql);

    $stmt->bindParam(':cr_fk_u_mua_id', $user_id);
    $stmt->bindParam(':lang', $lang);


    if($stmt->execute()){

        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }else{
            return NULL;
         }

 }


 /**
* Creating function client_history 
* @param $user_id
*/
   public function client_history($user_id, $lang='eng'){
    
    
    $sql = "SELECT  
            
            ROUND((rating_remarks.rr_rating),2) AS rating,
            IF((
            SELECT rr_status FROM rating_remarks WHERE rating_remarks.rr_fk_client_id = client_request.cr_fk_u_client_id AND rating_remarks.rr_review_by = 'client' AND rr_fk_cab_id = cab_fk_cr_id
            )!='null', rating_remarks.rr_status, 'false') AS review_status,

            IF(rating_remarks.rr_status!='null', rating_remarks.rr_status, 'false') AS review_status1,
            ROUND((rating_remarks.rr_rating),2)  AS avg_rating,
            client_request.cr_id AS request_id,
            artist_profile.ap_profile_name AS artist_name,
            artist_profile.ap_phone AS ap_phone,
            client_request.cr_fk_u_mua_id AS artist_id,
            client_request.cr_booking_date AS booking_date,
            client_request.cr_request_date AS booking_request_date,
            client_request.cr_start_time AS service_start_time,
            client_request.cr_end_time AS service_end_time,
            CONCAT('', FORMAT(artist_category.ac_price, 2)) AS service_price,
            CONCAT('', FORMAT(client_request.cr_grand_price , 2)) AS grand_price,
            client_request.cr_status AS service_status,
            category_attributes.ca_name AS category_name,
            category_attributes.ca_description AS category_description
            , 

            (
             SELECT im_image_name FROM image_master WHERE 
             image_master.im_fk_u_id = client_request.cr_fk_u_mua_id
             AND
             image_master.im_action='artist'
             AND
             image_master.im_status='true'
             AND
             image_master.im_deleted='false' ORDER BY im_id DESC LIMIT 1
            )
            AS artist_profile_image    
        
        FROM client_request 
        
        INNER JOIN artist_category
        ON
        artist_category.ac_id = client_request.cr_fk_ac_id
        INNER JOIN category_master
        ON
        category_master.cm_id = artist_category.ac_fk_cm_id
        INNER JOIN category_attributes
        ON
        category_attributes.ca_fk_cm_id = category_master.cm_id
        AND
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name=:lm_name)
        
        INNER JOIN artist_profile
        ON
        artist_profile.ap_fk_u_id = client_request.cr_fk_u_mua_id
        
        LEFT JOIN client_artist_booking
        ON
        client_artist_booking.cab_fk_cr_id = client_request.cr_id
        LEFT JOIN rating_remarks
        ON
        rating_remarks.rr_fk_cab_id=client_artist_booking.cab_id
        AND
        rating_remarks.rr_fk_client_id=client_request.cr_fk_u_client_id
        AND
        rating_remarks.rr_fk_mua_id=client_request.cr_fk_u_mua_id
        AND 
        rating_remarks.rr_review_by = 'mua'


        WHERE client_request.cr_fk_u_client_id = ".$user_id." AND (client_request.cr_status = 'finished' OR client_request.cr_status = 'declined' OR client_request.cr_status = 'cancelled') GROUP BY cr_id ORDER BY client_request.cr_id DESC";

    $stmt = $this->conn->prepare($sql);
    //$stmt->bindParam(':cr_fk_u_client_id', $user_id);
    $stmt->bindParam(':lm_name', $lang);

    

    if($stmt->execute()){

        $result = $stmt->fetchAll();
       //print_r($result); die;
        $stmt->closeCursor();
        return $result;
        
       }else{
              return NULL;
            }

   }



   public function get_notification($n_key){
    $sql = "SELECT * FROM notification WHERE n_key = :n_key";
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':n_key', $n_key);
    $stmt->execute();
    $result = $stmt->fetch();
    $stmt->closeCursor();
    return $result;
   }


   public function get_mua_commission($user_id){
    $result = '';
    $sql = "SELECT cpi_commission,
            (SELECT cab_fk_cr_id FROM client_artist_booking WHERE client_artist_booking.cab_id = client_payment_information.cpi_fk_cab_id) AS booking_id 
            FROM client_payment_information WHERE cpi_fk_artist_id = :cpi_fk_artist_id AND cpi_status = 'true' AND cpi_commission != 0";
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':cpi_fk_artist_id', $user_id);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $stmt->closeCursor();
    return $result;
   }

   public function get_mua_booking_count($user_id){
        $sql = "SELECT artist_payment_information.api_fk_asp_id AS plan_id,
            /*(SELECT COUNT(*) FROM client_request WHERE client_request.cr_fk_u_mua_id = artist_payment_information.api_fk_artist_id LIMIT 1) as booking_count,*/
            (SELECT COUNT(*) FROM client_request WHERE client_request.cr_fk_u_mua_id = artist_payment_information.api_fk_artist_id AND
            artist_payment_information.api_fk_asp_id = client_request.cr_mua_plan_id AND cr_booking_now BETWEEN api_created AND DATE_ADD(api_created, INTERVAL 1 MONTH) LIMIT 1) as booking_count,
            (SELECT asp_commission_after_limit FROM artist_subscription_plan WHERE artist_subscription_plan.asp_id = artist_payment_information.api_fk_asp_id LIMIT 1) AS commission,
            (SELECT asp_client_limit FROM artist_subscription_plan WHERE artist_subscription_plan.asp_id = artist_payment_information.api_fk_asp_id LIMIT 1) AS client_limit

            FROM artist_payment_information WHERE api_fk_artist_id = '".$user_id."' AND api_status = 'true' ORDER BY api_id LIMIT 1";
        $stmt = $this->conn->prepare($sql);
        
        $stmt->execute();
        $result = $stmt->fetch();
        $stmt->closeCursor();
        return $result;

   }

   public function get_free_plan_data(){
        $sql = "SELECT * FROM artist_subscription_plan WHERE asp_id = 4 AND asp_status = 'true' LIMIT 1";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        $stmt->closeCursor();
        return $result;
   }


}