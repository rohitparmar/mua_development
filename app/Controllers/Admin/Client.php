<?php



/**

 * Class to handle all db operations

 * This class will have CRUD methods for database tables

 *

 * @author Synergytop

 * @link http://synergytop.com/

 */

class Client extends Layout{



	protected $app;

	/**

    * Construct

    * @param $app

    */

	public function __construct($app){

		$this->app = $app;

        

		if(!isset($_SESSION['admin_info'])){

            redirect('admin/login');

        }

	}



	/**

    * Client

    * @param $req, $res, $args

    */

	public function index($req, $res, $args)

	{  

		$this->load_model('Admin/Client_model.php');/*Load client model*/



		$args['header']['title'] = 'Client';



        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['page']) && $args['page']){

            $page = ($args['page'] -1)*10;

            $page1 = $args['page'];

        }

        else{
            $page = 0;
            $args['page'] = 1;
        }



        $param['page'] = $page;

        $args['query'] = "";

        if($_GET){

            $args['query'] = '?'.http_build_query($_GET);

        }
        
        $args['client_count'] = $this->Client_model->get_client_count($param);/*Get client list*/
		$args['client_list'] = $this->Client_model->get_client_list($param);/*Get client list*/
        $this->layout($res,'Admin/Client/index.php',$args);
	}


    public function show_client_used_referrel_modal($req, $res, $args){
        $this->load_model('Admin/Client_model.php');/*Load client model*/
        $arr = $this->Client_model->get_client_used_referrel_modal($args['id']);

        $booked_list = array();
        $not_booked_list = array();
        $total_earning = 0;
        
        foreach($arr as $rows){
            if($rows['booking']>0){
                $booked_list[]=$rows;
                $total_earning+=(double)$rows['r_rebate'];
            }else{
                $not_booked_list[]=$rows;
            }
        }
        
        $args['booked_list']     = count($booked_list);
        $args['not_booked_list'] = count($not_booked_list);
        $args['total_earning']   = $total_earning;
        
        $args['result'] = array_merge($booked_list, $not_booked_list);
        
        $this->view($res,'Admin/Models/show_client_used_referrel_modal.php',$args);
    }



    /**

    * Client change status ajax call

    * @param $req, $res, $args

    */

    public function client_change_status($req, $res, $args)

    {

    	$this->load_model('Admin/Client_model.php');/*Load client model*/



        $param['id'] = $_POST['id'];

        $param['status'] = ($_POST['status'] == 'true')?'false':'true';



        $this->Client_model->client_change_status($param);



        echo $param['status'];

    }



    /**

    * Client profile ajax call

    * @param $req, $res, $args

    */

    public function client_profile($req, $res, $args)

    {

        $this->load_model('Admin/Client_model.php');/*Load client model*/



        $param = $_POST;



        $args['profile'] = $this->Client_model->get_client_profile($param);/*Get client profile*/



        $this->view($res,'Admin/Models/client_profile.php',$args);

    }

}

