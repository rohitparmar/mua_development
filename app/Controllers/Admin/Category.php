<?php

class Category extends Layout {

    protected $app;

    /**
     * Construct
     * @param $app
     */
    public function __construct($app) {
        $this->app = $app;

        if (!isset($_SESSION['admin_info'])) {
            redirect('admin/login');
        }
    }

    /**
     * Category
     * @param $req, $res, $args
     */
    public function index($req, $res, $args) {
        $args['header']['title'] = 'Category';

        $this->load_model('Admin/Category_model');

        $id = 0;

        if (isset($args['id'])) {
            $id = base64_decode($args['id']);
        }

        $param['language'] = isset($_GET['lang'])?$_GET['lang']:1;

        $args['language'] = isset($_GET['lang'])?$_GET['lang']:1;

        $param['id'] = $id;

        // $skin_tone = $this->Category_model->get_skin_tone();/*Get skin tone*/

        // $args['skin_tone'] = [];

        // foreach($skin_tone as $value){
        //     if($args['language'] == 1){
        //         $args['skin_tone'][$value['stm_id']]['name'] = $value['stm_name'];
        //     }
        //     else{
        //         $args['skin_tone'][$value['stm_id']]['name'] = $value['stm_name_spa'];
        //     }
        // }

        $args['category_list'] = $this->Category_model->get_sub_categorys_list($param); /* Selecting data from category_master */
        
        

        $this->layout($res, 'Admin/Category/index.php', $args);
    }

    /**
     * Add category
     * @param $req, $res, $args
     */
    public function add_category($req, $res, $args) {
        $this->load_model('Admin/Category_model');
        $args['header']['title'] = "Add category";
        if($_POST){
            $valid_ext = ['JPG','JPEG','PNG', 'GIF'];
            
            $param = $_POST;

            $param['category'] = end($_POST['category']);

            if(!$param['category']){
                array_pop($_POST['category']);
                $param['category'] = end($_POST['category']);
            }

            if(!$param['category']){
                $param['category'] = 0;
            }
            $param['category_img'] = '';
           
            
            if(isset($_FILES['sub_image']['name'])){
                
                $ext = explode('.',$_FILES['sub_image']['name']);
                $ext = end($ext);
                if(in_array(strtoupper($ext),$valid_ext)){
                    $new_file = "category-img_".time().rand(1,9999).".".$ext;
                    
                    if(move_uploaded_file($_FILES['sub_image']['tmp_name'], CATEGORY_IMG.$new_file)){
                        $param['category_img'] = $new_file;
                    }
                }
            }
            $param['category_attr_id'] = $this->Category_model->insert_sub_category($param);
            
            if(isset($param['sub_category_skin_tone'])){
                foreach($param['sub_category_skin_tone'] as $key => $value){

                    $param['skintone_img'][$key] = "";

                    if(isset($_FILES['sub_category_skintone_img']['name'][$key]) && $_FILES['sub_category_skintone_img']['name'][$key]){

                        $ext = explode('.',$_FILES['sub_category_skintone_img']['name'][$key]);
                        $ext = end($ext);
                        
                        if(in_array(strtoupper($ext),$valid_ext)){
                            $new_file = "skintone-img_".time().rand(1,9999).".".$ext;
                            $param['skintone_img'][$key] = $new_file;

                            move_uploaded_file($_FILES['sub_category_skintone_img']['tmp_name'][$key], CATEGORY_IMG.'skintone/'.$new_file);
                        }
                    }
                }
            }
            
            $this->Category_model->insert_category_mapp($param);

            $_SESSION['success'] = "Add category successfully";

            //redirect('admin/add-category');
        }
        $args['skin_tone'] = $this->Category_model->get_skin_tone();
        $args['lips_color'] = $this->Category_model->get_lips_color();
        $args['eyeshadow'] = $this->Category_model->get_eyeshadow();
        $args['category_list'] = $this->Category_model->get_main_categorys('eng');
        $this->layout($res, 'Admin/Category/add_category.php', $args);
    }

    /**
     * Get sub category ajax call
     * @param $req, $res, $args
     */
    public function get_sub_category_ajax($req, $res, $args) {
        $this->load_model('Admin/Category_model'); /* Connenting category model */

        $param = $_POST;

        $sub_category = $this->Category_model->get_sub_categorys($param); /* Selecting sub categorys */

        echo json_encode($sub_category);
    }

    /**
     * Get sub category ajax call
     * @param $req, $res, $args
     */
    public function category_change_status_ajax($req, $res, $args) {
        $this->load_model('Admin/Category_model'); /* Connenting category model */

        $param['id'] = $_POST['id'];
        $param['status'] = ($_POST['status'] == 'true') ? 'false' : 'true';

        $sub_category = $this->Category_model->category_change_status($param); /* Selecting sub categorys */

        echo $param['status'];
    }

    /**
     * Delete category
     * @param $req, $res, $args
     */
    public function delete_category($req, $res, $args) {
        $this->load_model('Admin/Category_model'); /* Connenting category model */

        $param['id'] = $_POST['id'];

        $count = $this->Category_model->check_sub_category($param); /* Check sub category exites */

        if ($count > 0) {
            $return = array(
                'status' => 'error',
                'text' => 'Before remove this category you have to remove all thier sub categorys.'
            );
        } else {
            $sub_category = $this->Category_model->delete_category($param); /* Selecting sub categorys */
            $return = array(
                'status' => 'success',
                'text' => 'Remove category successfully.'
            );
        }

        echo json_encode($return);
    }

    /**
     * Get category to edit ajax call
     * @param $req, $res, $args
     * Now no use of this function
     */
    public function get_category_to_edit($req, $res, $arg) {
        $param = $_POST;
      
        $arg['lang'] = $param['lang'];
        $arg['cat_id'] = $param['cat_id'];
        
        $this->load_model('Admin/Category_model'); /* Connenting category model */

        $cat_info = $this->Category_model->get_category_to_edit($param); /* Fetch category to edit */
        
        $skintone_info = array();
        $eyeshadow_info = array();
        $lipcolor_info = array();
        $category_info = array();
        
        $n=0;
		
        foreach($cat_info as $key => $row){
            if($n==0){
                $n++;
                $category_info = array(
                    'cm_id'         => $row['cm_id'],
                    'cm_name'       => $row['cm_name'],
                    'cm_price'      => $row['cm_price'],
                    'cm_duration'   => $row['cm_duration'],
                    'cm_status'     => $row['cm_status'],
                    'cm_image'      => $row['cm_image'],
                    'cm_description'=> $row['cm_description'],
                    'ca_id'         => $row['ca_id'],
                    'ca_fk_cm_id'   => $row['ca_fk_cm_id'],
                    'ca_name'       => $row['ca_name'],
                    'ca_description'=> $row['ca_description'],
                    'language'      => $row['language']
                );
            }
            
            if($row['mcm_service_name']=='master_skintone'){
                $skintone_info[] = array(
                    'mcm_id'            => $row['mcm_id'],
                    'mcm_service_id'    => $row['mcm_service_id'],
                    'mcm_fk_ca_id'      => $row['mcm_fk_ca_id'],
                    'mcm_img'           => $row['mcm_img'],
                    'mcm_status'        => $row['mcm_status']
                );
            }
            
            if($row['mcm_service_name']=='master_eyeshadow'){
                $eyeshadow_info[] = array(
                    'mcm_id'            => $row['mcm_id'],
                    'mcm_service_id'    => $row['mcm_service_id'],
                    'mcm_fk_ca_id'      => $row['mcm_fk_ca_id'],
                    'mcm_status'        => $row['mcm_status']
                );
            }
            
            if($row['mcm_service_name']=='master_lips_color'){
                $lipcolor_info[] = array(
                    'mcm_id'            => $row['mcm_id'],
                    'mcm_service_id'    => $row['mcm_service_id'],
                    'mcm_fk_ca_id'      => $row['mcm_fk_ca_id'],
                    'mcm_status'        => $row['mcm_status']
                );
            }
        }
        
        $arg['category'] = $category_info;
        $arg['skintone_info'] = $skintone_info;
        $arg['lipcolor_info'] = $lipcolor_info;
        $arg['eyeshadow_info'] = $eyeshadow_info;
        
        

        $skin_tone = $this->Category_model->get_skin_tone('all');/*Get skin tone*/
        
        //echo '<pre>';
        //print_r($skin_tone);
        //exit;
        
        foreach($skin_tone as $k=>$value){
            $skin_tone[$k]['checked']='checked';
        }
        
        $arg['skin_tone'] = $skin_tone;
        
        
        $lips_color = $this->Category_model->get_lips_color('all');
        
        foreach($lips_color as $k=>$value){
            $lips_color[$k]['checked']='checked';
        }
        
        $arg['lips_color'] = $lips_color;
        
        $eyeshadow = $this->Category_model->get_eyeshadow('all');
        
        foreach($eyeshadow as $k=>$value){
            $eyeshadow[$k]['checked']='checked';
        }
        
        $arg['eyeshadow'] = $eyeshadow;
        
        $this->view($res,'Admin/Models/Category/category_update.php',$arg);
    }

    /**
     * Update category ajax call
     * @param $req, $res, $args
     */
    public function update_category($req, $res, $arg) {
        
        $param = $_POST;
        
       
        
        $cat_id         = isset($param['cat_id']) && is_numeric($param['cat_id'])?$param['cat_id']:0;
        $this->load_model('Admin/Category_model'); /* Connenting category model */
       
        //now update all data
        $this->Category_model->update_category_info($param);
       
        //now update picture if exists
        if (($cat_id >0) && isset($_FILES['sub_image']['name']) && $_FILES['sub_image']) {
            $ext = explode('.', $_FILES['sub_image']['name']);
            $ext = end($ext);
            $valid_ext = array('JPG', 'JPEG', 'PNG', 'GIF');
            if (in_array(strtoupper($ext), $valid_ext)) {
                $img_name = $cat_id . "-" . time() . rand(10000, 99999) . "." . $ext;
                if($_FILES['sub_image']['error']==0){
                    if(move_uploaded_file($_FILES['sub_image']['tmp_name'], CATEGORY_IMG . $img_name)){
                        $data['id'] = $cat_id;
                        $data['image'] = $img_name;
                        $this->load_model('Admin/Category_model'); /* Connenting category model */
                        $this->Category_model->update_cat_image($data); /* Update category image */
                    }
                }
            }
        }
        
        
        $web_url = $_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:base_url("admin/category/1");

        header('Location:'.$web_url);
	exit;
      
    }

    /**
     * Change category image
     * @param $req, $res, $args
     */
    public function change_category_image($req, $res, $arg) {

        if (isset($_FILES['image']) && $_FILES['image']) {
            if($_FILES['image']['error']==0){
                $ext = explode('.', $_FILES['image']['name']);
                $ext = end($ext);
                $valid_ext = array('JPG', 'JPEG', 'PNG', 'GIF');
                if (in_array(strtoupper($ext), $valid_ext)) {
                    $img_name = $_POST['id'] . "-" . time() . rand(10000, 99999) . "." . $ext;

                    move_uploaded_file($_FILES['image']['tmp_name'], CATEGORY_IMG . $img_name);
                    unset($param);

                    $param['id'] = $_POST['id'];
                    $param['image'] = $img_name;
                    $this->load_model('Admin/Category_model'); /* Connenting category model */
                    $this->Category_model->update_category_image($param); /* Update category image */

                    echo base_url(CATEGORY_IMG . $img_name);
                }
            }
        }
    }

    /**
     * Change category image
     * @param $req, $res, $args
     */
    public function update_category_portfolio($req, $res, $arg) {

        $this->load_model('Admin/Category_model'); /* Connenting category model */
        $param = $_POST;
        
        if(isset($_FILES['image']) && $_FILES['image']){
            
            $ext = explode('.', $_FILES['image']['name']);
            $ext = end($ext);
            $valid_ext = array('JPG', 'JPEG', 'PNG');
            if (in_array(strtoupper($ext), $valid_ext)) {
                
                /* Image Croping start */
                $img_name = $_POST['id'] . "-" . time() . rand(10000, 99999) . "." . $ext;
                
                $path = "";
                if($_POST['type'] == 'before'){
                    $path = CATEGORY_PORTFOLIO_BEFORE_IMG.$img_name;
                }
                else{
                    $path = CATEGORY_PORTFOLIO_AFTER_IMG.$img_name;
                }
                
                if($_POST['skip'] != 1){
                    $size = getimagesize($_FILES['image']['tmp_name']);

                    $x = (int) $_POST['x'];
                    $y = (int) $_POST['y'];
                    $w = 600;
                    $h = 600;
                    $nw = $nh = 600; # image with # height

                    $data = file_get_contents($_FILES['image']['tmp_name']);
                    $vImg = imagecreatefromstring($data);
                    $dstImg = imagecreatetruecolor($nw, $nh);


                    imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);

                    if (strtoupper($ext) == 'JPEG' || strtoupper($ext) == 'JPG') {
                        imagejpeg($dstImg, $path);
                    } elseif ($ext == 'png') {
                        imagepng($dstImg, $path);
                    }

                    imagedestroy($dstImg);
                    /* Image Croping end */
                }
                else{
                    move_uploaded_file($_FILES['image']['tmp_name'], $path);
                }
                
                
                $param['image'] = $img_name;
                
                $this->Category_model->update_category_portfolio_image($param); /* Update category portfolio image */
                
                echo base_url($path);
            }
        }
        else{
            $arg['portfolio'] = $this->Category_model->get_category_portfolio($param); /* Get category portfolio */
            $arg['id'] = $param['id'];
            echo $this->view($res,'Admin/Models/add_update_category_portfolio_image.php',$arg,false);
        }
    }
    
    /**
     * Get crop model
     * @param $req, $res, $args
     */
    public function get_crop_model($req, $res, $arg) {

        echo $this->view($res,'Admin/Models/crop_image.php',$arg,false);
    }

}
