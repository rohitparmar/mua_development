<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payment
 *
 * @author abhay
 */
class Payment extends Layout{
    //put your code here
    public function __construct($app){
        
        if(!isset($_SESSION['admin_info'])){
            redirect('admin/login');
        }

                $this->app = $app;
        $this->load_model('Admin/Payment_model');
    }
    
    public function earning_artist_list($req, $res, $args){
        $args['header']['title'] = 'Artist Earning Information';
       
        $page = 0;

        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['id']) && $args['id']){
            $page = ($args['id']-1)*10;
        }

        $param['page'] = $page;

        $args['query'] = "";
        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }


        $signupdate = date("Y-m-d H:i:s");
        $signupweek = date("W",strtotime($signupdate));

        $year = date("Y",strtotime($signupdate));
        $currentweek = date("W");

        for($i=$signupweek;$i<=$currentweek;$i++) {
            $result = $this->getWeek($i,$year);
            $param['start_date'] = isset($result['start'])?$result['start']:'';
            $param['end_date']   = isset($result['end'])?$result['end']:'';
        }

        $args['count'] = $this->Payment_model->get_mua_payment_info_count($param);/*Get transaction count*/

        $args['transaction'] = $this->Payment_model->get_mua_payment_info($param);/*Get transaction*/

        /*echo'<pre>';
        print_r($args['transaction']);
        die;*/

        $args['page'] = isset($args['id'])?$args['id']:'';
        $this->layout($res, 'Admin/Payment/artist_earning_list.php', $args);
    }
    
    private function getWeek($week, $year) {
          
        $dto = new DateTime();
        $result['start'] = $dto->setISODate($year, $week, 0)->format('Y-m-d');
        $result['end'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
        return $result;
    }

    public function artist_transaction($req, $res, $args){
        $args['header']['title'] = 'Artist Transaction Information';
       
        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['id']) && $args['id']){
            $args['page'] = $args['id'];
        }

        if(isset($args['page']) && $args['page']){
            $page = ($args['page'] -1)*10;
            $page1 = $args['page'];
        }
        else{
            $page = 0;
            $args['page'] = 1;
        }

        $param['page'] = $page;

      

        $args['query'] = "";
        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }

        $param['id'] = $args['id'];



        $args['result'] = $this->Payment_model->get_artist_transaction($param);/*Get artist_transaction*/

        $this->layout($res, 'Admin/Payment/artist_transaction.php', $args);
    }

}
