<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Category_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }

    /**
     * Get main caregorys
     * @param null
     * @return $result
     */
    public function get_main_categorys($lang){
        $sql = "SELECT ca_fk_cm_id,ca_name
                FROM category_attributes
                WHERE ca_fk_cm_id IN (SELECT cm_id FROM category_master WHERE cm_fk_parent_id = 0 AND cm_status = 'true' AND cm_action_taken = 'default')
                AND language = (SELECT lm_id FROM language_master WHERE lm_name = :lm_name)
                ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':lm_name',$lang);

        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }

    /**
     * Get main caregorys
     * @param $param
     * @return $result
     */
    public function get_sub_categorys($param){
        $sql = "SELECT ca_id,ca_fk_cm_id,ca_name,ca_description
                FROM category_attributes
                WHERE ca_fk_cm_id IN (SELECT cm_id FROM category_master WHERE cm_fk_parent_id = :parent_id AND cm_action_taken = 'default' AND cm_status = 'true')
                AND language = (SELECT lm_id FROM language_master WHERE lm_name = :lm_name)
                ";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':parent_id',$param['id']);
        $stmt->bindParam(':lm_name',$param['language']);

        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }

    /**
     * Insert sub category
     * @param $param
     * @return $result
     */
    public function insert_sub_category($param){
        $sql = "INSERT INTO `category_master` (
                    cm_fk_parent_id,
                    cm_name,
                    cm_description,
                    cm_price,
                    cm_duration,
                    cm_image,
                    cm_status,
                    cm_created
                ) 
                VALUES(
                    :mc_fk_parent_id,
                    :mc_name,
                    :mc_description,
                    :mc_price,
                    :mc_duration,
                    :mc_image,
                    :mc_status,
                    :mc_created
                );
                INSERT INTO category_attributes (
                    ca_fk_cm_id,
                    ca_name,
                    ca_description,
                    language
                )
                VALUES(
                    LAST_INSERT_ID(),
                    :mc_name,
                    :mc_description,
                    (SELECT lm_id FROM language_master WHERE lm_name = 'eng')
                )";

        $stmt = $this->conn->prepare($sql);

        $date = date('Y-m-d',time());
        
        $stmt->bindParam(':mc_fk_parent_id',$param['category']);
        $stmt->bindParam(':mc_name',$param['sub_category_name']);
        $stmt->bindParam(':mc_image',$param['category_img']);
        $stmt->bindParam(':mc_description',$param['sub_category_desc']);
        $stmt->bindParam(':mc_price',$param['sub_category_price']);
        $stmt->bindParam(':mc_duration',$param['sub_category_time']);
        $stmt->bindParam(':mc_created',$date);
        $stmt->bindParam(':mc_status',$param['status']);

        $stmt->execute();
        $stmt->closeCursor();

        $last_insert_id = $this->conn->lastInsertId();

        return $last_insert_id;
    }

    public function insert_category_mapp($param){
        if($param['sub_category_skin_tone']){
            $sql = "INSERT INTO master_catgegory_mapping (
                        mcm_service_name,
                        mcm_service_id,
                        mcm_fk_ca_id,
                        mcm_img
                    )
                    VALUES (
                        'master_skintone',
                        :mcm_service_id,
                        :mca_id,
                        :mcm_img
                    )";

            $stmt = $this->conn->prepare($sql);

            foreach($param['sub_category_skin_tone'] as $key => $val){
                $stmt->bindParam(':mcm_service_id',$key);
                $stmt->bindParam(':mcm_img',$param['skin_tone_img'][$key]);
                $stmt->bindParam(':mca_id',$param['category_attr_id']);
                $stmt->execute();
            }

            $stmt->closeCursor();
        }

        if(isset($param['eyeshadow'])){

            $sql = "INSERT INTO master_catgegory_mapping (
                        mcm_service_name,
                        mcm_service_id,
                        mcm_fk_ca_id,
                        mcm_img
                    )
                    VALUES (
                        'master_eyeshadow',
                        :mcm_service_id,
                        :mca_id,
                        ''
                    )";
            $stmt = $this->conn->prepare($sql);

            foreach($param['eyeshadow'] as $key => $val){
                $stmt->bindParam(':mcm_service_id',$val);
                $stmt->bindParam(':mca_id',$param['category_attr_id']);
                $stmt->execute();
            }
        }

        if(isset($param['lips_color'])){

            $sql = "INSERT INTO master_catgegory_mapping (
                        mcm_service_name,
                        mcm_service_id,
                        mcm_fk_ca_id,
                        mcm_img
                    )
                    VALUES (
                        'master_lips_color',
                        :mcm_service_id,
                        :mca_id,
                        ''
                    )";
            $stmt = $this->conn->prepare($sql);

            foreach($param['lips_color'] as $key => $val){
                $stmt->bindParam(':mcm_service_id',$val);
                $stmt->bindParam(':mca_id',$param['category_attr_id']);
                $stmt->execute();
            }
        }
    }

    /**
     * Update category image
     * @param $param
     * @return null
     */
    public function update_category_image($param){
        $sql = "UPDATE `category_master` 
                SET cm_image = :cm_image
                WHERE cm_id = :cm_id
                AND (category_master.cm_image='' OR category_master.cm_image=NULL)";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':cm_id',$param['id']);
        $stmt->bindParam(':cm_image',$param['image']);

        $stmt->execute();
        $stmt->closeCursor();

        return;
    }

    /**
     * get category list
     * @param $param
     * @return $result
     */
    public function get_sub_categorys_list($param){

        $sql = "SELECT cm_image, cm_id,cm_id AS id,cm_price,cm_duration,cm_status,ca_name,ca_description,cm_fk_parent_id,
                (SELECT cm_fk_parent_id FROM category_master WHERE cm_fk_parent_id = id LIMIT 1) AS sub_category,
                (SELECT cm_fk_parent_id FROM category_master WHERE cm_id = cm.cm_fk_parent_id LIMIT 1) AS parent_id,
                (SELECT GROUP_CONCAT(ms_name) FROM master_skintone WHERE ms_id IN (SELECT mcm_service_id FROM master_catgegory_mapping WHERE mcm_fk_ca_id = cm_id AND mcm_service_name = 'master_skintone' AND mcm_status='true')) AS skin_tone,
                (SELECT GROUP_CONCAT(mcm_img) FROM master_catgegory_mapping WHERE mcm_fk_ca_id = cm_id AND mcm_service_name = 'master_skintone') AS skin_tone_img,
                
                (SELECT GROUP_CONCAT(me_name) FROM master_eyeshadow WHERE me_id IN (SELECT mcm_service_id FROM master_catgegory_mapping WHERE mcm_fk_ca_id = cm_id AND mcm_service_name = 'master_eyeshadow' AND mcm_status='true')) AS eyeshadow,
                (SELECT GROUP_CONCAT(mlc_name) FROM master_lips_color WHERE mlc_id IN (SELECT mcm_service_id FROM master_catgegory_mapping WHERE mcm_fk_ca_id = cm_id AND mcm_service_name = 'master_lips_color' AND mcm_status='true')) AS lips_color
                FROM category_master AS cm
                LEFT JOIN category_attributes ON ca_fk_cm_id = cm_id
                WHERE cm_action_taken = 'default'
                AND category_attributes.language = :language
                AND cm_fk_parent_id = :mc_fk_parent_id
                ";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':mc_fk_parent_id',$param['id']);
        $stmt->bindParam(':language',$param['language']);

        //echo $stmt->queryString;die;
        $stmt->execute();

        
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }

    /**
     * get category list
     * @param $param
     * @return $result
     */
    public function category_change_status($param){
        $stmt = $this->conn->prepare("UPDATE `master_category` SET `cm_status` = :cm_status WHERE `cm_id` = :cm_id");
        $stmt->bindParam(':cm_status',$param['status']);
        $stmt->bindParam(':cm_id',$param['id']);
        $stmt->execute();
        
        $stmt->closeCursor();
        return;
    }

    /**
     * get category check
     * @param $param
     * @return $result
     */
    public function get_cetegory_exites($param){
        $sql = "SELECT *,(SELECT `ca_name` FROM `category_attributes` WHERE `ca_fk_cm_id` = :cm_id LIMIT 1) AS `parent_name` FROM `category_master` JOIN category_attributes ON ca_fk_cm_id = cm_id WHERE `cm_fk_parent_id` <> :cm_fk_parent_id AND UPPER(`ca_name`) = :cm_name AND `cm_action_taken` <> :cm_action_taken";
        $stmt = $this->conn->prepare($sql);

        $name = strtoupper($param['name']);

        $stmt->bindParam(':cm_fk_parent_id',$param['id']);
        $stmt->bindParam(':cm_name',$name);
        $stmt->bindParam(':cm_id',$param['id']);
        $deleted = 'deleted';
        $stmt->bindParam(':cm_action_taken',$deleted);

        $stmt->execute();

        $result = $stmt->fetch();
        
        $stmt->closeCursor();

        return $result;
    }

    /**
     * Check sub category
     * @param $param
     * @return $result
     */
    public function check_sub_category($param){

        $stmt = $this->conn->prepare("SELECT * FROM `category_master` WHERE `cm_fk_parent_id` = :cm_fk_parent_id AND `cm_action_taken` <> :cm_action_taken");
        $deleted = 'deleted';
        $stmt->bindParam(':cm_fk_parent_id',$param['id']);
        $deleted = 'deleted';
        $stmt->bindParam(':cm_action_taken',$deleted);
        $stmt->execute();

        $result = $stmt->rowCount();
        $stmt->closeCursor();

        return $result;
    }

    /**
     * Delete category
     * @param $param
     * @return $result
     */
    public function delete_category($param){

        $stmt = $this->conn->prepare("UPDATE `category_master` SET `cm_action_taken` = :cm_action_taken WHERE `cm_id` = :cm_id");
        
        $deleted = 'deleted';

        $stmt->bindParam(':cm_id',$param['id']);
        $stmt->bindParam(':cm_action_taken',$deleted);
        $stmt->execute();

        $stmt->closeCursor();

        return;
    }
	
	public function update_category_info($param){
        
        echo '<pre>';
        print_r($param);
        die;
        
        $status         = isset($param['status'])?$param['status']:'true';
        $lang           = (isset($param['lang']) && ($param['lang']=='spn'))?2:1;
        $cat_id         = isset($param['cat_id']) && is_numeric($param['cat_id'])?$param['cat_id']:0;
        $cat_name       = (isset($param['name']) && $param['name']!='')? $param['name']:'';
        $description    = isset($param['description'])? $param['description']:'';
        $price          = isset($param['price'])? $param['price']:0;
        $duration       = isset($param['duration'])? $param['duration']:0;
        
        
        if($cat_name!=''){
            
            if($lang==1){//if language is english
                $sql = "UPDATE category_attributes SET ca_name='".$cat_name."', ca_description='".$description."', ca_updated = NOW() "
                        . "WHERE ca_fk_cm_id = ".$cat_id." AND language=".$lang;
                $sql .= ";";
                $sql .= "UPDATE category_master SET cm_name='".$cat_name."', cm_description='".$description."', cm_price='".$price."', cm_duration='".$duration."', "
                        . "cm_action_taken='default', cm_updated=NOW(), cm_image='".$description."', cm_status='true' WHERE cm_id = ".$cat_id;
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $stmt->closeCursor();
            }
            //now for both language
            //get category_attribute_id
            $sql = "SELECT ca_id FROM category_attributes WHERE ca_fk_cm_id=15 AND language=2";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $stmt->closeCursor();
        }
        
        
        
        /*
        Array
            (
                [cat_id] => 5
                [lang] => spn
                [name] => look natural span
                [description] => sap
                [price] => 7.5
                [duration] => 21
                [spn_skin_tone] => Array
                    (
                        [6] => 6
                        [9] => 9
                    )

                [spn_lip_color] => Array
                    (
                        [7] => 7
                        [9] => 9
                        [10] => 10
                    )

                [spn_eye_shadow] => Array
                    (
                        [6] => 6
                        [10] => 10
                    )

                [status] => true
            )
         * 
         */
    }
       
    
    public function update_category_to_for_edit($param){
        $param['status'] = isset($param['status'])?$param['status']:'true';
        $param['lang'] = isset($param['lang'])?$param['lang']:'eng';
        $lang = ($param['lang']=='spn')?2:1;
        //$insert_id = '';
        
        if($param['lang']=='eng'){
            //update category_master
            $sql = "UPDATE category_master SET cm_name='".$param['name']."', cm_description='".$param['description']."', "
                    . "cm_price='".$param['price']."', cm_duration='".$param['duration']."', cm_status='".$param['status']."' "
                    . "WHERE cm_id=".$param['cat_id'].";";
        }
        
        $sqlF = "SELECT *FROM category_attributes WHERE ca_fk_cm_id=".$param['cat_id']." AND "
                . "language = (SELECT lm_id FROM language_master WHERE lm_name = '".$param['lang']."' LIMIT 1)";

        $stmt = $this->conn->prepare($sqlF);
        $stmt->execute();
        $result = $stmt->fetch();
        if(!$result){
            $sqlF="INSERT INTO category_attributes(ca_fk_cm_id, ca_name, ca_description, language)"
                    . "VALUES('".$param['cat_id']."', '".$param['name']."', '".$param['description']."', "
                    . "'".$lang."');";
            $stmt = $this->conn->prepare($sqlF);
            $stmt->execute();

        }
        /*
        if($param['lang']!='eng'){
            //update category_master
            $sql = "UPDATE category_master SET cm_name='".$param['name']."', cm_description='".$param['description']."', "
                    . "cm_price='".$param['price']."', cm_duration='".$param['duration']."', cm_status='".$param['status']."' "
                    . "WHERE cm_id=".$param['cat_id'].";";
        }
        */
        $sql .= "UPDATE category_attributes SET ca_name='".$param['name']."', ca_description='".$param['description']."' "
            . "WHERE ca_fk_cm_id=".$param['cat_id']." AND language = (SELECT lm_id FROM language_master WHERE lm_name = '".$param['lang']."' LIMIT 1);";
        
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        
        
        //now get the attribute id 
        $sql = "SELECT ca_id FROM category_attributes WHERE ca_fk_cm_id=".$param['cat_id']." AND "
                . "language = (SELECT lm_id FROM language_master WHERE lm_name = '".$param['lang']."' LIMIT 1);";
        
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        if($result){
            $ca_id = $result['ca_id'];
            
            //manage skintone
            if(isset($param['skin_tone'])){
                foreach($param['skin_tone'] as $row){
                    $sql = "SELECT *FROM master_catgegory_mapping WHERE mcm_service_name='master_skintone' AND "
                            . "mcm_service_id='".$row."' AND mcm_fk_ca_id='".$ca_id."'";
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                    $result = $stmt->fetch();
                    if(!$result){
                        $sql = "INSERT INTO master_catgegory_mapping (mcm_service_name, mcm_service_id,mcm_fk_ca_id, mcm_status)"
                                . "VALUES('master_skintone','".$row."','".$ca_id."', 'true')";
                        $stmt = $this->conn->prepare($sql);
                        $stmt->execute();
                    }
                }
                //now disable all rest skintone
                if(count($param['skin_tone'])){
                    $in_list = implode(',', $param['skin_tone']);
                    $sql = "UPDATE master_catgegory_mapping SET mcm_status='false' WHERE mcm_service_name='master_skintone' AND "
                            . "mcm_fk_ca_id='".$ca_id."' AND mcm_service_id NOT IN (".$in_list.");";
                    $sql .= "UPDATE master_catgegory_mapping SET mcm_status='true' WHERE mcm_service_name='master_skintone' AND "
                            . "mcm_fk_ca_id='".$ca_id."' AND mcm_service_id IN (".$in_list.")";
                    echo $sql;
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                }
            }else{
                $sql .= "UPDATE master_catgegory_mapping SET mcm_status='false' WHERE mcm_service_name='master_skintone' AND "
                        . "mcm_fk_ca_id='".$ca_id."'";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
            }
            
            
            
            //manage lips_color
            if(isset($param['lips_color'])){
               foreach($param['lips_color'] as $row){
                    $sql = "SELECT *FROM master_catgegory_mapping WHERE mcm_service_name='master_lips_color' AND "
                            . "mcm_service_id='".$row."' AND mcm_fk_ca_id='".$ca_id."'";
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                    $result = $stmt->fetch();
                    if(!$result){
                        $sql = "INSERT INTO master_catgegory_mapping (mcm_service_name, mcm_service_id,mcm_fk_ca_id, mcm_status)"
                                . "VALUES('master_lips_color','".$row."','".$ca_id."', 'true')";
                        $stmt = $this->conn->prepare($sql);
                        $stmt->execute();
                    }
                }

                //now disable all rest lips_color
                if(count($param['lips_color'])){
                    $in_list = implode(',', $param['lips_color']);
                    $sql = "UPDATE master_catgegory_mapping SET mcm_status='false' WHERE mcm_service_name='master_lips_color' AND "
                            . "mcm_fk_ca_id='".$ca_id."' AND mcm_service_id NOT IN (".$in_list.");";
                    $sql .= "UPDATE master_catgegory_mapping SET mcm_status='true' WHERE mcm_service_name='master_lips_color' AND "
                            . "mcm_fk_ca_id='".$ca_id."' AND mcm_service_id IN (".$in_list.")";

                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                } 
            }else{
                $sql .= "UPDATE master_catgegory_mapping SET mcm_status='false' WHERE mcm_service_name='master_lips_color' AND "
                        . "mcm_fk_ca_id='".$ca_id."'";

                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
            }
            
            
            //manage eye_shadow
            if(isset($param['eye_shadow'])){
                foreach($param['eye_shadow'] as $row){
                    $sql = "SELECT *FROM master_catgegory_mapping WHERE mcm_service_name='master_eyeshadow' AND "
                            . "mcm_service_id='".$row."' AND mcm_fk_ca_id='".$ca_id."'";
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                    $result = $stmt->fetch();
                    if(!$result){
                        $sql = "INSERT INTO master_catgegory_mapping (mcm_service_name, mcm_service_id,mcm_fk_ca_id, mcm_status)"
                                . "VALUES('master_eyeshadow','".$row."','".$ca_id."', 'true')";
                        $stmt = $this->conn->prepare($sql);
                        $stmt->execute();
                    }
                }

                //now disable all rest eye_shadow
                if(count($param['eye_shadow'])){
                    $in_list = implode(',', $param['eye_shadow']);
                    $sql = "UPDATE master_catgegory_mapping SET mcm_status='false' WHERE mcm_service_name='master_eyeshadow' AND "
                            . "mcm_fk_ca_id='".$ca_id."' AND mcm_service_id NOT IN (".$in_list.");";
                    $sql .= "UPDATE master_catgegory_mapping SET mcm_status='true' WHERE mcm_service_name='master_eyeshadow' AND "
                            . "mcm_fk_ca_id='".$ca_id."' AND mcm_service_id IN (".$in_list.")";

                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                }
            }else{
                $sql .= "UPDATE master_catgegory_mapping SET mcm_status='false' WHERE mcm_service_name='master_eyeshadow' AND "
                        . "mcm_fk_ca_id='".$ca_id."'";

                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
            }
            
        }
        $stmt->closeCursor();
    }

        /**
     * Get category to edit
     * @param $param
     * @return $result
     */
    public function get_category_to_edit($param){
        $sql = "SELECT cm_id,cm_id AS id,cm_price,cm_time,cm_status,ca_name,ca_description,
                (SELECT cm_fk_parent_id FROM master_category WHERE cm_fk_parent_id = id LIMIT 1) AS sub_category,
                (SELECT GROUP_CONCAT(ms_name) FROM master_skintone WHERE ms_id IN (SELECT mcm_service_id FROM master_catgegory_mapping WHERE mcm_fk_ca_id = cm_id AND mcm_service_name = 'master_skintone')) AS skin_tone,
                (SELECT GROUP_CONCAT(mcm_img) FROM master_catgegory_mapping WHERE mcm_fk_ca_id = cm_id AND mcm_service_name = 'master_skintone') AS skin_tone_img,
                (SELECT GROUP_CONCAT(me_name) FROM master_eyeshadow WHERE me_id IN (SELECT mcm_service_id FROM master_catgegory_mapping WHERE mcm_fk_ca_id = cm_id AND mcm_service_name = 'master_eyeshadow')) AS eyeshadow,
                (SELECT GROUP_CONCAT(mlc_name) FROM master_lips_color WHERE mlc_id IN (SELECT mcm_service_id FROM master_catgegory_mapping WHERE mcm_fk_ca_id = cm_id AND mcm_service_name = 'master_lips_color')) AS lips_color
                FROM master_category 
                LEFT JOIN category_attributes ON ca_fk_cm_id = cm_id
                WHERE cm_action_taken = 'default'
                AND category_attributes.language = :language
                AND cm_id = :cm_id
                ";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cm_id',$param['cat_id']);
        $stmt->bindParam(':language',$param['lang']);

        $stmt->execute();

        
        $result = $stmt->fetch();
        $stmt->closeCursor();

        return $result;
    }

    /**
     * Update category
     * @param $param
     * @return null
     */
    public function update_category($param){
        $sql = "UPDATE `category_master` SET cm_image = :cm_image WHERE `cm_id` = :cm_id";

        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindParam(':cm_id',$param['id']);
        $stmt->bindParam(':cm_image',$param['image']);
        $stmt->execute();

        $stmt->closeCursor();

        $sql = "SELECT ca_id FROM category_attributes WHERE `ca_fk_cm_id` = :ca_fk_cm_id AND language = :language";
        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindParam(':ca_fk_cm_id',$param['id']);
        $stmt->bindParam(':language',$param['lang']);

        $stmt->execute();

        $result = $stmt->fetch();
        $stmt->closeCursor();

        if($result){
            $sql = "UPDATE `category_attributes` SET `ca_name` = :cm_name , `ca_description` = :cm_description WHERE `ca_fk_cm_id` = :cm_id AND language = :language";
        }
        else{
            $sql = "INSERT INTO `category_attributes` (`ca_name`, `ca_description` ,`ca_fk_cm_id`, language) VALUES(:cm_name,:cm_description ,:cm_id, :language)";
        }

        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindParam(':cm_id',$param['id']);
        $stmt->bindParam(':cm_name',$param['name']);
        $stmt->bindParam(':cm_description',$param['description']);
        $stmt->bindParam(':language',$param['lang']);

        $stmt->execute();

        $stmt->closeCursor();

        return;
    }

    /**
     * Update category
     * @param $param
     * @return null
     */
    public function update_category_attribute($param){
        $sql = "UPDATE `category_attributes` SET ca_price = :ca_price, ca_skintone = :ca_skintone, ca_eye_shadow = :ca_eye_shadow, ca_lip_color = :ca_lip_color, ca_duration = :ca_duration WHERE `ca_fk_cm_id` = :ca_fk_cm_id AND language = :language";

        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindParam(':ca_fk_cm_id',$param['id']);
        $stmt->bindParam(':ca_price',$param['price']);
        $stmt->bindParam(':ca_skintone',$param['skin_tone']);
        $stmt->bindParam(':ca_eye_shadow',$param['eye_shadow']);
        $stmt->bindParam(':ca_lip_color',$param['lips_color']);
        $stmt->bindParam(':ca_duration',$param['time']);
        $stmt->bindParam(':language',$param['lang']);

        $stmt->execute();

        $stmt->closeCursor();

        return;
    }

    /**
     * Get category to edit
     * @param $param
     * @return $result
     */
    public function get_cetegory_exites_to_update($param){
        $sql = "SELECT `cm_name` FROM `category_master` WHERE `ca_name` = :cm_name AND `cm_fk_parent_id` = (SELECT `cm_fk_parent_id` FROM `category_master` WHERE `cm_id` = :cm_id LIMIT 1) AND `cm_id` <> :cm_id";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':cm_id',$param['id']);
        $stmt->bindParam(':cm_name',$param['name']);
        
        $stmt->execute();

        $result = $stmt->fetch();

        $stmt->closeCursor();

        return $result;
    }
    
    /**
     * Get category to edit
     * @param $param
     * @return $result
     */
    public function get_category_portfolio($param){
        $sql = "SELECT cm_id,cm_before_image,cm_after_image FROM category_master WHERE cm_id = :cm_id";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':cm_id',$param['id']);
        
        $stmt->execute();

        $result = $stmt->fetch();

        $stmt->closeCursor();

        return $result;
    }
    
    /**
     * Get category to edit
     * @param $param
     * @return $result
     */
    public function update_category_portfolio_image($param){
        if($param['type'] == 'before'){
            $sql = "UPDATE category_master SET cm_before_image = :image WHERE cm_id = :cm_id";
        }
        else{
            $sql = "UPDATE category_master SET cm_after_image = :image WHERE cm_id = :cm_id";
        }

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':cm_id',$param['id']);
        $stmt->bindParam(":image",$param['image']);
        
        $stmt->execute();

        $stmt->closeCursor();

        return;
    }

    /**
     * Get category to edit
     * @param null
     * @return $result
     */
    public function get_skin_tone($lang='eng'){
        $sql = "SELECT *FROM master_skintone WHERE ms_status = 'true' "
                . "AND language = (SELECT lm_id FROM language_master WHERE lm_name = '".$lang."' LIMIT 1)";
        if($lang=='all'){
            $sql = "SELECT * FROM `master_skintone` WHERE ms_status='true'";
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * Get Lips color
     * @param null
     * @return $result
     */
    public function get_lips_color($lang='eng'){
        $sql = "SELECT mlc_id,mlc_name,mlc_fk_mlc_id, language FROM master_lips_color WHERE mlc_status = 'true' AND "
                . "language = (SELECT lm_id FROM language_master WHERE lm_name = '".$lang."' LIMIT 1)";
        if($lang=='all'){
            $sql = "SELECT * FROM master_lips_color WHERE mlc_status = 'true'";
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * Get Lips color
     * @param null
     * @return $result
     */
    
    public function get_eyeshadow($lang='eng'){
        $sql = "SELECT me_id,me_name,me_fk_me_id, language FROM master_eyeshadow WHERE me_status = 'true' AND "
                . "language = (SELECT lm_id FROM language_master WHERE lm_name = '".$lang."' LIMIT 1)";
        if($lang=='all'){
            $sql = "SELECT * FROM master_eyeshadow WHERE me_status = 'true'";
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }
}