<?php
class Referral extends Layout{

	protected $app;
	/**
    * Construct
    * @param $app
    */
	public function __construct($app){
		if(!isset($_SESSION['admin_info'])){
            redirect('admin/login');
        }

        $this->app = $app;

        $this->load_model('Admin/Referral_model');/* Make connection referral model */
	}

	/**
    * Referral
    * @param $req, $res, $args
    */
	public function index($req, $res, $args)
	{  
		
	}

    /**
    * Add referral
    * @param $req, $res, $args
    */
    public function add_referral($req, $res, $args)
    {  
        if($_POST){

            $date_range = explode(" - ",$_POST['date_range']);

            $start_date = trim(current($date_range));

            $end_date = trim(end($date_range));

            $param = $_POST;

            $param['start_date'] = date('Y-m-d',strtotime($start_date));

            $param['end_date'] = date('Y-m-d',strtotime($end_date));

            $this->referral_model->add_referral($param);/*Add referral*/

            $_SESSION['success'] = "Add referral successfully";

            redirect('admin/referral_list');
        }
        else{
            $args['header']['title'] = "Add referral";

            $this->load_library('Admin/ViewData');

            $args['ViewData'] = $this->ViewData;

            $this->layout($res,"Admin/Referral/add_update_referral.php",$args);
        }
    }

    /**
    * Referral list
    * @param $req, $res, $args
    */
    public function referral_list($req, $res, $args)
    {  
        $args['header']['title'] = "Celebrity referral";

        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['page']) && $args['page']){
            $page = ($args['page'] -1)*10;
            $page1 = $args['page'];
        }
        else{
            $page = 0;
            $args['page'] = 1;
        }

        $param['page'] = $page;

        $args['query'] = "";
        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }

        $args['referral_count'] = $this->Referral_model->get_referral_count($param);/*Getting artist count*/
        $args['referral_list'] = $this->Referral_model->get_referral_list($param);/*Getting all artist*/

        //echo '<pre>';print_r($args);
        $this->layout($res,"Admin/Referral/referral_list.php",$args);
    }



    public function show_celebrity_modal($req, $res, $args){
        $args['result'] = $this->Referral_model->get_celebrity_used_referrel($args['id']);
        $this->view($res,'Admin/Models/show_celebrity_modal.php',$args);
    }

    /**
    * Referral list
    * @param $req, $res, $args
    */
    public function referral_list_user($req, $res, $args)
    {  
        $args['header']['title'] = "User referral";

        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['page']) && $args['page']){
            $page = ($args['page'] -1)*10;
            $page1 = $args['page'];
        }
        else{
            $page = 0;
            $args['page'] = 1;
        }

        $param['page'] = $page;

        $args['query'] = "";
        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }

        $args['referral_count'] = $this->Referral_model->get_user_referral_count($param);/*Getting artist count*/
        $args['referral_list'] = $this->Referral_model->get_user_referral_list($param);/*Getting all artist*/
        
        $this->layout($res,"Admin/Referral/referral_list_user.php",$args);
    }

    /**
    * Change referral status
    * @param $req, $res, $args
    */
    public function change_referral_status($req, $res, $args)
    {
        $id = $_POST['id'];

        $referral = $this->Referral_model->get_referral($id);/*Getting referral*/

        $param['id'] = $id;

        if($referral['r_status'] == true){
            $param['status'] = false;

        }
        else{
            $param['status'] = true;
        }

        $this->Referral_model->change_referral_status($param);/*Change referral status*/

        echo $param['status'];
    }

    /**
    * Update referral
    * @param $req, $res, $args
    */
    public function update_referral($req, $res, $args)
    {
        if($_POST){

            $date_range = explode(" - ",$_POST['date_range']);

            $start_date = trim(current($date_range));

            $end_date = trim(end($date_range));

            $param = $_POST;

            $param['start_date'] = date('Y-m-d',strtotime($start_date));

            $param['end_date'] = date('Y-m-d',strtotime($end_date));

            $param['id'] = $args['id'];

            $this->referral_model->update_referral($param);/*Update referral*/

            $_SESSION['success'] = "Update referral successfully";

            redirect('admin/referral_list');
        }
        else{
            $args['header']['title'] = "Update referral";

            $id = $args['id'];
            $args['referral'] = $this->Referral_model->get_referral($id);/*Getting referral*/

            $this->load_library('Admin/ViewData');

            $args['ViewData'] = $this->ViewData;

            $this->ViewData->data = array(
                                          'celebrity_name'  => $args['referral']['r_name'],
										  'celebrity_code'  => $args['referral']['r_code'],
                                          'rebate_price'    => $args['referral']['r_rebate'],
                                          'date_range'   => date('m/d/Y',strtotime($args['referral']['r_start_date']))." - ".date('m/d/Y',strtotime($args['referral']['r_end_date'])),
                                          'user_type'       => $args['referral']['r_usertype']
                                        );

            $this->layout($res,"Admin/Referral/add_update_referral.php",$args);
        }
    }
}
