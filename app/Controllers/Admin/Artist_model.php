<?php
class Artist_model{

	protected $conn;

	function __construct($app){
		$this->conn = $app->get('db');
	}

	/**
	* Get artist list
	* @param null
	* @return $result
	*/
	public function get_artist_list($param){
		$page = isset($param['page'])?$param['page']:0;

		$sql = "SELECT * FROM (
		SELECT *,
		
                MUA_TOTAL_REFERREL(u_id,'reffered') AS used_referral,
                MUA_TOTAL_REFERREL(u_id,'earning') AS earned_referral,

		(SELECT r_id FROM referral WHERE r_fk_u_id = user.u_id LIMIT 1) AS r_id,
		
		(SELECT ap_city FROM artist_profile WHERE u_id = ap_fk_u_id LIMIT 1) AS ap_city,(SELECT ap_profile_name 
                
                    FROM artist_profile WHERE u_id = ap_fk_u_id LIMIT 1) AS ap_name,
                   (SELECT ap_pay_commission FROM artist_profile WHERE u_id = ap_fk_u_id LIMIT 1) AS ap_pay_commission 
                   
                    FROM user WHERE u_type = 'mua' AND u_varified = 'accepted'
		) AS user ";

		if(isset($param['search']) && $param['search']){
			$sql .= "WHERE (user.u_name LIKE :search
					OR 
					user.u_email LIKE :search 
					OR user.`ap_city` LIKE :search
					)";
		}

		$sql .= " ORDER BY `u_id` DESC LIMIT $page , 10";
               
		$stmt = $this->conn->prepare($sql);
		if(isset($param['search']) && $param['search']){
			$search = '%'.$param['search'].'%';
			$stmt->bindParam(':search',$search);
		}

		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}


	public function get_mua_used_referrel_modal($id){
            
            $sql = "SELECT *,
                (SELECT cr_id FROM client_request WHERE cr_fk_u_mua_id = ur_fk_u_id AND cr_status = 'finished' LIMIT 1) AS booking,
                (SELECT r_rebate FROM referral WHERE r_id = ur_r_id LIMIT 1) AS r_rebate, 

                (SELECT u_email FROM user WHERE u_id = ur_fk_u_id LIMIT 1) AS u_email

                FROM used_referral WHERE ur_usertype='mua' AND ur_r_id = (SELECT r_id FROM referral WHERE r_fk_u_id = ".$id.")";
            
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $stmt->closeCursor();

            return $result;
	}

	/**
	* Get artist list
	* @param null
	* @return $result
	*/
	public function get_artist_count($param){

		$sql = "SELECT *FROM (SELECT *,(SELECT ap_city FROM artist_profile WHERE u_id = ap_fk_u_id LIMIT 1) AS ap_city FROM user WHERE u_type = 'mua' AND u_varified = 'accepted') AS user ";

		if(isset($param['search']) && $param['search']){
			$sql .= "WHERE (user.u_name LIKE '%".$param['search']."%'
					OR 
					user.u_email LIKE '%".$param['search']."%' 
					OR user.`ap_city` LIKE '%".$param['search']."%'
					)";
		}

		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->rowCount();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get artist list
	* @param null
	* @return $result
	*/
	public function get_artist_request_list($param){
		$page = isset($param['page'])?$param['page']:0;

		$sql = "SELECT *FROM (SELECT *,(SELECT ap_city FROM artist_profile WHERE u_id = ap_fk_u_id LIMIT 1) AS ap_city,(SELECT ap_profile_name FROM artist_profile WHERE u_id = ap_fk_u_id LIMIT 1) AS ap_name FROM user WHERE u_type = 'mua' AND (u_varified = 'new' OR u_varified = 'on-hold')) AS user ";

		if(isset($param['search']) && $param['search']){
			$sql .= "WHERE (user.u_name LIKE '%".$param['search']."%'
					OR 
					user.u_email LIKE '%".$param['search']."%' 
					OR user.`ap_city` LIKE '%".$param['search']."%'
					)";
		}

		$sql .= " ORDER BY `u_id` DESC LIMIT $page , 10";

		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get artist list
	* @param null
	* @return $result
	*/
	public function get_artist_request_count($param){

		$sql = "SELECT *FROM (SELECT *,(SELECT ap_city FROM artist_profile WHERE u_id = ap_fk_u_id LIMIT 1) AS ap_city FROM user WHERE u_type = 'mua' AND (u_varified = 'new' OR u_varified = 'on-hold')) AS user ";

		if(isset($param['search']) && $param['search']){
			$sql .= "WHERE (user.u_name LIKE '%".$param['search']."%'
					OR 
					user.u_email LIKE '%".$param['search']."%' 
					OR user.`ap_city` LIKE '%".$param['search']."%'
					)";
		}

		$stmt = $this->conn->prepare($sql);

		$stmt->execute();
		$result = $stmt->rowCount();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Change artist request status
	* @param $param
	* @return 
	*/
	public function change_artist_request_status($param){
		$sql = "UPDATE `user` SET `u_varified` = :u_varified WHERE `u_id` = :u_id";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":u_varified",$param['status']);
		$stmt->bindParam(":u_id",$param['id']);
		$stmt->execute();
		$stmt->closeCursor();
		return;
	}

	/**
	* Get artist availability
	* @param $param
	* @return $result
	*/
	public function get_artist_availability($param){
		$sql = "SELECT * FROM `artist_availability` WHERE `aa_fk_u_id` = :u_id";

		$stmt = $this->conn->prepare($sql);

		$stmt->bindParam(':u_id',$param['id']);

		$stmt->execute();
		
		$result = $stmt->fetchAll();
		
		$stmt->closeCursor();
		
		return $result;
	}

	/**
	* Change artist status
	* @param $param
	* @return 
	*/
	public function artist_change_status($param){
		$sql = "UPDATE `user` SET `u_status` = :u_status WHERE `u_id` = :u_id";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":u_status",$param['status']);
		$stmt->bindParam(":u_id",$param['id']);
		$stmt->execute();
		$stmt->closeCursor();
		return;
	}

	/**
	* Get artist profile
	* @param $param
	* @return $result
	*/
	public function get_artist_profile($param){
		$sql = "SELECT *,
				(SELECT im_image_name FROM image_master WHERE im_fk_u_id = :ap_fk_u_id AND im_action = 'artist') AS image FROM `artist_profile`
				WHERE `ap_fk_u_id` = :ap_fk_u_id";

		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":ap_fk_u_id",$param['id']);
		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Change artist availability status
	* @param $param
	* @return 
	*/
	public function change_artist_availability_status($param){
		$sql = "UPDATE `artist_availability` SET `aa_active_status` = :aa_active_status WHERE `aa_id` = :aa_id";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":aa_id",$param['id']);
		$stmt->bindParam(":aa_active_status",$param['status']);
		$stmt->execute();
		$stmt->closeCursor();
		return;
	}

	/**
	* Artist category
	* @param $param
	* @return $result
	*/
	public function artist_category($param){
		$sql = "SELECT *,(SELECT `cm_name` FROM `category_master` WHERE `cm_id` = `ac_fk_cm_id` LIMIT 1) AS `cm_name` FROM `artist_category` WHERE `ac_fk_u_id` = :ac_fk_u_id";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":ac_fk_u_id",$param['id']);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get artist portfolio
	* @param $param
	* @return $result
	*/
	public function get_artist_portfolio($param){
		$page = $param['page'];

		//$sql = "SELECT *,(SELECT (SELECT `cm_name` FROM `category_master` WHERE `cm_id` = `ac_fk_cm_id` LIMIT 1) AS `cm_name` FROM `artist_category` WHERE `ac_id` = `ap_fk_ac_id`) AS `cm_name` FROM `artist_portfolio` WHERE `ap_fk_u_id` = :ap_fk_u_id ORDER BY `ap_id` DESC LIMIT $page,10";
		$sql = "SELECT *,
		(SELECT im_image_name FROM image_master WHERE im_fk_ac_id = artist_portfolio.ap_id AND im_status = 'true' AND im_deleted = 'false' AND 	im_portfolio_action = 'before' ORDER BY image_master.im_id DESC LIMIT 1) AS portfolio_image_before,
		(SELECT im_image_name FROM image_master WHERE im_fk_ac_id = artist_portfolio.ap_id AND im_status = 'true' AND im_deleted = 'false' AND 	im_portfolio_action = 'after' ORDER BY image_master.im_id DESC LIMIT 1) AS portfolio_image_after, 
		(SELECT (SELECT `cm_name` FROM `category_master` WHERE `cm_id` = `ac_fk_cm_id` LIMIT 1) AS `cm_name` FROM `artist_category` WHERE `ac_id` = `ap_fk_ac_id`) AS `cm_name` FROM `artist_portfolio` WHERE `ap_fk_u_id` = :ap_fk_u_id ORDER BY `ap_id` DESC LIMIT $page,10";

		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":ap_fk_u_id",$param['id']);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get artist portfolio
	* @param $param
	* @return $result
	*/
	public function get_artist_portfolio_count($param){
		/*$sql = "SELECT *,(SELECT (SELECT `cm_name` FROM `category_master` WHERE `cm_id` = `ac_fk_cm_id` LIMIT 1) AS `cm_name` FROM `artist_category` WHERE `ac_id` = `ap_fk_ac_id`) AS `cm_name` FROM `artist_portfolio` WHERE `ap_fk_u_id` = :ap_fk_u_id ORDER BY `ap_id` DESC";*/

		$sql = "SELECT * FROM `artist_portfolio` WHERE `ap_fk_u_id` = :ap_fk_u_id ORDER BY `ap_id` DESC";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":ap_fk_u_id",$param['id']);
		$stmt->execute();
		$result = $stmt->rowCount();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get artist portfolio image
	* @param $param
	* @return $result
	*/
	public function get_artist_portfolio_img($param){
		$sql = "SELECT im_image_name FROM image_master WHERE im_fk_ac_id == :im_fk_ac_id AND im_action == 'portfolio' AND im_status = 'true' AND im_deleted <> true";
		
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(":im_fk_ac_id",$param['id']);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get common commision
	* @param null
	* @return $result
	*/
	public function get_commision(){
		$sql = "SELECT s_value FROM settings WHERE s_types = 'commision'";
		$stmt = $this->conn->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Get artist commision
	* @param $param
	* @return $result
	*/
	public function get_artist_commision($param){
		$sql = "SELECT ap_pay_commission FROM artist_profile WHERE ap_fk_u_id = :ap_fk_u_id";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(':ap_fk_u_id',$param['id']);
		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}

	/**
	* Change artist commision
	* @param $param
	* @return $result
	*/
	public function change_artist_commision($param){
		$sql = "UPDATE artist_profile SET ap_pay_commission = :ap_pay_commission WHERE ap_fk_u_id = :ap_fk_u_id";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(':ap_pay_commission',$param['commision']);
		$stmt->bindParam(':ap_fk_u_id',$param['id']);
		$result = $stmt->execute();
		$stmt->closeCursor();
		return $result;
	}

	/*
	||==============================================
	|| Function get_artist_certificate
	||==============================================
	*/
	public function get_artist_certificate($param){
		$sql = "SELECT *,
			(SELECT ap_certificate_status FROM artist_profile WHERE ap_fk_u_id = :im_fk_u_id LIMIT 1) AS ap_certificate_status,
			(SELECT ap_comment FROM artist_profile WHERE ap_fk_u_id = :im_fk_u_id LIMIT 1) AS ap_comment

		FROM image_master WHERE im_action = 'certificate' AND im_fk_u_id = :im_fk_u_id ORDER BY im_id DESC";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(':im_fk_u_id', $param['id']);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}

	/*
	||==============================================
	|| Function change_certificate_status
	||==============================================
	*/
	public function change_certificate_status($param){
		$sql = "UPDATE artist_profile SET ap_certificate_status = :ap_certificate_status, ap_comment = :ap_comment WHERE ap_fk_u_id = :ap_fk_u_id";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(':ap_fk_u_id', $param['js_id']);
		$stmt->bindParam(':ap_certificate_status', $param['js_status']);
		$stmt->bindParam(':ap_comment', $param['js_comment']);
		$result = $stmt->execute();
		$stmt->closeCursor();
		return $result;
	}
}