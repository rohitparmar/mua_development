<?php

class Dashboard extends Layout {

    protected $app;

    /**
     * Counstruct
     * @param $app
     */
    public function __construct($app) {

        if (!isset($_SESSION['admin_info'])) {
            redirect('admin/login');
        }

        $this->load_library('Admin/Validation');

        $this->app = $app;
    }

    /**
     * Dashboard
     * @param $req,$res,$args
     */
    public function index($req, $res, $args) {
        $args['header']['title'] = 'Dashboard';
        $this->load_model('Admin/Dashboard_model.php'); /* Load dashboard model */

        $args['artist_count'] = $this->Dashboard_model->get_artist_count(); /* Get total artist count */

        $args['client_count'] = $this->Dashboard_model->get_client_count(); /* Get total client count */

        $args['artist_request_count'] = $this->Dashboard_model->get_artist_request_count(); /* Get total artist request count */

        $args['artist_payment_count'] = $this->Dashboard_model->get_artist_payment_count(); /* Get total artist payment count */

        $args['booking_count'] = $this->Dashboard_model->get_booking_count(); /* Get total booking count */

        $args['recent_5_artist'] = $this->Dashboard_model->get_recent_5_artist(); /* Get recent 5 artist */

        $args['recent_5_client'] = $this->Dashboard_model->get_recent_5_client(); /* Get recent 5 client */

        $this->layout($res, 'Admin/Dashboard/index.php', $args);
    }

    /**
     * Change password
     * @param $req,$res,$args
     */
    public function change_password($req, $res, $args) {
        if ($_POST) {

            $this->Validation->rules('Old password', 'old_pass', 'required');
            $this->Validation->rules('New password', 'new_pass', 'required|min_length[8]');
            $this->Validation->rules('Confirm password', 'confirm_pass', 'required|min_length[8]|equalTo[new_pass]');

            if (!$this->Validation->error) {
                $this->load_model('Admin/Dashboard_model.php'); /* Load dashboard model */
                $param = $_POST;
                $result = $this->Dashboard_model->validate_password($param); /* Validate password */

                if ($result == 1) {
                    $this->Dashboard_model->update_password($param); /* Update password */
                    $_SESSION['success'] = 'Password updated successfully';

                    $mail_data = array(
                        'to' => $_SESSION['admin_info']['email'],
                        'from' => $_SESSION['admin_info']['email'],
                        'subject' => 'Change password',
                        'message' => 'Your password has been changed',
                        'from_name' => SITE_NAME,
                        'file' => ''
                    );
                    $this->send_mail_api($mail_data);
                    redirect('admin/profile');
                } else {
                    $args['old_pass_error'] = 'Wrong password';
                }
            }
        }

        $args['header']['title'] = 'Change password';
        $args['Validation'] = $this->Validation;
        $this->layout($res, 'Admin/Dashboard/change_password.php', $args);
    }

    /**
     * Send mail api
     * @param null
     * @return $result
     */
    private function send_mail_api($data) {
        $url = 'http://synergytop.com/api/email';

        $params = http_build_query($data, NULL, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url); //Remote Location URL
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser   
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        //We add these 2 lines to create POST request
        curl_setopt($ch, CURLOPT_POST, count($data)); //number of parameters sent
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params); //parameters data

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    /**
     * Profile
     * @param $req,$res,$args
     */
    public function profile($req, $res, $args) {
        $this->load_model('Admin/Dashboard_model.php'); /* Load dashboard model */

        if ($_POST) {
            $param = $_POST;
            $param['id'] = $_SESSION['admin_info']['id'];
            $result = $this->Dashboard_model->check_email_exites($param);
            if ($result > 0) {
                $_SESSION['error'] = 'This email is already in use';
            } else {
                $this->Dashboard_model->update_admin_profile($param); /* Update admin profile */
                $_SESSION['success'] = 'Profile updated successfully';

                $message = "Your profile has been changed";

                $mail_data = array(
                    'to' => $_SESSION['admin_info']['email'],
                    'from' => $_SESSION['admin_info']['email'],
                    'subject' => 'Update profile',
                    'message' => $message,
                    'from_name' => SITE_NAME,
                    'file' => ''
                );
                $this->send_mail_api($mail_data);
                $_SESSION['admin_info']['name'] = $_POST['name'];
                $_SESSION['admin_info']['email'] = $_POST['email'];
            }
        }

        $args['header']['title'] = 'Profile';

        $param['id'] = $_SESSION['admin_info']['id'];
        $args['profile'] = $this->Dashboard_model->get_profile($param); /* Get admin profile */

        $this->layout($res, 'Admin/Dashboard/profile.php', $args);
    }

    /**
     * Update admin image
     * @param $req,$res,$args
     */
    public function update_admin_image($req, $res, $args) {
        if ($_FILES['file']) {
            $valid_ext = array('jpg', 'jpeg', 'png', 'gif');
            $img_ext = explode('.', $_FILES['file']['name']);
            $img_ext = end($img_ext);
            if (in_array($img_ext, $valid_ext)) {
                $image_name = rand_value($_SESSION['admin_info']['id'], $img_ext);
                if (file_exists($_FILES['file']['tmp_name'])) {
                    move_uploaded_file($_FILES['file']['tmp_name'], ADMIN_PROFILE_IMG . $image_name);
                    $this->load_model('Admin/Dashboard_model.php'); /* Load dashboard model */

                    $param['id'] = $_SESSION['admin_info']['id'];
                    $param['image'] = $image_name;

                    $this->Dashboard_model->update_admin_profile_image($param); /* Update profile image */
                    $_SESSION['admin_info']['image'] = $image_name;
                    echo base_url(ADMIN_PROFILE_IMG . $image_name);
                }
            }
        }
    }

}
