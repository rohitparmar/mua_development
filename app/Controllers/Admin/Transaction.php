<?php

class Transaction extends Layout{

	protected $app;

    /**
    * Counstruct
    * @param $app
    */
	public function __construct($app){
        
        if(!isset($_SESSION['admin_info'])){
            redirect('admin/login');
        }

		$this->app = $app;
        $this->load_model('Admin/Transaction_model');
	}

    /**
    * Transaction
    * @param $req,$res,$args
    */
	public function index($req, $res, $args)
    {
        $args['header']['title'] = 'Artist payment information';
        

        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;
		if(isset($args['id']) && $args['id']){
            $args['page'] = $args['id'];
        }

        if(isset($args['page']) && $args['page']){
            $page = ($args['page'] -1)*10;
            $page1 = $args['page'];
        }
        else{
            $page = 0;
            $args['page'] = 1;
        }

        $param['page'] = $page;

        $args['query'] = "";
        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }

        $args['count'] = $this->Transaction_model->get_transaction_count($param);/*Get transaction count*/

        $args['transaction'] = $this->Transaction_model->get_transaction($param);/*Get transaction*/

        

        $this->layout($res, 'Admin/Transaction/index.php', $args);
	}

    public function deleted_subscription($req, $res, $args)
    {   
        $id = isset($args['id'])?$args['id']:'';
        $this->Transaction_model->deleted_subscription($id);
        redirect('admin/transaction');
    }


    /**
    * View transaction details
    * @param $req,$res,$args
    */
    public function view_transaction_details($req, $res, $args)
    {
       
        $param = $_POST;
        $details = $this->Transaction_model->get_transaction_details($param);/*Get transaction details*/
        //echo $details['cpi_transaction_details'];
        $args['details'] = @unserialize($details['api_transaction_details']);
        echo $this->view($res,'Admin/Models/transation_details.php',$args,false);
    }

    /**
    * Client trastion info
    * @param $req,$res,$args
    */
    public function client_payment_info($req, $res, $args)
    {

        $args['header']['title'] = 'Client payment information';
       

        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['id']) && $args['id']){
            $args['page'] = $args['id'];
        }

        if(isset($args['page']) && $args['page']){
            $page = ($args['page'] -1)*10;
            $page1 = $args['page'];
        }
        else{
            $page = 0;
            $args['page'] = 1;
        }

        $param['page'] = $page;

      

        $args['query'] = "";
        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }

        $args['count'] = $this->Transaction_model->get_client_transaction_count($param);/*Get client transaction count*/

        $args['transaction'] = $this->Transaction_model->get_client_transaction($param);/*Get client transaction*/

        $this->layout($res,'Admin/Transaction/client_payment_info.php',$args);
    }


    /**
    * payment_info
    * @param $req,$res,$args
    */
    public function payment_info_mua($req, $res, $args)
    {

        

        $args['header']['title'] = 'payment information';
        
        $page = 0;

        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['id']) && $args['id']){
            $page = ($args['id']-1)*10;
        }

        $param['page'] = $page;

        $args['query'] = "";
        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }


        $signupdate = date("Y-m-d H:i:s");
        $signupweek = date("W",strtotime($signupdate));

        $year = date("Y",strtotime($signupdate));
        $currentweek = date("W");

        for($i=$signupweek;$i<=$currentweek;$i++) {
            $result = $this->getWeek($i,$year);
            $param['start_date'] = isset($result['start'])?$result['start']:'';
            $param['end_date']   = isset($result['end'])?$result['end']:'';
        }

        $args['count'] = $this->Transaction_model->get_mua_payment_info_count($param);/*Get transaction count*/

        $args['transaction'] = $this->Transaction_model->get_mua_payment_info($param);/*Get transaction*/
        $args['page'] = isset($args['id'])?$args['id']:'';
        $this->layout($res, 'Admin/Transaction/payment_info.php', $args);
    }

    private function getWeek($week, $year) {
          
        $dto = new DateTime();
        $result['start'] = $dto->setISODate($year, $week, 0)->format('Y-m-d');
        $result['end'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
        return $result;
    }

    /**

    * change_paid_status change status ajax call

    * @param $req, $res, $args

    */

    public function get_paid_data($req, $res, $args)

    {

        $param = $_POST;
        
        $args['profile'] = $this->Transaction_model->get_paid_data($param);/*Get artist profile*/
       
        $args['mua_id']  = isset($param['id'])?$param['id']:0;

        $args['account_info'] = $this->Transaction_model->get_account_info($args['mua_id']);/*Get artist profile*/



        if($args['mua_id']){
            echo $this->view($res,'Admin/Models/transaction/paid.php',$args,false);
        }else{
            echo '<div style="min-height: 100px;
                    padding-top: 43px;
                    text-align: center;">Invalid Users</div>';
        }
    }


    public function add_paid_data($req, $res, $args){
       
        $param = $_POST;

        $data = $this->Transaction_model->add_paid_data($param);/*Get artist commision*/

        echo $data;
        die;
    }




    public function payment_received($req, $res, $args){
         
         $param = $_POST;
         
         $user_id = isset($_POST['id'])?$_POST['id']:'';

         $args['payment_received'] = $this->Transaction_model->payment_received($user_id);/*Get artist profile*/


         echo $this->view($res,'Admin/Models/transaction/payment_received.php',$args,false);
         die;
    }

    
     public function total_payment_received($req, $res, $args){
         
         $param = $_POST;
         
         $user_id = isset($_POST['id'])?$_POST['id']:'';

         $args['payment_received'] = $this->Transaction_model->total_payment_received($user_id, $lang = 'eng');/*Get artist profile*/


         echo $this->view($res,'Admin/Models/transaction/total_received_payment.php',$args,false);
         die;
    }


}