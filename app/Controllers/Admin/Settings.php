<?php



class Settings extends Layout { 
    protected $app;
    /**

     * Counstruct

     * @param $app

     */

    public function __construct($app) {

        if (!isset($_SESSION['admin_info'])) {

            redirect('admin/login');

        }

        $this->load_library('Admin/Validation.php'); /* Load validation */

        $this->app = $app; 
		$this->load_library('Api/Braintree');
        $this->load_model('Admin/Settings_model.php'); /* Load settings model */

    }

    

    /**

     * Settings

     * @param $req,$res,$args

     */

    public function common($req, $res, $args) {

        if ($_POST) {

            if (isset($_POST['commission'])) {

                $param['value'] = $_POST['commission'];
                $param['type'] = 'commission';
                $param['status'] = 'true';
                $this->Settings_model->update_settings($param); /* Updating settings for commision */

            }

            if (isset($_POST['ratepermile'])) {
                $param['value'] = $_POST['ratepermile'];
                $param['type'] = 'rate_per_mile';
                $param['status'] = 'true';
                $this->Settings_model->update_settings($param); /* Updating settings for commision */
            }

            if (isset($_POST['referral_amount'])) {
                $param['value'] = $_POST['referral_amount'];
                $param['status'] = 'true';
                $param['type'] = 'referral_amount';
                $this->Settings_model->update_settings($param); /* Updating settings for commision */
            }

            $param['status'] = isset($_POST['status']) ? 'true' : 'false';
            
            if (isset($_POST['referral_expiry'])) {

                $param['value'] = $_POST['referral_expiry'];

                $param['type'] = 'referral_expiry';
                $param['status'] = 'true';

                $this->Settings_model->update_settings($param); /* Updating settings for commision */

            }

            if (isset($_POST['app_price'])) {

                $param['value'] = $_POST['app_price'];

                $param['type'] = 'app_prices';
                $param['status'] = 'true';

                $this->Settings_model->update_settings($param); /* Updating settings for commision */

            }


            if (isset($_POST['app_discount'])) {

                $param['value'] = $_POST['app_discount'];

                $param['type'] = 'app_commission';
                $param['status'] = 'true';

                $this->Settings_model->update_settings($param); /* Updating settings for commision */

            }
        }

        $args['header']['title'] = "Commission";



        $settings = $this->Settings_model->get_settings();



        $setttingArr = array();

        foreach ($settings as $row) {

            $setttingArr[$row['s_types']] = array(

                's_id' => $row['s_id'],

                's_value' => $row['s_value'],

                's_status' => $row['s_status'],

                's_created' => $row['s_created'],

                's_updated' => $row['s_updated']

            );

        }



        $args['settings'] = $setttingArr;

        $args['validation'] = $this->Validation;

        $this->layout($res, 'Admin/Settings/commision.php', $args);

    }



    /**

     * Plan

     * @param $req,$res,$args

     */

    public function plan($req, $res, $args) {

        $args['header']['title'] = "Plan";

        $args['plan'] = $this->Settings_model->get_plans();

        $this->layout($res, 'Admin/Settings/plan.php', $args);
    }
	
	public function get_subscription_plan_list($req, $res, $args) {echo '<pre>';
		$plan_list = $this->Braintree->get_plan_list();
		print_r($plan_list);
	}



    /**

     * Add plan

     * @param $req,$res,$args

     */

    public function add_plan($req, $res, $args) {

        if ($_POST) {

            $this->Validation->rules('Name', 'name', 'required');

            $this->Validation->rules('Amount', 'amount', 'required|number');

            $this->Validation->rules('Duration', 'duration', 'required|number');

            $this->Validation->rules('Description', 'description', '');

            $this->Validation->rules('Status', 'status', 'required');



            //if ($this->Validation->error) 
            {

                $param = $_POST;

                
                if ($_POST['id']) {


                    $param['id'] = $_POST['id'];

                    $new_value = $param['order_number'];
                    $old_value = $param['old_number'];

                    $new_id = $this->Settings_model->get_all_where($new_value);

                    $old_id = $this->Settings_model->get_all_where($old_value);


                    $param['new_id']    = $new_id['asp_id'];
                    $param['old_id']    = $old_id['asp_id'];
                    $param['new_value'] = $new_value;
                    $param['old_value'] = $old_value;

                    
                    if($new_value && $old_value){

                        $this->Settings_model->update_plan_orderby($param);
                        
                        
                    }


                    $this->Settings_model->update_plan($param); /* Update plan */
                    
                    
                    $id = $args['id'];

                    $_SESSION['success'] = 'Plan updated successfully';

                } else {

                    $id = $this->Settings_model->insert_plan($param); /* Insert plan */

                    $_SESSION['success'] = 'Plan added successfully';

                }



                if (isset($_FILES['image']) && $_FILES['image']) {

                    $img_valid_ext = array('jpg', 'jpeg', 'png', 'gif', 'bmp');



                    $img_array = explode('.', $_FILES['image']['name']);

                    $img_ext = strtolower(end($img_array));



                    if (in_array($img_ext, $img_valid_ext)) {



                        $image_name = rand_value($id, $img_ext);



                        if (file_exists($_FILES['image']['tmp_name'])) {

                            move_uploaded_file($_FILES['image']['tmp_name'], SUBSCRIPTION_PLAN_IMG . $image_name);



                            unset($param);

                            $param['image_name'] = $image_name;

                            $param['id'] = $id;



                            $this->Settings_model->update_image($param); /* Update image in plan */

                        }

                    }

                }

                redirect('admin/plan');

            }

        }



        $args['header']['title'] = "Add plan";



        $args['validation'] = $this->Validation;



        if (isset($args['id']) && $args['id']) {

            $args['header']['title'] = "Edit plan";

            $param['id'] = $args['id'];

            $args['plan'] = $this->Settings_model->get_plan_by_id($param); /* Get plan */


            $args['all_plan'] = $this->Settings_model->get_plans();

        }

        $this->layout($res, 'Admin/Settings/add_plan.php', $args);

    }



    /**

     * Change plan status ajax call

     * @param $req,$res,$args

     */

    public function change_plan_status($req, $res, $args) {

        $param['id'] = $_POST['id'];

        $param['status'] = ($_POST['status'] == 'true' ? 'false' : 'true');

        $this->load_model('Admin/Settings_model.php'); /* Load settings model */



        $this->Settings_model->change_plan_status($param); /* Change plan status */



        echo $param['status'];

    }



    /**

     * Delete plan

     * @param $req,$res,$args

     */

    public function delete_plan($req, $res, $args) {

        $param['id'] = $args['id'];

        



        $this->Settings_model->delete_plan($param); /* Delete plan status */



        redirect('admin/plan');

    }





    /**

     * Term Condition

     * @param $req,$res,$args

     */

    public function term_condition($req, $res, $args) {
       


        if (isset($_POST) && isset($_POST['term_condition']) ) {

           
        
            $param['value']  = $_POST['term_condition'];
            $param['lang']   = isset($_POST['lang'])?$_POST['lang']:'eng';


            if(isset($param['lang']) && $param['lang'] == 'spn'){
                file_put_contents('app/Views/Front/term_condition/term_condition_spn.html', $param['value']);
            }else{
                file_put_contents('app/Views/Front/term_condition/term_condition.html', $param['value']);
            }
            
        }

        $args['header']['title'] = "Term Condition";

        $args['validation'] = $this->Validation;
        $args['file_eng'] = (file_get_contents('app/Views/Front/term_condition/term_condition.html'));
        $args['file_spn'] = (file_get_contents('app/Views/Front/term_condition/term_condition_spn.html'));

        $this->layout($res, 'Admin/Settings/term_condition.php', $args, true);

    }


    public function notification($req, $res, $args){

        if($_POST){

            $id          = isset($_POST['id'])?$_POST['id']:'';
            $title       = isset($_POST['title'])?$_POST['title']:'';
            $message     = isset($_POST['message'])?$_POST['message']:'';
            $title_spn       = isset($_POST['title_spn'])?$_POST['title_spn']:'';
            $message_spn     = isset($_POST['message_spn'])?$_POST['message_spn']:'';

            $id = $this->Settings_model->update_notification($id, $title, $message, $title_spn, $message_spn);

            if(isset($id) && $id){
                echo "<script>alert('update successfully');</script>";
            }
        }


        $args['notification'] = $this->Settings_model->notification(); /* Change plan status */

        $this->layout($res, 'Admin/Settings/notification.php', $args, true);

    }



    public function skintone($req, $res, $args){

        $data['skintone_list'] = $this->Settings_model->get_skintone_list();

        $this->layout($res, 'Admin/Settings/skintone.php', $data);

    }

    

    public function upate_skintone_status_ajax($req, $res, $args){

        if ($_POST) {

            echo $this->Settings_model->upate_skintone_status_ajax($_POST['id']); 

        }

    }

    public function update_skintone($req, $res, $args){
       
        $id = isset($_POST['id'])?$_POST['id']:'';
        
        $args['skintone'] = $this->Settings_model->get_skintone($id); /* Change plan status */
        
        $this->view($res,'Admin/Models/update_skintone.php',$args);

    }


    public function edit_skinton($req, $res, $args){
        
        if(isset($_POST['type']) && $_POST['type']=='skintone'){
            
            $id   = isset($_POST['id'])?$_POST['id']:'';
            $name = isset($_POST['name'])?$_POST['name']:'';
            $type = isset($_POST['type'])?$_POST['type']:'';
            $this->Settings_model->edit_skinton($id, $name, $type);     
        
        }elseif(isset($_POST['type']) && $_POST['type']=='lipscolor'){
         
            $id   = isset($_POST['id'])?$_POST['id']:'';
            $name = isset($_POST['name'])?$_POST['name']:'';
            $type = isset($_POST['type'])?$_POST['type']:'';
            $this->Settings_model->edit_skinton($id, $name, $type);     
        
        }elseif(isset($_POST['type']) && $_POST['type']=='eyeshadow'){
        
            $id   = isset($_POST['id'])?$_POST['id']:'';
            $name = isset($_POST['name'])?$_POST['name']:'';
            $type = isset($_POST['type'])?$_POST['type']:'';
            $this->Settings_model->edit_skinton($id, $name, $type);     
        }
    }


    public function lipscolor($req, $res, $args){
       
       $data['result'] = $this->Settings_model->get_lipscolor();
       $this->layout($res, 'Admin/Settings/lipscolor.php', $data);

    }

    public function update_lipscolor($req, $res, $args){

        $id = isset($_POST['id'])?$_POST['id']:'';
        
        $args['result'] = $this->Settings_model->get_lipscolor($id); /* Change plan status */

        $this->view($res,'Admin/Models/update_lipscolor.php',$args);


    }

    public function upate_lipscolor_status_ajax($req, $res, $args){
        
        if ($_POST) {

            echo $this->Settings_model->upate_lipscolor_status_ajax($_POST['id']); 

        }

    }


    public function eyeshadow($req, $res, $args){

       $data['result'] = $this->Settings_model->get_eyeshadow();
       $this->layout($res, 'Admin/Settings/eyeshadow.php', $data);

    }

    public function update_eyeshadow($req, $res, $args){
       
        $id = isset($_POST['id'])?$_POST['id']:'';
        
        $args['result'] = $this->Settings_model->get_eyeshadow($id); /* Change plan status */

        $this->view($res,'Admin/Models/update_eyeshadow.php',$args);

    }

    public function upate_eyeshadow_status_ajax($req, $res, $args){
        
        if ($_POST) {

            echo $this->Settings_model->upate_eyeshadow_status_ajax($_POST['id']); 

        }

    }


    
    /*
    ||====================================================
    || Function user E-mail List
    ||====================================================
    */
    public function user_email_list($req, $res, $args){

       $args['header']['title'] = 'Artist';

        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['page']) && $args['page']){
            $page = ($args['page'] -1)*10;
            $page1 = $args['page'];
        }
        else{
            $page = 0;
            $args['page'] = 1;
        }

        $param['page'] = $page;
        $args['query'] = "";

        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }

        $args['email_count'] = $this->Settings_model->user_email_list_count($param);/*Getting artist count*/

        $args['email_list'] = $this->Settings_model->user_email_list($param);/*Getting all artist*/
        
        $this->layout($res, 'Admin/Email/index.php', $args);
    }

    

    public function email_cooment_model($req, $res, $args){
       
        $param = $_POST;
        
        if(isset($param) && $param){
            $args['result'] = $this->Settings_model->email_cooment_model($param['id']);    
        }
        $this->view($res,'Admin/Models/email_cooment_model.php',$args);    
    }


    public function update_email_cooment($req, $res, $args){

        $this->load_library('Api/Email', 'email');
       
        $param = $_POST;
        
        $args['result'] = $this->Settings_model->update_email_cooment($param);

        if(isset($param['js_id']) && $param['js_id']){

            $data = $this->Settings_model->email_cooment_model($param['js_id']);

            $data = isset($data)?current($data):'';

            if(isset($data) && $data){
                $user_data = $this->Settings_model->get_data_by_id($data['um_fk_u_id']);

                $data['subject']  = 'subject';
                $data['type']     = 'admin';
                $data['name']     = isset($user_data)?$user_data['u_name']:'';
                $data['phone']    = isset($user_data)?$user_data['u_phone']:'';
                $data['email']    = isset($user_data)?$user_data['u_email']:'';
                $data['status']   = isset($data)?$data['um_admin_status']:'';
                $data['question'] = isset($data)?$data['um_message']:'';
                $data['comment']  = isset($data)?$data['um_comment']:'';

                $msg = $this->_view('Front/Email/Users/email_communication.php', $data, true);
                
                $this->email->to($data['email']);
                $this->email->subject($data['subject']);
                $this->email->messageType('html');
                $this->email->message($msg);
                $this->email->send(); 

                
				
            }
			
            
        }
    }

    public function admin_email_model($req, $res, $args){
       
        $param = $_POST;

        if(isset($param) && $param){
            $args['result'] = $this->Settings_model->get_admin_email_data($param['id']);    
        }
        
        $this->view($res,'Admin/Models/admin_email_model.php',$args);    
    }

}

