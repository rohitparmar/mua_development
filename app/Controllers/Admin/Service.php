<?php
class Service extends Layout{

	protected $app;
	/**
    * Construct
    * @param $app
    */
	public function __construct($app){
		$this->app = $app;
        
		if(!isset($_SESSION['admin_info'])){
            redirect('admin/login');
        }
        $this->load_model('Admin/Service_model');/*Connection Service model*/
	}

	/**
    * Booking
    * @param $req, $res, $args
    */
	public function index($req, $res, $args)
	{  
		$args['header']['title'] = 'Service';
        

        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['page']) && $args['page']){
            $page = ($args['page'] -1)*10;
            $page1 = $args['page'];
        }
        else{
            $page = 0;
            $args['page'] = 1;
        }

        $param['page'] = $page;

        $args['query'] = "";
        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }

        $args['service_count'] = $this->Service_model->get_service_count($param);/*Getting service count*/
        $args['service_list'] = $this->Service_model->get_service_list($param);/*Getting all service*/
        //echo '<pre>';print_r($args['booking_list']);die;
        
        $this->layout($res,'Admin/Service/index.php',$args);
	}

}
