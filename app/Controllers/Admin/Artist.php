<?php

class Artist extends Layout{



	protected $app;

	/**

    * Construct

    * @param $app

    */

	public function __construct($app){

		$this->app = $app;
		if(!isset($_SESSION['admin_info'])){
            redirect('admin/login');
        }

	}



	/**

    * Artist

    * @param $req, $res, $args

    */

	public function index($req, $res, $args)

	{  

		$args['header']['title'] = 'Artist';

        $this->load_model('Admin/Artist_model');/*Connection Artist model*/

        if($_GET){
            $_POST = $_GET;
        }

        else{

            $_GET = $_POST;

        }



        $param = $_GET;

        if(isset($args['page']) && $args['page']){
            $page = ($args['page'] -1)*10;
            $page1 = $args['page'];
        }
        else{
            $page = 0;
            $args['page'] = 1;
        }

        $param['page'] = $page;

        $args['query'] = "";

        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }

        $args['artist_count'] = $this->Artist_model->get_artist_count($param);/*Getting artist count*/

        $args['artist_list'] = $this->Artist_model->get_artist_list($param);/*Getting all artist*/

        $args['commision'] = $this->Artist_model->get_commision();/*Get comman commision*/

        $this->layout($res,'Admin/Artist/index.php',$args);

	}


    public function show_mua_used_referrel_modal($req, $res, $args){
        $this->load_model('Admin/Artist_model');
        $arr = $this->Artist_model->get_mua_used_referrel_modal($args['id']);

        $booked_list = array();
        $not_booked_list = array();
        $total_earning = 0;
        
        foreach($arr as $rows){
            if($rows['booking']>0){
                $booked_list[]=$rows;
                $total_earning+=(double)$rows['r_rebate'];
            }else{
                $not_booked_list[]=$rows;
            }
        }
        
        $args['booked_list']     = count($booked_list);
        $args['not_booked_list'] = count($not_booked_list);
        $args['total_earning']   = $total_earning;
        
        $args['result'] = array_merge($booked_list, $not_booked_list);

        $this->view($res,'Admin/Models/show_mua_used_referrel_modal.php',$args);
    }


    public function show_used_celebrity_referral($req, $res, $args){
        $this->load_model('Admin/Artist_model');
        $total = 0;
        $args['result'] = $this->Artist_model->get_used_celebrity_referral($args['id']);
        
        foreach ($args['result'] as $key => $value) {
           $total = ($value['app_prices'])-(($value['app_prices'] * $value['app_commission'])/100);
           $args['result'][$key]['total'] = number_format(floor($total*100)/100,2, '.', '');
        }

        $this->view($res,'Admin/Models/show_celebrity_referrel_modal.php',$args);
    }

    public function get_paid_referrel_amount($req, $res, $args){
        $this->load_model('Admin/Artist_model');
        $args['result'] = $this->Artist_model->update_paid_referrel_status($args['id']);
    }



    /**

    * Get request

    * @param $req, $res, $args

    */

    public function artist_request($req, $res, $args)

    {

        $args['header']['title'] = 'Artist';

        $this->load_model('Admin/Artist_model');/*Connection Artist model*/



        if($_GET){

            $_POST = $_GET;

        }

        else{

            $_GET = $_POST;

        }



        $param = $_GET;



        if(isset($args['page']) && $args['page']){

            $page = ($args['page'] -1)*10;

            $page1 = $args['page'];

        }

        else{

            $page = 0;

            $args['page'] = 1;

        }



        $param['page'] = $page;



        $args['query'] = "";

        if($_GET){

            $args['query'] = '?'.http_build_query($_GET);

        }



        $args['artist_count'] = $this->Artist_model->get_artist_request_count($param);/*Getting artist count*/

        $args['artist_list'] = $this->Artist_model->get_artist_request_list($param);/*Getting all artist*/



        $this->layout($res,'Admin/Artist/artist_request.php',$args);

    }



    /**

    * Artist change request status ajax call

    * @param $req, $res, $args

    */

    public function change_artist_request_status($req, $res, $args)

    {

        $this->load_model('Admin/Artist_model');/*Connection Artist model*/



        $param = $args;



        $availability = $this->Artist_model->change_artist_request_status($param);/*Change artist status*/

        header('Location:'.$_SERVER['HTTP_REFERER']);

        exit;

    }

	

    /**

    * Get artist availability ajax call

    * @param $req, $res, $args

    */

    public function get_artist_availability($req, $res, $args)

    {

        $this->load_model('Admin/Artist_model');/*Connection Artist model*/



        $param = $_POST;



        $args['availability'] = $this->Artist_model->get_artist_availability($param);/*Getting artist availability by id*/



        echo $this->view($res,'Admin/Models/artist_availability.php',$args,false);

    }



    /**

    * Artist change status ajax call

    * @param $req, $res, $args

    */

    public function artist_change_status($req, $res, $args)

    {

        $this->load_model('Admin/Artist_model');/*Connection Artist model*/



        $param['status'] = ($_POST['status'] == 'true')?'false':'true';

        $param['id'] = $_POST['id'];



        $availability = $this->Artist_model->artist_change_status($param);/*Change artist status*/

        echo $param['status'];

    }



    /**

    * Get artist profile ajax call

    * @param $req, $res, $args

    */

    public function artist_profile($req, $res, $args)

    {

        $this->load_model('Admin/Artist_model');/*Connection Artist model*/



        $param = $_POST;



        $args['profile'] = $this->Artist_model->get_artist_profile($param);/*Get artist profile*/



        echo $this->view($res,'Admin/Models/artist_profile.php',$args,false);

    }



    /**

    * Change artist availability status ajax call

    * @param $req, $res, $args

    */

    public function change_artist_availability_status($req, $res, $args)

    {

        $this->load_model('Admin/Artist_model');/*Connection Artist model*/



        $param['id'] = $_POST['id'];

        $param['status'] = ($_POST['status'] == 'true')?'false':'true';



        $this->Artist_model->change_artist_availability_status($param);/*Change artist availability status*/



        echo $param['status'];

    }



    /**

    * Artist category status ajax call

    * @param $req, $res, $args

    */

    public function artist_category($req, $res, $args)

    {

        $this->load_model('Admin/Artist_model');/*Connection Artist model*/



        $param['id'] = $_POST['id'];



        $args['category'] = $this->Artist_model->artist_category($param);/*Change artist availability status*/



        $this->view($res,'Admin/Models/artist_category.php',$args);

    }



    /**

    * Artist protfolio ajax call

    * @param $req, $res, $args

    */

    public function artist_portfolio($req, $res, $args)

    {

        $this->load_model('Admin/Artist_model');/*Connection Artist model*/



        if($_GET){

            $_POST = $_GET;

        }

        else{

            $_GET = $_POST;

        }



        $args['query'] = "";

        if($_GET){

            $args['query'] = '?'.http_build_query($_GET);

        }



        $param['id'] = $_POST['id'];



        if(isset($args['page']) && $args['page']){

            $page = ($args['page'] -1)*10;

            $page1 = $args['page'];

        }

        else{

            $page = 0;

            $args['page'] = 1;

        }



        $param['page'] = $page;



        $args['count'] = $this->Artist_model->get_artist_portfolio_count($param);/*Get artist portfolio*/



        $args['protfolio'] = $this->Artist_model->get_artist_portfolio($param);/*Get artist portfolio*/

        unset($param);

        //$args['get_artist_portfolio_img'] = $this->Artist_model->get_artist_portfolio($param);/*Get artist portfolio*/

        $this->view($res,'Admin/Models/artist_portfolio.php',$args);

    }



    /**

    * Artist client ajax call

    * @param $req, $res, $args

    */

    public function artist_client($req, $res, $args)

    {

        $this->view($res,'Admin/Models/artist_client.php',$args);

    }



    /**

    * Artist commision ajax call

    * @param $req, $res, $args

    */

    public function artist_commision($req, $res, $args){

        $this->load_model('Admin/Artist_model');/*Load artist model*/
        $param = $_POST;
        $args['id'] = $_POST['id'];
        $commision = $this->Artist_model->get_artist_commision($param);/*Get artist commision*/

        if(isset($commision['ap_pay_commission']) && $commision['ap_pay_commission']){

            $args['commision'] = $commision['ap_pay_commission'];

        }

        else{

            $commision = $this->Artist_model->get_commision();/*Get comman commision*/

            $args['commision'] = $commision['s_value'];

        }
        $this->view($res,'Admin/Models/artist_commision.php',$args);
    }


    /*
    ||==================================================
    || Function artist_certificate
    ||==================================================
    */
    public function artist_certificate($req, $res, $args){

        $this->load_model('Admin/Artist_model');/*Load artist model*/
        $param = $_POST;
        
        if(isset($param) && $param){
            $args['result'] = $this->Artist_model->get_artist_certificate($param);/*Get artist commision*/    
        }
        $this->view($res,'Admin/Models/artist_certificate.php',$args);    
    }



    /*
    ||==================================================
    || Function change_certificate_status
    ||==================================================
    */
    public function change_certificate_status($req, $res, $args){

        $this->load_model('Admin/Artist_model');/*Load artist model*/
        $param = $_POST;
        
        if(isset($param) && $param){
            $args['result'] = $this->Artist_model->change_certificate_status($param);/*Get artist commision*/    
        }
    }





    /**

    * Change artist commision ajax call

    * @param $req, $res, $args

    */

    public function change_artist_commision($req, $res, $args)

    {

        $this->load_model('Admin/Artist_model');/*Load artist model*/



        $param = $_POST;



        $result = $this->Artist_model->change_artist_commision($param);/*Change artist commision*/



        echo $result;

    }

}

