<?php
class Payment_info extends Layout{

	protected $app;
	/**
    * Construct
    * @param $app
    */
	public function __construct($app){
		$this->app = $app;
        
		if(!isset($_SESSION['admin_info'])){
            redirect('admin/login');
        }

        $this->load_model('Admin/Payment_info_model');/*Connection Payment info model*/
	}

	/**
    * Payment info
    * @param $req, $res, $args
    */
	public function index($req, $res, $args)
	{
		$args['header']['title'] = 'Payment info';

        if($_GET){
            $_POST = $_GET;
        }
        else{
            $_GET = $_POST;
        }

        $param = $_GET;

        if(isset($args['page']) && $args['page']){
            $page = ($args['page'] -1)*10;
            $page1 = $args['page'];
        }
        else{
            $page = 0;
            $args['page'] = 1;
        }

        $param['page'] = $page;

        $args['query'] = "";
        if($_GET){
            $args['query'] = '?'.http_build_query($_GET);
        }

        $args['count'] = $this->Payment_info_model->get_payment_info_count($param);/*Getting artist count*/
        $args['list'] = $this->Payment_info_model->get_payment_info_list($param);/*Getting all artist*/

        $this->layout($res,'Admin/Payment_info/index.php',$args);
	}


    /**
    * Update payment info
    * @param $req, $res, $args
    */
    public function update_payment_info($req, $res, $args)
    {
        if($_POST){
            $param = $_POST;

            $this->Payment_info_model->update_payment_info($param);/* Update payment info */

            $web_url = $_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:base_url("admin/payment_info");

            header('Location:'.$web_url);
            exit;
        }
        else{
            $param['id'] = $args['id'];
            $args['payment_info'] = $this->Payment_info_model->get_payment_info($param);/* Get payment info */

            $this->view($res,'Admin/Models/payment_info_update.php',$args);
        }
    }

    /**
    * Pay to user
    * @param $req, $res, $args
    */
    public function pay_to_user($req, $res, $args)
    {
        if($_POST){
            $param = $_POST;

            $this->Payment_info_model->pay_to_user($param);/* Pay to user */

            $web_url = $_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:base_url("admin/payment_info");

            header('Location:'.$web_url);
            exit;
        }
        else{
            $param['id'] = $args['id'];
            $args['payment_info'] = $this->Payment_info_model->get_payment_info($param);/* Get payment info */
            $this->view($res,'Admin/Models/pay_to_user.php',$args);
        }
    }
}