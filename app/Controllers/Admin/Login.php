<?php

class Login extends Layout{

	protected $app;

    /**
    * Counstruct
    * @param $app
    */
	public function __construct($app){

        $this->load_library("Admin/Validation");/*Form validation*/

		$this->app = $app;

	}

    /**
    * Index for admin login
    * @param $req,$res,$args
    */
	public function index($req, $res, $args)
	{
        $args['header']['title'] = 'Login';

    	if($_POST){
    		$post = $_POST;

            $this->Validation->rules('Email','email','required|email');
            $this->Validation->rules('Password','pass','required');

            if(!$this->Validation->error){
                $this->load_model('Admin/Login_model.php');/*Load admin model*/
                $param = array(
                                'email' => $post['email'],
                                'pass'  => md5($post['pass'])
                                );
                $admin_data = $this->Login_model->get_user($param);/*Get user*/

                if($admin_data){

                    $_SESSION['admin_info'] = array(
                                                    'id'    => $admin_data['ad_id'],
                                                    'name'  => $admin_data['ad_name'],
                                                    'email' => $admin_data['ad_email'],
                                                    'image' => $admin_data['ad_image']
                                                    );

                    $_SESSION['success'] = "Login successfully.";
                    redirect('admin');
                }
                else{
                    $_SESSION['error'] = "The email or password you entered is incorrect.";
                    return $this->view($res, 'Admin/Login/login.php', $args);
                }
            }
            else{
                $args['form_error'] = $this->Validation->error;
                $args['header']['title'] = 'Admin login';
                return $this->view($res, 'Admin/Login/login.php', $args);
            }
    	}
    	else{
            $args['header']['title'] = 'Admin login';
    		return $this->view($res, 'Admin/Login/login.php', $args);
    	}
	}

    /**
    * Logout
    * @param $req,$res,$args
    */
    public function logout($req, $res, $args)
    {
        unset($_SESSION['admin_info']);
        $_SESSION['success'] = 'Loout successfully.';
        redirect('admin/login');
    }

    /**
    * Forgot password
    * @param $req,$res,$args
    */
    public function forgot_password($req, $res, $args)
    {
        if($_POST){
            $this->Validation->rules('Email','email','required|email');/*Validate email*/
            if(!$this->Validation->error){
                $param = $_POST;
                $this->load_model('Admin/Login_model');/*Load login model*/
                $result = $this->Login_model->check_admin($param);/*Check email exiets*/
                if($result){
                    $new_pass = rand(100000,999999);

                    $param['new_pass'] = $new_pass;

                    $this->Login_model->update_password($param);

                    $mail_data = array(
                                  'to'          => $result['ad_email'],
                                  'from'        => $result['ad_email'],
                                  'subject'     => 'Forgot password',
                                  'message'     => 'Your new password is '.$new_pass,
                                  'from_name'   => SITE_NAME,
                                  'file'        => ''
                                );
                    $this->send_mail_api($mail_data);
                }
                else{
                    $args['error'] = "Invalid email";
                }
            }
        }
        $args['header']['title'] = 'Forgot password';
        $args['validation'] = $this->Validation;
        $this->view($res,'Admin/Login/forgot_password.php',$args);
    }

    /**
    * Send mail api
    * @param null
    * @return $result
    */
    private function send_mail_api($data){
        $url = 'http://synergytop.com/api/email';
        
        $params = http_build_query($data, NULL, '&');
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url); //Remote Location URL
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser   
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
            
        //We add these 2 lines to create POST request
        curl_setopt($ch, CURLOPT_POST, count($data)); //number of parameters sent
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params); //parameters data
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }
}
