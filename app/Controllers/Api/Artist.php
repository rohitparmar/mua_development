<?php

/**
 * Class to handle all apis
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Artist extends Layout {
  protected $app;

  public function __construct($app) {
    $this->app = $app;
    $this->load_library('Api/Validation');
    $this->load_model('Api/Artist_model');
    $this->load_model('Api/Booking_model');
    $this->load_library('Api/Push_notification');
    $this->load_model('Api/Category_model');
    $this->load_model('Api/Payment_model', 'payment_model');
  }
  
  private function get_time_from_time_zone($time_zone){
    $sign = substr($time_zone, 0, 1);
    $hrs = substr($time_zone, 1, 2);
    $mits = substr($time_zone, 3, 2);

    $seconds = ($hrs * 3600 + $mits * 60)-(5*3600 + 30*60);
    $sign = $sign=='-'?'+':$sign;

    return $seconds;

  }

  private function timezone_to_seconds($timezone){
    $sign = '';
    $minutes = 0;
    $hours = 0;

    if($timezone && strlen($timezone)>3 && strlen($timezone) <6){
      $sign    = substr($timezone,0,1);
      $timezone = str_replace($sign, '', $timezone);
      $minutes  = (int)substr($timezone,-2);
      $timezone = str_replace($minutes, '', $timezone);
      $hours    = (int)$timezone;
    }

    $seconds    = $hours*3600 + $minutes*60;
    $sign       = ($sign=='-')?'-':'+';
    return (object)array(
                'seconds' => $seconds,
                'sign'    => $sign
              );
  }

  public function get_artist_availabile_time_slot(){
    
    $mua_id               = isset($_POST['mua_id'])?decode_base64($_POST['mua_id']):"";
    $booking_time         = isset($_POST['booking_date'])?$_POST['booking_date']:"";  //yyyy-mm-dd
    $client_time_zone     = isset($_POST['client_time_zone'])?$_POST['client_time_zone']:"";
    $service_duration     = isset($_POST['service_duration'])?((int)$_POST['service_duration']==0?15:$_POST['service_duration']):15;
    $day_name             = isset($_POST['week_day'])?$_POST['week_day']:"";
    $category_id          = isset($_POST['category_id'])?decode_base64($_POST['category_id']):"";

    $lang      = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    if(isset($lang) && $lang == 'spn'){

      switch ($day_name) {
          case "lunes":
            $day_name = 'Monday';
            break;
          case "martes":
            $day_name = 'Tuesday';
            break;
          case "miércoles":
            $day_name = 'Wednesday';
            break;
          case "jueves":
            $day_name = 'Thursday';
            break;
          case "viernes":
            $day_name = 'Friday';
            break;
          case "sábado":
            $day_name = 'Saturday';
            break;
          case "domingo":
            $day_name = 'Sunday';
            break;
      }
    }


    /*print_r($mua_id);
    print_r($booking_time);
    print_r($client_time_zone);
    print_r($service_duration);
    print_r($day_name);
    print_r($category_id);

    die;*/


    $increment_time   = 15*60;  //15 minutes 

    $service_duration = (int)$service_duration * 60;

   
    $time_info = array();
    $mua_time_info = (object) $this->artist_model->get_mua_time_info($mua_id, $day_name);
    
  
    if($mua_time_info && isset($mua_time_info->aa_time_from)){
      $start_time = $mua_time_info->aa_time_from;
      $end_time = $mua_time_info->aa_time_to;

      $client_zone_time = $this->timezone_to_seconds($client_time_zone);
      $artist_zone_time = $this->timezone_to_seconds($mua_time_info->mua_time_zone);
      
      //get artist booked time list for current date
      $booked_time_list = $this->artist_model->get_artist_booked_time_list($mua_id, $booking_time, $category_id);

      //echo '<pre>';
      //print_r($booked_time_list);
      //die;
      //get zone difference
      $zone_difference = ($client_zone_time->sign.$client_zone_time->seconds)-($artist_zone_time->sign.$artist_zone_time->seconds);
      //now get all available time
      while($start_time<$end_time){
        $flag = true;
        foreach($booked_time_list as $row){
          /*
          if($flag && (($row['cr_start_time'] >= $start_time) && ($row['cr_start_time'] <= ($start_time + $increment_time*1000)))){
            $flag = false;
            
          }
          */
          if($flag && ($row['cr_start_time'] >= $start_time) && ($row['cr_start_time'] <= $start_time + $increment_time)){
            $flag = false;
          }

        }
        //$time_info[] = ($start_time+$zone_difference);
        //
        if($flag){
          $time_info[] = ($start_time+$zone_difference)*1000;
          $start_time += $service_duration;
        }else{
          $start_time += $increment_time;
        }
      }
    }

    $time_info = $time_info;
    //print_r($mua_time_info);
    //print_r($time_info = $time_info);
    //die;

    if($time_info){
            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success',
                              'data'          => $time_info
                              );
        } 
        else{
              // password failed to update
               $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success',
                              'data'          => $time_info
                              );
        } 
    echo json_encode($response);
  }


  /**
  *
  *@param: array
  *@response: json
  **/

  public function get_services($request, $response, $args){
    if(!$_POST){

      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }
     
    $response = array();

    // reading post params
    $user_id   = isset($_POST['user_id'])?$_POST['user_id']:"";
	  $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    $this->validation->rules('user_id',$user_id,'required');
	  
        
    if($this->validation->error){
       $response["error"] = true;
       $response["message"] = array_pop($this->validation->error);
    } else{
      $user_id      = decode_base64($user_id);
	    //$client_id      = decode_base64($client_id);
      $result = $this->artist_model->get_services($user_id, '', $lang);

      $skinResult = $this->Category_model->get_skinton($lang);

      $objSkin = array();
      foreach($skinResult as $k => $rows){
        $arr = array(
                      'id'    => encode_base64($rows['ms_id']),
                      'name'  => $rows['ms_name'],
                      'image' => $rows['ms_image']?(file_exists(APP_DIR.'/'.CATEGORY_IMG.$rows['ms_image'])?(BASE_URL.CATEGORY_IMG.$rows['ms_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE
                    );
        $objSkin[] = (object)$arr;
      }
      
      foreach($result as $k=>$v){

				$result[$k]['cat_image'] = $v['cm_image'] ? (file_exists(APP_DIR . '/' . CATEGORY_IMG . $v['cm_image']) ? (BASE_URL . CATEGORY_IMG . $v['cm_image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE;
			
	  		
          $result[$k]['cm_image'] = $objSkin;
      }

      /*foreach($result as $k => $row){
        $result[$k]['ac_id'] = encode_base64($row['ac_id']);
        //$imgArr              = @unserialize($row['cm_image']);

        $imgArr = @unserialize($row['cm_image']);
      
        $image_list = array();
        if ($imgArr !== false) {
          foreach($imgArr as $rows){
            $img = (object)array(
                              'id'    => encode_base64($rows['id']),
                              'name'  => $rows['name'],
                              'image' => $rows['image']?(file_exists(APP_DIR.'/'.CATEGORY_IMG.$rows['image'])?(BASE_URL.CATEGORY_IMG.$rows['image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE
                          );
             array_push($image_list, $img);
          }
        }
        $result[$k]['cm_image'] = $image_list;
      }*/


      if($result){
            // password updated successfully
            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success',
                              'data'          => $result
                              );
        } 
        else{
              // password failed to update
               $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success',
                              'data'          => $result
                              );
        } 
    }

    echo json_encode($response);

  }
   
  
  /**
  * URL - artist_profile 
  * FUNCTION - artist_profile
  * METHOD - POST
  */
  public function artist_profile($request, $response, $args){

    if(!$_POST){

      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 
     
    $response = array();

    // reading post params
    $user_id   = isset($_POST['user_id'])?$_POST['user_id']:"";
    $name      = isset($_POST['name'])?$_POST['name']:"";
    $phone     = isset($_POST['phone'])?$_POST['phone']:"";
    $website   = isset($_POST['website'])?$_POST['website']:"";
    $about     = isset($_POST['about'])?$_POST['about']:"";
    $email     = isset($_POST['email'])?$_POST['email']:"";
    $city      = isset($_POST['city'])?$_POST['city']:"";
    $address   = isset($_POST['address'])?$_POST['address']:"";
    $rate      = isset($_POST['rate'])?$_POST['rate']:"";
    $language  = isset($_POST['language'])?$_POST['language']:"";
    $zip       = isset($_POST['zip'])?$_POST['zip']:"";
    $certificate = isset($_POST['certificate'])?$_POST['certificate']:"";
    $artist_image = isset($_POST['artist_image'])?$_POST['artist_image']:"";


    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('name',$name,'required');
    

    if($this->validation->error){          
          $response["error"] = true;
          $response["message"] = array_pop($this->validation->error);        
    }
    else{

      $user_id      = decode_base64($user_id);  
      $certificate  = decode_base64($certificate);  
      $artist_image = decode_base64($artist_image);  

      $result = $this->artist_model->get_validate_user_id($user_id);

      if(!empty($result)){

        
        $params = (object)array( 
                              'user_id'     => $user_id, 
                              'name'        => $name,
                              'phone'       => $phone,
                              'website'     => $website, 
                              'about'       => $about, 
                              'email'       => $email, 
                              'city'        => $city,
                              'certificate' => $certificate,
                              'artist_image'=> $artist_image,
                              'address'     => $address,
                              'rate'        => $rate,
                              'language'    => $language, 
                              'zip'         => $zip
                              );

          $res = $this->artist_model->artist_profile($params);

          if($res == 1){
            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success'
                              );
          }
          else if($res == 0){

            $response = array(
                              'response_code'  => 400,
                              'status'         => 'error',
                              'message'        => 'bad request'
                              );
      
          }
          else if($res == 2){
            $response = array(
                              'response_code' =>400,
                              'status'        =>'error',
                              'message'       =>'Sorry, this user information already existed'
                              );
          }
      
      }else{
        $response = array(
                          'response_code'  => 400,
                          'status'         => 'error',
                          'message'        => 'Login credentials incorrect'
                          );
      }
    } 
    // echo json response
    echo json_encode($response);
}


/**
  * FUNCTION artist_profile_get
  * METHOD - GET
*/
  public function artist_profile_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
 * FUNCTION - edit_artist_profile
 * METHOD - POST
*/
  public function edit_artist_profile($request, $response, $args){

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    
    $response = array();

    $user_id   = isset($_POST['user_id'])?$_POST['user_id']:"";
    $name      = isset($_POST['name'])?$_POST['name']:"";
    $phone     = isset($_POST['phone'])?$_POST['phone']:"";
    $website   = isset($_POST['website'])?$_POST['website']:"";
    $about     = isset($_POST['about'])?$_POST['about']:"";
    $email     = isset($_POST['email'])?$_POST['email']:"";
    $city      = isset($_POST['city'])?$_POST['city']:"";
    $address   = isset($_POST['address'])?$_POST['address']:"";
    $rate      = isset($_POST['rate'])?$_POST['rate']:"";
    $language  = isset($_POST['language'])?$_POST['language']:"";
    $zip       = isset($_POST['zip'])?$_POST['zip']:"";
    $certificate = isset($_POST['certificate'])?$_POST['certificate']:"";
    //$artist_image = isset($_POST['artist_image'])?$_POST['artist_image']:"";
    $lat        = isset($_POST['lat'])?$_POST['lat']:"";
    $long       = isset($_POST['long'])?$_POST['long']:"";
    $profile_status = isset($_POST['profile_status'])?$_POST['profile_status']:'false';

    $type_of_arts  = isset($_POST['type_of_arts'])?$_POST['type_of_arts']:"";
    


    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('name',$name,'required');
        
    if($this->validation->error){
       $response["error"] = true;
       $response["message"] = array_pop($this->validation->error);
    }
    else{

        $user_id = decode_base64($user_id);
        $certificate  = decode_base64($certificate);  
        //$artist_image = decode_base64($artist_image);   

        $result = $this->artist_model->get_validate_user_id($user_id);

        if($result){

            //get the detail by email
            $user = $this->artist_model->get_artist_profile($user_id);

            //print_r($user); die;

            if($user['ap_id'] != 0 || $user['ap_id'] != ""){

                 $params = (object)array( 
                              'user_id'     => $user_id, 
                              'name'        => $name,
                              'phone'       => $phone,
                              'website'     => $website, 
                              'about'       => $about, 
                              'email'       => $email, 
                              'city'        => $city,
                              'certificate' => $certificate,
                              //'artist_image'=> $artist_image,
                              'address'     => $address,
                              'rate'        => $rate,
                              'language'    => $language, 
                              'zip'         => $zip,
                              'lat'        => $lat,
                              'long'       => $long,
                              'profile_status' => $profile_status                           
                              );

                //print_r($params); die;

                $result = $this->artist_model->update_artist_profile($params);
                if($type_of_arts!=""){
                      $this->artist_model->update_type_of_arts($type_of_arts, $user_id);
                    }



                  
                if($result){


                    // password updated successfully
                    $response = array(
                                      'response_code' => 200,
                                      'status'        => 'ok',
                                      'message'       => 'success'
                                      );
                } 
                else{
                      // password failed to update
                      $response = array(
                                      'response_code' => 200,
                                      'status'        => 'ok',
                                      'message'       => 'success'
                                      );
                } 
            }
            else{

                  // user credentials are wrong
                  $response = array(
                                    'response_code' => 400,
                                    'status'        => 'error',
                                    'message'       => 'Login credentials incorrect'
                                    );
            } 

        

        }else{
          $response = array(
                            'response_code'  => 400,
                            'status'         => 'error',
                            'message'        => 'Login credentials incorrect'
                            );
        }
    }
    echo json_encode($response);
  }



/**
  * FUNCTION edit_artist_profile_get
  * METHOD - GET
*/
  public function edit_artist_profile_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
  * URL - find/artist/profile
  * FUNCTION - get_artist_profile
  * METHOD - POST
*/
  public function get_artist_profile($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 
    
    $response = array();    
    
    // reading post params
    $mua_id = isset($_POST['mua_id'])?$_POST['mua_id']:"";
    $lang   = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    $this->validation->rules('mua_id',$mua_id,'required');

    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);                
    }
    else{

      $mua_id = decode_base64($mua_id);


      $user = $this->artist_model->get_artist_detailes($mua_id);
      
      $data = array();

      
      

      $mua_data = array();
      
      foreach($user as $row){
          
          $img = $row['im_image_name'];
          $action = $row['im_action'];
          unset($row['im_action']);
          unset($row['im_image_name']);

          $image[$action] = ($action=='artist')?BASE_URL.ARTIST_PROFILE_IMG.$img:($action=='certificate'?BASE_URL.ARTIST_CERTIFICATE_IMG.$img:'');

          $row['total_earn'] = money_format('%.2n', ($row['total_earn']=='null' && $row['total_earn']=='')?0:$row['total_earn']);

          if(isset($lang) && $lang == 'spn'){
             $row['u_type_of_arts'] = $row['ta_name_spn'];
          }else{
             $row['u_type_of_arts'] = $row['ta_name'];  
          }
          unset($row['ta_name']);
          unset($row['ta_name_spn']);
          
          $mua_data = $row;
      }

      $ap_image = array(
                  'image_id'  => isset($mua_data['im_id'])?encode_base64($mua_data['im_id']):'',
                  'image_url' => isset($image['artist'])?$image['artist']:''
              );

      $mua_data['ap_image'] = isset($image['artist'])?$ap_image:(object)array();

      $mua_data['ap_image2'] = isset($image['artist'])?$image['artist']:CATEGORY_DEFAULT_IMAGE;

      $ap_certificate = array(
                  'image_id'  => isset($mua_data['im_id'])?encode_base64($mua_data['im_id']):'',
                  'image_url' => isset($image['certificate'])?$image['certificate']:''
              );

      $mua_data['ap_certificate']  = isset($image['certificate'])?$ap_certificate:(object)array();
      $mua_data['ap_certificate2'] = isset($image['certificate'])?$image['certificate']:CERTIFICATE_DEFAULT_IMAGE;


      
      $user = $mua_data;
    


      if(!empty($user)){

        $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success',
                          'data'          => $user
                          );
      }
      else{
          $response = array(
                            'response_code'  => 400,
                            'status'         => 'error',
                            'message'        => 'bad request'
                            );
      }
    }
    //echo json response
    echo json_encode($response);
  }


/**
  * FUNCTION get_artist_profile_get
  * METHOD - GET
*/
  public function get_artist_profile_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
  * FUNCTION - add_portfolio
  * METHOD - POST
*/
  public function add_portfolio($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 
     
    $response = array();

    // reading post params
    $user_id          = isset($_POST['user_id'])?$_POST['user_id']:"";
    $category_id      = isset($_POST['category_id'])?$_POST['category_id']:"";
    $p_name           = isset($_POST['p_name'])?$_POST['p_name']:"";
    $description      = isset($_POST['description'])?$_POST['description']:"";
    $portfolio_status = isset($_POST['portfolio_status'])?$_POST['portfolio_status']:'false';
     
    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('category_id',$category_id,'required');
    $this->validation->rules('p_name',$p_name,'required');
    $this->validation->rules('description',$description,'required');
            
    if($this->validation->error){          
          $response["error"] = true;
          $response["message"] = array_pop($this->validation->error);
    }
    else{

      $user_id      = decode_base64($user_id);
      //$category_id      = decode_base64($category_id);
      
      $result = $this->artist_model->get_validate_user_id($user_id);

      if(!empty($result)){

        $params = (object)array( 
                                'user_id'     => $user_id,
                                'category_id' => $category_id,
                                'p_name'      => $p_name,
                                'description' => $description,
                                'portfolio_status' => $portfolio_status
                                );

        $res = $this->artist_model->create_portfolio($params);


        if($res['res'] == 1){
          $data = array(
                        'portfolio_id' => encode_base64($res['portfolio_id']),
                        'category_id'  => encode_base64($category_id),
                        'p_name'       => $p_name,
                        'description'  => $description
                       
                        );
          
          $response = array(
                            'response_code' => 200,
                            'status'        => 'ok',
                            'message'       => 'success',
                            'data'          => $data
                            );
                    
        }
        else if($res['res'] == 0){

          $response = array(
                            'response_code' => 400,
                            'status'        =>'error',
                            'message'       =>'bad request'
                            );
        }
        else if($res['res'] == 2){
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'Sorry, current portfolio already exists'
                            );
        }
      }
      else{

        $response = array(
                            'response_code'  => 400,
                            'status'         => 'error',
                            'message'        => 'Login credentials incorrect'
                            );
      }  
    } 
    // echo json response
    echo json_encode($response);
  }



/**
  * FUNCTION add_portfolio_get
  * METHOD - GET
  */
  public function add_portfolio_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }





/**
   *function - edit_portfolio
   * method - POST
*/
  public function edit_portfolio($request, $response, $args) {

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id      = isset($_POST['user_id'])?$_POST['user_id']:"";
    //$category_id  = isset($_POST['category_id'])?$_POST['category_id']:"";
    $portfolio_id = isset($_POST['portfolio_id'])?$_POST['portfolio_id']:"";
    $p_name       = isset($_POST['p_name'])?$_POST['p_name']:"";
    $description  = isset($_POST['description'])?$_POST['description']:"";
    $portfolio_status = isset($_POST['portfolio_status'])?$_POST['portfolio_status']:'false';
    
            
    /*Validation*/
      
    $this->validation->rules('user_id',$user_id,'required');
    //$this->validation->rules('category_id',$category_id,'required');
    $this->validation->rules('portfolio_id',$portfolio_id,'required');
    $this->validation->rules('p_name',$p_name,'required');
    $this->validation->rules('description',$description,'required');
        


    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      
      $user_id      = decode_base64($user_id);
      //$category_id = decode_base64($category_id);
      $portfolio_id = decode_base64($portfolio_id);

      // get the detail by email
      $user = $this->artist_model->get_portfolio_ID($portfolio_id);

      if($user['ap_id'] != 0 || $user['ap_id'] != ''){

        $params = (object)array( 
                                'user_id'       => $user_id, 
                                //'category_id'   => $category_id,
                                'portfolio_id'  => $portfolio_id,
                                'p_name'        => $p_name,
                                'description'   => $description,
                                'portfolio_status' => $portfolio_status
                               );
        //print_r($params); die;
                                  
          $result = $this->artist_model->update_portfolio($params);
                
          if($result){
            // updated successfully
            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success'
                               );
          }
          else{
            // failed to update
            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success'
                               );
          } 
      }
      else{
        // user credentials are wrong
        $response = array(
                          'response_code' => 404,
                          'status'        => 'error',
                          'message'       => 'Login credentials incorrect'
                          );
      } 
    }
    //echo joson response
    echo json_encode($response);
  }


/**
  * FUNCTION - edit_portfolio_get
  * METHOD - GET
  */
  public function edit_portfolio_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


/**
  * FUNCTION - get_portfolio
  * METHOD - POST
  */
  public function get_portfolio($request, $response, $args) {

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 
     
    $response = array();

    // reading post params
    $user_id     = isset($_POST['user_id'])?$_POST['user_id']:"";
    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
    $this->validation->rules('user_id',$user_id,'required');

        
    if($this->validation->error){          
          $response["error"] = true;
          $response["message"] = array_pop($this->validation->error);
    }else{

      $user_id     = decode_base64($user_id);
	  
	       
      $user = $this->artist_model->get_portfolio_userlist($user_id, $lang);


     
      $ac_data = array();

      foreach ($user as $row) {

        if($row['image_type']){
          $ac_data[$row['portfolio_id']]['img'][] = array(
                                                        'image_id'      => encode_base64((int)$row['im_id']),
                                                        'image_type'   => $row['image_type'],
                                                        'image_url'     =>$row['im_image_name']?(file_exists(APP_DIR.'/'.ARTIST_PORTFOLIO_IMG.$row['im_image_name'])?(BASE_URL.ARTIST_PORTFOLIO_IMG.$row['im_image_name']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE
                                                          );
        }

        
        $ac_data[$row['portfolio_id']]['data'] = array(
                                            'portfolio_id' => encode_base64($row['portfolio_id']),
                                            'category_id' => encode_base64($row['category_id']),
                                            'portfolio_name' => $row['portfolio_name'],
                                            'portfolio_desc' => $row['portfolio_desc'],
                                            'category_description' => $row['category_description'],
                                            'category_name' => $row['category_name'],
                                            'ap_image_path' => ''
                                          );

        /*if(encode_base64($row['portfolio_id'])=="NDEyNjEzNDQ4MjA="){
          print_r($row);
          die;
        }*/

      }

      $data = array();

      foreach ($ac_data as $row) {

        if(!isset($row['img']) || count($row['img'])==0){
          $row['img'][] = array(
                                'image_id'      => encode_base64(0),
                                'image_type'   => 'before',
                                'image_url'     =>CATEGORY_DEFAULT_IMAGE
                                  );                

          $row['img'][] = array(
                                'image_id'      => encode_base64(0),
                                'image_type'   => 'after',
                                'image_url'     =>CATEGORY_DEFAULT_IMAGE
                                  );
              

        }else if(count($row['img'])==1){
          if($row['img'][0]['image_type']=='after'){
            $row['img'][] = array(
                                  'image_id'      => encode_base64(0),
                                  'image_type'   => 'before',
                                  'image_url'     =>CATEGORY_DEFAULT_IMAGE
                                    );
                  
          }else{
            $row['img'][] = array(
                                  'image_id'      => encode_base64(0),
                                  'image_type'   => 'after',
                                  'image_url'     =>CATEGORY_DEFAULT_IMAGE
                                    );
                  
            }
          
        }
        
        $row['data']['ap_image_path'] = $row['img'];
        $data[]= $row['data'];
      }
      $user = $data;
      

     
             
                  if ($user)
                      {    
                              // success get_portfolio
                              $response = array(
                                                'response_code' => 200,
                                                'status'        => 'ok',
                                                'message'       => 'success',
                                                'data'          => $user
                                                );
                      }else{
                              // error get_portfolio
                              $response = array(
                                                'response_code'  => 400,
                                                'status'         => 'error',
                                                'message'        => 'Please add portfolio first to tap at + icon on this screen',
                                                );
                      }
              } 
                  // echo json response
              echo json_encode($response);
  }


/**
  * FUNCTION get_portfolio
  * METHOD - GET
  */
  public function get_portfolio_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }




/*
 * URL - deleted_portfolio
 * FUNCTION - deleted_portfolio
 * METHOD - POST
*/
  public function deleted_portfolio($request, $response, $args) {
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }

    $response = array();

    $user_id      = isset($_POST['user_id'])?$_POST['user_id']:"";
    $portfolio_id = isset($_POST['portfolio_id'])?$_POST['portfolio_id']:"";

    $this->validation->rules('user_id', $user_id, 'required');
    $this->validation->rules('portfolio_id', $portfolio_id, 'required');

    if($this->validation->error){          
          $response["error"] = true;
          $response["message"] = array_pop($this->validation->error);
    }
    else{

      $user_id      = decode_base64($user_id);
      $portfolio_id = decode_base64($portfolio_id);

      $result = $this->artist_model->deleted_portfolio($user_id, $portfolio_id);
      //print_r($result); die;

      if($result == 1){
        $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success'
                          );
      } 
      else if($result == 0){
        $response = array(
                          'response_code'  => 400,
                          'status'         => 'error',
                          'message'        => 'bad request'
                          );
      }
     //echo response
     echo json_encode($response);
    }
  }  





/*
 * URL - deleted_portfolio
 * FUNCTION - deleted_portfolio_get
 * METHOD - GET
*/
  public function deleted_portfolio_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }  






/**
  * FUNCTION - artist_availability
  * METHOD - GET
  */

public function artist_availability($request, $response, $args){

  if(!$_POST){
    $response = array(
                      'response_code' => 400,
                      'status'        => 'error',
                      'message'       => 'some fields required'
                      );

    echo json_encode($response);
    die;
  } 

  $response = array();

  $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
  $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
  
  $this->validation->rules('user_id', $user_id, 'required');

  if($this->validation->error){
    $response["error"] = true;
    $response["message"] = array_pop($this->validation->error);
  }
  else{

    $user_id = decode_base64($user_id);
    $result = $this->artist_model->get_availability($user_id);

    foreach ($result as $key => $value) {
                

            $result[$key]['aa_active_status'] = (bool)$value['aa_active_status'];

            if(isset($lang) && $lang == 'spn'){

              switch ($value['aa_days']) {
                  case "Monday":
                      $result[$key]['aa_days'] = 'lunes';
                      break;
                  case "Tuesday":
                      $result[$key]['aa_days'] = 'martes';
                      break;
                  case "Wednesday":
                      $result[$key]['aa_days'] = 'miércoles';
                      break;
                  case "Thursday":
                    $result[$key]['aa_days'] = 'jueves';
                  break;
                  case "Friday":
                    $result[$key]['aa_days'] = 'viernes';
                  break;
                  case "Saturday":
                    $result[$key]['aa_days'] = 'sábado';
                  break;
                  case "Sunday":
                    $result[$key]['aa_days'] = 'domingo';
                  break;
              }
            }
    
    }

            if(!empty($result)){

                                $response = array(
                                                    'response_code' => 200,
                                                    'status'        => 'ok',
                                                    'message'       => 'success',
                                                    'data'          => $result
                                                  );
            }else{

                  $response = array(
                                      'response_code'  => 400,
                                      'status'         => 'error',
                                      'message'        => 'bad request'
                                    );

                 }


            }

            // echo json response  
            echo json_encode($response);

}

/**
  * function artist_availability
  * method - GET
  */
  public function artist_availability_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
  * function get_mua_client
  * URL - find/client
  * method - POST
  */
  public function get_mua_client($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
            echo json_encode($response);
            die;  
    }

    $response = array();
    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    $this->validation->rules('user_id', $user_id, 'required');

    if($this->validation->error){

      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);

    }
    else{

      $user_id = $this->decode_base64($user_id);


      $result = $this->artist_model->get_mua_client($user_id, $lang);
      
      foreach ($result as $key => $value) {
        $result[$key]['client_id'] = encode_base64($value['client_id']);
        $result[$key]['avg_rating'] = (int)$value['avg_rating'];
        $result[$key]['avg_reviews'] = (int)$value['avg_reviews'];
        $result[$key]['client_profile_image'] = $value['client_profile_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$value['client_profile_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$value['client_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
      }
      
      if(!empty($result)){
        $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success',
                          'data'          => $result
                          );
      }
      else{
        $response = array(
                          'response_code' => 400,
                          'status'        => 'error',
                          'message'       => 'bad request'
                          );
          }

    }
    //echo json response
    echo json_encode($response);
    
  }




/**
  * URL - find/client
  * function get_mua_client_get
  * method - GET
  */
  public function get_mua_client_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }




/**
  * URL - find/mua/timeline
  * function get_mua_time_line
  * method - POST
  */
  public function get_mua_timeline($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;  
    }

    $response = array();
    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";

    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    $this->validation->rules('user_id', $user_id, 'required');

    if($this->validation->error){

      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);

    }
    else{

      $user_id = $this->decode_base64($user_id);
      
      $result = $this->artist_model->get_mua_timeline($user_id, $lang);
      //print_r($result); die;

      //now encrypt requestId and userId
      foreach ($result as $key => $value) {
        $result[$key]['bookingId'] = encode_base64($value['bookingId']); 
        $result[$key]['clientId']  = encode_base64($value['clientId']); 
      }

      if(!empty($result)){
          $response = array(
                            'response_code' => 200,
                            'status'        => 'ok',
                            'message'       => 'success',
                            'data'          => $result
                            );
      }
      else{
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'No request found'
                            );
      }

    }
    //echo json response
    echo json_encode($response);
    
  }




/**
  * function get_mua_time_line
  * URL - find/mua/timeline
  * method - GET
  */
  public function get_mua_timeline_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
  * FUNCTION - mua/action
  * URL - mua_action
  * METHOD - POST
  */
  public function mua_action($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
       
    }

    $response = array();

    $user_id      = isset($_POST['user_id'])?$_POST['user_id']:false;
    $mua_id       = isset($_POST['mua_id'])?$_POST['mua_id']:false;
    $requsetId    = isset($_POST['requsetId'])?$_POST['requsetId']:false;
    $action       = isset($_POST['action'])?$_POST['action']:false;
    $devices_type = isset($_POST['devices_type'])?$_POST['devices_type']:"android";
    $lang         = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
    
    
    
    // $this->validation->rules('user_id', $user_id, 'required');
    // $this->validation->rules('mua_id', $mua_id, 'required');
    $this->validation->rules('requsetId', $requsetId, 'required');
    $this->validation->rules('action', $action, 'required');

    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{
      
      
      $user_id    = decode_base64($user_id);
      $requsetId  = decode_base64($requsetId);
      $mua_id     = decode_base64($mua_id);

     
      $last_time = $this->payment_model->get_last_payment_time($requsetId);
      
      /*
       $continue = TRUE;
        if (isset($last_time) && $last_time) {

            $interval = PAYMENT_INTERVAL;

            $start_time = strtotime($last_time['cpi_created']);

            $final = time();

            $diff = floor(($final - $start_time) / 60);

            $continue = $diff >= $interval ? TRUE : FALSE;
        }
        
        if (!$continue) { 
            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'time' => isset($diff) ? $diff : '',
                'message' => TIME_DURATION_ENG,
                'message_spn' => TIME_DURATION_SPN
            );

            echo json_encode($response);
            die;
        }
       */
      
     

      $params = (object)array(
                              'user_id'   => $user_id,
                              'mua_id'    => $mua_id,
                              'requsetId' => $requsetId,
                              'action'    => $action 
                              );

      /*if($action == 'cancelled'){
        $this->artist_model->insert_canceled_bookings($params);
        //echo 'ok';
      }*/

      $valid = $this->artist_model->chek_mua_client($params);

      

      if($valid){
        $result = $this->artist_model->mua_action($params);
        $request = $this->artist_model->get_client_request($params);

      

        $userData = $this->booking_model->get_mua_gcmID($request['cr_fk_u_client_id']);

        
        $from_id = $params->mua_id?$params->mua_id:$params->user_id;

        $this->Push_notification->send_notification_action($userData, $request['cr_status'], $requsetId, $from_id, $devices_type, $lang);
        $response = array(
                        'response_code' => 200,
                        'status'        => 'ok',
                        'message'       => 'success'
                        );

      }else{
        $response = array(
                        'response_code' => 200,
                        'status'        => 'ok',
                        'message'       => 'service already finished'
                        );
      }

      

      
      
    }
    //echo json response
    echo json_encode($response);
  }


/**
  * FUBCTION mua/action
  * URL - mua_action_get
  * METHOD - GET
  */
  public function mua_action_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }




/**
  * FUNCTION - mua/action
  * URL - mua_action
  * METHOD - POST
  */
  public function edit_artist_availability($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
    }

    $response = array();

    $user_id     = isset($_POST['user_id'])?$_POST['user_id']:"";
    $aa_id       = isset($_POST['aa_id'])?$_POST['aa_id']:"";
    $days        = isset($_POST['days'])?$_POST['days']:"";
    $time_from   = isset($_POST['time_from'])?$_POST['time_from']:"";
    $time_to     = isset($_POST['time_to'])?$_POST['time_to']:"";
    $status      = isset($_POST['status'])?$_POST['status']:"";
   
    $this->validation->rules('user_id', $user_id, 'required');
    $this->validation->rules('status', $status, 'required');
    $this->validation->rules('aa_id', $aa_id, 'required');
   
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{
      
      $user_id = decode_base64($user_id);
      
      
      $params = (object)array(
                              'user_id'   => $user_id,
                              'aa_id'     => $aa_id,
                              'time_from' => $time_from,
                              'time_to'   => $time_to,
                              'status'    => $status,
                             );


      $result = $this->artist_model->edit_artist_availability($params);

      $response = array(
                        'response_code' => 200,
                        'status'        => 'ok',
                        'message'       => 'success'
                        );
    }
    //echo json response
    echo json_encode($response);
  }


/**
  * URL - get/subscription
  * FUNCTION - get_subscription
  * METHOD - POST
*/
  public function get_subscription($request, $response, $args){

    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
    
    $response = array();

    $result = $this->artist_model->get_subscription();
   



    foreach ($result as $key => $value) {
		$result[$key]['is_free'] = (($value['asp_name']=='free')?true:false);
      	if(isset($lang) && $lang == 'spn'){
			$result[$key]['asp_name'] = $value['asp_name_spn'];
			$result[$key]['asp_description'] = $value['asp_description_spn'];
      	}
      unset($result[$key]['asp_name_spn']);
        unset($result[$key]['asp_description_spn']);

      $result[$key]['plan_id'] = encode_base64($value['plan_id']);
      $result[$key]['asp_image'] = $value['asp_image']?(file_exists(APP_DIR.'/'.SUBSCRIPTION_PLAN_IMG.$value['asp_image'])?(BASE_URL.SUBSCRIPTION_PLAN_IMG.$value['asp_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
    }

    //print_r($result); die;

    
    if($result){
      $response = array(
                        'response_code' => 200,
                        'status'        => 'ok',
                        'message'       => 'success',
                        'data'          => $result
                        );
    }else{
      $response = array(
                        'response_code' => 200,
                        'status'        => 'ok',
                        'message'       => 'success',
                        'data'          => $result
                        );

    }
    
    //$response = utf8_encode($response, true);

    //echo json response
    echo json_encode($response);
    

  }



  /**
  * FUBCTION get_subscription_get
  * URL - get/subscription
  * METHOD - GET
  */
  public function get_subscription_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }






/**
* FUNCTION - get_artist_rating
* URL - rating/reviews
* METHOD - POST
*/

  public function get_artist_rating($request, $response, $args){
    $response = array();

    if(!$_POST){
      
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'some fields required'
                            );
          echo json_encode($response);
          die;
       }

    
     $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
     $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
          
     $this->validation->rules('user_id', $user_id, 'required');

     if($this->validation->error){
       $response["error"] = true;
       $response["message"] = array_pop($this->validation->error);

     }else{
            
            $user_id = $this->decode_base64($user_id);

            $artist_reviews = $this->artist_model->get_rating($user_id, $lang);
      
      foreach ($artist_reviews as $k => $row) {
        $artist_reviews[$k]['client_id'] = encode_base64($row['client_id']);
        $artist_reviews[$k]['rating'] = (double)$row['rating'];
        $artist_reviews[$k]['client_profile_image'] = $row['client_profile_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$row['client_profile_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$row['client_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
      }


             
             if(!empty($artist_reviews)){
                                  //successfully
                                  $response = array(
                                                    'response_code' => 200,
                                                    'status'        => 'OK',
                                                    'message'       => 'success',
                                                    'data'          => $artist_reviews
                                                    );
                                }else{
                                        //failed
                                      $response = array(
                                                        'response_code' => '400',
                                                        'status'        => 'Bad Request',
                                                        'message'       => 'error'
                                                        );
                                      }

          }    

          //echo response
          echo json_encode($response);

  }


  /**
  * Create get_rating_get
  */
  public function get_artist_rating_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }

  public function get_type_of_arts($request, $response, $args){

    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    $result = $this->artist_model->get_type_of_arts();

    foreach ($result as $key => $value) {

      if(isset($lang) && $lang == 'spn'){
         $result[$key]['ta_name']        = $value['ta_name_spn'];
      }
      unset($result[$key]['ta_name_spn']);
    }
  
    $response = array(
                      'response_code' => 200,
                      'status'        => 'OK',
                      'message'       => 'success',
                      'data'          => $result
                      );

   //echo JSON response
   echo json_encode($response);
  }



/*--- user id decode function on base64_decode ---*/
  protected function decode_base64($code){
    $int = base64_decode($code);
    $id = $int/(45421*45421);
    return $id;
  }


/*--- user id encode function on base64_encode ---*/
  protected function encode_base64($requestData){
    $sting = base64_encode($requestData*45421*45421);

    return $sting;
  }



/*--- user id decode function on base64_decode ---*/
  
  public function decode_base64_call_back($request, $response, $args){ 
    $response = array();
    if(!$_POST){      
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'some fields required'
                            );
          echo json_encode($response);
          die;
    }

    $code = isset($_POST['code'])&&($_POST['code'] != '')?$_POST['code']:'';
    $int = base64_decode($code);
    $id = $int/(45421*45421);
    echo $id;
    die;
  }


/*--- user id encode function on base64_encode ---*/
  public function encode_base64_call_back($request, $response, $args){ 
    $response = array();
    if(!$_POST){      
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'some fields required'
                            );
          echo json_encode($response);
          die;
    }

    $requestData = isset($_POST['code'])&&($_POST['code'] != '')?$_POST['code']:'';
    $sting = base64_encode($requestData*45421*45421);
    echo $sting;
    die;
  }




}
