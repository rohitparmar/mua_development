<?php

/**
 * Class to handle all apis
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Client extends Layout{
  protected $app;

  public function __construct($app) {
    $this->app = $app;
    $this->load_library('Api/Validation');
    $this->load_model('Api/Client_model');
    $this->load_model('Api/Artist_model');
    $this->load_model('Api/Category_model');
  }
   
  /**
  * Create client
  * url - /create_client
  * method - POST
  */
  public function create_client($request, $response, $args){   //this is out of use

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 
  
    $response = array();

    // reading post params
    $user_id    = isset($_POST['user_id'])?$_POST['user_id']:"";
    $skin_color = isset($_POST['skin_color'])?$_POST['skin_color']:"";
    $phone      = isset($_POST['phone'])?$_POST['phone']:"";
    $about      = isset($_POST['about'])?$_POST['about']:"";
    $email      = isset($_POST['email'])?$_POST['email']:"";
    $city       = isset($_POST['city'])?$_POST['city']:"";
    $address    = isset($_POST['address'])?$_POST['address']:"";
    $language   = isset($_POST['language'])?$_POST['language']:"";
    $zip        = isset($_POST['zip'])?$_POST['zip']:"";

     /*Validation*/
    
    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('skin_color',$skin_color,'required');
    

    if($this->validation->error){
          $response["error"] = true;
          $response["message"] = array_pop($this->validation->error);
        
    }
    else{

      $user_id = decode_base64($id);

      // add client image          
      $cp_image = '';
      if(isset($_FILES['cp_image'])){
        $ext = explode('.',$_FILES['cp_image']['name']);
//        $cp_image = 'img_'.time().rand().'.'array_pop($ext);
        move_uploaded_file($_FILES['cp_image']['tmp_name'],APP_DIR.'/artist-client-upload-file/client/profile/'.$cp_image);
      }


        
      $params = (object)array(
                               'user_id'    => $user_id,
                               'skin_color' => $skin_color,
                               'cp_image'   => $cp_image,
                               'phone'      => $phone,
                               'about'      => $about,
                               'email'      => $email,
                               'city'       => $city, 
                               'address'    => $address,
                               'language'   => $language,
                               'zip'        => $zip
                              );


      $res = $this->Client_model->create_client_profile($params);

    
        if($res == 1){
          $response = array(
                            'response_code' =>200,
                            'status'        =>'ok',
                            'message'       =>'success'
                            );
        }
        else if($res == 0){

          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'Oops! An error occurred while added information'
                            );
        }
        else if($res == 2){

          $response = array(
                            'response_code' =>400,
                            'status'        =>'error',
                            'message'       =>'Sorry, this user information already existed'
                            );
        
        }
    } 
  // echo json response
  echo json_encode($response);
}



/**
  * Create client by get
  * url - /create_client
  */
  public function create_client_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


/**
  * URL - edit/client/profile
  * FUNCTION - edit_client_profile
  * METHOD - POST
*/
  public function edit_client_profile($request, $response, $args) {

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();

    $user_id    = isset($_POST['user_id'])?$_POST['user_id']:"";
    $skin_color = isset($_POST['skin_color'])?$_POST['skin_color']:"";
    $phone      = isset($_POST['phone'])?$_POST['phone']:"";
    $about      = isset($_POST['about'])?$_POST['about']:"";
    $email      = isset($_POST['email'])?$_POST['email']:"";
    $city       = isset($_POST['city'])?$_POST['city']:"";
    $state      = isset($_POST['state'])?$_POST['state']:"";
    $address    = isset($_POST['address'])?$_POST['address']:"";
    $language   = isset($_POST['language'])?$_POST['language']:"";
    $zip        = isset($_POST['zip'])?$_POST['zip']:"";
    $lat        = isset($_POST['lat'])?$_POST['lat']:"";
    $long       = isset($_POST['long'])?$_POST['long']:"";


    $name       = isset($_POST['name'])?$_POST['name']:"";
            
    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('skin_color',$skin_color,'required');
        


    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      $user_id = decode_base64($user_id);

      // get the detail by user_id
      $user = $this->Client_model->get_client_profile($user_id);

      //print_r($user); die;
               
      if ($user['cp_id'] != 0 || $user['cp_id'] != ""){
        $params = (object)array(
                                'user_id'    => $user_id,
                                'skin_color' => $skin_color,
                                'phone'      => $phone,
                                'about'      => $about,
                                'email'      => $email, 
                                'city'       => $city,
                                'state'      => $state, 
                                'address'    => $address, 
                                'language'   => $language, 
                                'zip'        => $zip,
                                'lat'        => $lat,
                                'long'       => $long,
                                'status'     => "true",
                                'name'       => $name,
                                );

        //print_r($params); die;  
        $result = $this->Client_model->update_client_profile($params);
                  $this->Client_model->get_client_name($params);
                  
          if ($result) {
            // password updated successfully
            
            $params->user_id = encode_base64($params->user_id); 
            
            
            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success',
                              'data'          => $params  
                              );
              
          } else {
              // password failed to update
            $params->user_id = encode_base64($params->user_id); 
              $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success',
                              'data'          => $params  
                              );
            } 
      }
      else{ 
          // user credentials are wrong
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'Login credentials incorrect'
                            );
      } 
    }
    //echo json response
    echo json_encode($response);
  }


 /**
  * Create edit_client_profile_get
  * url - /create_client
  */
  public function edit_client_profile_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


  public function get_skintone_list($request, $response, $args) {
    
	   $lang    = isset($_POST['lang'])?($_POST['lang']=='spn'?'spn':'eng'):"eng";
  	 $result = $this->Client_model->get_skintone_list($lang);
     if ($result) {

              $response = array(
                                'response_code' => '200',
                                'status'        => 'ok',
                                'message'       => 'success',
                                'data'          => $result
                                );
            }else{

              $response = array(
                                'response_code' => 200,
                                'status'        => 'ok',
                                'message'       => 'success',
                                'data'          => $result
                                );
            }

     echo json_encode($response);
  }



/**
  * URL - edit/client/profile
  * FUNCTION - edit_client_profile
  * METHOD - POST
*/
  public function set_skin_ton($request, $response, $args) {

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    $user_id    = isset($_POST['user_id'])?$_POST['user_id']:"";
    $skin_ton   = isset($_POST['skin_ton'])?$_POST['skin_ton']:"";
   
    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('skin_ton',$skin_ton,'required');
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      $user_id = decode_base64($user_id);
      
      // get the detail by user_id
      $user = $this->Client_model->get_client_profile($user_id);
                     
      if ($user){
        $params = (object)array(
                                'user_id'    => $user_id,
                                'skin_color' => $skin_ton,
                                );

        //print_r($params); die;  
        $result = $this->Client_model->update_client_profile($params);
                  
          
            // password updated successfully
            
            $params->user_id = encode_base64($params->user_id); 
            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success'  
                              );
              
          
      }
      else{ 
          // user credentials are wrong
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'Login credentials incorrect'
                            );
      } 
    }
    //echo json response
    echo json_encode($response);
  }




/**
  * Create edit_client_profile_get
  * url - /create_client
*/
  public function set_skin_ton_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


/**
  * Create get_client
  * url - /create_client
  */
  public function get_client($request, $response, $args){

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();


    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    /*Validation*/
     
    $this->validation->rules('user_id',$user_id,'required');
          
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

     $user_id = decode_base64($user_id);

    // print_r($user_id); die;

      
      // get the detail by user_id 
      $user = $this->Client_model->get_client_profile($user_id);

      

      if($user){

      $user['cp_phone'] = ($user['cp_phone'] == '')?$user['u_phone']:$user['cp_phone'];
      $user['cp_email'] = ($user['cp_email'] == '')?$user['u_email']:$user['cp_email'];

      unset($user['u_phone']);
      unset($user['u_email']);

      
      $img = $user['im_image_name'];
      $action = $user['im_action'];
      unset($user['im_action']);
      unset($user['im_image_name']);

      $image[$action] = ($action=='client')?BASE_URL.CLIENT_PROFILE_IMG.$img:'';

      $img = array(
                  'image_id'  => encode_base64($user['im_id']),
                  'image_url' => isset($image['client'])?$image['client']:''
                  );


      $user['cp_image'] = isset($image['client'])?$img:(object)array();

      $user['cp_image2'] = isset($image['client'])?$image['client']:'';
      

      $user['avg_rating'] = isset($user['avg_rating'])?(double)$user['avg_rating']:'';
      
        $response = array(
                          'response_code' => 200,
                          'status'        => 'OK',
                          'message'       => 'success',
                          'data'          => $user
                         );
      }
      else{
         $response = array(
                          'response_code' => 200,
                          'status'        => 'OK',
                          'message'       => 'success',
                          'data'          => $user
                         );
      }

    }
  //echo json response
  echo json_encode($response);
}



  /**
  * Create get_client
  * url - /create_client
  */
  public function get_client_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


/**
* function add_rating
* url - add/rating/client
*/
  public function add_rating($request, $response, $args) {
    
     if(!$_POST){
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'some fields required'
                            );
          echo json_encode($response);
          die;
     } 

    $response = array();
    
    $user_id    = isset($_POST['user_id'])?$_POST['user_id']:"";
    $request_id = isset($_POST['request_id'])?$_POST['request_id']:"";
    $mua_id     = isset($_POST['mua_id'])?$_POST['mua_id']:"";
    $rating     = isset($_POST['rating'])?$_POST['rating']:"";
    $review     = isset($_POST['review'])?$_POST['review']:"";
    $user_type  = isset($_POST['user_type'])?$_POST['user_type']:"client";

    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('request_id',$request_id,'required');
    $this->validation->rules('mua_id',$mua_id,'required');
    $this->validation->rules('rating',$rating,'required');
    $this->validation->rules('review',$review,'required');
    $this->validation->rules('user_type',$user_type,'required');


    if($this->validation->error){
            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
    }
    else{
          /*database*/
          $user_id    = $this->decode_base64($user_id);
          $mua_id     = $this->decode_base64($mua_id);
          $request_id = $this->decode_base64($request_id);
          
          

          $params = (object)array(
                                  'user_id'     => $user_id,
                                  'request_id'  => $request_id,
                                  'mua_id'      => $mua_id,
                                  'rating'      => $rating,
                                  'review'      => $review,
                                  'user_type'   => $user_type
                                  );

          $result = $this->Client_model->create_rating($params);
          
          if($result == 1){
                      // success
                      $response = array(
                                        'response_code' => 200,
                                        'status'        => 'OK',
                                        'message'       => 'success'
                                        );
                              
          }
          else if($result == 0){
                // failed
                $response = array(
                                  'response_code' => 400,
                                  'status'        => 'bad request',
                                  'message'       => 'error'
                                  );
          } 
            else if($result == 2){
                  // failed
                  $response = array(
                                    'response_code' => 400,
                                    'status'        => 'bad request',
                                    'message'       => 'this user already added the rating'
                                    );
            } 
        }

        echo json_encode($response);

  }


/**
* function add_rating_get
* url - add/rating/client
*/
  public function add_rating_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


/**
* FUNCTION - get_rating
* URL - rating/reviews
* METHOD - POST
*/

  public function get_client_rating($request, $response, $args){
    $response = array();

    if(!$_POST){
      
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'some fields required'
                            );
          echo json_encode($response);
          die;
       }

    
     $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
     $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
          
     $this->validation->rules('user_id', $user_id, 'required');

     if($this->validation->error){
       $response["error"] = true;
       $response["message"] = array_pop($this->validation->error);

     }else{
            
            $user_id = $this->decode_base64($user_id);

            $artist_reviews = $this->Client_model->get_rating($user_id, $lang);
      
      foreach ($artist_reviews as $k => $row) {
        $artist_reviews[$k]['mua_id'] = isset($row['mua_id'])?encode_base64($row['mua_id']):'';
        $artist_reviews[$k]['rating'] = isset($row['rating'])?(double)ROUND($row['rating'],2):'';
        $artist_reviews[$k]['artist_profile_image'] = $row['artist_profile_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$row['artist_profile_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$row['artist_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
      }

    

             
             if(!empty($artist_reviews)){
                                  //successfully
                                  $response = array(
                                                    'response_code' => 200,
                                                    'status'        => 'OK',
                                                    'message'       => 'success',
                                                    'data'          => $artist_reviews
                                                    );
                                }else{
                                        //failed
                                       $response = array(
                                                    'response_code' => 200,
                                                    'status'        => 'OK',
                                                    'message'       => 'success',
                                                    'data'          => $artist_reviews
                                                    );
                                      }

          }    

          //echo response
          echo json_encode($response);

  }


  /**
  * Create get_rating_get
  */
  public function get_client_rating_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }





/**
  * URL - /find/nearby
  * CREATED FUNCTION get_nearby
  * METHOD - POST
  */
  public function get_nearby($request, $response, $args){


    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
        echo json_encode($response);
        die;
    }

       $response = array();

       $lang      = isset($_POST['lang'])?$_POST['lang']:"eng";
       $user_id   = isset($_POST['user_id'])?$_POST['user_id']:"";
       $latitude  = isset($_POST['lat'])?$_POST['lat']:false;
       $longitude = isset($_POST['long'])?$_POST['long']:false;
       $distance  = isset($_POST['distance'])?$_POST['distance']:"";
  	   $start     = isset($_POST['start'])?$_POST['start']:false;
  	   $limit     = isset($_POST['limit'])?$_POST['limit']:false;
  	   $skintone  = isset($_POST['skintone'])?$_POST['skintone']:false;


       $cat_array = isset($_POST['cat_array'])?$_POST['cat_array']:''; 
       $cat_array = @json_decode($cat_array);

       $cat_array = is_array($cat_array)?implode(',', $cat_array):false;
      

       $search = isset($_POST['search'])?$_POST['search']:false;

       $skintype = isset($_POST['skintype'])?$_POST['skintype']:false;
       $skintype = $skintype?@json_decode($skintype):false;

       if(is_array($skintype) && count($skintype)){
          $skintype = "'".implode("','", $skintype)."'";
       }
       

       $speakinglang = isset($_POST['speakinglang'])?$_POST['speakinglang']:false;
       $speakinglang = $speakinglang?@json_decode($speakinglang):false;
      
       if(is_array($speakinglang) && count($speakinglang)){
          $speakinglang = "'".implode("','", $speakinglang)."'";
       }
       
       if($this->validation->error){

        $response["error"] = true;
        $response["message"] = array_pop($this->validation->error);

       }
       else{

        $user_id = $this->decode_base64($user_id);
        //die;
        $user_type = 'client';
        //now first of all update the loction
        //$this->Client_model->update_user_lat_long($user_id, $user_type, $latitude, $longitude);
        /*
		if($latitude && $longitude){
          $this->Client_model->update_client_lat_long($user_id, $user_type, $latitude, $longitude);
        }
		*/
        //biw get list of mua nearby
        $result = $this->client_model->get_nearby($user_id, $distance, $cat_array, $search, $skintype, $speakinglang, $start, $limit);
        //get_nearby($user_id, $distance, $cat_array= fasle, $search = false, $skintype=false, $speakinglang=false, $lang='eng')
        
		$client_location = $this->client_model->get_client_location($user_id); //(object)array('lat'=>$value['client_lat'], 'long'=>$value['client_long']);	

        $data = array();
        //print_r($result);die;
		//$client_location = (object)array();
        foreach ($result as $key => $value) {
		  
		  //unset($result[$key]['client_lat']);
		  //unset($result[$key]['client_long']);
		  
          $result[$key]['userName'] = ($value['userName'] == '')?$value['u_name']:$value['userName'];
                 
          $result[$key]['avgRating'] = (double)$value['avgRating'];

          $result[$key]['distance'] = (isset($value['distance']) && $value['distance'])?$value['distance']:'0.00';

          $result[$key]['favorite'] = (bool)$value['favorite'];
          $result[$key]['artist_id'] = encode_base64($value['artist_id']);
          $result[$key]['artist_profile_image'] = $value['artist_profile_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$value['artist_profile_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$value['artist_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
        }

        if(!empty($result)){
          //successfully
          $response = array(
								'response_code' 	=> 200,
								'status'        	=> 'OK',
								'message'       	=> 'success',
								'client_location' 	=> $client_location,
								'data'         		=> $result
                       );
        }
        else{
          //failed
         $response = array(
                            'response_code' => 200,
                            'status'        => 'OK',
                            'message'       => 'success',
							'client_location' => $client_location,
                            'data'          => $result
                           );
        }

       }

       //echo json response
       echo json_encode($response);
    
  }


/**
  * Create function get_nearby_get
  * url - /find/nearby
  * method - get
  */
  public function get_nearby_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }  



/**
  * function add/client/long
  * url - /add_client_long
  * method - POST
  */
  public function add_client_long($request, $response, $args) {


    if(!$_POST){
    	$response = array(
                          'response_code' => 400,
                          'status'        => 'error',
                          'message'       => 'some fields required'
                          );
        echo json_encode($response);
        die;
    } 
  
    $response = array();

    // reading post params
    
    $user_id     = isset($_POST['user_id'])?$_POST['user_id']:"";
    $user_type     = isset($_POST['user_type'])?$_POST['user_type']:"";
    $latitude   = isset($_POST['lat'])?$_POST['lat']:"";
    $longitude    = isset($_POST['long'])?$_POST['long']:"";


     /*Validation*/
    
    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('user_type',$user_type,'required');
    //$this->validation->rules('longitude',$longitude,'required');
    //$this->validation->rules('latitude',$latitude,'required');
  
    
    if($this->validation->error){
          $response["error"] = true;
          $response["message"] = array_pop($this->validation->error);
    }
    else{
        /*Database*/
        $user_id = $this->decode_base64($user_id);
        //print($user_id); die;
        $result = $this->Client_model->update_user_lat_long($user_id, $user_type, $latitude, $longitude);
        
        // password updated successfully
        $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success'
                          );
    } 
  // echo json response
  echo json_encode($response);
}


/**
  * url - find/favorite
  * funcion add_client_long_get
  * METHOD - POST
  */
  public function add_client_long_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


/**
  * url - add/favorite
  * funcion add_favorite
  * METHOD - POST
*/
  public function add_favorite($request, $response, $args){
  	if(!$_POST){
    	$response = array(
                          'response_code' => 400,
                          'status'        => 'error',
                          'message'       => 'some fields required'
                          );
        echo json_encode($response);
        die;
    } 
  
    $response = array();

    // reading post params
    
    $user_id   = isset($_POST['user_id'])?$_POST['user_id']:"";
    $mua_id    = isset($_POST['mua_id'])?$_POST['mua_id']:"";
    $favorite  = isset($_POST['favorite'])?$_POST['favorite']:"false";

    $this->validation->rules('user_id', $user_id, 'required');
    $this->validation->rules('mua_id', $mua_id, 'required');
    $this->validation->rules('favorite', $favorite, 'required');

    if($this->validation->error){
          $response["error"] = true;
          $response["message"] = array_pop($this->validation->error);
    }
    else{

        /*Database*/
        $user_id = decode_base64($user_id);
        $mua_id  = decode_base64($mua_id);
        //print_r($mua_id); die;
        
        $result = $this->Client_model->add_favorite($user_id, $mua_id, $favorite);

        
          $response = array(
                            'response_code' => 200,
                            'status'        => 'ok',
                            'message'       => 'success',
                            
                           );
        

    } 
    //echo json response
    echo json_encode($response);
  }




/**
  * url - add/favorite
  * funcion add_favorite
  * METHOD - POST
  */
  public function add_favorite_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }  






/**
  * URL - find/favorite
  * FUNCTION get_favorite_list
  * METHOD - POST
*/
  public function get_favorite_list($request, $response, $args){
  	if(!$_POST){
    	$response = array(
                          'response_code' => 400,
                          'status'        => 'error',
                          'message'       => 'some fields required'
                          );
        echo json_encode($response);
        die;
    } 
  
    $response = array();

    // reading post params
    
    $user_id   = isset($_POST['user_id'])?$_POST['user_id']:"";
    $this->validation->rules('user_id', $user_id, 'required');
    
	if($this->validation->error){
          $response["error"] = true;
          $response["message"] = array_pop($this->validation->error);
    }
    else{

        /*Database*/
        $user_id = decode_base64($user_id);
        
        $result = $this->Client_model->get_favorite_list($user_id);



        foreach ($result as $k=>$value) {
           $result[$k]['mua_id'] = encode_base64($value['mua_id']);
           $result[$k]['avgRating'] = number_format($value['avgRating'],1);
		   $result[$k]['cf_is_favorite'] = (bool)($value['cf_is_favorite']=='true'?1:0);
		}
		

       

        if(!empty($result)){
            //successfully
            $response = array(
                              'response_code' => 200,
                              'status'        => 'OK',
                              'message'       => 'success',
                              'data'          => $result
                              );
        }else{
              //failed
              $response = array(
                                'response_code' => '400',
                                'status'        => 'Bad Request',
                                'message'       => 'error'
                                );
        }

    } 
    //echo json response
    echo json_encode($response);
  }

/**
  * URL - find/favorite
  * FUNCTION get_favorite_list_get
  * METHOD - GET
  */
  public function get_favorite_list_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  } 


/**
  * URL - find/mua/full/info
  * FUNCTION get_mua_full_info
  * METHOD - POST
  */
  public function get_mua_full_info($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                      );
      echo json_encode($response);
      die;
    } 
  
    $response  = array();
    $user_id   = isset($_POST['user_id'])?$_POST['user_id']:"";
    $mua_id    = isset($_POST['mua_id'])?$_POST['mua_id']:"";
    $lang      = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';


    $this->validation->rules('user_id', $user_id, 'required');
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    } else{

      $user_id = decode_base64($user_id);
      $mua_id = decode_base64($mua_id);
      
      
    // start get arties_info 
      $user = $this->client_model->get_mua_info($mua_id, $user_id);

      $data = array();
    
      $mua_data = array();
      foreach($user as $row){
          $img = $row['im_image_name'];
          $action = $row['im_action'];
          unset($row['im_action']);
          unset($row['im_image_name']);
          
          $row['total_earn'] = ($row['total_earn']=='null' || $row['total_earn']=='')?0.00: $row['total_earn'];
		  
          $row['favorite']   = ($row['favorite']=='null' || $row['favorite']=='')?'false':$row['favorite'];

          $image[$action] = ($action=='artist')?BASE_URL.ARTIST_PROFILE_IMG.$img:($action=='certificate'?BASE_URL.ARTIST_CERTIFICATE_IMG.$img:'');
          $mua_data = $row;
      }


      $ap_image = array(
                  'image_id'  => isset($mua_data['im_id'])?encode_base64($mua_data['im_id']):false,
                  'image_url' => isset($image['artist'])?$image['artist']:''
              );


      $demo_ap_image = array(
                  'image_id'  => 0,
                  'image_url' => CATEGORY_DEFAULT_IMAGE
              );

      $mua_data['ap_image'] = isset($image['artist'])?$ap_image:(object)$demo_ap_image;

      $ap_certificate = array(
                  'image_id'  => isset($mua_data['im_id'])?encode_base64($mua_data['im_id']):false,
                  'image_url' => isset($image['certificate'])?$image['certificate']:''
              );

      $demo_certificate = array(
                  'image_id'  => 0,
                  'image_url' => CERTIFICATE_DEFAULT_IMAGE
              );

      /*'image_url'     =>$row['im_image_name']?(file_exists(APP_DIR.'/'.ARTIST_PORTFOLIO_IMG.$row['im_image_name'])?(BASE_URL.ARTIST_PORTFOLIO_IMG.$row['im_image_name']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE
                                                          );*/



      $mua_data['ap_certificate'] = isset($image['certificate'])?$ap_certificate:(object)$demo_certificate;


      
      $user = $mua_data;
    // ends get arties_info 

        
      //artist services info

      $artist_services = $this->artist_model->get_services($mua_id, $user_id, $lang);

      $skinResult = $this->Category_model->get_skinton($lang);

      $objSkin = array();
      foreach($skinResult as $k => $rows){
        $arr = array(
                      'id'    => encode_base64($rows['ms_id']),
                      'name'  => $rows['ms_name'],
                      'image' => $rows['ms_image']?(file_exists(APP_DIR.'/'.CATEGORY_IMG.$rows['ms_image'])?(BASE_URL.CATEGORY_IMG.$rows['ms_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE
                    );
        $objSkin[] = (object)$arr;
      }
      
      foreach($artist_services as $k=>$v){
          $is_file_exists = 0;
          if($artist_services[$k]['skintone_img']!=''){
            $ex = explode('.',$artist_services[$k]['skintone_img']);
            if(count($ex)>1){
                      $is_file_exists = file_exists(APP_DIR . '/' . SKINTONE_IMAGE . $artist_services[$k]['skintone_img']);
            }
          }
                  
          if($is_file_exists){
                  $artist_services[$k]['cat_image'] = BASE_URL . SKINTONE_IMAGE . $artist_services[$k]['skintone_img'];
          }else{
              $artist_services[$k]['cat_image'] = $artist_services[$k]['cm_image'] ? (file_exists(APP_DIR . '/' . CATEGORY_IMG . $artist_services[$k]['cm_image']) ? (BASE_URL . CATEGORY_IMG . $artist_services[$k]['cm_image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE;
          }

          $artist_services[$k]['cm_image'] = $objSkin;
      }
    
    
    //$artist_services['cm_image'] = $objSkin;


     
      
     /* foreach($artist_services as $k => $row){
        $artist_services[$k]['ac_id'] = encode_base64($row['ac_id']);
        $imgArr = @unserialize($row['cm_image']);
      
        $image_list = array();
        if ($imgArr !== false) {
          foreach($imgArr as $rows){
            $img = (object)array(
                              'id'    => encode_base64($rows['id']),
                              'name'  => $rows['name'],
                              'image' => $rows['image']?(file_exists(APP_DIR.'/'.CATEGORY_IMG.$rows['image'])?(BASE_URL.CATEGORY_IMG.$rows['image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE
                          );
             array_push($image_list, $img);
          }
        }

         $artist_services[$k]['cm_image'] = $image_list;
      }*/

      


  //artist details info starts
      
        $artist_details = $this->Client_model->get_artist_details($mua_id);
        $result         = $this->Client_model->get_availability($mua_id);

        foreach ($result as $key => $value) {

            $result[$key]['aa_active_status'] = (bool)$value['aa_active_status'];

            if(isset($lang) && $lang == 'spn'){

              switch ($value['aa_days']) {
                  case "Monday":
                      $result[$key]['aa_days'] = 'lunes';
                      break;
                  case "Tuesday":
                      $result[$key]['aa_days'] = 'martes';
                      break;
                  case "Wednesday":
                      $result[$key]['aa_days'] = 'miercoles';
                      break;
                  case "Thursday":
                    $result[$key]['aa_days'] = 'jueves';
                  break;
                  case "Friday":
                    $result[$key]['aa_days'] = 'viernes';
                  break;
                  case "Saturday":
                    $result[$key]['aa_days'] = 'sabado';
                  break;
                  case "Sunday":
                    $result[$key]['aa_days'] = 'domingo';
                  break;
              }
            }
        }

        $artist_details['availablity']  = $result;

  //artist details info ends


     
  //artist_portfolio info starts
      $artist_portfolio = $this->artist_model->get_portfolio_userlist($mua_id, $lang);

      $ac_data = array();
      foreach ($artist_portfolio as $row) {
        if($row['image_type']){
          $ac_data[$row['portfolio_id']]['img'][] = array(
                                                        'image_id'  => encode_base64((int)$row['im_id']),
                                                        'image_type'   => $row['image_type'],
                                                        'image_url' =>$row['im_image_name']?(file_exists(APP_DIR.'/'.ARTIST_PORTFOLIO_IMG.$row['im_image_name'])?(BASE_URL.ARTIST_PORTFOLIO_IMG.$row['im_image_name']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE
                                                          );
        }
        
        
        $ac_data[$row['portfolio_id']]['data'] = array(
                                            'portfolio_id' => encode_base64($row['portfolio_id']),
                                            'category_id' => encode_base64($row['category_id']),
                                            'portfolio_name' => $row['portfolio_name'],
                                            'portfolio_desc' => $row['portfolio_desc'],
                                            'category_description' => $row['category_description'],
                                            'category_name' => $row['category_name'],
                                            'ap_image_path' => ''
                                          );
      }

      $data = array();

      foreach ($ac_data as $row) {

        if(!isset($row['img']) || count($row['img'])==0){
          $row['img'][] = array(
                                'image_id'      => encode_base64(0),
                                'image_type'   => 'before',
                                'image_url'     =>CATEGORY_DEFAULT_IMAGE
                                  );                

          $row['img'][] = array(
                                'image_id'      => encode_base64(0),
                                'image_type'   => 'after',
                                'image_url'     =>CATEGORY_DEFAULT_IMAGE
                                  );
              

        }else if(count($row['img'])==1){
          if($row['img'][0]['image_type']=='after'){
            $row['img'][] = array(
                                  'image_id'      => encode_base64(0),
                                  'image_type'   => 'before',
                                  'image_url'     =>CATEGORY_DEFAULT_IMAGE
                                    );
                  
          }else{
            $row['img'][] = array(
                                  'image_id'      => encode_base64(0),
                                  'image_type'   => 'after',
                                  'image_url'     =>CATEGORY_DEFAULT_IMAGE
                                    );
                  
            }
          
        }
        
        $row['data']['ap_image_path'] = $row['img'];
        $data[]= $row['data'];
      }
      $artist_portfolio = $data;

  //artist_portfolio info ends    
     


  //artist_reviews info starts
      

      $artist_reviews = $this->Client_model->get_mua_rating($mua_id, $lang);
      
      foreach ($artist_reviews as $k => $row) {
        
        $artist_reviews[$k]['client_id'] = encode_base64($row['client_id']);
        $artist_reviews[$k]['rating']    = (double)$row['rating'];
        $artist_reviews[$k]['client_profile_image'] = $row['client_profile_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$row['client_profile_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$row['client_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
      
      }

  //artist_reviews info ends
      

      $data = array(
                    'artist_info'      => $user,
                    'artist_services'  => $artist_services,
                    'artist_details'   => $artist_details,
                    'artist_portfolio' => $artist_portfolio,
                    'artist_reviews'   => $artist_reviews
                    );

      //print_r($data); die;
      if(!empty($data)){
            //successfully
            $response = array(
                              'response_code' => 200,
                              'status'        => 'OK',
                              'message'       => 'success',
                              'data'          => $data
                              );
        }else{
              //failed
              $response = array(
                                'response_code' => '400',
                                'status'        => 'Bad Request',
                                'message'       => 'error'
                                );
        }

    }
    //echo json response
    echo json_encode($response);


  }


/**
  * URL - find/mua/full/info
  * FUNCTION get_mua_full_info
  * METHOD - GET
  */
  public function get_mua_full_info_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }  


  /*--- user id decode function on base64_decode ---*/
  protected function decode_base64($code){
    $int = base64_decode($code);

    $id = $int/(45421*45421);

    return $id;
  }


/*--- user id encode function on base64_encode ---*/
  protected function encode_base64($requestData){
    $sting = base64_encode($requestData*45421*45421);

    return $sting;
  }

}