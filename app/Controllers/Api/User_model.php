<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class User_model {

    private $conn;

    function __construct($app) {
        $this->conn = $app->get('db');
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */

     public function nearby($user_id, $distance=false, $cat_array= fasle, $search = false, $skintype=false, $speakinglang=false, $lang='eng'){
      
        $WHERE = '';
        $WHERE_NAME = '';
        $WHERE_SKINTYPE = '';
        $WHERE_LANGUAGE = '';

        $GWHERE = 1;
        
        if($cat_array){
          $WHERE = "INNER JOIN artist_category 
                    ON
                    artist_category.ac_fk_u_id = artist_profile.ap_fk_u_id AND artist_category.ac_fk_cm_id IN(".$cat_array.") ";
        }

        if($skintype && $cat_array){
          $WHERE_SKINTYPE = " INNER JOIN category_master
                             ON
                             artist_category.ac_fk_cm_id=category_master.cm_id
                             INNER JOIN category_attributes
                             ON
                             category_attributes.ca_fk_cm_id=category_master.cm_id
                             AND
                             category_attributes.ca_skintone IN (".$skintype.")
                             AND
                             category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name='".$lang."')
                             ";
        }

        
        if($search){
          $GWHERE = " artist_profile.ap_profile_name LIKE '%".$search."%'";
          $GWHERE .= " OR user.u_name LIKE '%".$search."%' AND user.u_type='mua'";
        }

        if($speakinglang){

          $WHERE_LANGUAGE = " artist_profile.ap_language IN (".$speakinglang.")"; 
          
          if($GWHERE<>1){
            $GWHERE = ' ('.$GWHERE. ' AND '. $WHERE_LANGUAGE. ')';
          }else{
            $GWHERE = $WHERE_LANGUAGE;
          }
        }
        
        
        $ARIAL_DISTANCE = ($distance)?$distance : ARIAL_DISTANCE;
        $sql = "SELECT a.ap_language, a.ap_fk_u_id AS artist_id, a.ap_profile_name AS userName, a.ap_lat, a.ap_long, a.avgRating AS avgRating, a.reviews, a.distance, IF(a.favorite='true', 1, 0) AS favorite, a.artist_profile_image FROM 
                
                (
                  SELECT ap_fk_u_id, ap_profile_name, ap_lat, ap_long, ap_language,

                  (
                    ROUND(ARIAL_DISTANCE((SELECT cp_lat FROM client_profile WHERE cp_fk_u_id = :cp_fk_u_id), ap_lat,   
                    (SELECT cp_long FROM client_profile WHERE cp_fk_u_id = :cp_fk_u_id LIMIT 1), ap_long, 'M'), 2)

                  ) AS distance,
                 
                  (
                   SELECT ROUND(AVG(rating_remarks.rr_rating),2) FROM rating_remarks 
                   WHERE rating_remarks.rr_fk_mua_id = ap_fk_u_id AND rating_remarks.rr_status='true' AND rating_remarks.rr_review_by='mua' 
                   )  AS avgRating,

                  (
                   SELECT COUNT(rating_remarks.rr_rating) FROM rating_remarks 
                   WHERE rating_remarks.rr_fk_mua_id = ap_fk_u_id AND rating_remarks.rr_status='true' AND rating_remarks.rr_review_by='mua'
                  )  AS reviews,

                  (
                   SELECT IF(cf_is_favorite='true', 'true','false') AS cf_is_favorite FROM client_favorite 
                   WHERE client_favorite.cf_fk_client_id = :cp_fk_u_id AND client_favorite.cf_fk_mua_id = ap_fk_u_id ORDER BY cf_id DESC LIMIT 1
                  ) AS favorite,

                  (
                    SELECT im_image_name FROM image_master 
                    WHERE image_master.im_fk_u_id = artist_profile.ap_fk_u_id 
                    AND im_action = 'artist' AND im_status = 'true' AND im_deleted = 'false' 
                    ORDER BY im_id DESC LIMIT 1
                  ) AS artist_profile_image

               
                FROM artist_profile 

                INNER JOIN user 
                ON
                user.u_id = artist_profile.ap_fk_u_id

                ###catsearch### 

                ###mua-skintype###

                WHERE artist_profile.ap_status ='true' AND ###mua-name-language-search###
                ) AS a 
                
                WHERE distance <= :ARIAL_DISTANCE GROUP BY a.ap_fk_u_id ORDER BY distance ASC";


        $sql = str_replace('###catsearch###', $WHERE, $sql);
        $sql = str_replace('###mua-name-language-search###', $GWHERE, $sql);
        $sql = str_replace('###mua-skintype###', $WHERE_SKINTYPE, $sql);
        
        //echo $sql; die;

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cp_fk_u_id', $user_id);
        $stmt->bindParam(':ARIAL_DISTANCE',$ARIAL_DISTANCE);

        //echo $sql; die;

        if($stmt->execute()){
            $result = $stmt->fetchAll();
            //print_r($result); die;
            $stmt->closeCursor();
            return $result;
        }
        else{
               return NULL;  
        }
    } 

    public function createUser($param) {

       

        $fbID    = '';
        $gmailID = '';

        $fbID    = ($param->social_type == 'facebook')?$param->fbID:'';
       
        $gmailID = ($param->social_type == 'gmail')?$param->fbID:'';

       
        $response = 0;
        
        $sql = "SELECT USER_REGISTRATION('".$param->name."', '".$param->email."', '".$param->password."', '".$param->phone."',  '".$fbID."', '".$param->user_type."', '".$param->language."', '".$param->fb_login."', '".$param->gcm_id."', '".$param->time_zone."', '".$param->social_type."', '".$param->type_of_arts."', '".$param->username."', '".$gmailID."') AS tot";

        $stmt = $this->conn->prepare($sql);


        if($stmt->execute()){
            $result = $stmt->fetch();
            $response = $result['tot'];
        }
        
        $stmt->closeCursor();

        return $response;
    }


    public function update_refrral_code($param){
        
        
        
        $sql = "INSERT INTO referral (r_fk_u_id, r_usertype, r_name, r_rebate, r_duration, r_code, r_device_id, r_created_date, r_start_date, r_end_date, r_used_count) values (:r_fk_u_id, :r_usertype, :r_name, :r_rebate, :r_duration, :r_code, :r_device_id, :r_created_date, :r_start_date, :r_end_date, '0')";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':r_fk_u_id',$param->user_id);
        $stmt->bindParam(':r_usertype',$param->user_type);
        $stmt->bindParam(':r_name',$param->name);
        $stmt->bindParam(':r_rebate',$param->rebate);
        $stmt->bindParam(':r_duration',$param->duration);
        $stmt->bindParam(':r_code',$param->refrral_code);
        $stmt->bindParam(':r_device_id',$param->gcm_id);
        $stmt->bindParam(':r_created_date',$param->date);
        $stmt->bindParam(':r_start_date',$param->start_date);
        $stmt->bindParam(':r_end_date',$param->end_date);
        
        $result = $stmt->execute(); //now 

        $last_id = '';
        if($result){
			$last_id = $this->conn->lastInsertId();
			
            $sql = "INSERT INTO user_account_info (uai_fk_u_id, uai_usertype, uai_created, uai_account_no) VALUES (:uai_fk_u_id, :uai_usertype, :uai_created , '')";
            $stmt = $this->conn->prepare($sql);

            $stmt->bindParam(':uai_fk_u_id',$param->user_id);
            $stmt->bindParam(':uai_usertype',$param->user_type);
            $stmt->bindParam(':uai_created',$param->date);
            $stmt->execute();  
            $stmt->closeCursor(); 

            $refrral_code = create_referral_code($param->name, $last_id);

            $sql = "UPDATE referral SET r_code = :r_code WHERE r_id = :r_id";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':r_code',$refrral_code);
            $stmt->bindParam(':r_id',$last_id);

            $stmt->execute();
        }
        $stmt->closeCursor();
		return $refrral_code;
    }

/**
 * Checking add_client_artist_user_id
 * @param $user_id
*/

    public function add_client_artist_user_id($user_id, $user_type, $lat, $long){
        $user_type = strtolower($user_type);
        //echo $user_type; 
        $table = ($user_type=='mua')?'artist_profile':'client_profile';
        $table_col_name = ($user_type=='mua')?'ap_fk_u_id':'cp_fk_u_id';

        $table_col_lat = ($user_type=='mua')?'ap_lat':'cp_lat';

        $table_col_long = ($user_type=='mua')?'ap_long':'cp_long';

        $sql = "INSERT INTO ".$table."(".$table_col_name.",".$table_col_lat.",".$table_col_long.") VALUES ('".$user_id."','".$lat."','".$long."')";



    
        /*remove folling query when subscription part is completed*/
        //$sql .= ";";
        //$sql .= "INSERT INTO artist_payment_information(api_fk_artist_id, api_fk_asp_id, api_paid_amount, api_service_amout, api_transaction_id, api_transaction_details, api_status, api_created) VALUES ('".$user_id."', '1', '60', '60', '', 'auto', 'true', NOW())";
        /**end query here for remove purpose */

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();

}



    /**
     * Checking user email
     * @param String $email
     */
    public function getUserEmail($email)  {
        //print_r($email); die;
        $sql = "SELECT u_id, u_name, u_email, u_phone, u_fb_id, u_type, u_lang, u_fb_login, u_status, u_gcm_id FROM user WHERE u_email = ? ";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1,$email);

        if ($stmt->execute()) {

           $user = $stmt->fetch();
           $stmt->closeCursor();


            return $user;

        } else {
            return NULL;
        }
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($param) {



        $where = '';

        $md5_password = md5($param->password);
/*
        if(isset($param->email) && $param->email){
            $where = " WHERE u_email != '' AND u_email = '".$param->email."' AND u_password = '".$md5_password."' AND u_password!= ''";
        }elseif(isset($param->username) && $param->username){
            $where = " WHERE u_username!='' AND u_username = '".$param->username."' AND u_password = '".$md5_password."'";
        }elseif((isset($param->email) && $param->email) && (isset($param->username) && $param->username)){
            $where = " WHERE u_email != '' AND u_email = '".$param->email."' AND u_password = '".$md5_password."'";
        }
*/
		$where = " WHERE (u_email = '".$param->email."' AND u_password = '".$md5_password."') OR (u_username = '".$param->email."' AND u_password = '".$md5_password."')";
        
       $sql = "SELECT u_id, u_name, u_email, u_fb_id, u_type, u_lang, IF(u_fb_login='true',1,0) AS  u_fb_login, IF(u_status='true', 1, 0) AS u_status, u_gcm_id, u_varified,


            IF(user.u_phone='', (
                IF(user.u_type='mua', (SELECT ap_phone FROM artist_profile WHERE artist_profile.ap_fk_u_id = user.u_id), 
                (SELECT cp_phone FROM client_profile WHERE client_profile.cp_fk_u_id = user.u_id)
            )), user.u_phone) AS u_phone,

            (
             SELECT IF(COUNT(*)>0,'true','false') 
             FROM artist_availability
             WHERE aa_fk_u_id = user.u_id AND aa_active_status = 'true'
             ) AS availability_status,

            (
             SELECT IF(COUNT(*)>0,'true','false')  FROM artist_category
             WHERE ac_fk_u_id = 2 AND ac_status = 'true'
            ) AS artist_category_status,
			
            (
             SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = user.u_id AND (image_master.im_action = 'artist' OR image_master.im_action = 'client') ORDER BY image_master.im_id DESC LIMIT 1
            ) AS u_image,

             (
             SELECT im_action FROM image_master WHERE image_master.im_fk_u_id = user.u_id AND (image_master.im_action = 'artist' OR image_master.im_action = 'client') ORDER BY image_master.im_id DESC LIMIT 1
            ) AS u_action,

            user.u_profile_status AS profile_status, user.u_portfolio_status AS portfolio_status,
            (
             SELECT r_code FROM referral WHERE referral.r_fk_u_id = user.u_id AND r_status = 'true' ORDER BY r_id DESC LIMIT 1
            ) AS referral_code,
			
			(
				SELECT IF (EXISTS(SELECT api_fk_asp_id AS subscription_id FROM artist_payment_information WHERE api_fk_artist_id = user.u_id AND api_status = 'true' AND TIMESTAMPDIFF(MONTH, api_created, NOW())<1 ORDER BY api_id DESC LIMIT 1),(SELECT api_fk_asp_id AS subscription_id FROM artist_payment_information WHERE api_fk_artist_id = user.u_id AND api_status = 'true' AND TIMESTAMPDIFF(MONTH, api_created, NOW())<1 ORDER BY api_id DESC LIMIT 1),(SELECT asp_id FROM artist_subscription_plan WHERE asp_name='free' LIMIT 1))
			) AS subscription_id,
			
			(
			SELECT IF (EXISTS(SELECT api_fk_asp_id AS subscription_id FROM artist_payment_information WHERE api_fk_artist_id = user.u_id AND api_status = 'true' AND TIMESTAMPDIFF(MONTH, api_created, NOW())<1 ORDER BY api_id DESC LIMIT 1),(SELECT api_fk_asp_id AS subscription_id FROM artist_payment_information WHERE api_fk_artist_id = user.u_id AND api_status = 'true' AND TIMESTAMPDIFF(MONTH, api_created, NOW())<1 ORDER BY api_id DESC LIMIT 1),'free')
			) AS subscription_check
			

         FROM user " .$where ;


       
        $stmt = $this->conn->prepare($sql);
        
        if ($stmt->execute()) {

            $user = $stmt->fetch();
            $stmt->closeCursor();

            if(isset($user) && $user){

                $sql = "UPDATE user SET u_gcm_id = '".$param->gcmID."' WHERE u_id = 
                '".$user['u_id']."'";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $stmt->closeCursor();
            }

            
            return $user;
        }
        else {
            return NULL;
        }
    }



    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function get_social_login($param) {

        $where = '';

        $email =isset($param->email)?$param->email:'';

        if(isset($param->fbID) && $param->fbID){
            
            $where = " WHERE (u_fb_id = '".$param->fbID."' OR u_gmail_id = '".$param->fbID."' 
                    OR ( u_email = '".strtolower($email)."' AND u_email != '' ))";
        
        }

        
        $sql = "SELECT u_id, u_name, u_email, u_social_type, u_fb_id, u_gmail_id, u_type, u_lang, IF(u_fb_login='true',1,0) AS u_fb_login, IF(u_status='true',1,0) AS u_status, u_gcm_id, u_varified,

            IF(user.u_phone='', (
                IF(user.u_type='mua', (SELECT ap_phone FROM artist_profile WHERE artist_profile.ap_fk_u_id = user.u_id), 
                (SELECT cp_phone FROM client_profile WHERE client_profile.cp_fk_u_id = user.u_id)
            )), user.u_phone) AS u_phone,

            (
             SELECT IF(COUNT(*)>0,'true','false') 
             FROM artist_availability
             WHERE aa_fk_u_id = user.u_id AND aa_active_status = 'true'
             ) AS availability_status,

            (
             SELECT IF(COUNT(*)>0,'true','false')  FROM artist_category
             WHERE ac_fk_u_id = 2 AND ac_status = 'true'
            ) AS artist_category_status,

            (
             SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = user.u_id AND (image_master.im_action = 'artist' OR image_master.im_action = 'client') ORDER BY image_master.im_id DESC LIMIT 1
            ) AS u_image,

             (
             SELECT im_action FROM image_master WHERE image_master.im_fk_u_id = user.u_id AND (image_master.im_action = 'artist' OR image_master.im_action = 'client') ORDER BY image_master.im_id DESC LIMIT 1
            ) AS u_action,

            user.u_profile_status AS profile_status, user.u_portfolio_status AS portfolio_status,
            (
             SELECT r_code FROM referral WHERE referral.r_fk_u_id = user.u_id AND r_status = 'true' ORDER BY r_id DESC LIMIT 1
            ) AS referral_code,
			
			(
				SELECT IF (EXISTS(SELECT api_fk_asp_id AS subscription_id FROM artist_payment_information WHERE api_fk_artist_id = user.u_id AND api_status = 'true' AND TIMESTAMPDIFF(MONTH, api_created, NOW())<1 ORDER BY api_id DESC LIMIT 1),(SELECT api_fk_asp_id AS subscription_id FROM artist_payment_information WHERE api_fk_artist_id = user.u_id AND api_status = 'true' AND TIMESTAMPDIFF(MONTH, api_created, NOW())<1 ORDER BY api_id DESC LIMIT 1),(SELECT asp_id FROM artist_subscription_plan WHERE asp_name='free' LIMIT 1))
			) AS subscription_id,
			
			(
				SELECT IF (EXISTS(SELECT api_fk_asp_id AS subscription_id FROM artist_payment_information WHERE api_fk_artist_id = user.u_id AND api_status = 'true' AND TIMESTAMPDIFF(MONTH, api_created, NOW())<1 ORDER BY api_id DESC LIMIT 1),(SELECT api_fk_asp_id AS subscription_id FROM artist_payment_information WHERE api_fk_artist_id = user.u_id AND api_status = 'true' AND TIMESTAMPDIFF(MONTH, api_created, NOW())<1 ORDER BY api_id DESC LIMIT 1),'free'
			)) AS subscription_check

            FROM user " .$where;

        $stmt = $this->conn->prepare($sql);
        
        if ($stmt->execute()) {

            $user = $stmt->fetch();
           
            if(isset($user) && $user && isset($param->fbID) && $param->fbID){

                if(isset($param->social_type) && $param->social_type == 'facebook' && $user['u_fb_id'] == ''){
                    $sql = "UPDATE user SET u_fb_id = '".$param->fbID."', u_fb_login = 'true' WHERE u_id = '".$user['u_id']."'";    
                }elseif(isset($param->social_type) && $param->social_type == 'gmail' && $user['u_gmail_id'] == ''){
                    $sql = "UPDATE user SET u_gmail_id = '".$param->fbID."', u_fb_login = 'true' WHERE u_id = '".$user['u_id']."'";
                }

                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
            
            }

            $stmt->closeCursor();
            return $user;

        } else {
            return array();
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $sql = "SELECT u_id from user WHERE u_email!='' AND u_email = ?";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam("1", $email);
        $stmt->execute();
        $num_rows = $stmt->fetch();
        $stmt->closeCursor();
        return $num_rows;
    }



    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    public function get_user_id($user_id) {
      
        $sql = "SELECT * from user WHERE u_id = ?";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam("1", $user_id);
        $stmt->execute();
        $result = $stmt->fetch();
        $stmt->closeCursor();
        return $result;
    }

    public function get_client_data_id($user_id, $request_id) {
      
        $sql = "SELECT u_name, u_phone, cp_phone, cp_city, cp_state, cp_address, cp_zipcode, ca_name, ca_description FROM user
                INNER JOIN client_profile
                ON
                client_profile.cp_fk_u_id = user.u_id

                INNER JOIN client_request
                ON 
                client_request.cr_id = :cr_id 

                INNER JOIN artist_category
                ON 
                artist_category.ac_id = client_request.cr_fk_ac_id

                INNER JOIN category_master
                ON
                category_master.cm_id = artist_category.ac_fk_cm_id
                
                INNER JOIN category_attributes
                ON
                category_attributes.ca_fk_cm_id = category_master.cm_id

                WHERE u_id = :u_id";
        $stmt = $this->conn->prepare($sql);

        
        $stmt->bindParam(':u_id',  $user_id);
        $stmt->bindParam(':cr_id', $request_id);
        
        $stmt->execute();
        $result = $stmt->fetch();
        $stmt->closeCursor();
        return $result;
    }

 

/**
     * Creating new update_Password
     * @param String $email, $password, $id 
     */
    public function update_Password($email, $password, $id) {
        $sql = "UPDATE user us set us.u_password = :u_password WHERE us.u_email = :u_email AND us.u_id = :u_id";
        $stmt = $this->conn->prepare($sql);
        $md5_password = md5($password);
        $stmt->bindParam(':u_password', $md5_password);
        $stmt->bindParam(':u_email', $email);
        $stmt->bindParam(':u_id', $id);
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return 1;
    }


/**
* Creating update_profile
* @param String $email, $password, $id 
     */
    public function update_profile($email, $name, $password, $phone) {
        $sql = "UPDATE user us set us.u_name = ?, us.u_phone = ?, us.u_password = ? WHERE us.u_email = ? ";
        $stmt = $this->conn->prepare($sql);
        $md5_password = md5($password);
        $stmt->bindParam("1", $name);
        $stmt->bindParam("2", $md5_password);
        $stmt->bindParam("3", $phone);
        $stmt->bindParam("4", $email);
        
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }




  /**
     * Creating new update_gcmID
     * @param String $email, $password, $id 
     */
    public function update_gcmID($user_id, $gcmID, $time_zone) {
        //print_r($user_id); die;
        $sql = "UPDATE user us set us.u_gcm_id = ?, u_time_zone = ? WHERE us.u_id = ?";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1,$gcmID);
        $stmt->bindParam(2,$time_zone);
        $stmt->bindParam(3,$user_id);
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }


    /**
     * Creating new update_gcmID
     * @param String $email, $password, $id 
     */
    public function insert_userAction($param) {

        //print_r($param); die;
        $sql = "INSERT INTO user_action(ua_fk_u_id, ua_device_info, ua_long, ua_lat, ua_type) values (:ua_fk_u_id, :ua_device_info, :ua_long, :ua_lat, :ua_type)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ua_fk_u_id',$param->user_id);
        $stmt->bindParam(':ua_device_info',$param->gcmID);
        $stmt->bindParam(':ua_long',$param->user_long);
        $stmt->bindParam(':ua_lat',$param->user_lat);
        $stmt->bindParam(':ua_type',$param->user_type);

        $stmt->execute();
        $stmt->closeCursor();


    }


/**
   * Creating new get_mua_profile
*/

    public function get_mua_profile(){

        $sql = "SELECT * FROM user WHERE u_type = 'mua'";
        $stmt = $this->conn->prepare($sql);
        if($stmt->execute()){

            $result = $stmt->fetchAll();
            $stmt->closeCursor();
            return $result;

        }else{
            return NULL;
        }
        
    }


    public function get_payment(){
        $sql = "SELECT cpi_transaction_details FROM client_payment_information WHERE cpi_id = 13";
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if($stmt->execute()) {
           $result = $stmt->fetch();
        }
        $stmt->closeCursor();
        return $result;
    }


/**
 * Creating function insert_artist_availability($id)
*/

    public function insert_artist_availability($param){
        //print_r($param); die;

        
    $sql = "INSERT INTO artist_availability(aa_fk_u_id, aa_days, aa_time_from, aa_time_to, aa_created) VALUES (:aa_fk_u_id, :aa_days, :aa_time_from, :aa_time_to, :aa_created)";

    
    $stmt = $this->conn->prepare($sql);
    //print_r($param); die;
    $stmt->bindParam(':aa_fk_u_id',$param->user_id);
    $stmt->bindParam(':aa_days',$param->days);
    $stmt->bindParam(':aa_time_from',$param->time_from);
    $stmt->bindParam(':aa_time_to',$param->time_to);
    $stmt->bindParam(':aa_created',$param->date);

    
    $stmt->execute();
    $stmt->closeCursor();

        
    }


    public function get_mua_name($id){
        $sql = "SELECT ap_profile_name FROM artist_profile WHERE ap_fk_u_id = ".$id."";
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if($stmt->execute()) {
           $result = $stmt->fetch();
        }
        $stmt->closeCursor();
        return $result;
    }


    public function insert_cron($param) {

        //print_r($param); die;
        $sql = "INSERT INTO user_action(ua_device_info) values (:ua_device_info)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ua_device_info',$param->name);
        

        $stmt->execute();
        $stmt->closeCursor();


    }


    public function get_data_by_id($id){
        $sql = "SELECT * FROM user WHERE u_id = ".$id."";
        $result = false;
        $stmt = $this->conn->prepare($sql);
        if($stmt->execute()) {
           $result = $stmt->fetch();
        }
        $stmt->closeCursor();
        return $result;
    }



    public function get_all_where($table ,$where=""){
        
        if(isset($where) && $where){

            $where = " WHERE ".$where."";
        }
        
       $sql = "SELECT * FROM ".$table."".$where;
        $result = false;
        $stmt = $this->conn->prepare($sql);

        if($stmt->execute()) {
           $result = $stmt->fetchAll();
        }
        $stmt->closeCursor();
        return $result;
    }


    public function _where($where){//recursion method which arrange where condition
      $result="";
      $pp="";
      foreach($where as $key=>$value){
       if(is_array($value)){
        $result=" ".$result." (".$this->where($value)." )";
       }else{
        if($value=='and' | $value=='or'){
         $result.=" ".$value;
        }else{
         if(substr($value,0,2)=='!='){
          $value=substr($value,2,strlen($value)-2);
          $result.=' '.$key.' != "'.$value.'"';
         }else if(substr($value,0,2)=='<='){
          $value=substr($value,2,strlen($value)-2);
          $result.=' '.$key.' <= "'.$value.'"';
         }else if(substr($value,0,2)=='>='){
          $value=substr($value,2,strlen($value)-2);
          $result.=' '.$key.' >= "'.$value.'"';
         }else if(substr($value,0,1)=='<'){
          $value=substr($value,1,strlen($value)-1);
          $result.=' '.$key.' < "'.$value.'"';
         }else if(substr($value,0,1)=='>'){
          $value=substr($value,1,strlen($value)-1);
          $result.=' '.$key.' > "'.$value.'"';
         }else if(strtolower(substr($value,0,4))=='like'){
          $value=substr($value,4,strlen($value)-4);
          $result.=" ".$key." like '%".$value."%'";
         }else{
          $result.=' '.$key.' = "'.$value.'"';
         }
        }
       }
      }
      return($result);
     }



    public function social_register($param) {



        $response = 0;

        if(isset($param->social_type) && $param->social_type == 'facebook'){

            $sql = "INSERT user (u_name, u_username, u_email, u_password, u_phone, u_fb_id, u_type, u_lang, u_social_type, u_fb_login, u_created, u_gcm_id, u_time_zone, u_type_of_arts)

            VALUES (:u_name, :u_username, :u_email, :u_password, :u_phone, :u_fb_id, :u_type, :u_lang, :u_social_type, :u_fb_login, :u_created, :u_gcm_id, :u_time_zone, :u_type_of_arts)";

        }elseif(isset($param->social_type) && $param->social_type == 'gmail'){

            $sql = "INSERT user (u_name, u_username, u_email, u_password, u_phone, u_gmail_id, u_type, u_lang, u_social_type, u_fb_login, u_created, u_gcm_id, u_time_zone, u_type_of_arts)

            VALUES (:u_name, :u_username, :u_email, :u_password, :u_phone, :u_gmail_id, :u_type, :u_lang, :u_social_type, :u_fb_login, :u_created, :u_gcm_id, :u_time_zone, :u_type_of_arts)";


        }

        

        $stmt = $this->conn->prepare($sql);

        $date = date('Y-m-d h:i:s');
        
        $stmt->bindParam(':u_name', $param->name);
        $stmt->bindParam(':u_username', $param->username);
        $stmt->bindParam(':u_email', $param->email);
        $stmt->bindParam(':u_password', $param->password);
        $stmt->bindParam(':u_phone', $param->phone);
        $stmt->bindParam(':u_type', $param->user_type);
        $stmt->bindParam(':u_lang', $param->language);
        $stmt->bindParam(':u_social_type', $param->social_type);
        $stmt->bindParam(':u_fb_login', $param->fb_login);
        $stmt->bindParam(':u_created', $date);
        $stmt->bindParam(':u_gcm_id', $param->gcmID);
        $stmt->bindParam(':u_time_zone', $param->time_zone);
        $stmt->bindParam(':u_type_of_arts', $param->type_of_arts);

        if(isset($param->social_type) && $param->social_type == 'facebook'){
            $stmt->bindParam(':u_fb_id', $param->fbID);
        }elseif(isset($param->social_type) && $param->social_type == 'gmail'){
            $stmt->bindParam(':u_gmail_id', $param->fbID);
        }

        
        $result = $stmt->execute();

        $user_id = '';
        if ($result) {
            $user_id = $this->conn->lastInsertId();
        }
        
        $stmt->closeCursor();
        
        return $user_id;
                
        $stmt->closeCursor();

        
    }

    public function get_validate_socialid($param){


        
        $sql = "SELECT * FROM user WHERE u_fb_id = '".$param->fbID."'";
        
        $stmt = $this->conn->prepare($sql);
        
        if($stmt->execute()) {
           $result = $stmt->fetch();
        }
        $stmt->closeCursor();

        if($result){
            return $result;    
        }
        return array();
    }


    public function get_earned_data($user_id, $lang){

        
        $sql =  "SELECT 

            client_payment_information.cpi_fk_client_id AS client_id,

            user.u_name AS client_name,

            client_payment_information.cpi_paid_amount AS ammount_recvied,
            client_payment_information.cpi_service_amout AS service_amout,
            client_payment_information.cpi_rebate_amount AS rebate_amount,
            client_payment_information.cpi_total_rebate_amount AS total_rebate_amount,
            client_payment_information.cpi_payment_mode AS payment_mode,
            client_payment_information.cpi_created AS ammount_recvied_date,
            
            client_request.cr_id AS services_id,

            category_attributes.ca_name AS services_name, 
            category_attributes.ca_description AS services_description,

            (
             SELECT im_image_name FROM image_master WHERE image_master.im_fk_u_id = client_payment_information.cpi_fk_client_id AND (image_master.im_action = 'client' OR image_master.im_action = 'client') ORDER BY image_master.im_id DESC LIMIT 1
            ) AS client_image,

            (SELECT SUM(client_payment_information.cpi_paid_amount) FROM client_payment_information WHERE cpi_fk_artist_id = '".$user_id."' LIMIT 1) 
        AS mua_total


         FROM client_payment_information

         INNER JOIN client_profile
         ON
         client_profile.cp_fk_u_id = client_payment_information.cpi_fk_client_id

         INNER JOIN user
         ON
         user.u_id = client_payment_information.cpi_fk_client_id

         INNER JOIN client_artist_booking
         ON
         client_artist_booking.cab_id = client_payment_information.cpi_fk_cab_id

         INNER JOIN client_request
         ON
         client_request.cr_id = client_artist_booking.cab_fk_cr_id

         INNER JOIN artist_category
        ON
        artist_category.ac_id = client_request.cr_fk_ac_id
        INNER JOIN category_master
        ON
        category_master.cm_id = artist_category.ac_fk_cm_id
        INNER JOIN category_attributes
        ON
        category_attributes.ca_fk_cm_id = category_master.cm_id
        AND
        category_attributes.language = (SELECT lm_id FROM language_master WHERE lm_name='".$lang."')

         WHERE cpi_fk_artist_id = '".$user_id."' AND cpi_status = 'true' ORDER BY cpi_id DESC";    
        
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':cpi_fk_artist_id', $user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    public function get_earned_data_admin($user_id, $lang='eng'){

        if($lang=='eng'){
            $paypal = 'By Paypal';
            $check = 'By Check';
            $bankAccount = 'By Bank Account';
            $unknown = 'Unknown';
        }else{
            $paypal = 'Por Paypal';
            $check = 'Por cheque';
            $bankAccount = 'Por cuenta bancaria';
            $unknown = 'Desconocido';
        }

        $sql =  "SELECT *, IF(aph_payment_via='bank_account','".$bankAccount."',IF(aph_payment_via='check','".$check."',IF(aph_payment_via='paypal','".$paypal."','".$unknown."'))) AS aph_payment_via ,

        (SELECT SUM(aph_payment_amount) FROM admin_payment_history_to_user WHERE aph_fk_u_id = '".$user_id."' LIMIT 1) 
        AS admin_total,

        aph_payment_via AS aph_payment_mode FROM admin_payment_history_to_user WHERE aph_fk_u_id = '".$user_id."'";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':aph_fk_u_id', $user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }


    public function get_subscription_data($user_id, $lang){

        $sql =  "SELECT *,

        (SELECT SUM(artist_payment_information.api_paid_amount) FROM artist_payment_information WHERE api_fk_artist_id = '".$user_id."' LIMIT 1) 
        AS subscription_total 
        


        FROM artist_payment_information

        INNER JOIN artist_subscription_plan
        ON
        artist_payment_information.api_fk_asp_id = artist_subscription_plan.asp_id

        WHERE api_fk_artist_id = '".$user_id."'";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':aph_fk_u_id', $user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }


    public function add_user_location($user_id, $address, $lat, $long){

        $status = 'false';

       $sql = "INSERT user_location (ul_fk_u_id, ul_address, ul_lat, ul_long, ul_status) VALUES (:ul_fk_u_id, :ul_address, :ul_lat, :ul_long, :ul_status);

            SET @max = (SELECT count(*) FROM user_location WHERE ul_fk_u_id=:ul_fk_u_id);
            UPDATE user_location SET ul_status='true' WHERE ul_fk_u_id=:ul_fk_u_id AND @max=1;
        ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':ul_fk_u_id', $user_id);
        $stmt->bindParam(':ul_address', $address);
        $stmt->bindParam(':ul_lat',     $lat);
        $stmt->bindParam(':ul_long',    $long);
        $stmt->bindParam(':ul_status',  $status);
       
        $result = $stmt->execute();

        $user_id = '';
        if ($result) {
            $user_id = $this->conn->lastInsertId();

            //now update here mua or client lat and long where status is true
        }
        $stmt->closeCursor();
        return $user_id;
        $stmt->closeCursor();
    }


    public function get_user_location($user_id){
        $sql =  "SELECT * FROM user_location WHERE ul_fk_u_id = :ul_fk_u_id ORDER BY ul_status ASC";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ul_fk_u_id', $user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }


    public function edit_user_location($user_id, $ul_id, $status) {
        
        $sql = "UPDATE user_location SET ul_status = 'false' WHERE ul_fk_u_id = :ul_fk_u_id AND ul_status = 'true';

        UPDATE user_location SET ul_status = :ul_status WHERE ul_id = :ul_id AND ul_fk_u_id = :ul_fk_u_id;
        ";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ul_status', $status);
        $stmt->bindParam(':ul_id', $ul_id);
        $stmt->bindParam(':ul_fk_u_id', $user_id);
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }


    public function get_user_location_id($user_id){
        $sql = "SELECT * FROM user_location WHERE ul_fk_u_id = :ul_fk_u_id AND ul_status = 'true' ORDER BY ul_id DESC";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ul_fk_u_id', $user_id);
        $stmt->execute();
        $result = $stmt->fetch();
        $stmt->closeCursor();
        return $result;
    }


    public function deleted_user_location($user_id, $ul_id) {
        
        $sql = "DELETE FROM user_location WHERE ul_id = :ul_id AND ul_fk_u_id = :ul_fk_u_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':ul_fk_u_id', $user_id);
        $stmt->bindParam(':ul_id', $ul_id);
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        
        $stmt->closeCursor();
        return $num_affected_rows > 0;
    }


    public function user_send_mail($user_id, $subject, $message){
        $date = date("Y-m-d h:i:sa");
        $sql = "INSERT user_mail (um_fk_u_id, um_subject, um_message, um_created) VALUES (:um_fk_u_id, :um_subject, :um_message, :um_created)";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(':um_fk_u_id', $user_id);
        $stmt->bindParam(':um_subject', $subject);
        $stmt->bindParam(':um_message', $message);
        $stmt->bindParam(':um_created', $date);
        
        $result = $stmt->execute();

        $user_id = '';
        if ($result) {
            $user_id = $this->conn->lastInsertId();
        }
        $stmt->closeCursor();
        return $user_id;
        $stmt->closeCursor();
    }

    public function user_mail_list($user_id){
        $sql = "SELECT * FROM user_mail WHERE um_fk_u_id = :um_fk_u_id ORDER BY um_updated DESC";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':um_fk_u_id', $user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }


/*
 * function update_user_lat_long
 * @param $user_id, $user_type, $latitude, $longitude
*/
    public function update_user_lat_long($user_id, $user_type, $latitude, $longitude){
       
        
        $table = $user_type=='mua'?'artist_profile':'client_profile';
        $latCol = $user_type=='mua'?'ap_lat':'cp_lat';
        $longCol = $user_type=='mua'?'ap_long':'cp_long';
        $user_id_Col = $user_type=='mua'?'ap_fk_u_id':'cp_fk_u_id';

        $sql = "UPDATE ".$table." SET ".$latCol." = '".$latitude."', ".$longCol." = '".$longitude."' WHERE ".
                $user_id_Col." = '".$user_id."'";
                
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        $num_affected_rows = $stmt->rowCount();
        $stmt->closeCursor();
        return $num_affected_rows > 0;
        
       

    }

}