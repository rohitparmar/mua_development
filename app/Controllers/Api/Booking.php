<?php

/**
 * Class to handle all apis
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Booking extends Layout{
  protected $app;

  public function __construct($app) {
    $this->app = $app;
    $this->load_library('Api/Validation');
    $this->load_library('Api/Email', 'email');
    $this->load_library('Api/Push_notification');
    $this->load_model('Api/Booking_model');
    $this->load_model('Api/Payment_model');
    $this->load_model('Api/User_model');
    $this->load_model('Api/Client_model');
  }
  
  public function get_booking_info(){
      if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }
    
    $mua_id         = isset($_POST['mua_id'])? decode_base64($_POST['mua_id']):false; //13
    $client_id      = isset($_POST['client_id'])? decode_base64($_POST['client_id']):false; //12
    $service_id     = isset($_POST['service_id'])?($_POST['service_id']):false; //6

    //print_r($mua_id); die;
   
    $this->validation->rules('mua_id', $mua_id, 'required');
    $this->validation->rules('client_id', $client_id, 'required');
    $this->validation->rules('service_id', $service_id, 'required');
    
    $info = $this->booking_model->get_booking_info($mua_id, $client_id, $service_id);
    

    if(isset($info) && $info){
                $response = array(
                                  'response_code' => 200,
                                  'status'        => 'ok',
                                  'message'       => 'success',
                                  'data'          => (object)$info
                                  );

    } else {

            $response = array(
                        'response_code' => 400,
                        'status'        => 'no',
                        'message'       => 'error',
                        'data'          => array()
                        );
      }


    echo json_encode($response);
  }
  
  /**
  * function booking
  * URL - add/booking
  * method - POST
  */
  public function booking($request, $response, $args){
      
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }
    
    $response = array();  
       

    $user_id      = isset($_POST['user_id'])?$_POST['user_id']:"";
    $mua_id       = isset($_POST['mua_id'])?$_POST['mua_id']:"";
    $booking_data = isset($_POST['booking_data'])?$_POST['booking_data']:"";
    $devices_type = isset($_POST['devices_type'])?$_POST['devices_type']:"android";

    //$book_date    = isset($_POST['start_time'])?date('Y-m-d H:i:s', $_POST['start_time']/1000):"";
    $book_date    = isset($_POST['book_date'])?$_POST['book_date']:"";
    $start_time   = isset($_POST['start_time'])?$_POST['start_time']:"";
    $end_time     = isset($_POST['end_time'])?$_POST['end_time']:"";
    $request_date = date('Y-m-d H:i:s');
    $message      = isset($_POST['message'])?$_POST['message']:"";
    $category_id  = isset($_POST['category_id'])?$_POST['category_id']:"";
    $date         = date('Y-m-d H:i:s'); 
    $gift         = isset($_POST['gift'])?$_POST['gift']:"false";
    $lang         = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
    
    
    $this->validation->rules('user_id', $user_id, 'required');
    $this->validation->rules('mua_id', $mua_id, 'required');
    
    if($this->validation->error){          
        $response["error"] = true;
        $response["message"] = array_pop($this->validation->error);        
    } else{

        $booking_data = json_decode($booking_data);

        $user_id = $this->decode_base64($user_id);
        $mua_id  = $this->decode_base64($mua_id);
        
        $params['user_id'] = $user_id;
        $params['mua_id']  = $mua_id;
        $params['request_date'] = date('Y-m-d H:i:s');

        $response = array();
        
        $response['response_code'] = 200;
        $response['status'] = 'ok';
        $response['message'] = 'success';
        $response['data'] = array();

        foreach ($booking_data as $key => $value){

          $params['plan_booking_date'] = date("Y-m-d H:i:s", substr($value->booking_date ,0,-3));

          $params['book_date']   = $value->booking_date; 
          $params['category_id'] = $value->category_id; 
          $params['end_time']    = $value->end_time; 
          $params['start_time']  = $value->start_time; 
          $params['message']     = "demo";
          $params['grand_price'] = isset($value->grand_price)?$value->grand_price:0.00;

          $params['gift']        = @serialize($value->gift); 

          $mua_data = [];

          $mua_data = $this->booking_model->get_mua_booking_count($mua_id);
          
          

          $params['grand_price'] = str_replace(",","",$params['grand_price']);

          $commission = 0;

          if(isset($mua_data) && $mua_data){
              if($mua_data['plan_id'] == 4){                
                $commission = (isset($mua_data['commission']) && $mua_data['commission'])?$mua_data['commission']:0;
                $retailPrice = ($params['grand_price'] - ($params['grand_price'] * ($mua_data['commission']/100)));

                $params['com_grand_price'] = number_format(floor($retailPrice*100)/100,2, '.', '');
                
              }else{
                
                  if($mua_data['booking_count'] >= $mua_data['client_limit']){                   
                    $commission = (isset($mua_data['commission']) && $mua_data['commission'])?$mua_data['commission']:0;
                    
                    $retailPrice = ($params['grand_price'] - ($params['grand_price'] * ($mua_data['commission']/100)));

                    $params['com_grand_price'] = number_format(floor($retailPrice*100)/100,2, '.', '');
                  }else{
                    $params['com_grand_price'] = number_format(floor($params['grand_price']*100)/100,2, '.', '');
                  }
              }

          }else{
            

            $free_plan_data = $this->booking_model->get_free_plan_data();
            if(isset($free_plan_data['asp_commission_after_limit']) && $free_plan_data['asp_commission_after_limit']){
                
                $commission = $free_plan_data['asp_commission_after_limit']; 
                $retailPrice = ($params['grand_price'] - ($params['grand_price'] * ($free_plan_data['asp_commission_after_limit']/100)));
                $params['com_grand_price'] = number_format(floor($retailPrice*100)/100,2, '.', '');
            }else{              
                $commission = $mua_data['commission'];
                $retailPrice = ($params['grand_price'] - ($params['grand_price'] * ($commission/100)));
                $params['com_grand_price'] = number_format(floor($retailPrice*100)/100,2, '.', '');
            }

            
          }

          $params['commission'] = isset($commission)?$commission:0;

          $params['mua_plan_id'] = (isset($mua_data['commission']) && $mua_data['plan_id'])?$mua_data['plan_id']:0;
          

          // get the detail by user_id 
          $user = $this->Client_model->get_client_profile($user_id);

          if(isset($user) && $user['cp_profile_completed'] == 'true'){
            $res = $this->booking_model->booking($params);  

          }else{

              $response = array(
                                'response_code' => 400,
                                'status'        => 'error',
                                'message'       => 'please complete your profile'
                                );
              echo json_encode($response);
              die;
          }
          
            
           
            if(isset($res['res']) && $res['res'] == 1){

              /*--- Starts send E-mail booking to mua ---*/
              $clientData = $this->user_model->get_client_data_id($user_id, $res['request_id']);
              $muaData    = $this->user_model->get_user_id($mua_id);

              $data['client_name']    = isset($clientData['u_name'])?$clientData['u_name']:'';
              $data['client_phone']   = isset($clientData['u_phone'])?$clientData['u_phone']:'';
              $data['client_phone2']  = isset($clientData['cp_phone'])?$clientData['cp_phone']:'';
              $data['cp_city']        = isset($clientData['cp_city'])?$clientData['cp_city']:'';
              $data['cp_state']       = isset($clientData['cp_state'])?$clientData['cp_state']:'';
              $data['cp_address']     = isset($clientData['cp_address'])?$clientData['cp_address']:'';
              $data['cp_zipcode']     = isset($clientData['cp_zipcode'])?$clientData['cp_zipcode']:'';
              $data['ca_name']        = isset($clientData['ca_name'])?$clientData['ca_name']:'';
              $data['ca_description'] = isset($clientData['ca_description'])?$clientData['ca_description']:'';

              $data['mua_name']     = isset($muaData['u_name'])?$muaData['u_name']:'';
              $data['mua_email']    = isset($muaData['u_email'])?$muaData['u_email']:'';


              $sub = BOOKING_MSG;

              $msg = $this->_view('Front/Email/Booking/booking_template.php', $data, true);
              $this->email->to($data['mua_email']);
              $this->email->subject($sub);
              $this->email->messageType('html');
              $this->email->message($msg);
              $this->email->send();

              /*--- Ends send E-mail booking to mua ---*/

                if(isset($value->gift->email)){
                    	
                    $subject = BOOKING_GIFT_MSG;

                    $data['client_name']    = isset($clientData['u_name'])?$clientData['u_name']:'';
                		$data['name']    = isset($value->gift->name)?$value->gift->name:'';
                		$data['phone']   = isset($value->gift->phone)?$value->gift->phone:'';
                		$data['email'] 	 = isset($value->gift->email)?$value->gift->email:'';
                		$data['address'] = isset($value->gift->address)?$value->gift->address:'';
                
    		            $msg = $this->_view('Front/Email/Booking/gift.php', $data, 1);

                    


    		            $this->email->to($value->gift->email);
      	      			$this->email->subject($subject);
      	      			$this->email->messageType('html');
      	      			$this->email->message($msg);
      	      			$this->email->send(); 
  				      }
                
                //keep current location in table client_request
                
                $this->booking_model->update_current_location($user_id, $mua_id, $res['request_id']);

                $data = array(
                              'booking_id'    => $this->encode_base64($res['request_id']),
                              'mua_id'        => $this->encode_base64($mua_id),
                              'book_date'     => $book_date,
                              'request_date'  => $request_date,
                              'message'       => $message,
                              'category_id'   => $value->category_id,
                              'date'          => $date,
                              'booking_status'=> true,
                              'gift'          => isset($value->gift)?$value->gift:''
                              );
                $request_id  = $res['request_id'];
                $this->booking_model->insert_cab_table($request_id);
                

                $response['response_code'] = 200;
                $response['status'] = 'ok';
                $response['message'] = 'success';

                array_push($response['data'], $data);

                $userData = $this->booking_model->get_mua_gcmID($mua_id);

                $this->Push_notification->send_notification_booking($userData, $this->encode_base64($res['request_id']), $user_id, $devices_type, $lang);
            } else if(isset($res['res']) && ($res['res'] == 0 || $res['res'] == 2)){
                  $response['response_code'] = 400;
                  $response['status'] = 'error';
                  $response['message'] = ($res['res'] == 2)?'already_booked_error':'bad request';
                  $data = array(
                                  'booking_id'        =>  0,
                                  'booking_status'    =>  false
                                );
                  array_push($response['data'], $data);
            }
        }// foreach close
    }
    //echo json response
   echo json_encode($response);
  }

  private function send_mail($to_email, $message){
     //now send mail to user with password
      $subject = urlencode('You are gifted from your frends');
      $msg = urlencode($message);
      $from =  'info@bebebella.com';
      $from_name = urlencode('Bebebella');

      $url = 'http://103.15.67.70:88/Bebebella/mail/index.php/front/email?to='.$to_email.'&from='.$from.'&subject='.$subject.'&msg='.$msg.'&from_name='.$from_name.'&file';

      //now send mail
      file_get_contents($url);
  }




 /**
  * function booking_get
  * method - get
  */
  public function booking_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
  * function get_client_booking
  * method - post
  */
  public function get_client_booking($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }
    $response = array();               

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";

    $this->validation->rules('user_id', $user_id, 'required');
    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      $user_id = decode_base64($user_id);

      //print_r($user_id); die;
      
      $result = $this->booking_model->get_client_booking($user_id, $lang);

      $gift = array();
      
      foreach ($result as $key => $value){

        $payment = $this->Payment_model->is_already_paid_for_current_booking($user_id, $value['artist_id'], $value['request_id']);

        $result[$key]['payment_status'] = (bool)$payment;
        $result[$key]['artist_id'] = encode_base64($value['artist_id']);
        $result[$key]['request_id'] = encode_base64($value['request_id']);
        $result[$key]['service_price'] = $value['service_price'];
        $result[$key]['artist_profile_image'] = $value['artist_profile_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$value['artist_profile_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$value['artist_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
        
        //following logic instruction given by Rohit - 27-June-2016
        //$result[$key]['booking_status'] = isset($value['booking_status'])?$value['booking_status']:$value['service_status'];

         // print_r($value['address']); 
        if(@unserialize($value['address'])){
          
          $gift =    (object)array(
                                   'gift_status' => 'true',
                                   'address'    => @unserialize($value['address'])
                                   );
         
        }else{
          $gift =    (object)array(
                                   'gift_status' => 'false',
                                   'address'     => (object)array()
                                   );
           

        }
        
        $result[$key]['gift'] = $gift;

        unset($result[$key]['address']);
       
      }
      //die;

      if(!empty($result)){
        $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success',
                          'data'          => $result
                          );
      }
      else{
        $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success',
                          'data'          => []
                          );
      }
    }
    //echo json response
    echo json_encode($response);
}




/**
  * function get_client_booking_get
  * method - get parameter
  */
  public function get_client_booking_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }






/**
  * function client_privious_booking
  * method - post paramerter
  */
  public function client_privious_booking($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }
    $response = array();               

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    $this->validation->rules('user_id', $user_id, 'required');

    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    } 
    else {

      $user_id = $this->decode_base64($user_id);

      $result = $this->booking_model->client_privious_booking($user_id, $lang);
      
      foreach ($result as $key => $value) {
        $result[$key]['artist_id'] = encode_base64($value['artist_id']);
        $result[$key]['request_id'] = encode_base64($value['request_id']);
        $result[$key]['service_price'] = (int)($value['service_price']);
        $result[$key]['artist_profile_image'] = $value['artist_profile_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$value['artist_profile_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$value['artist_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

        $result[$key]['review_status'] = (bool)($value['review_status']=='false'?0:1);
        $result[$key]['rating']        = (double)$value['rating'];
        $result[$key]['avg_rating']    = (double)$value['avg_rating'];
      }

      

        if(!empty($result)){
                $response = array(
                                  'response_code' => 200,
                                  'status'        => 'ok',
                                  'message'       => 'success',
                                  'data'          => $result
                                  );

        } else {

                $response = array(
                            'response_code' => 200,
                            'status'        => 'ok',
                            'message'       => 'success',
                            'data'          => array()
                            );
          }

        }
        //echo json response
        echo json_encode($response);

  }



/**
  * function client_privious_booking_get
  * method - get parameter
  */
  public function client_privious_booking_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }





/**
  * function get_mua_booking
  * method - post parameter
  */
  public function get_mua_booking($request, $response, $args){
    
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }
    $response = array();               

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
    $this->validation->rules('user_id', $user_id, 'required');

    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{
      
      $user_id = $this->decode_base64($user_id);
      
      $result = $this->booking_model->get_mua_booking($user_id, $lang);

      //$mua_data = $this->booking_model->get_mua_booking_count($user_id);

      $gift = array();

      $client_info = array();

      foreach ($result as $key => $value) {

        /*if(count($mua_data)){

          if($mua_data['plan_id'] == 4){

            $retailPrice = ($value['grand_price'] - ($value['grand_price'] * ($mua_data['commission']/100)));

            $result[$key]['grand_price'] = number_format(floor($retailPrice*100)/100,2, '.', '');
            
          }else{
              if($mua_data['booking_count'] >= $mua_data['client_limit']){
                $retailPrice = ($value['grand_price'] - ($value['grand_price'] * ($mua_data['commission']/100)));

                $result[$key]['grand_price'] = number_format(floor($retailPrice*100)/100,2, '.', '');
              }
          }

        }*/

        $result[$key]['grand_price'] = $value['cr_com_grand_price'];

        
        $payment = $this->Payment_model->is_already_paid_for_current_booking($value['client_id'], $user_id, $value['request_id'],2,1);

        //print_r($payment); die;
        $result[$key]['payment_status'] = (bool)$payment;

        $result[$key]['client_id'] = encode_base64($value['client_id']);
        $result[$key]['request_id'] = encode_base64($value['request_id']);
        $result[$key]['service_price'] = $value['service_price'];
        $result[$key]['client_profile_image'] = $value['client_profile_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$value['client_profile_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$value['client_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
        //following logic instruction given by Rohit - 27-June-2016
        //$result[$key]['booking_status'] = isset($value['booking_status'])?$value['booking_status']:$value['service_status'];


          $client_info = (object)array(
                                       'client_city'    => $value['cp_city'],
                                       'client_state'   => $value['cp_state'],
                                       'client_zipcode' => $value['cp_zipcode'],
                                       'client_address' => $value['client_address'],
                                       'client_lat'     => $value['client_lat'],
                                       'client_long'    => $value['client_long']
                                      );



       if(@unserialize($value['address'])){
          
          $gift =    (object)array(
                                   'gift_status' => 'true',
                                   'address'    => @unserialize($value['address'])
                                   );
         
        }else{
          $gift =    (object)array(
                                   'gift_status' => 'false',
                                   'address'     => (object)array()
                                   );
        }
        
        $result[$key]['client_info'] = $client_info;
        
        $result[$key]['gift'] = $gift;

        

        unset($result[$key]['address']);
        unset($result[$key]['cp_city']);
        unset($result[$key]['cp_state']);
        unset($result[$key]['cp_zipcode']);
        unset($result[$key]['client_lat']);
        unset($result[$key]['client_long']);
        unset($result[$key]['client_address']);
        


      }
      


      if(!empty($result)){
        $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success',
                          'data'          => $result
                          );
      } else {
             $response = array(
                            'response_code' => 200,
                            'status'        => 'ok',
                            'message'       => 'success',
                            'data'          => $result
                            );
        }
    }

        //echo json response
        echo json_encode($response);

}




/**
  * function get_mua_booking_get
  * method - get parameters
*/
  public function get_mua_booking_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
  * function get_mua_privious_booking
  * method - post parameters
*/
  public function get_mua_privious_booking($request, $response, $args){

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;  
    }

    $response = array();
    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    $this->validation->rules('user_id', $user_id, 'required');

    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      $user_id = $this->decode_base64($user_id);
      
      $result = $this->booking_model->get_mua_privious_booking($user_id, $lang);

      foreach ($result as $key => $value) {
        $result[$key]['client_id'] = encode_base64($value['client_id']);
        $result[$key]['request_id'] = encode_base64($value['request_id']);
        $result[$key]['service_price'] = (int)($value['service_price']);
        $result[$key]['client_profile_image'] = $value['client_profile_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$value['client_profile_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$value['client_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

        $result[$key]['review_status'] = (bool)($value['review_status']=='false'?0:1);
        $result[$key]['rating']       = (double)$value['rating'];
        $result[$key]['avg_rating']    = (double)$value['avg_rating'];
      }
      

      if(!empty($result)){
        $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success',
                          'data'          => $result
                          );
      } else {
        $response = array(
                            'response_code' => 200,
                            'status'        => 'ok',
                            'message'       => 'success',
                            'data'          => array()
                            );
        }

    }
    //echo json response
    echo json_encode($response);
  }




/**
  * function get_mua_privious_booking_get
  * method - get parameters
*/
  public function get_mua_privious_booking_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }  






/**
  * FUNCTION - get_service_status
  * URL - get/service/status
  * METHOD - POST
*/
  public function get_service_status($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
       
    }

    $response = array();

    $requsetId    = isset($_POST['requsetId'])?$_POST['requsetId']:false;

     if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{
      
      $params = (object)array(
                              'requsetId' => decode_base64($requsetId)
                              );

      $data = $this->booking_model->get_service_status($params);
        
      $response = array(
                        'response_code' => 200,
                        'status'        => 'ok',
                        'message'       => 'success',
                        'data'          => $data
                        );
        
    }
    //echo json response
    echo json_encode($response);
  }
  

/**
  * FUBCTION get_service_status_get
  * URL - get/service/status
  * METHOD - GET
  */
  public function get_service_status_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }





/*
  * FUNCTION - set_service_status
  * URL - set/service/status
  * METHOD - POST
*/
   public function set_service_status($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
       
    }

    $response = array();

    $requsetId    = isset($_POST['requsetId'])?$_POST['requsetId']:false;
    $user_id      = isset($_POST['user_id'])?$_POST['user_id']:false;
    $action       = isset($_POST['action'])?$_POST['action']:false;
    $device_info  = isset($_POST['device_info'])?$_POST['device_info']:false;
    $date         = date('Y-m-d H:i:s');
    

    $this->validation->rules('requsetId', $requsetId, 'required');   
    $this->validation->rules('user_id', $user_id, 'required');
    $this->validation->rules('action', $action, 'required');
    $this->validation->rules('device_info', $device_info, 'required');

    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{
      
      $params = (object)array(
                              'requsetId'   => decode_base64($requsetId),
                              'user_id'     => decode_base64($user_id),
                              'action'      => $action,
                              'device_info' => $device_info,
                              'date'        => $date
                              );
      

      $data = $this->booking_model->get_service_status($params);

      if($params->action == 'service_finished'){
        
        $data = $this->booking_model->get_cr_status($params);
      }
      
      if($data['ss_status'] == $action){

        $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'you have already '.$action
                        );

      }
      else{
        
          $result = $this->booking_model->set_service_status($params);
          
          $params->requsetId = encode_base64($params->requsetId);
          $params->user_id   = encode_base64($params->user_id);

          $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success',
                          'data'          => $params
                          );
      }
    }
    //echo json response
    echo json_encode($response);
  }


/**
  * FUBCTION set_service_status_get
  * URL - set/service/status
  * METHOD - GET
  */
  public function set_service_status_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }




  /*
  * FUNCTION - mua_history
  * URL - set/service/status
  * METHOD - POST
*/
   public function mua_history($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
       
    }

    $response = array();

    
    $user_id = isset($_POST['user_id'])?$_POST['user_id']:false;
    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    
    $this->validation->rules('user_id', $user_id, 'required');
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{      
    	$user_id = $this->decode_base64($user_id);
      //print_r($user_id); die;

      $result = $this->booking_model->mua_history($user_id, $lang);

      foreach ($result as $key => $value) {

        

          $result[$key]['grand_price'] = $value['cr_com_grand_price'];

          $result[$key]['client_id']     = encode_base64($value['client_id']);
          $result[$key]['request_id']    = encode_base64($value['request_id']);
          $result[$key]['service_price'] = $value['service_price'];
          $result[$key]['client_profile_image'] = $value['client_profile_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$value['client_profile_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$value['client_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
    
          $result[$key]['review_status']  = (bool)($value['review_status']=='false'?0:1);
          $result[$key]['review_status1'] = (bool)($value['review_status1']=='false'?0:1);
      
          $result[$key]['rating']         = (double)$value['rating'];
          $result[$key]['avg_rating']     = (double)$value['avg_rating'];
          unset($result[$key]['data']);
        
      }
     

      

      /*foreach ($result as $key => $value) {
       

        $result[$key]['client_id'] 	  = encode_base64($value['client_id']);
        $result[$key]['request_id']   = $value['request_id'];//encode_base64($value['request_id']);
        $result[$key]['service_price'] = $value['service_price'];
        $result[$key]['client_profile_image'] = $value['client_profile_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$value['client_profile_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$value['client_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
		
    		$result[$key]['review_status'] 	= (bool)($value['review_status']=='false'?0:1);
    		$result[$key]['review_status1'] 		   = (bool)($value['review_status1']=='false'?0:1);
		
        $result[$key]['rating']       	= (double)$value['rating'];
        $result[$key]['avg_rating']    	= (double)$value['avg_rating'];
        $result[$key]['data']  = $data;
      }*/


      
     

	    if($result){
	      	$response = array(
	                        'response_code' => 200,
	                        'status'        => 'ok',
	                        'message'       => 'success',
	                        'data'			=> $result
	                        );
		}
		else{

	      	$response = array(
	                        'response_code' => 200,
	                        'status'        => 'ok',
	                        'message'       => 'success',
	                        'data'			=> array()
	                        );
		}
    }
    //echo json response
    echo json_encode($response);
  }



/**
  * function mua_history
  * method - get parameter
  */
  public function mua_history_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }




/**
  * function client_history
  * method - post paramerter
  */
  public function client_history($request, $response, $args){
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    }
    $response = array();               

    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $lang = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    $this->validation->rules('user_id', $user_id, 'required');

    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    } 
    else {

      $user_id = $this->decode_base64($user_id);
      //print_r($user_id); die;
      $result = $this->booking_model->client_history($user_id, $lang);

     
      
      foreach ($result as $key => $value) {
        $result[$key]['artist_id'] = encode_base64($value['artist_id']);
        $result[$key]['request_id'] = encode_base64($value['request_id']);
        $result[$key]['service_price'] = $value['service_price'];
        $result[$key]['artist_profile_image'] = $value['artist_profile_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$value['artist_profile_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$value['artist_profile_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

        $result[$key]['review_status'] 	= (bool)($value['review_status']=='false'?0:1);
		$result[$key]['review_status1'] 		   = (bool)($value['review_status1']=='false'?0:1);
        $result[$key]['rating']        = (double)$value['rating'];
        $result[$key]['avg_rating']    = (double)$value['avg_rating'];
      }

      

        if(!empty($result)){
                $response = array(
                                  'response_code' => 200,
                                  'status'        => 'ok',
                                  'message'       => 'success',
                                  'data'          => $result
                                  );

        } else {

                $response = array(
                            'response_code' => 200,
                            'status'        => 'ok',
                            'message'       => 'success',
                            'data'          => array()
                            );
          }

        }
        //echo json response
        echo json_encode($response);

  }



/**
  * function client_history_get
  * method - get parameter
  */
  public function client_history_get($request, $response, $args){
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }
































/*--- user id decode function on base64_encode ---*/
  protected function decode_base64($code){
    $int = base64_decode($code);

    $id = $int/(45421*45421);

    return $id;
  }


/*--- user id encode function on base64_encode ---*/
  protected function encode_base64($requestData){
    $sting = base64_encode($requestData*45421*45421);

    return $sting;
  }

}