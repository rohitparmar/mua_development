<?php

/**
 * Class to handle all apis
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Cronjob extends Layout {
  protected $app;

  public function __construct($app) {
    $this->app = $app;
    $this->load_library('Api/Validation');
    $this->load_model('Api/Cron_model');
    $this->load_model('Api/User_model');
    $this->load_library('Api/Push_notification');
    $this->load_library('Api/Email', 'email');
  }



  public function send_notification(){

        

    
    $data = $this->Cron_model->get_pending_booking();

    

    $current_date = date("d-m-Y h:i");

		foreach ($data as $key => $value) {

  			$seconds = $value['cr_booking_date'] / 1000;

  			$data[$key]['start_date'] 		= date("d-m-Y", $seconds);
			  $data[$key]['start_date_time']   = date("d-m-Y h:i", $seconds);

			if($data[$key]['start_date_time'] == $current_date){

				if(isset($value['client_gcm_id']) && $value['client_gcm_id']){

					$fields = (object)array(
				                            "to" => $value['client_gcm_id'],
				                            "notification" => (object)array(
				                                                            "title" => "Bebe Bella",
				                                                            "body"  =>  "Client notification",
                                                                   )
				                             );
					
					$this->PushNotificationsAndroid($fields);

				}elseif (isset($value['mua_gcm_id']) && $value['mua_gcm_id']) {

					$fields = (object)array(
				                            "to" 		   => $value['mua_gcm_id'],
				                            "notification" => (object)array(
				                                                            "title" => "Bebe Bella",
				                                                            "body"  =>  "Mua notification",
				                                                            )
				                             );
					
					$this->PushNotificationsAndroid($fields);
				
				}			
			}           
		}
	}


  /*PushNotifications for Android mobile using the curl*/
    
    private function PushNotificationsAndroid($fields){

        $headers = array
                        (
                          'Authorization:key=' . API_ACCESS_KEY,
                          'Content-Type: application/json'
                        );
     
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($fields) );
        $result = curl_exec($ch );
        //echo $result;
        curl_close( $ch );

        $params = (object)array(
                            	'name' => $result                            
                            	);
        $user_id = $this->User_model->insert_cron($params);
    }


    /*PushNotifications for ios mobile using the pam file and ssl certificate*/
  
  

}