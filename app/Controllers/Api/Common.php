<?php

/**
 * Class to handle all apis
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Common extends Layout {
  protected $app;

  public function __construct($app) {
    $this->app = $app;
    $this->load_library('Api/Validation');
    $this->load_model('Api/Common_model');
  }
   
  
  /**
  * URL - IMAGE-UPLOAD
  * FUNCTION image_upload
  * METHOD - POST
  * param : Array(
        image       => ....,
        user_id     => ....,
        user_type   => ....,
        location    => Array(lat=>..., long=>....),
        for         => client-profile / artist-profile / portfolio
      )
  * Response: JSON(
        "response_code": 200,
        "status": "ok",
        "message": "success",
        "data": {
          "image_url": ".........",
          "image_id": base64_encode(.....)
        }
  */
    public function upload_image($request, $response, $args){

      if(!$_POST){
         $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'some fields required'
                            );
        echo json_encode($response);
        die;
      }

      $response = array();

      $user_id            = isset($_POST['user_id'])?$_POST['user_id']:"";
      $user_type          = isset($_POST['user_type'])?$_POST['user_type']:"";
      $portfolio_id       = isset($_POST['portfolio_id'])?$_POST['portfolio_id']:"";
      $portfolio_action   = isset($_POST['portfolio_action'])?$_POST['portfolio_action']:'';


      $action             = isset($_POST['action'])?$_POST['action']:"";
      $user_lat           = isset($_POST['user_lat'])?$_POST['user_lat']:"";
      $user_long          = isset($_POST['user_long'])?$_POST['user_long']:"";
      //for base64 image upload
      $base64image        = isset($_POST['base64image'])?$_POST['base64image']:false;


      $this->validation->rules('user_id', $user_id, 'required');
      $this->validation->rules('action', $action, 'required');

      if($this->validation->error){
        $response["error"] = true;
        $response["message"] = array_pop($this->validation->error);
      } else{
          $user_id = decode_base64($user_id);
          $portfolio_id = decode_base64($portfolio_id);

          $image = false;
          $img_path = false;
          if(isset($_FILES['image'])){
            $ext = explode('.',$_FILES['image']['name']);
            $image = $user_id.'-'.$action.'-img-'.time().'-'.rand(1000,9999).'.png';
          }// close if condition

          if($action=='client'){        
              $img_path = APP_DIR.'/'.CLIENT_PROFILE_IMG;
          } else if($action=='artist'){
              $img_path = APP_DIR.'/'.ARTIST_PROFILE_IMG;
          } else if($action=='certificate'){
              $img_path = APP_DIR.'/'.ARTIST_CERTIFICATE_IMG;
          } else if($action=='portfolio'){
              $img_path = APP_DIR.'/'.ARTIST_PORTFOLIO_IMG;
          }

          //now upload images
          if($img_path && $base64image){
              $image = $image = $user_id.'-'.$action.'-img-'.time().'-'.rand(1000,9999).'.png';
              $img_path = $img_path.$image;
              $this->base64_file_upload($img_path, $base64image);
          } else if($img_path && $image){
              $img_path = $img_path.$image;
              $this->normal_file_upload($_FILES['image']['tmp_name'], $img_path);
          }
          
          
          $params = (object)array(
                                  'user_id'             => $user_id,
                                  'im_portfolio_action' => $portfolio_action,
                                  'user_type'           => $user_type,
                                  'user_lat'            => $user_lat,
                                  'user_long'           => $user_long,
                                  'action'              => $action,
                                  'image'               => $image,
                                  'portfolio_id'        => $portfolio_id,
                                  'date'                => date('Y-m-d H:i:s')
                             );


          $check_upload_image = $this->common_model->check_upload_image($params);

         
          

          if($check_upload_image){

              $params = (object)array(
                                    'user_id'   => $user_id,
                                    'image_id'  => $check_upload_image['im_id'],
                                    'image'     => ($image)?$image:$check_upload_image['im_image_name']
                                    );

            $res = $this->common_model->update_upload_image($params);

           }else{
            $res = $this->common_model->update_image($params);
           }
           
          
          $id = isset($res['id'])?$res['id']:$check_upload_image['im_id'];

          $result = $this->common_model->get_image_name($id);

        if($result['im_action'] == 'artist'){

            $result['im_image_name'] = $result['im_image_name']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$result['im_image_name'])?(BASE_URL.ARTIST_PROFILE_IMG.$result['im_image_name']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

        }else if($result['im_action'] == 'client'){

            $result['im_image_name'] = $result['im_image_name']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$result['im_image_name'])?(BASE_URL.CLIENT_PROFILE_IMG.$result['im_image_name']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

        }else if($result['im_action'] == 'certificate'){
            $result['im_image_name'] = $result['im_image_name']?(file_exists(APP_DIR.'/'.ARTIST_CERTIFICATE_IMG.$result['im_image_name'])?(BASE_URL.ARTIST_CERTIFICATE_IMG.$result['im_image_name']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
        }else if($result['im_action'] == 'portfolio'){
            $result['im_image_name'] = $result['im_image_name']?(file_exists(APP_DIR.'/'.ARTIST_PORTFOLIO_IMG.$result['im_image_name'])?(BASE_URL.ARTIST_PORTFOLIO_IMG.$result['im_image_name']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

        }
          
          
        $data = array(
                      'im_id'       => encode_base64($result['im_id']),
                      'image_name'  => $result['im_image_name']
                      );

        


          if($result){

            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success',
                              'data'          => $data
                              );

          }
          else{

            $response = array(
                              'response_code' => 400,
                              'status'        =>'error',
                              'message'       =>'bad request'
                              );
          }
        }

      //echo json response
      echo json_encode($response);
    }
        
    public function update_upload_image($request, $response, $args){

        if(!$_POST){
           $response = array(
                              'response_code' => 400,
                              'status'        => 'error',
                              'message'       => 'some fields required'
                              );
          echo json_encode($response);
          die;
        }

        $response       = array();

        $user_id        = isset($_POST['user_id'])?$_POST['user_id']:"";
        $image_id       = isset($_POST['image_id'])?$_POST['image_id']:"";
        $action         = isset($_POST['action'])?$_POST['action']:"";
        //for base64 image upload
        $base64image    = isset($_POST['base64image'])?$_POST['base64image']:false;

        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('image_id', $image_id, 'required');
        $this->validation->rules('action', $action, 'required');



        if($this->validation->error){
          $response["error"] = true;
          $response["message"] = array_pop($this->validation->error);
        } else {


            $user_id = decode_base64($user_id);
            $image_id = decode_base64($image_id);
            
            /*
            $image = '';
            if(isset($_FILES['image'])){

              $ext = explode('.',$_FILES['image']['name']);
              $image = 'img_'.time().rand().'.'.end($ext);

              if($action=='client'){        
                  move_uploaded_file($_FILES['image']['tmp_name'],APP_DIR.'/'.CLIENT_PROFILE_IMG.$image);
              }
              else if($action=='artist'){
                  move_uploaded_file($_FILES['image']['tmp_name'],APP_DIR.'/'.ARTIST_PROFILE_IMG.$image);
              }
              else if($action=='certificate'){
                move_uploaded_file($_FILES['image']['tmp_name'],APP_DIR.'/'.ARTIST_CERTIFICATE_IMG.$image);
              }
              else if($action=='portfolio'){
                move_uploaded_file($_FILES['image']['tmp_name'],APP_DIR.'/'.ARTIST_PORTFOLIO_IMG.$image);
              }

            }// close if condition
            */
            
            
            $image = false;
            $img_path = false;
            if(isset($_FILES['image'])){
              $ext = explode('.',$_FILES['image']['name']);
              $image = $user_id.'-'.$action.'-img-'.time().'-'.rand(1000,9999).'.png';
            }// close if condition

            if($action=='client'){        
                $img_path = APP_DIR.'/'.CLIENT_PROFILE_IMG;
            } else if($action=='artist'){
                $img_path = APP_DIR.'/'.ARTIST_PROFILE_IMG;
            } else if($action=='certificate'){
                $img_path = APP_DIR.'/'.ARTIST_CERTIFICATE_IMG;
            } else if($action=='portfolio'){
                $img_path = APP_DIR.'/'.ARTIST_PORTFOLIO_IMG;
            }
            
            //now upload images
            if($img_path && $base64image){
                $image = $user_id.'-'.$action.'-img-'.time().'-'.rand(1000,9999).'.png';
                $img_path = $img_path.$image;
                $this->base64_file_upload($img_path, $base64image);
            } else if($img_path && $image){
                $img_path = $img_path.$image;
                $this->normal_file_upload($_FILES['image']['tmp_name'], $img_path);
            }

            $params = (object)array(
                                    'user_id'   => $user_id,
                                    'image_id'  => $image_id,
                                    'image'     => $image
                                    );

            $res = $this->common_model->update_upload_image($params);
            //print_r($res); die;

            if($res){

              $response = array(
                                'response_code' => 200,
                                'status'        => 'ok',
                                'message'       => 'success'
                               );

            }
            else {

              $response = array(
                                'response_code' => 200,
                                'status'        => 'ok',
                                'message'       => 'success'
                               );
            }
          }

        //echo json response
        echo json_encode($response);
      }
        
    private function base64_file_upload($path, $base64String){
        try {
            $base64String = base64_decode($base64String);     
            header('Content-Type: image; charset=utf-8');
            $file = fopen($path, 'wb');
            fwrite($file, $base64String);
            fclose($file);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
        
    private function normal_file_upload($source, $destination){
        try{
            if(@move_uploaded_file($source, $destination)){
                return true;
            }
            return false;
        } catch (Exception $ex) {
            return false;
        }
    }
}