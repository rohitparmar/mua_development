<?php

/**
 * Class to handle all apis
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Referral extends Layout{
  protected $app;

  public function __construct($app) {
    $this->app = $app;
    $this->load_library('Api/Validation');
    $this->load_model('Api/Referral_model');
    $this->load_model('Api/Artist_model');
    $this->load_model('Api/Category_model');
  }
   
 


/**
  * URL - 
  * FUNCTION - use_referral_code
  * METHOD - POST
*/
  public function use_referral_code($request, $response, $args) {

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id        = isset($_POST['user_id'])?$_POST['user_id']:"";
    $user_type      = isset($_POST['user_type'])?$_POST['user_type']:"";
    $referral_code  = isset($_POST['referral_code'])?$_POST['referral_code']:"";
    $device_id      = isset($_POST['device_id'])?$_POST['device_id']:"";
   
    
    /*Validation*/
    /*$this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('user_type',$user_type,'required');
    $this->validation->rules('referral_code',$referral_code,'required');
    $this->validation->rules('device_id',$device_id,'required');*/
    
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      $user_id = decode_base64($user_id);

      $params = (object)array(
                              'user_id'       => $user_id,
                              'user_type'     => $user_type,
                              'referral_code' => $referral_code,
                              'device_id'     => $device_id,
                              'date'          => date("Y-m-d h:i:sa")
                             );
      
      // get the detail by user_id
      $user = $this->Referral_model->add_referral_code($params);

     if ($user == 'this code allready used'){
        
        $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'code already used.'  
                              );
              
          
      }elseif($user == 'this code be exits in table'){ 
          // user credentials are wrong
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'code not match.'
                            );
      } elseif($user == 'success'){ 
          // user credentials are wrong
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'success'
                            );
      } 
    }
    //echo json response
    echo json_encode($response);
  }




/**
  * Create edit_client_profile_get
  * url - /create_client
*/
  public function use_referral_code_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
  * URL - 
  * FUNCTION - get_referral_code
  * METHOD - POST
*/
  public function get_referral_code($request, $response, $args) {

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    
    $referral_code  = isset($_POST['referral_code'])?$_POST['referral_code']:"";
   
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      $result = $this->Referral_model->get_referral_code($referral_code,$type=0);

      if ($result){
        $response = array(
                          'response_code' => 200,
                          'status'        => 'true',
                          );
      }
      else{ 
          
          $response = array(
                          'response_code' => 200,
                          'status'        => false,
                          );
      }
    }
    //echo json response
    echo json_encode($response);
  }




/**
  * Create get_referral_code_get
  * url - /create_client
*/
  public function get_referral_code_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


  public function get_user_account_info($request, $response, $args) {

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id  = isset($_POST['user_id'])?$_POST['user_id']:"";
   
    
    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      $user_id = decode_base64($user_id);
      
      // get the detail by user_id
      $result = $this->Referral_model->get_user_account_info($user_id);

     $response = array(
                        'response_code'   => 200,
                        'status'          => 'ok',
                        'data'            => $result
                    );
    }
    //echo json response
    echo json_encode($response);
  }

  /**
  * URL - http://bebebella.cloudapp.net/v1/api/add/user_account_info
  * FUNCTION - edit_user_account_info
  * METHOD - POST
*/
  public function edit_user_account_info($request, $response, $args) {

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id        = isset($_POST['user_id'])?$_POST['user_id']:"";
    $account_no     = isset($_POST['account_no'])?$_POST['account_no']:"";
    $rounting_no    = isset($_POST['rounting_no'])?$_POST['rounting_no']:"";
    $account_name   = isset($_POST['account_name'])?$_POST['account_name']:"";
    $paypal_id      = isset($_POST['paypal_id'])?$_POST['paypal_id']:"";
   
    
    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      $user_id = decode_base64($user_id);

      $params = (object)array(
                              'user_id'       => $user_id,
                              'account_no'    => $account_no,
                              'rounting_no'   => $rounting_no,
                              'account_name'  => $account_name,
                              'paypal_id'     => $paypal_id,
                              'date'          => date("Y-m-d")
                              
                             );
      
      // get the detail by user_id
      $user = $this->Referral_model->edit_user_account_info($params);

     if ($user == 1){
        
        $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success'
                              );
              
          
      }else{ 
          // user credentials are wrong
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'error'
                            );
      } 
    }
    //echo json response
    echo json_encode($response);
  }




/**
  * Create edit_user_account_info_get
  * url - /create_client
*/
  public function edit_user_account_info_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



/**
  * url : http://bebebella.cloudapp.net/v1/api/get/notification?user_id=MjQ3NTY4MDY4OTI=
  * Create function : get_notification
  * url - /create_client
*/
  public function get_notification($request, $response, $args){
    
    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $type    = isset($_POST['type'])?$_POST['type']:"current";
    $lang    = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';

    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('type',$type,'required');
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

        $result = '';

        if($type=='current' || $type=='history'){

            
          $result = $this->Referral_model->get_current_notification(decode_base64($user_id), $lang, $type);
            
         

          $userData = array();

          $count = 0;

            foreach ($result as $key => $value) {
              if($value['un_is_read'] == 'false'){
                $count++;
              }

              $result[$key]['un_id'] = encode_base64($value['un_id']);
              $result[$key]['un_fk_from_id'] = encode_base64($value['un_fk_from_id']);
              
              if($value['un_user_type'] == 'mua'){
                  $userData = $this->Referral_model->get_notification_name($value['un_fk_from_id'], $value['un_user_type']);

                  $userData['image'] = $userData['image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$userData['image'])?(BASE_URL.ARTIST_PROFILE_IMG.$userData['image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE; 

              }else if ($value['un_user_type'] == 'client') {

                  $userData = $this->Referral_model->get_notification_name($value['un_fk_from_id'], $value['un_user_type']);
                  $userData['image'] = $userData['image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$userData['image'])?(BASE_URL.CLIENT_PROFILE_IMG.$userData['image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;
              }

              $result[$key]['name'] = isset($userData['name'])?$userData['name']:'';
              $result[$key]['image'] = isset($userData['image'])?$userData['image']:'';

            }

            if ($result){
              
              $response = array(
                                'response_code' => 200,
                                'status'        => 'ok',
                                'message'       => 'success',
                                'count'         => $count,
                                'data'          => $result
                                );
            }else{ 
              $response = array(
                                'response_code' => 200,
                                'status'        => 'ok',
                                'message'       => 'success',
                                'count'         => $count,
                                'data'          => $result
                                );
            }
         
        }else{
          $response = array(
                            'response_code' => 400,
                            'status'        => 'error',
                            'message'       => 'type invalid.'
                            );
          echo json_encode($response);
          die;
        }
        
        
    }
    //echo json response
    echo json_encode($response);
  }


/**
  * Create get_notification_get
  * url - /create_client
*/
  public function get_notification_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }





/**
  * url : http://bebebella.cloudapp.net/v1/api/update/notification?user_id=MjQ3NTY4MDY4OTI=
  * Create function : get_notification
  * url - /create_client
*/
  public function update_notification($request, $response, $args){

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
    $lang    = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
    
    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

          /*=== get count current history in notification starts ===*/
          $current_data = $this->Referral_model->get_current_notification(decode_base64($user_id), $lang, 'current');

          $current_count = 0;

          foreach ($current_data as $key => $value) {
            if($value['un_is_read'] == 'false'){
              $current_count++;
            }
          }

          $history_data = $this->Referral_model->get_current_notification(decode_base64($user_id), $lang, 'history');
          $history_count = 0;

          foreach ($history_data as $key => $value) {
            if($value['un_is_read'] == 'false'){
              $history_count++;
            }
          }
          /*=== get count current history in notification ends ===*/



          $result = $this->Referral_model->update_notification(decode_base64($user_id));

          $response = array(
                            'response_code' => 200,
                            'status'        => 'ok',
                            'message'       => 'success',
                            'current_count' => $current_count,
                            'history_count' => $history_count
                            );
            
    }
    //echo json response
    echo json_encode($response);
  }




/**
  * Create update_notification_get
  * url - /create_client
*/
  public function update_notification_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


  public function add_gift_info($request, $response, $args){

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id  = isset($_POST['user_id'])?$_POST['user_id']:"";
    $request_id  = isset($_POST['request_id'])?$_POST['request_id']:"";
    $gender   = isset($_POST['gender'])?$_POST['gender']:"";
    $email    = isset($_POST['email'])?$_POST['email']:"";
    $age      = isset($_POST['age'])?$_POST['age']:"";
    $mobile   = isset($_POST['mobile'])?$_POST['mobile']:"";



    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('email',$email,'email|required');
    

    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{
            $params = (object)array(
                                    'user_id'     => decode_base64($user_id),
                                    'request_id'  => decode_base64($request_id),
                                    'gender'      => $gender,
                                    'email'       => $email,
                                    'age'         => $age,
                                    'mobile'      => $mobile
                                    );
            
            /*$result = $this->Referral_model->add_gift_info($params);*/

            $response = array(
                              'response_code' => 200,
                              'status'        => 'ok',
                              'message'       => 'success'
                              );
            
    }
    echo json_encode($response);


  }



/**
  * Create add_gift_info_get
*/
  public function add_gift_info_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



  public function user_promocode_info($request, $response, $args){



    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id  = isset($_POST['user_id'])?decode_base64($_POST['user_id']):"";

    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    
    

    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{



      $result = $this->Referral_model->user_promocode_info($user_id);

      $services = array();

      //print_r($result);

      foreach ($result as $key => $value) {


        $result[$key]['referral_id'] = encode_base64($value['referral_id']);

        $result[$key]['user_id']   = encode_base64($value['user_id']);
        $result[$key]['ur_id']     = encode_base64($value['ur_id']);
        $result[$key]['r_rebate']  = $value['r_rebate'];
    		$result[$key]['earning']  = ($value['active_status']>0)?$value['r_rebate']:'pending';
    		$result[$key]['payment_made']  = ($value['ur_paid'] == 'true')?true:false;
        $result[$key]['is_used']   = (bool)$value['is_used'];
        $result[$key]['user_image'] = $value['user_image']?(file_exists(APP_DIR.'/'.CLIENT_PROFILE_IMG.$value['user_image'])?(BASE_URL.CLIENT_PROFILE_IMG.$value['user_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;  

        if($value['services_name']){
          
          $services =    (object)array(
                                   'services_id'    => encode_base64($value['ur_services_id']),
                                   'services_name'  => $value['services_name']
                                   );
         
        }else{
          $services =    (object)array(
                                   'services_id'    => "",
                                   'services_name'  => ""
                                   );
           

        }
        
        $result[$key]['services'] = $services;

        unset($result[$key]['services_name']);
        unset($result[$key]['ur_services_id']);
      }

      $response = array(
                        'response_code' => 200,
                        'status'        => 'ok',
                        'message'       => 'success',
                        'data'          => $result
                        );
            
    }
    echo json_encode($response);

  }


  /**
  * Create add_gift_info_get
*/
  public function user_promocode_info_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }




public function mua_promocode_info($request, $response, $args){



    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $mua_id  = isset($_POST['mua_id'])?decode_base64($_POST['mua_id']):"";

    /*Validation*/
    $this->validation->rules('mua_id',$mua_id,'required');
    
    

    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{


            
      $result = $this->Referral_model->mua_promocode_info($mua_id);

      $services = array();

      foreach ($result as $key => $value) {


        $result[$key]['referral_id'] = encode_base64($value['referral_id']);

        $result[$key]['user_id']   = encode_base64($value['user_id']);
        $result[$key]['ur_id']     = encode_base64($value['ur_id']);
        $result[$key]['r_rebate']  = $value['r_rebate'];
		$result[$key]['earning']  = ($value['active_status']>0)?$value['r_rebate']:'pending';
		$result[$key]['payment_made']  = ($value['ur_paid'] == 'true')?true:false;
        $result[$key]['is_used']   = (bool)$value['is_used'];
        $result[$key]['user_image'] = $value['user_image']?(file_exists(APP_DIR.'/'.ARTIST_PROFILE_IMG.$value['user_image'])?(BASE_URL.ARTIST_PROFILE_IMG.$value['user_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;  

        
      }

      $response = array(
                        'response_code' => 200,
                        'status'        => 'ok',
                        'message'       => 'success',
                        'data'          => $result
                        );
            
    }
    echo json_encode($response);

  }


  /**
  * Create add_gift_info_get
*/
  public function mua_promocode_info_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


  /*
  ||========================================================================
  || Function get_used_referral_list 
  ||========================================================================
  */
  public function get_used_referral_list($request, $response, $args){

    if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id  = isset($_POST['user_id'])?decode_base64($_POST['user_id']):"";

    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{

      $result = $this->Referral_model->get_used_referral_list($user_id);

      if($result){
          $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success',
                          'data'          => $result
                          );
      }else{
          $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => 'success',
                          'data'          => $result
                          );
      }
      


    }

    //echo JSON response
      echo json_encode($response);

  }


  /*
  ||========================================================================
  || Create get_used_referral_list_get
  ||========================================================================
  */
  public function get_used_referral_list_get($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }



  /*
  ||=============================================
  || @Function ios_notification_count
  ||=============================================
  */
  public function ios_notification_count(){

      if(!$_POST){
        $response = array(
                          'response_code' => 400,
                          'status'        => 'error',
                          'message'       => 'some fields required'
                          );
        echo json_encode($response);
        die;
      } 

      $response = array();
      
      $user_id  = isset($_POST['user_id'])?decode_base64($_POST['user_id']):"";

      /*Validation*/
      $this->validation->rules('user_id',$user_id,'required');
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{
      $result = current($this->Referral_model->get_ios_notification_count($user_id));


      $response = array(
                      'response_code' => 200,
                      'status'        => 'ok',
                      'message'       => 'success',
                      'count'         => (isset($result) && $result)?$result['notification_count']:0
                      );
    }

    //echo JSON response
      echo json_encode($response);
  }


  public function celebrity_code(){
      if(!$_POST){
      $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => 'some fields required'
                        );
      echo json_encode($response);
      die;
    } 

    $response = array();
    
    $user_id        = isset($_POST['user_id'])?decode_base64($_POST['user_id']):"";
    $device_id      = isset($_POST['device_id'])?$_POST['device_id']:"";
    $usertype       = isset($_POST['usertype'])?$_POST['usertype']:"";
    $celebrity_code = isset($_POST['celebrity_code'])?$_POST['celebrity_code']:"";

    /*Validation*/
    $this->validation->rules('user_id',$user_id,'required');
    $this->validation->rules('device_id',$device_id,'required');
    $this->validation->rules('celebrity_code',$celebrity_code,'required');
    $this->validation->rules('usertype',$usertype,'required');
    
    if($this->validation->error){
      $response["error"] = true;
      $response["message"] = array_pop($this->validation->error);
    }
    else{
      
        $result = $this->Referral_model->get_referral_code($celebrity_code,$type=1);      

        if(isset($result) && $result){

            $params = (object)array(
                                  'user_id'        => $user_id,
                                  'device_id'      => $device_id,
                                  'celebrity_code' => $celebrity_code,
                                  'r_id'           => (isset($result['r_id']) && $result['r_id'])?$result['r_id']:'',
                                  'usertype'       => $usertype
                              );


            
            $used_celebrity_code = $this->Referral_model->get_user_used_celebrity_code_data($params);

            if(isset($used_data) && $used_data){

                $response = array(
                          'response_code' => 400,
                          'status'        => 'error',
                          'message'       => CELEBRITY_CODE_ER_1_ENG,
                          'message_spn'   => CELEBRITY_CODE_ER_1_SPN,
                          );

            }else{

              $used_data = $this->Referral_model->check_valid_celebrity_code($params);

              if(isset($used_data) && $used_data){

                $response = array(
                          'response_code' => 400,
                          'status'        => 'error',
                          'message'       => CELEBRITY_CODE_ER_1_ENG,
                          'message_spn'   => CELEBRITY_CODE_ER_1_SPN,
                          );
                  
              }else{

                 $insert_id = $this->Referral_model->insert_celebrity_code($params);

                 $response = array(
                          'response_code' => 200,
                          'status'        => 'ok',
                          'message'       => CELEBRITY_CODE_SU_ENG,
                          'message_spn'   => CELEBRITY_CODE_SU_SPN,
                          );

              }

            }

            

        }else{

          $response = array(
                        'response_code' => 400,
                        'status'        => 'error',
                        'message'       => CELEBRITY_CODE_ER_ENG,
                        'message_spn'   => CELEBRITY_CODE_ER_SPN,
                        );

        }

        
    }

    //echo JSON response
      echo json_encode($response);
  }


  public function get_celebrity_code($request, $response, $args) {
    $return["error"] = true;
    $return["message"] = "Invalid url";
    echo json_encode($return);
  }


}