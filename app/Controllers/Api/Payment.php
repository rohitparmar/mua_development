<?php

/**
 * Class to handle all apis
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Payment extends Layout {

    protected $app;

    public function __construct($app) {
        $this->app = $app;
        $this->load_library('Api/Braintree');
        $this->load_library('Api/Push_notification');
        $this->load_library('Api/Validation');
        $this->load_model('Api/Payment_model', 'payment_model');
        $this->load_model('Api/Booking_model', 'booking_model');
        $this->load_model('Api/Artist_model', 'artist_model');
        $this->load_model('Api/Referral_model', 'Referral_model');
        $this->load_library('Api/Email', 'email');
        $this->load_model('Api/User_model', 'user_model');
    }

    /*
     * @author    - Synergytop-amit
     * @function  - set_user_default_card
     * @param     - user_id, card_id, status
     * @response  - json of an array()
     */




    public function set_user_default_card($request, $response, $args) {
        $response = array();
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : false;
        $card_id = isset($_POST['card_id']) ? $_POST['card_id'] : false;
        $status = true;

        $result = $this->payment_model->set_user_default_card(decode_base64($user_id), decode_base64($card_id), $status);
        $response = array(
            'response_code' => 200,
            'status' => 'ok',
            'message' => 'success'
        );
        echo json_encode($response);
    }

    /*
     *
     * @author    - synergytop-amit
     * @function  - update_user_card_details
     * @param     - $user_id, $card_token, $cvv ,$expirationMonth, $expirationYear, $number, $streetAddress , $extendedAddress,   
      $locality , $region, $postalCode, $card_id
     * @response  - json of an array()
     * @for-help  - url - http://bebebella.cloudapp.net/v1/api/payment/update/user_card_details?user_id=MjA2MzA2NzI0MQ==&card_token=5z
      k8s2&cvv=404&expirationMonth=11&expirationYear=2018&number=4012000033330026&streetAddress=indore&extendedAddress=indore&locality=indore&region=indore&postalCode=452010&card_id=MTg1Njc2MDUxNjk=
     */

    public function update_user_card_details($request, $response, $args) {
        /*
          needs to apply required validation
         */
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : false;
        $card_token = isset($_POST['card_token']) ? $_POST['card_token'] : false;
        $cvv = isset($_POST['cvv']) ? $_POST['cvv'] : false;
        $expirationMonth = isset($_POST['expirationMonth']) ? $_POST['expirationMonth'] : false;
        $expirationYear = isset($_POST['expirationYear']) ? $_POST['expirationYear'] : false;
        $number = isset($_POST['number']) ? $_POST['number'] : false;
        $streetAddress = isset($_POST['streetAddress']) ? $_POST['streetAddress'] : false;
        $extendedAddress = isset($_POST['extendedAddress']) ? $_POST['extendedAddress'] : false;
        $locality = isset($_POST['locality']) ? $_POST['locality'] : false;
        $region = isset($_POST['region']) ? $_POST['region'] : false;
        $postalCode = isset($_POST['postalCode']) ? $_POST['postalCode'] : false;

        $card_details = (object) array(
                    'user_id' => $user_id,
                    'card_token' => $card_token,
                    'cvv' => $cvv,
                    'expirationMonth' => $expirationMonth,
                    'expirationYear' => $expirationYear,
                    'number' => $number,
                    'streetAddress' => $streetAddress,
                    'extendedAddress' => $extendedAddress,
                    'locality' => $locality,
                    'region' => $region,
                    'postalCode' => $postalCode
        );

        try {
            //update card using braintree api to braintree server
            $result = $this->Braintree->update_card_details($card_details);
            //if update success then get info
            if (isset($result->success) && ($result->success == 'true')) {
                $paymentMethod = isset($result->paymentMethod) ? $result->paymentMethod : '';
                $maskedNumber = isset($paymentMethod->maskedNumber) ? $paymentMethod->maskedNumber : '';

                //now replace first 6 digits with star (*)
                if ($maskedNumber) {
                    $maskedNumber = '******' . substr($maskedNumber, 6, strlen($maskedNumber));
                }

                //now create param1 for response
                $param1 = (object) array(
                            'user_id' => decode_base64($user_id),
                            'response_data' => $result,
                            'card_token' => $card_token,
                            'maskedNumber' => $maskedNumber
                );

                //update to database to our server database
                $res = $this->payment_model->update_user_card_details($param1);
                $result = array(
                    'response_code' => 200,
                    'status' => 'success',
                    'message' => 'card updated'
                );
            } else {
                $result = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'update failed'
                );
            }
        } catch (Exception $e) {
            $result = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'update fail'
            );
        }
        return json_encode($result);
    }

    /*
      @url - http://bebebella.cloudapp.net/v1/api/payment/get_user_card_details_list?user_id=MjA2MzA2NzI0MQ==
      @function - get_user_card_details_list
      @param - $user_id
      @response - json of an array()
     */

    public function get_user_card_details_list($request, $response, $args) {

        $response = array();

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : false;

        $result = $this->payment_model->get_user_card_details_list(decode_base64($user_id));

        foreach ($result as $key => $value) {
            $result[$key]['bt_id'] = encode_base64($value['bt_id']);
        }

        if ($result) {
            $response = array(
                'response_code' => 200,
                'status' => 'ok',
                'message' => 'success',
                'data' => $result
            );
        } else {
            $response = array(
                'response_code' => 200,
                'status' => 'ok',
                'message' => 'success',
                'data' => $result
            );
        }
        // echo json response  
        echo json_encode($response);
    }

    /*
      @url - http://bebebella.cloudapp.net/v1/api/payment/update/user_card_details?user_id=MjA2MzA2NzI0MQ==&card_token=5zk8s2&cvv=404&expirationMonth=11&expirationYear=2018&number=4012000033330026&streetAddress=indore&extendedAddress=indore&locality=indore&region=indore&postalCode=452010&card_id=MTg1Njc2MDUxNjk=

      @function - update_user_card_details

      @param - $user_id, $card_token, $cvv ,$expirationMonth, $expirationYear, $number, $streetAddress , $extendedAddress, $locality , $region, $postalCode, $card_id
     */

    public function delete_user_card_details($request, $response, $args) {
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : false;
        $card_token = isset($_POST['card_token']) ? $_POST['card_token'] : false; //'ask from android'

        $response = array();

        try {
            $result = $this->Braintree->braintree_delete_card_token($card_token);

            if (isset($result->success) && ($result->success == 'true')) {
                $res = $this->payment_model->delete_user_card_details(decode_base64($user_id), $card_token);
                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success'
                );
            }
        } catch (Exception $e) {
            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'failed'
            );
        }
        return json_encode($response);
    }

    /*
      FUNCTION : add_user_card_details
      @PARAM   : user_id, customerId, nonce
     */

    public function add_user_card_details($request, $response, $args) {

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : false;
        $nonce = isset($_POST['nonce']) ? $_POST['nonce'] : false;

        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('nonce', $nonce, 'required');

        if ($this->validation->error) {
            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {
            try {
                $result = $this->Braintree->add_user_card_details($nonce);

                if (isset($result->success) && $result->success) {
                    $customer = isset($result->customer) ? $result->customer : '';
                    if (sizeof($customer->creditCards) > 0) {
                        $token = isset($customer->creditCards[0]->token) ? $customer->creditCards[0]->token : '';
                        $maskedNumber = $customer->creditCards[0]->maskedNumber;
						$cardTypeImg = $customer->creditCards[0]->imageUrl;
                    } else if (sizeof($customer->paypalAccounts) > 0) {
                        $token = isset($customer->paypalAccounts[0]->token) ? $customer->paypalAccounts[0]->token : '';
                        $maskedNumber = isset($customer->creditCards[0]->maskedNumber) ? $customer->creditCards[0]->maskedNumber : '';
						$cardTypeImg = $customer->creditCards[0]->imageUrl;
                    }

                    //now replace first 6 digits with star (*)
                    if ($maskedNumber) {
                        $maskedNumber = '******' . substr($maskedNumber, 6, strlen($maskedNumber));
                    }

                    $param = (object) array(
                                'user_id' => decode_base64($user_id),
                                'response_data' => $result,
                                'token' => $token,
                                'maskedNumber' => $maskedNumber,
								'cardTypeImg' => $cardTypeImg,
                                'date' => date('Y-m-d H:i:s'),
                                'bt_is_default' => 'true'
                    );

                    $res = $this->payment_model->insert_user_card_details($param);
                    //create response
                    $response = array(
                        'response_code' => 200,
                        'status' => 'success',
                        'message' => 'card added successfully'
                    );
                } else {
                    $response = array(
                        'response_code' => 400,
                        'status' => 'error',
                        'message' => 'failed'
                    );
                }
            } catch (Exception $e) {
                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'failed'
                );
            }
        }
        return json_encode($response);
    }

    /*
      @author - Synergytop-Abhay
      @function - payment_with_token
      @params - user_id, amount
      @response - boolean
     */

    private function payment_with_token($u_id, $u_type, $booking_amount, $booking_id = false, $requset_id = false, $devices_type = 'android') {
        //get current default taken
        $token_details = $this->payment_model->get_user_default_token($u_id);

        

        $response = 'credit-card-issue';
        //proceed if default valid token exists
        if (is_array($token_details) && isset($token_details['bt_token'])) {
            $token = $token_details['bt_token'];
            try {
                $amount=explode('.',$booking_amount);
                $final_amount='';
                if(isset($amount[1])){
                    if(strlen($amount[1])>2){
                        $amount[1]=substr($amount[1],0,2);
                    }
                    $final_amount=$amount[0].'.'.$amount[1];
                }
                else{
                    $final_amount=$amount[0];
                }
                $param = (object) array(
                            'amount' => $final_amount,
                            'token' => $token
                );
                
                $result = $this->Braintree->payment_with_token($param);
                if (isset($result->success) && $result->success) {
                    //now update booking status
                    if ($u_type == 'client') {
                        $params = (object) array(
                                    'user_id' => $u_id,
                                    'requsetId' => $requset_id,
                                    'action' => 'accepted'
                        );

                        $this->payment_model->mua_action($params);
                    }
                    $userData = $this->booking_model->get_mua_gcmID($u_id);
                    //$this->Push_notification->send_notification_payment($userData, $devices_type);
                    return $result;
                } else {
                    $response = false;
                }
            } catch (Exception $e) {
                $response = false;
            }
        }
        return $response;
    }

    /*
     * method: post
     * param: nonce
      for client = array(
      'user_type',
      'u_id',
      'mua_id',
      'booking_id'
      )
      for mua = array(
      'user_type'
      'mua_id',
      'plan_id'
      )
     */

    public function braintree_checkout($request, $response, $args) {
        $referral_ids       = array();
        $referrel_rebate    = 0;
        $tot_rebate_amount  = $paid_booking_amount = 0; 
        $referrelIDs        = '';
        
        $user_type          = isset($_POST['user_type']) ? $_POST['user_type'] : false;
        $u_id               = isset($_POST['client_id']) ? decode_base64($_POST['client_id']) : false;
        $mua_id             = isset($_POST['mua_id']) ? decode_base64($_POST['mua_id']) : false;
        $booking_id         = isset($_POST['booking_id']) ? decode_base64($_POST['booking_id']) : false;
        $requset_id         = isset($_POST['requset_id']) ? decode_base64($_POST['requset_id']) : false;
        $plan_id            = isset($_POST['plan_id']) ? decode_base64($_POST['plan_id']) : false;
        $devices_type       = isset($_POST['devices_type']) ? $_POST['devices_type'] : 'android';

        $payment_mode       = isset($_POST['payment_mode']) ? ($_POST['payment_mode']) : 2;
        $payment_status     = isset($_POST['action']) ? ($_POST['action']) : 1;
        //for promocode if can be used or not
        $use_promocode      = isset($_POST['use_promocode']) ? $_POST['use_promocode'] : ($payment_status == $payment_mode ? true : false);

        $lang         = isset($_POST['lang'])&&($_POST['lang'] != '')?$_POST['lang']:'eng';
        
        //check if mua/artist is active for getting current work (He must be active and paid member for current month)
        $mua_subscription   = $this->payment_model->is_mua_subscription_active($mua_id);


        

        if ($user_type === 'mua' && $plan_id && $mua_id) {
            if ($mua_subscription) {
                //deny booking process
                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'You already subscribed a plan for current month'
                );
            } else {
                //check if plan id exists for subscription
                $subscription_plan_amount = $this->payment_model->get_subscription_plan_amount($plan_id);
                //if subscription plan amout exists then proceed for payment process
                if ($subscription_plan_amount) {
                    //now make payment to admin
                    //$result = $this->Braintree->create_payment($subscription_plan_amount, $nonceFromTheClient);
                    $u_type = 'mua';
                    $result = $this->payment_with_token($mua_id, $u_type, $subscription_plan_amount, $booking_id = false, $requset_id = false, $devices_type);

                    //check if payment is successfull
                    if ($result === 'credit-card-issue') {
                        $response = array(
                            'response_code' => 410,
                            'status' => 'error',
                            'message' => 'credit card is not set to default or some issue with credit card'
                        );
                    } else if (isset($result->success) && $result->success) {

                        $transaction_id = $result->transaction->id;
                        $transaction_details = serialize(array(
                            'token' => $result->transaction->creditCardDetails->token,
                            'expirationDate' => $result->transaction->creditCardDetails->expirationDate,
                            'maskedNumber' => $result->transaction->creditCardDetails->maskedNumber
                        ));

                        //now put information into database table since payment is successfull
                        $info = (object) array(
                                    'plan_id' => $plan_id,
                                    'artist_id' => $mua_id,
                                    'paid_amount' => $subscription_plan_amount,
                                    'service_amount' => $subscription_plan_amount,
                                    'transaction_id' => $transaction_id,
                                    'transaction_details' => $transaction_details
                        );
                        $this->payment_model->create_payment_history_for_mua($info);

                        $muaData    = $this->user_model->get_user_id($mua_id);

                        $data['mua_name']     = isset($muaData['u_name'])?$muaData['u_name']:'';
                        $data['mua_email']    = isset($muaData['u_email'])?$muaData['u_email']:'';
                        $data['subscription_amount']  = isset($subscription_plan_amount)?$subscription_plan_amount:'';

                        $sub = PAYMENT_MSG;

                        $msg = $this->_view('Front/Email/Payment/payment_template.php', $data, true);
                        $this->email->to($data['mua_email']);
                        $this->email->subject($sub);
                        $this->email->messageType('html');
                        $this->email->message($msg);
                        $this->email->send();

                        $response = array(
                            'response_code' => 200,
                            'status' => 'success',
                            'message' => 'payment made successfully'
                        );
                    } else {
                        $response = array(
                            'response_code' => 400,
                            'status' => 'error',
                            'message' => 'payment unsuccessful'
                        );
                    }
                } else {
                    //for free subscription
                    //now put information into database table since payment is successfull
                    
                    
                        $subscription_plan_amount = $this->payment_model->get_subscription_plan_amount($plan_id);
                        $info = (object) array(
                                    'plan_id' => $plan_id,
                                    'artist_id' => $mua_id,
                                    'paid_amount' => 0,
                                    'service_amount' => 0,
                                    'transaction_id' => 0,
                                    'transaction_details' => "free"
                        );
                        $this->payment_model->create_payment_history_for_mua($info);
                        
                    
                        $response = array(
                            'response_code' => 200,
                            'status' => 'success',
                            'message' => 'Your free subscription plan activated successfully'
                        );
                    
                }
            }
        } else if ($user_type == 'client' && $booking_id && $u_id && $mua_id && $requset_id) {


            /*==== calculate time difference with continue two payment starts ===*/
            $last_time = $this->payment_model->get_last_payment_time($u_id);

            if(isset($last_time) && $last_time){

                $time  = strtotime($last_time['cpi_created']);
            
                $final = date("Y-m-d H:i:s", strtotime("+".PAYMENT_INTERVAL." seconds", $time));

                $date_a = new DateTime($final);
                $date_b = new DateTime(date("Y-m-d H:i:s"));

                $interval = date_diff($date_a,$date_b);
               
                $total_time = $interval->format('%i:%s');

                if(strtotime(date("Y-m-d H:i:s")) <= strtotime($final)){
                    /*$response = array(
                                    'response_code' => 400,
                                    'status'        => 'error',
                                    'time'          => isset($total_time)?$total_time:'0',
                                    'message'       => str_replace(PAYMENT_INTERVAL, $total_time, TIME_DURATION_ENG),
                                    'message_spn'   => str_replace(PAYMENT_INTERVAL, $total_time, TIME_DURATION_SPN)
                                );*/

                    $response = array(
                                    'response_code' => 400,
                                    'status'        => 'error',
                                    'time'          => isset($total_time)?$total_time:'0',
                                    'message'       => (isset($payment_status) && $payment_status==1)?str_replace(PAYMENT_INTERVAL, $total_time, TIME_DURATION_ENG):str_replace(PAYMENT_INTERVAL, $total_time, TIME_DURATION_END_ENG),
                                    'message_spn'   => (isset($payment_status) && $payment_status==1)?str_replace(PAYMENT_INTERVAL, $total_time, TIME_DURATION_SPN):str_replace(PAYMENT_INTERVAL, $total_time, TIME_DURATION_END_SPN)
                                );

                    echo json_encode($response);
                    die;
                }
            }
            /*==== calculate time difference with continue two payment ends ===*/

            






            $free = false;
            if (!$mua_subscription) {
                $mua_subscription = true;
                $free = true;
            }

            if ($mua_subscription) {
                //check if already booked
                $is_booked = $this->payment_model->is_already_paid_for_current_booking($u_id, $mua_id, $booking_id, $payment_mode, $payment_status);
                //print_r($is_booked); die;
                if ($is_booked) {
                    //deny booking process
                    $response = array(
                        'response_code' => 402,
                        'status' => 'error',
                        'message' => 'you have already booked for current service'
                    );
                } else {
                    //accept booking process
                    //now get mua price for related category/booking_id $distance
                     $booking_amount = $this->payment_model->get_booking_price($booking_id);
                    
                    if ($booking_amount) {
                        
                        /*Start code Abhay SIR*/

                        //get booking info
                        //$BOOK_INFO = $this->payment_model->get_booking_info($booking_id); comment on 11-may-2017
                        //total amount
                        //$paid_booking_amount = round(ceil($booking_amount + $BOOK_INFO['cr_mua_rate_per_mile'] * $BOOK_INFO['cr_booking_time_distance']) / $payment_mode, 2);
                        //as per Tiffany - dated 06-04-2017
                        //$paid_booking_amount = round(($booking_amount + $BOOK_INFO['cr_mua_rate_per_mile'] * $BOOK_INFO['cr_booking_time_distance']) / $payment_mode, 2); comment on 11-may-2017

                        /*Ends code Abhay SIR*/




                        /*Start code amit*/

                        $BOOK_INFO = $this->payment_model->get_booking_info($booking_id);

                        if(isset($BOOK_INFO['cr_com_grand_price']) && $BOOK_INFO['cr_com_grand_price']){
                            $paid_booking_amount = ($BOOK_INFO['cr_com_grand_price']/2);
                        }

                      


                        /*Ends code amit*/
                        
                        //now use referrel code rebate to reduce paid amount 
                        if ($use_promocode) {
                            $referral_ids = array();
                            $referrel_rebate = 0;
                            
                            $tot_rebate = 0;
                            $PROMOCODE_INFO = $this->Referral_model->get_promocode_info_used_by_other($u_id);
                            if (count($PROMOCODE_INFO)) {
                                foreach ($PROMOCODE_INFO as $r) {
                                    //print_r($r);
                                    $referrel_rebate = $r['rr_rebate'];
                                    $tot_rebate = ($tot_rebate + $referrel_rebate);
                                    if ($paid_booking_amount >= $tot_rebate) {
                                        $referral_ids[] = $r['ur_id'];
                                    }
                                }
                            }
                            
                            $tot_rebate_amount = count($referral_ids)*$referrel_rebate;
                            //$this->Referral_model->make_used_promocode_used($requset_id, $referral_ids, $referrel_rebate, $paid_booking_amount);
                            $this->Referral_model->make_used_promocode_used($requset_id, $referral_ids);
                            $paid_booking_amount = $paid_booking_amount - $tot_rebate_amount;
                            $referrelIDs = implode(',',$referral_ids );
                        }



                        //now make payment to mua
                        $u_type = 'client';
                        $result = $this->payment_with_token($u_id, $u_type, $paid_booking_amount, $booking_id, $requset_id, $devices_type);


                        if ($result === 'credit-card-issue') {
                            $response = array(
                                'response_code' => 410,
                                'status' => 'error',
                                'message' => 'credit card is not set to default or some issue with credit card'
                            );
                        } else {
                            //keep response in a file
                            //check if payment is successfull
                            if (isset($result->success) && $result->success) {
                                $transaction_id = $result->transaction->id;
                                $transaction_details = ''; // serialize($result->transaction->_attributes);
                                //now put information into database table since payment is successfull
                                $token = $result->transaction->creditCardDetails->token;
                                $expirationDate = $result->transaction->creditCardDetails->expirationDate;
                                $maskedNumber = $result->transaction->creditCardDetails->maskedNumber;

                                $info = (object) array(
                                            'client_id'                     => $u_id,
                                            'artist_id'                     => $mua_id,
                                            'booking_id'                    => $booking_id,
                                            'paid_amount'                   => $paid_booking_amount,
                                            'booking_amount'                => $booking_amount,
                                            'transaction_id'                => $transaction_id,
                                            'transaction_details'           => $transaction_details,
                                            'token'                         => $token,
                                            'expirationDate'                => $expirationDate,
                                            'maskedNumber'                  => $maskedNumber,
                                            'cpi_payment_mode'              => $payment_mode,
                                            'cpi_payment_action'            => $payment_status,
                                            'is_free'                       => $free,
                                            'booking_time_distance'         => $BOOK_INFO['cr_booking_time_distance'],
                                            'booking_time_rate_per_mile'    => $BOOK_INFO['cr_mua_rate_per_mile'],
                                            'cpi_total_rebate_amount'       => $tot_rebate_amount,
                                            'cpi_rebate_amount'             => $referrel_rebate,
                                            'cpi_fk_ur_ids'                 => $referrelIDs
                                );

                                $this->payment_model->create_payment_history_for_client($info);

                                //now update request status and send notification
                                if ($payment_mode == $payment_status) {
                                    $params = (object) array(
                                                'user_id' => $u_id,
                                                'mua_id' => $mua_id,
                                                'requsetId' => $requset_id,
                                                'action' => 'finished'
                                    );
                                    //$result = $this->artist_model->mua_action($params);
                                } else {
                                    $params = (object) array(
                                                'user_id' => $u_id,
                                                'mua_id' => $mua_id,
                                                'requsetId' => $requset_id,
                                                'action' => 'accepted'
                                    );

                                    //$result = $this->artist_model->mua_action($params);
                                    //$userData = $this->booking_model->get_mua_gcmID($u_id);
                                    //$this->Push_notification->send_notification_action($userData, 'accepted', $requset_id, $mua_id, $devices_type);
                                    //$this->Push_notification->send_notification_action($userData, $params->action, $requset_id, $mua_id, $devices_type);
                                }
                                
                                $result = $this->artist_model->mua_action($params);
                                $userData = $this->booking_model->get_mua_gcmID($u_id);
                                $this->Push_notification->send_notification_action($userData, $params->action, $requset_id, $mua_id, $devices_type, $lang);

                                

                                
                                /* --- Starts send E-mail process--- */

                                $muaData    = $this->user_model->get_user_id($u_id);

                                $data['mua_name']     = isset($muaData['u_name'])?$muaData['u_name']:'';
                                $data['mua_email']    = isset($muaData['u_email'])?$muaData['u_email']:'';


                                $data['paid_booking_amount']       = isset($paid_booking_amount)?$paid_booking_amount:'0';
                                $data['booking_amount']            = isset($booking_amount)?$booking_amount:'0';
                                $data['cr_booking_time_distance']  = isset($BOOK_INFO['cr_booking_time_distance'])?$BOOK_INFO['cr_booking_time_distance']:'0';
                                $data['cr_mua_rate_per_mile']      = isset($BOOK_INFO['cr_mua_rate_per_mile'])?$BOOK_INFO['cr_mua_rate_per_mile']:'0';
                                $data['tot_rebate_amount']         = isset($tot_rebate_amount)?$tot_rebate_amount:'0';
                                $data['referrel_rebate']           = isset($referrel_rebate)?$referrel_rebate:'0';
                                

                                $sub = PAYMENT_MSG;

                                $msg = $this->_view('Front/Email/Payment/client_payment_template.php', $data, true);
                                $this->email->to($data['mua_email']);
                                $this->email->subject($sub);
                                $this->email->messageType('html');
                                $this->email->message($msg);
                                $this->email->send();

                                /* --- Ends send E-mail process--- */



                                //now create response for success
                                $response = array(
                                    'response_code' => 200,
                                    'status' => 'success',
                                    'message' => 'Payment made successfull'
                                );
                            } else {
                                $response = array(
                                    'response_code' => 406,
                                    'status' => 'error',
                                    'message' => 'Payment failed'
                                );
                            }
                        }
                    } else {
                        $response = array(
                            'response_code' => 403,
                            'status' => 'error',
                            'message' => 'invalid booking amount'
                        );
                    }
                }
            }
        } else {
            $response = array(
                'response_code' => 404,
                'status' => 'error',
                'message' => 'invalid user type'
            );
        }
        echo json_encode($response);
    }

    public function braintree_token($request, $response, $args) {
        try {
            $clientToken = Braintree_ClientToken::generate();
            $res = array(
                'clientToken' => $clientToken,
                'error' => 0,
                'message' => ''
            );
        } catch (Exception $e) {
            $res = array(
                'clientToken' => '',
                'error' => 1,
                'message' => $e->getMessage()
            );
        }
        echo json_encode($res);
    }

    /* end here */


    /*
      @url - http://bebebella.cloudapp.net/v1/api/payment/set_user_default_card?user_id=MjA2MzA2NzI0MQ==&card_id=MjA2MzA2NzI0MQ==&status=true
      @function - set_user_default_card
      @param - $user_id, $card_id ,$status
     */



    /* <!-- not needed now --> */

    public function add_customerId($request, $response, $args) {

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : false;
        $firstName = isset($_POST['firstName']) ? $_POST['firstName'] : false;
        $lastName = isset($_POST['lastName']) ? $_POST['lastName'] : false;
        $company = isset($_POST['company']) ? $_POST['company'] : false;
        $email = isset($_POST['email']) ? $_POST['email'] : false;
        $phone = isset($_POST['phone']) ? $_POST['phone'] : false;
        $fax = isset($_POST['fax']) ? $_POST['fax'] : false;
        $website = isset($_POST['website']) ? $_POST['website'] : false;

        $param = (object) array(
                    'user_id' => $user_id,
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'company' => $company,
                    'email' => $email,
                    'phone' => $phone,
                    'fax' => $fax,
                    'website' => $website
        );

        $result = $this->Braintree->add_customerId($param);
        print_r($result);
        die;

        file_put_contents("nonce/" . time() . ".txt", $result);

        //print_r($result); die;


        return json_encode($result);

        //echo json_encode($result);  
    }

    public function get_user_card_details($request, $response, $args) {

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : false; //ask from android
        $card_token = isset($_POST['card_token']) ? $_POST['card_token'] : false; //get card token from 



        $user_token = $this->payment_model->get_token($user_id);
        //print_r($user_token); die;
        $user_token = $user_token ? $user_token : (object) array();



        $result = array(
            'response_code' => 200,
            'status' => 'ok',
            'message' => 'success',
            'data' => $user_token
        );




        //$result = $this->Braintree->get_braintree_payment_details($card_token);
        return json_encode($result);
    }

    public function get_payment($request, $response, $args) {
        $result = $this->payment_model->get_payment();
        print_r($result);
        die;
    }

    public function user_payment_info($request, $response, $args) {

        if (!$_POST) {
            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();

        $user_id = isset($_POST['user_id']) ? decode_base64($_POST['user_id']) : false; //ask from android
        //$userData = $this->booking_model->get_mua_gcmID(decode_base64($user_id));
        $result = $this->payment_model->user_payment_info($user_id);

        $result['card_status'] = $result['card_status'] == 'true' ? true : false;
        $result['is_subscription'] = $result['is_subscription'] == 'true' ? true : false;
        $result['subscription_id'] = encode_base64($result['subscription_id']);

        if ($result) {

            $result = array(
                'response_code' => 200,
                'status' => 'ok',
                'message' => 'success',
                'data' => $result
            );
        } else {

            $response = array(
                'response_code' => 400,
                'status' => 'bad request',
                'message' => 'error'
            );
        }

        echo json_encode($result);
    }

    public function verify_payment_card($request, $response, $args) {



        //$user_id        = isset($_POST['user_id'])?$_POST['user_id']:false;
        $nonce = isset($_POST['nonce']) ? $_POST['nonce'] : false;

        //$customer = isset($_POST['customer'])?$_POST['customer']:false;
        $go = isset($_POST['go']) ? true : false;

        if (!$go) {
            file_put_contents('nonce/aa.txt', $nonce);
            die;
        }

        //$this->validation->rules('user_id', $user_id, 'required');
        //$this->validation->rules('nonce', $nonce, 'required');
        //$result = $this->Braintree->create_nonce($nonce);
        // print_r($result); die;

        $result = $this->Braintree->add_customerId();
        $customer = $result->customer->id;

        //print_r($customer); die;

        $result = $this->Braintree->verify_payment_card($nonce, $customer);


        if ($this->validation->error) {
            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {
            try {
                $result = $this->Braintree->verify_payment_card($nonce);
                print_r($result);
                die;

                if (isset($result->success) && $result->success) {
                    $customer = isset($result->customer) ? $result->customer : '';
                    if (sizeof($customer->creditCards) > 0) {
                        $token = isset($customer->creditCards[0]->token) ? $customer->creditCards[0]->token : '';
                        $maskedNumber = $customer->creditCards[0]->maskedNumber;
                    } else if (sizeof($customer->paypalAccounts) > 0) {
                        $token = isset($customer->paypalAccounts[0]->token) ? $customer->paypalAccounts[0]->token : '';
                        $maskedNumber = isset($customer->creditCards[0]->maskedNumber) ? $customer->creditCards[0]->maskedNumber : '';
                    }

                    $param = (object) array(
                                'user_id' => decode_base64($user_id),
                                'response_data' => $result,
                                'token' => $token,
                                'maskedNumber' => $maskedNumber,
                                'date' => date('Y-m-d H:i:s'),
                                'bt_is_default' => 'true'
                    );
                    $res = $this->payment_model->insert_user_card_details($param);
                    //create response
                    $response = array(
                        'response_code' => 200,
                        'status' => 'success',
                        'message' => 'card added successfully'
                    );
                } else {
                    $response = array(
                        'response_code' => 400,
                        'status' => 'error',
                        'message' => 'failed'
                    );
                }
            } catch (Exception $e) {
                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'failed'
                );
            }
        }
        return json_encode($response);
    }

    /*
      ||=============================================================
      || Function deleted_card number
      ||=============================================================
     */

    public function deleted_card($request, $response, $args) {

        if (!$_POST) {
            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();

        // reading post params
        $user_id = isset($_POST['user_id']) ? decode_base64($_POST['user_id']) : "";
        $bt_id = isset($_POST['bt_id']) ? decode_base64($_POST['bt_id']) : "";

        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('user_id', $user_id, 'required');


        if ($this->validation->error) {
            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {

            $result = $this->payment_model->deleted_card($user_id, $bt_id);

            if (isset($result) && $result == 1) {
                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success'
                );
            } else {
                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'error'
                );
            }
        }
        //echo JSON RESPONSE
        echo json_encode($response);
    }

    public function manage_subscription($request, $response, $args) {
        //$arr = $_REQUEST;
        //file_put_contents('nonce/subscription/'.time().'.txt', $arr);


        if (
                isset($_REQUEST["bt_signature"]) &&
                isset($_REQUEST["bt_payload"])
        ) {
            $webhookNotification = Braintree_WebhookNotification::parse(
                            $_POST["bt_signature"], $_POST["bt_payload"]
            );

            $message = "[Webhook Received "
                    . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] "
                    . "Kind: " . $webhookNotification->kind . " | "
                    . "Subscription: " . $webhookNotification->subscription->id . "\n";
            $webhookNotification = json_encode($webhookNotification);
            file_put_contents('nonce/subscription/subscription.txt', $webhookNotification, FILE_APPEND);
        }
    }

    public function create_subscription() {
        $arr = $this->Braintree->create_subscription();
    }

}
