<?php

/**
 * Class to handle all apis
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Subscription extends Layout {

    protected $app;

    public function __construct($app) {
        $this->app = $app;
        $this->load_library('Api/Braintree');
        $this->load_library('Api/Push_notification');
        $this->load_library('Api/Validation');
        $this->load_model('Api/Payment_model', 'payment_model');
        $this->load_model('Api/Booking_model', 'booking_model');
        $this->load_model('Api/Artist_model', 'artist_model');
        $this->load_model('Api/Referral_model', 'Referral_model');
        $this->load_library('Api/Email', 'email');
        $this->load_model('Api/User_model', 'user_model');
        $this->load_model('Api/Subscription_model', 'subscription_model');
    }

    public function manage_subscription($request, $response, $args) {
        //file_put_contents('nonce/subscription/koi.txt', $webhookNotification);
        if (
                isset($_REQUEST["bt_signature"]) &&
                isset($_REQUEST["bt_payload"])
        ) {
            $webhookNotification = Braintree_WebhookNotification::parse(
                            $_POST["bt_signature"], $_POST["bt_payload"]
            );

            $message = "[Webhook Received "
                    . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] "
                    . "Kind: " . $webhookNotification->kind . " | "
                    . "Subscription: " . $webhookNotification->subscription->id . "\n";

            //now update in data
            if ($webhookNotification->kind == 'subscription_charged_successfully') {
                $api_subscription_id = $webhookNotification->subscription->id;
                $webhookNotification = json_encode($webhookNotification);
                $this->subscription_model->update_subscription($api_subscription_id, $webhookNotification);
                //now update data
            }

            $webhookNotification = json_encode($webhookNotification);
            file_put_contents('nonce/subscription/subscription.txt', $webhookNotification);
        }
    }

    /*
      ||==================================================
      || @function cancel_subscription
      ||==================================================
     */

    public function cancel_subscription($request, $response, $args) {
        if (!$_POST) {

            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();

        // reading post params
        $user_id = isset($_POST['user_id']) ? decode_base64($_POST['user_id']) : "";
        $plan_id = isset($_POST['plan_id']) ? decode_base64($_POST['plan_id']) : '';

        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('plan_id', $user_id, 'plan_id');


        if ($this->validation->error) {
            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {

            $data = current($this->subscription_model->get_subscription_data($user_id, $plan_id));

            if (isset($data) && $data) {

                $currentDate = strtotime($data['api_created']);
                $futureDate = $currentDate + (60 * 15);
                $formatDate = date("Y-m-d H:i:s", $futureDate);
                $current_time = date('Y-m-d H:i:s');

                if (strtotime($current_time) <= strtotime($formatDate)) {
                    $data = $this->subscription_model->cancel_subscription($user_id, $plan_id);

                    $response = array(
                        'response_code' => 200,
                        'status' => 'ok',
                        'message' => 'success'
                    );
                } else {
                    $response = array(
                        'response_code' => 400,
                        'status' => 'error',
                        'message' => 'time_exceed'
                    );
                }
            } else {
                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'invalid_plan'
                );
            }
        }
        echo json_encode($response);
    }

    public function renew_subscription() {
        $susbcription = $this->subscription_model->select_subscription();        
        foreach ($susbcription as $value) {
            $this->subscription_model->disable_subscription($value['api_id'], $value['plan_id']);
            $user_id = $value['api_fk_artist_id'];
            $plan_id = $value['api_fk_asp_id'];
            if ($value['plan']) {
                $message = 'Your plan ' . $value['update_plan'] . ' activated ';
            }
            else{
                $result=$this->renew_artist_subscription_plan($user_id, $plan_id, $value['plan_amount'],$value['api_retry']);
                $deveice_type = 'ios';
                if ($value['u_device_type'] == 1) {
                    $deveice_type = 'android';
                }
                if($result==1){
                    $message = 'Your plan ' . $value['plan_name'] . ' activated ';
                }
                else{
                    $this->subscription_model->disable_subscription($value['api_id'], '',true);
                    $message = 'Renewal of  plan ' . $value['plan_name'] . ' failed, We will try agian in two days, Now free plan activated '; 
                }                
                $this->push_notification->send_renew_reminder_notification($value, $deveice_type, $message);
                if($value['email']){
                    //$this->user_notification($value['email'],$message);
                }
            }
        }
    }

    public function send_notifications() {
        $susbcription = $this->subscription_model->select_subscription(true);

        foreach ($susbcription as $value) {
           
            $message = 'Your plan ' . $value['plan_name'] . ' will be renew on ' . $value['api_nextBillingDate'];
            if ($value['plan']) {
                $message = 'Your plan ' . $value['plan_name'] . ' will be update to ' . $value['update_plan'] . ' on' . $value['api_nextBillingDate'];
            }
            $this->push_notification->send_renew_reminder_notification($value, $message);
            if($value['email']){
                $this->user_notification($value['email'],$message);
            }
        }
    }

    /*
      ||==============================================
      || @function Renew subscription
      ||==============================================
     */

    public function renew_artist_subscription_plan($user_id, $plan_id, $amount,$retry) {
        $flag=1;
        $mua_subsctiption_info = $this->subscription_model->get_mua_subscription_token($user_id);
        $paymentMethodToken = false;
        
        if ($mua_subsctiption_info) {
            $paymentMethodToken = isset($mua_subsctiption_info['bt_token']) ? $mua_subsctiption_info['bt_token'] : false;
        }
        if ($paymentMethodToken) {
            $flag=1;
            try {
                $result=$this->braintree->payment_with_token1($amount,$paymentMethodToken);
                if($result->success){
                    $this->subscription_model->renew_subscription($user_id,$plan_id,$amount,$result,$paymentMethodToken,'',$retry);
                }
                else{
                    if($retry==0){
                        $this->subscription_model->renew_subscription($user_id,4,0,'','',true);
                    }                    
                    $flag=2;
                }
            } catch (Exception $e) {
                if($retry==0){
                    $this->subscription_model->renew_subscription($user_id,4,0,'','',true);
                }
                $flag=2;
            }
        }
        else{
            $flag=2;
        }
        return $flag;
    }

    public function user_notification($email,$message){
        $data['msg']=$message;
        $sub = 'Your Plan renew';
        $msg = $this->_view('Front/Email/notification.php', $data, true);
        $this->email->to($email);
        $this->email->subject($sub);
        $this->email->messageType('html');
        $this->email->message($msg);
        $this->email->send();
    }
    
              
    /*
    ||==============================================
    || @function update_subscription
    ||==============================================
    */
    public function update_subscription() {

        if (!$_POST) {

            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();
        
        $user_id = isset($_POST['user_id']) ? decode_base64($_POST['user_id']) : '';
        $plan_id = isset($_POST['plan_id']) ? decode_base64($_POST['plan_id']) : '';
        
        // reading post params      
        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('plan_id', $plan_id, 'required');

        if ($this->validation->error) {
            $response["error"]   = true; 
            $response["message"] = array_pop($this->validation->error);
        } else {

            $plan_data = $this->subscription_model->get_subscription_plan_id_data($plan_id);

            if($plan_data){

                $user_token_data  = $this->payment_model->get_user_default_token($user_id);

                if (isset($user_token_data) && $user_token_data) {

                    $mua_subscription = $this->subscription_model->is_mua_subscription_active($user_id);

                    if ($mua_subscription) {

                        if($mua_subscription['api_fk_asp_id'] == 4){
                            $status = 'true';
                            $this->subscription_model->update_free_plan($user_id);
                        }else{
                            $status = 'update';
                        }
                        
                        $update_plan_id = $this->subscription_model->get_subscription_update_plan_id($user_id);

                        if (isset($update_plan_id) && $update_plan_id) {
                            $response = [
                                    'response_code' => 411,
                                    'status' => 'error',
                                    'message' => PLAN_UPDATE_ENG,
                                    'message_spn' => PLAN_UPDATE_SPN,
                                ];

                            echo json_encode($response);
                            die;
                        }
                    
                    } else {
                        $status = 'true';
                    }

                    //get mua payment information ...
                    $mua_subsctiption_info = $this->subscription_model->get_mua_subscription_token($user_id);
                    
                    $paymentMethodToken = false; //'kxp92y';
                    if ($mua_subsctiption_info) {
                        //bt_token
                        $paymentMethodToken = isset($mua_subsctiption_info['bt_token']) ? $mua_subsctiption_info['bt_token'] : false;
                    }
                
                    if ($paymentMethodToken != false) {

                        if(isset($plan_data['asp_id']) && $plan_data['asp_id']==4){

                            $data = (object)array(
                                       'user_id' => $user_id,   
                                       'amount'  => $plan_data['asp_amount'],
                                       'plan_id' => $plan_data['asp_id'],
                                       'status'  => $status,
                                       'paymentMethodToken' => $paymentMethodToken,
                                       'result' => '',
                                       'nextBillingDate' => ''
                                    );


                            $this->subscription_model->insert_subscription($data);

                            $response = array(
                                    'response_code' => 200,
                                    'status'        => 'ok',
                                    'message'       => PAYMENT_SUCCESS_ENG,
                                    'message_spn'   => PAYMENT_SUCCESS_SPN
                                );
                        }else{

                            $result=1;
                            try {
                                $result = $this->braintree->payment_with_token1($plan_data['asp_amount'],$paymentMethodToken);
                            } catch (Exception $e) {
                                $result=2;
                            }

                            if (isset($result->success) && $result->success) {
                                $data = (object)array(
                                       'user_id' => $user_id,   
                                       'amount'  => $plan_data['asp_amount'],
                                       'plan_id' => $plan_data['asp_id'],
                                       'status'  => $status,
                                       'paymentMethodToken' => $paymentMethodToken,
                                       'result' => $result,
                                       'nextBillingDate' => date('Y-m-d',strtotime('+1 month',strtotime(date('y-m-d'))))
                                    );
                        
                                $this->subscription_model->insert_subscription($data);

                                $response = array(
                                    'response_code' => 200,
                                    'status'        => 'ok',
                                    'message'       => PAYMENT_SUCCESS_ENG,
                                    'message_spn'   => PAYMENT_SUCCESS_SPN
                                );
                            }else{
                                $response = array(
                                            'response_code'=> 400,
                                            'status'       => 'error',
                                            'message'      => PAYMENT_FAILED_ENG,
                                            'message_spn'  => PAYMENT_FAILED_SPN,
                                        );
                            }
                        
                        }

                        
                    
                    } else {
                        $response = array(
                            'response_code' => 400,
                            'status' => 'error',
                            'message' => UPDATE_PAYMENT_ENG,
                            'message_spn' => UPDATE_PAYMENT_SPN,
                        );
                    }
                } else {
                    $response = array(
                        'response_code' => 400,
                        'status' => 'error',
                        'message' => UPDATE_PAYMENT_ENG,
                        'message_spn' => UPDATE_PAYMENT_SPN,
                    );
                }

            }else{
                $response = array(
                                'response_code'=> 400,
                                'status'       => 'error',
                                'message'      => INVALID_PLAN_ENG,
                                'message_spn'  => INVALID_PLAN_SPN,
                            );
            }

            
        }
        
        echo json_encode($response);
        
    }

    public function update_subscription_old($user_id1 = '', $plan_id1 = '') {

        if (!$_POST) {

            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();
        
        $user_id = isset($_POST['user_id']) ? decode_base64($_POST['user_id']) : '';
        $plan_id = isset($_POST['plan_id']) ? decode_base64($_POST['plan_id']) : '';
        
        // reading post params      
        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('plan_id', $plan_id, 'required');

        if ($this->validation->error) {
            $response["error"] = true; 
            $response["message"] = array_pop($this->validation->error);
        } else {

            $mua_subscription = $this->payment_model->is_mua_subscription_active($user_id);

            $user_token_data  = $this->payment_model->get_user_default_token($user_id);

            if (isset($user_token_data) && $user_token_data) {

                if ($mua_subscription) {
                    //deny subscription process
                    $info_arr = $this->subscription_model->get_subscription_status($user_id);

                    $billingDate = strtotime($info_arr['api_nextBillingDate']);
                    $today = time();

                    $remaining_days = ceil(($billingDate - $today) / 86400);


                    /* Changed after testing ##Rahul## */
                    /* $trialDuration = $remaining_days; */
                    $trialDuration = 1;

                    $status_data = $this->subscription_model->get_status_mua_subscription($user_id);

                    /*

                      if(isset($status_data['api_status']) && $status_data['api_status'] == 'true'){
                      if($status_data['api_fk_asp_id']==4){
                      $trialDuration = 0;
                      }
                      }

                     */

                    $status = 'update';
                    $update_plan_id = $this->subscription_model->get_subscription_update_plan_id($user_id);

                    if (isset($update_plan_id) && $update_plan_id) {
                        $response = array(
                            'response_code' => 411,
                            'status' => 'error',
                            'message' => PLAN_UPDATE_ENG,
                            'message_spn' => PLAN_UPDATE_SPN,
                        );

                        echo json_encode($response);
                        die;
                    }
                } else {
                    $trialDuration = 0;
                    $status = 'true';
                }

                //now cancel old subscription
                //do coding here for canceling subscription
                //get mua information ...
                $mua_subsctiption_info = $this->subscription_model->get_mua_subscription_token($user_id);
                $paymentMethodToken = false; //'kxp92y';
                $planId = $this->subscription_model->get_subscription_plan_id($plan_id); //'bb_1';
                if ($mua_subsctiption_info) {
                    //bt_token
                    $paymentMethodToken = isset($mua_subsctiption_info['bt_token']) ? $mua_subsctiption_info['bt_token'] : false;
                }

                if ($paymentMethodToken != false || $planId != false) {
                    $result = $this->Braintree->create_subscription($paymentMethodToken, $planId, $trialDuration);
                    $data = array();
                    if (isset($result->success) && $result->success) {
                        $data['subscription_id'] = isset($result->subscription->id) ? $result->subscription->id : '';
                        $data['nextBillAmount'] = isset($result->subscription->nextBillAmount) ? $result->subscription->nextBillAmount : '';
                        $data['paymentMethodToken'] = isset($result->subscription->paymentMethodToken) ? $result->subscription->paymentMethodToken : '';   //this is the token from which subscription started
                        $data['planId'] = isset($result->subscription->planId) ? $result->subscription->planId : '';

                        $nextBillingDate = (array) $result->subscription->nextBillingDate;
                        $data['nextBillingDate'] = isset($nextBillingDate['date']) ? $nextBillingDate['date'] : '';

                        $data['transactions_id'] = isset($result->subscription->transactions[0]->id) ? $result->subscription->transactions[0]->id : '';
                        $data['customer_id'] = isset($result->subscription->transactions[0]->customer['id']) ? $result->subscription->transactions[0]->customer['id'] : '';
                        $data['user_id'] = $user_id;
                        $data['plan_id'] = $plan_id;
                        $data['api_transaction_details'] = isset($result) ? serialize($result) : '';

                        $data['status'] = $status;
                        $data['paid_amount'] = isset($result->subscription->nextBillAmount) ? $result->subscription->nextBillAmount : '';
                        $data['api_service_amout'] = isset($result->subscription->nextBillAmount) ? $result->subscription->nextBillAmount : '';

                        //now insert data 

                        $status_data = $this->subscription_model->get_status_mua_subscription($user_id);
                        /*
                          if(isset($status_data['api_status']) && $status_data['api_status']=='true'){
                          $data['status'] = 'true';
                          $this->subscription_model->update_free_plan($user_id);
                          }
                         * */

                        $data = (object) $data;
                        $this->subscription_model->update_subscription_info($data);



                        $response = array(
                            'response_code' => 200,
                            'status' => 'ok',
                            'message' => PAYMENT_SUCCESS_ENG,
                            'message_spn' => PAYMENT_SUCCESS_SPN
                        );
                    }
                } else {
                    $response = array(
                        'response_code' => 400,
                        'status' => 'error',
                        'message' => UPDATE_PAYMENT_ENG,
                        'message_spn' => UPDATE_PAYMENT_SPN,
                    );
                }
            } else {
                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => UPDATE_PAYMENT_ENG,
                    'message_spn' => UPDATE_PAYMENT_SPN,
                );
            }
        }
        if ($user_id1) {
            return $response;
        } else {
            echo json_encode($response);
        }
    }

    public function get_mua_subscription() {
        if (!$_POST) {

            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();

        // reading post params
        $user_id = isset($_POST['user_id']) ? decode_base64($_POST['user_id']) : "";
        $lang = isset($_POST['lang']) && ($_POST['lang'] != '') ? $_POST['lang'] : 'eng';


        $this->validation->rules('user_id', $user_id, 'required');

        if ($this->validation->error) {
            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {


            $response = array();

            $update_arr = array();
            $final_date = '';
            $is_subscription = '';


            $update_plan_data = $this->subscription_model->get_mua_update_plan_data($user_id);



            if (isset($update_plan_data) && $update_plan_data) {

                $time = strtotime($update_plan_data['api_created']);

                $final = date("F d, Y", strtotime("+1 month", $time));

                $final_date = (isset($final) && $final) ? $final : '';


                $update_arr = array(
                    'eng' => "Your " . ucfirst($update_plan_data['asp_name']) . " Subscription Will Take Effect On " . $final_date . ".",
                    'spn' => "Su suscripción de " . $update_plan_data['asp_name_spn'] . " entrará en vigor el " . $final_date . ".",
                );
            } else {
                $update_arr = array(
                    'eng' => "",
                    'spn' => "",
                );
            }


            $new_date = '';

            $result = $this->subscription_model->get_mua_subscription($user_id);

            /* Get current status */
            $status = $this->subscription_model->get_subscription_status($user_id);

            $is_free_data = $this->subscription_model->is_free_data($user_id);

            if (isset($is_free_data) && $is_free_data) {


                foreach ($result as $key => $value) {

                    $result[$key]['is_free'] = (($value['asp_name'] == 'free') ? true : false);
                    $result[$key]['status'] = (isset($value['status']) && $value['status'] && $status['api_planId'] == $value["plan_id"]) ? $value['status'] : 'false';

                    if (isset($lang) && $lang == 'spn') {
                        $result[$key]['asp_name'] = $value['asp_name_spn'];
                        $result[$key]['asp_description'] = $value['asp_description_spn'];
                    }
                    unset($result[$key]['asp_name_spn']);
                    unset($result[$key]['asp_description_spn']);

                    $result[$key]['plan_id'] = encode_base64($value['plan_id']);
                    $result[$key]['asp_image'] = $value['asp_image'] ? (file_exists(APP_DIR . '/' . SUBSCRIPTION_PLAN_IMG . $value['asp_image']) ? (BASE_URL . SUBSCRIPTION_PLAN_IMG . $value['asp_image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE;


                    if ($value['api_created']) {
                        $time = strtotime($value['api_created']);
                        $new_date = date("F d, Y", strtotime("+1 month", $time));
                    } else {
                        $new_date = date("F d, Y");
                    }


                    $result[$key]['plan_message'] = array(
                        'eng' => "Your " . $value['asp_name'] . " Subscription Will Take Effect On " . date("F d, Y") . ".",
                        'spn' => "Su suscripción de " . $value['asp_name_spn'] . " entrará en vigor el " . date("F d, Y") . ".",
                    );
                }
            } else {



                foreach ($result as $key => $value) {

                    $result[$key]['is_free'] = (($value['asp_name'] == 'free') ? true : false);

                    $result[$key]['status'] = (isset($value['status']) && $value['status'] && $status['api_planId'] == $value["plan_id"]) ? $value['status'] : 'false';



                    if (isset($lang) && $lang == 'spn') {
                        $result[$key]['asp_name'] = $value['asp_name_spn'];
                        $result[$key]['asp_description'] = $value['asp_description_spn'];
                    }
                    unset($result[$key]['asp_name_spn']);
                    unset($result[$key]['asp_description_spn']);

                    $result[$key]['plan_id'] = encode_base64($value['plan_id']);
                    $result[$key]['asp_image'] = $value['asp_image'] ? (file_exists(APP_DIR . '/' . SUBSCRIPTION_PLAN_IMG . $value['asp_image']) ? (BASE_URL . SUBSCRIPTION_PLAN_IMG . $value['asp_image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE;


                    if ($value['api_created']) {
                        $time = strtotime($value['api_created']);
                        $new_date = date("F d, Y", strtotime("+1 month", $time));
                    } else {
                        $new_date = date("F d, Y");
                    }

                    $result[$key]['plan_message'] = array(
                        'eng' => "Your " . $value['asp_name'] . " Subscription Will Take Effect On " . $new_date . ".",
                        'spn' => "Su suscripción de " . $value['asp_name_spn'] . " entrará en vigor el " . $new_date . ".",
                    );
                }
            }





            $is_subscription = $this->subscription_model->get_subscription_status($user_id);


            if ($result) {
                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success',
                    'is_subscription' => isset($is_subscription['api_status']) ? 'true' : 'false',
                    'update_plan_status' => (isset($update_plan_data) && $update_plan_data) ? 'true' : 'false',
                    'update_plan_message' => $update_arr,
                    'data' => $result
                );
            } else {
                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success',
                    'is_subscription' => isset($is_subscription['api_status']) ? 'true' : 'false',
                    'update_plan_status' => (isset($update_plan_data) && $update_plan_data) ? 'true' : 'false',
                    'update_plan_message' => $update_arr,
                    'data' => $result
                );
            }
        }
        echo json_encode($response);
    }

    private function payment_with_token($user_id, $subscription_plan_amount) {

        //get current default taken
        $token_details = $this->payment_model->get_user_default_token($user_id);


        $response = 'credit-card-issue';

        //proceed if default valid token exists
        if (is_array($token_details) && isset($token_details['bt_token'])) {

            $token = $token_details['bt_token'];

            try {
                $param = (object) array(
                            'amount' => $subscription_plan_amount,
                            'token' => $token
                );
                $result = $this->Braintree->payment_with_token($param);

                if (isset($result->success) && $result->success) {


                    $userData = $this->booking_model->get_mua_gcmID($user_id);
                    //$this->Push_notification->send_notification_payment($userData, $devices_type);
                    return $result;
                } else {
                    $response = false;
                }
            } catch (Exception $e) {
                $response = false;
            }
        }
        return $response;
    }

}
