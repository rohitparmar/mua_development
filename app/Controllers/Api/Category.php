<?php

/**
 * Class to handle all apis
 *
 * @author Synergytop
 * @link http://synergytop.com/
 */
class Category extends Layout {

    protected $app;
    protected $res;
    private $conn;

    public function __construct($app) {
        $this->conn = $app->get('db');
        $this->app = $app;
        $this->load_library('Api/Validation');
        $this->load_model('Api/Category_model');
    }

    /**
     * function get_category
     * method - POST
     */
    public function select_category($request, $response, $args) {

        $lang = isset($_POST['lang']) ? $_POST['lang'] : "eng";
        $response = array();

        $rs = $result = $this->Category_model->get_category($lang);

        $data = array();

        foreach ($result as $k => $row) {
            //$row['cm_image'] = $row['cm_image']?(file_exists(APP_DIR.'/'.CATEGORY_IMG.$row['cm_image'])?(BASE_URL.CATEGORY_IMG.$row['cm_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

            $row['ca_price'] = money_format('%.2n', (double) $row['ca_price']);
            $row['ca_duration'] = (double) $row['ca_duration'];
            $row['cm_status'] = (bool) $row['cm_status'];
            //$row['cm_image'] = $row['cm_image']?(file_exists(APP_DIR.'/'.CATEGORY_IMG.$row['cm_image'])?(BASE_URL.CATEGORY_IMG.$row['cm_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

            $imgArr = @unserialize($row['cm_image']);

            $image_list = array();
            if ($imgArr !== false) {
                foreach ($imgArr as $rows) {
                    $img = (object) array(
                                'id' => encode_base64($rows['id']),
                                'name' => $rows['name'],
                                'image' => $rows['image'] ? (file_exists(APP_DIR . '/' . CATEGORY_IMG . $rows['image']) ? (BASE_URL . CATEGORY_IMG . $rows['image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE
                    );
                    array_push($image_list, $img);
                }
            }


            $row['cm_image'] = $image_list;

            if ($row['cm_fk_parent_id'] == 0) {
                $row['sub_category'] = [];
                $value1 = $this->make_category_array($rs, $row['cm_id']);
                //print_r($value1); die;
                if ($value1) {
                    $row['sub_category'] = $value1;
                }

                array_push($data, $row);
            }
        }

        if ($data) {

            // successfully
            $this->res = array(
                'response_code' => 200,
                'status' => 'ok',
                'message' => 'success',
                'data' => $data
            );
        } else {
            $this->res = array(
                'response_code' => 200,
                'status' => 'ok',
                'message' => 'success',
                'data' => array()
            );
        }
        echo json_encode($this->res);
    }

    protected function make_category_array($data, $id) {

        $return = array();

        foreach ($data as $value) {
            if ($value['cm_fk_parent_id'] == $id) {
                $value['sub_category'] = [];
                $value1 = $this->make_category_array($data, $value['cm_id']);

                $value['ca_price'] = money_format('%.2n', (double) $value['ca_price']);
                $value['ca_duration'] = (double) $value['ca_duration'];
                $value['cm_status'] = (bool) $value['cm_status'];


                //$value['cm_image'] = $value['cm_image']?(file_exists(APP_DIR.'/'.CATEGORY_IMG.$value['cm_image'])?(BASE_URL.CATEGORY_IMG.$value['cm_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE;

                $imgArr = @unserialize($value['cm_image']);
                //echo $value['cm_image'];
                //print_r($imgArr);

                $image_list = array();
                if ($imgArr !== false) {
                    foreach ($imgArr as $rows) {
                        $img = (object) array(
                                    'id' => $rows['id'],
                                    'name' => $rows['name'],
                                    'image' => $rows['image'] ? (file_exists(APP_DIR . '/' . CATEGORY_IMG . $rows['image']) ? (BASE_URL . CATEGORY_IMG . $rows['image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE
                        );
                        array_push($image_list, $img);
                    }
                }



                $value['cm_image'] = $image_list;
                //print_r($value['cm_image']);


                if ($value1) {
                    $value['sub_category'] = $value1;
                    //$value['cm_image'][] = 'hi india';//$row['cm_image']?(file_exists(APP_DIR.'/'.CATEGORY_IMG.$row['cm_image'])?(BASE_URL.CATEGORY_IMG.$row['cm_image']):CATEGORY_DEFAULT_IMAGE):CATEGORY_DEFAULT_IMAGE
                }
                array_push($return, $value);
            }
        }
        return $return;
    }

    /**
     * function get_category_get
     * method - GET
     */
    public function get_category_get($request, $response, $args) {
        $return["error"] = true;
        $return["message"] = "Invalid url";
        echo json_encode($return);
    }

    /**
     * FUNCTION - add_category
     * METHOD - GET
     */
    public function add_category_stop($request, $response, $args) {

        if (!$_POST) {
            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : "";
        $category_id = isset($_POST['category_id']) ? $_POST['category_id'] : "";
        $price = isset($_POST['price']) ? $_POST['price'] : "";
        $products = isset($_POST['products']) ? $_POST['products'] : "";
        $duration = isset($_POST['duration']) ? $_POST['duration'] : "";


        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('category_id', $category_id, 'required');


        if ($this->validation->error) {

            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {
            $user_id = $this->decode_base64($user_id);
            //$category_id = $this->decode_base64($category_id);

            $params = (object) array(
                        'user_id' => $user_id,
                        'category_id' => $category_id,
                        'price' => $price,
                        'products' => $products,
                        'duration' => $duration
            );

            //$this->category_model->manage_portfolio_during_artist_category_create($params);



            $res = $this->Category_model->manage_category($params);

            /* get category before after images */
            /*             * *start** */
            $category_image = $this->Category_model->get_category_image($category_id);

            $params2 = (object) array(
                        'user_id' => $user_id,
                        'category_id' => $category_id,
                        'cm_before_image' => $category_image['cm_before_image'],
                        'cm_after_image' => $category_image['cm_after_image']
            );

            //$this->Category_model->create_portfolio($params2);
            /*             * *end** */

            //print_r($res); die;


            if ($res['res'] == 1) {

                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success',
                    'ac_id' => encode_base64($res['id'])
                );
            } else if ($res['res'] == 0) {

                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'bad request'
                );
            } else if ($res['res'] == 2) {
                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'Sorry, current user information already exists'
                );
            }
        }

        //echo json response

        echo json_encode($response);
    }

    /**
     * FUNCTION - add_category
     * METHOD - GET
     */
    public function add_category_get($request, $response, $args) {
        $return["error"] = true;
        $return["message"] = "Invalid url";
        echo json_encode($return);
    }

    /**
     * FUNCTION add_category
     * METHOD - GET
     */
    public function mua_category($request, $response, $args) {

        if (!$_POST) {
            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : "";


        $this->validation->rules('user_id', $user_id, 'required');

        if ($this->validation->error) {
            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {

            $user_id = $this->decode_base64($user_id);

            $mua_category = $this->Category_model->mua_category($user_id);

            $category = $this->Category_model->get_category();


            $user = array();
            foreach ($mua_category as $value) {
                $user[$value['ac_fk_cm_id']] = $value;
                //print_r($user[$value['ac_fk_cm_id']]);
            }

            $data = array();


            foreach ($category as $row) {
                $row['cm_status'] = (bool) $row['cm_status'];

                if ($row['cm_fk_parent_id'] == 0) {


                    $row['artist_category'] = isset($user[$row['cm_id']]) ? $user[$row['cm_id']] : [];

                    $row['sub_category'] = [];

                    $value1 = $this->mua_category_array($category, $row['cm_id'], $user);
                    //print_r($value1); die;
                    if ($value1) {
                        $row['sub_category'] = $value1;
                    }

                    array_push($data, $row);
                }
            }


            if ($data) {

                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success',
                    'data' => $data
                );
            } else {

                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'bad request'
                );
            }
        }
        //echo response
        echo json_encode($response);
    }

    protected function mua_category_array($data, $id, $user) {

        $return = array();

        foreach ($data as $value) {
            if ($value['cm_fk_parent_id'] == $id) {

                $value['artist_category'] = isset($user[$value['cm_id']]) ? $user[$value['cm_id']] : [];

                $value['sub_category'] = [];


                $value1 = $this->mua_category_array($data, $value['cm_id'], $user);
                if ($value1) {
                    $value['sub_category'] = $value1;
                }

                array_push($return, $value);
            }
        }
        return $return;
    }

    /**
     * function mua_category_get
     * method - GET
     */
    public function mua_category_get($request, $response, $args) {
        $return["error"] = true;
        $return["message"] = "Invalid url";
        echo json_encode($return);
    }

    /*     * * function delete_category
     * method - post */

    public function delete_category($request, $response, $args) {
        $response = array();
        if (!$_POST) {

            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : "";
        $category_id = isset($_POST['category_id']) ? $_POST['category_id'] : "";

        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('category_id', $category_id, 'required');

        if ($this->validation->error) {

            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {

            $user_id = $this->decode_base64($user_id);
            //print_r($user_id); die;

            $result = $this->Category_model->delete_category($user_id, $category_id);
            // print_r($result); die;


            if ($result) {

                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success'
                );
            } else {

                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'bad request'
                );
            }
        }

        //echo response
        echo json_encode($response);
    }

    /*     * * Create get_rating_get */

    public function delete_category_get($request, $response, $args) {
        $return["error"] = true;
        $return["message"] = "Invalid url";
        echo json_encode($return);
    }

    /*     * * function delete_category
     * method - post */

    public function edit_category($request, $response, $args) {

        if (!$_POST) {
            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : "";
        $ac_id = isset($_POST['ac_id']) ? $_POST['ac_id'] : "";
        $price = isset($_POST['price']) ? $_POST['price'] : 0;
        $products = isset($_POST['products']) ? $_POST['products'] : "";
        $duration = isset($_POST['duration']) ? $_POST['duration'] : "";
        $status = isset($_POST['status']) ? $_POST['status'] : "";

        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('ac_id', $ac_id, 'required');

        if ($this->validation->error) {
            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {

            $user_id = $this->decode_base64($user_id);
            //$ac_id = $this->decode_base64($ac_id);
            //print_r($user_id); die;

            $params = (object) array(
                        'user_id' => $user_id,
                        'ac_id' => $ac_id,
                        'price' => money_format('%.2n', $price),
                        'products' => $products,
                        'duration' => $duration,
                        'status' => $status
            );

            $result = $this->Category_model->edit_category($params);
            if ($result) {
                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success'
                );
            } else {
                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success'
                );
            }
        }
        //echo response
        echo json_encode($response);
    }

    /*     * * Create get_rating_get */

    public function edit_category_get($request, $response, $args) {
        $return["error"] = true;
        $return["message"] = "Invalid url";
        echo json_encode($return);
    }

    /*     * * function get_skinton
     * method - post */

    public function get_skinton($request, $response, $args) {



        $response = array();


        if ($this->validation->error) {
            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {

            $result = $this->Category_model->get_skinton();

            foreach ($result as $value) {
                print_r($value);
            }
            die;

            //$result['stm_id'] = encode_base64($result['stm_id']);

            if ($result) {
                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success',
                    'data' => $result
                );
            } else {
                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'bad request'
                );
            }
        }
        //echo response
        echo json_encode($response);
    }

    /*     * * Create get_rating_get */

    public function get_skinton_get($request, $response, $args) {
        $return["error"] = true;
        $return["message"] = "Invalid url";
        echo json_encode($return);
    }

    /* --- user id decode function on base64_decode --- */

    protected function decode_base64($code) {
        $int = base64_decode($code);

        $id = $int / (45421 * 45421);

        return $id;
    }

    /* --- user id encode function on base64_encode --- */

    protected function encode_base64($requestData) {
        $sting = base64_encode($requestData * 45421 * 45421);

        return $sting;
    }

    public function get_child_cids($request, $response, $args) {

        $_level = array();
        if ($this->conn) {
            $_result = $this->conn->prepare("SELECT cm_id FROM category_master WHERE cm_id = 1");
            $_result->execute();
            if ($_result->rowCount()) {
                $_i = 1;
                //while ($_row = $_result->fetchAll()) {
                foreach ($_result->fetchAll() as $_row) {
                    $_level[$_i][] = $_row['cm_id'];
                }
                $_result->closeCursor();
                while (TRUE) {
                    foreach ($_level[$_i] as $_value) {
                        $_result = $this->conn->prepare("SELECT cm_id FROM category_master WHERE cm_fk_parent_id = '$_value'");
                        $_result->execute();
                        //while ($_row = $_result->fetchAll()) {
                        foreach ($_result->fetchAll() as $_row) {
                            $_level[$_i + 1][] = $_row['cm_id'];
                        }
                        $_result->closeCursor();
                    }
                    if (!empty($_level[$_i + 1])) {
                        $_i++;
                        continue;
                    } else {
                        break;
                    }
                }
            }
            //print_r($_level);die;
            return $_level;
        }
    }

    public function get_top_level_cids($_cid, $_level = 0, $_arr = array()) {
        $_result = tep_db_query("SELECT cm_id FROM category_master WHERE cm_id = 1");
        if (count($_arr) == $_level && $_level != 0) {
            return $_arr;
        }

        if (tep_db_num_rows($_result) > 0) {
            $_data = tep_db_fetch_array($_result);
            if ($_data['parent_id']) {
                $_arr[count($_arr)] = $_data['parent_id'];
                return get_top_level_cids($_data['parent_id'], $_level, $_arr);
            } else {
                return $_arr;
            }
        } else {
            return $_arr;
        }
    }

    public function get_category($request, $response, $args) {
        /*
          $data = isset($_POST['text'])&&($_POST['text'] != '')?$_POST['text']:'text';

          echo $data = htmlentities($data);
          echo '<br>';
          echo html_entity_decode($data);
          die;
         */

        $lang = isset($_POST['lang']) && ($_POST['lang'] != '') ? $_POST['lang'] : 'eng';
		$client_id = isset($_POST['client_id']) ? decode_base64($_POST['client_id']):'';
        
		$response = array();

        $rs = $result = $this->Category_model->select_category($lang, $client_id);

        $skinResult = $this->Category_model->get_skinton($lang);

        

        $objSkin = array();
        //get skintones obj
        foreach ($skinResult as $rows) {
            $arr = array(
                'id' => encode_base64($rows['ms_id']),
                'name' => $rows['ms_name'],
                'image' => $rows['ms_image'] ? (file_exists(APP_DIR . '/' . CATEGORY_IMG . $rows['ms_image']) ? (BASE_URL . CATEGORY_IMG . $rows['ms_image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE
            );
            $objSkin[] = (object) $arr;
        }



        $data = array();
        $uarr = [];
        
        foreach ($result as $k => $row) {
            if (!in_array($row['cm_id'], $uarr)) {
                $uarr[] = $row['cm_id'];
                /* $row['cm_price']    = (double)$row['cm_price'];
                  $row['cm_time']     = (double)$row['cm_time'];
                  $row['cm_status']   = (bool)$row['cm_status']; */

                $row['ca_price'] = money_format('%.2n', (double) $row['ca_price']);
                $row['ca_duration'] = (double) $row['ca_duration'];
                $row['cm_status'] = (bool) $row['cm_status'];
                
				$is_file_exists = 0;
				if($row['skintone_img']!=''){
					$ex = explode('.',$row['skintone_img']);
					if(count($ex)>1){
                		$is_file_exists = file_exists(APP_DIR . '/' . SKINTONE_IMAGE . $row['skintone_img']);
					}
				}
                
                if($is_file_exists){
					//$row['skintone_img']; echo '<br>';
                    $row['cat_image'] = BASE_URL . SKINTONE_IMAGE . $row['skintone_img'];
					//die;
                }else{
                    $row['cat_image'] = $row['cm_image'] ? (file_exists(APP_DIR . '/' . CATEGORY_IMG . $row['cm_image']) ? (BASE_URL . CATEGORY_IMG . $row['cm_image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE;
                }
                                
                $row['cm_image'] = $objSkin;


                if ($row['cm_fk_parent_id'] == 0) {
                    $row['sub_category'] = [];
                    $value1 = $this->make_category_array_2($rs, $row['cm_id'], $objSkin);
                    //print_r($value1); die;
                    if ($value1) {
                        $row['sub_category'] = $value1;
                    }

                    array_push($data, $row);
                }
            }
        }

        //$data = $this->utf8_converter($data);

        if ($data) {

            // successfully
            $this->res = array(
                'response_code' => 200,
                'status' => 'ok',
                'message' => 'success',
                'data' => $data
            );
        } else {
            $this->res = array(
                'response_code' => 200,
                'status' => 'ok',
                'message' => 'success',
                'data' => array()
            );
        }

        echo json_encode($this->res);
    }

    function utf8_converter($array) {
        array_walk_recursive($array, function(&$item, $key) {
            if (!@mb_detect_encoding($item, 'utf-8', true)) {
                $item = @utf8_encode($item);
            }
        });
        return $array;
    }

    protected function make_category_array_2($data, $id, $objSkin) {
		$return = array();

        $unq = array();

        foreach ($data as $value) {
            if (!in_array($value['cm_id'], $unq)) {
                $unq[] = $value['cm_id'];
                if ($value['cm_fk_parent_id'] == $id) {
                    $value['sub_category'] = [];
                    
                    
					$is_file_exists = 0;
					if($value['skintone_img']!=''){
						$ex = explode('.',$value['skintone_img']);
						if(count($ex)>1){
							$is_file_exists = file_exists(APP_DIR . '/' . SKINTONE_IMAGE . $value['skintone_img']);
						}
					}
                
                    if($is_file_exists){
						$ex = explode('.',$value['skintone_img']);
						if(count($ex)>1){
                        	$value['cat_image'] = BASE_URL . SKINTONE_IMAGE . $value['skintone_img'];
						}
                    }else{
                        $value['cat_image'] = $value['cm_image'] ? (file_exists(APP_DIR . '/' . CATEGORY_IMG . $value['cm_image']) ? (BASE_URL . CATEGORY_IMG . $value['cm_image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE;
                    }		
                    $value1 = $this->make_category_array_2($data, $value['cm_id'], $objSkin);
                    /* $value['cm_price']  = (double)$value['cm_price'];
                      $value['cm_time']   = (double)$value['cm_time'];
                      $value['cm_status']   = (bool)$value['cm_status']; */


                    $value['ca_price'] = money_format('%.2n', (double) $value['ca_price']);
                    $value['ca_duration'] = (double) $value['ca_duration'];
                    $value['cm_status'] = (bool) $value['cm_status'];
					
                    $value['cm_image'] = $objSkin;

                    if ($value1) {
                        $value['sub_category'] = $value1;
                    }

                    //now get value for skintones, eyeshadows, lipscolor
                    $service = array();
                    $service['ca_lip_color'] = array();
                    $service['ca_eye_shadow'] = array();
                    $service['ca_skintone'] = array();
                    foreach ($data as $row) {
                        
                        /*if ($row['mcm_service_name'] == 'master_lips_color') {
                            switch ($row['service_name']) {
                                case 'RED':
                                   $row['service_name'] = 'ROJO';
                                break;
                                case 'Nude':
                                   $row['service_name'] = 'Natural';
                                break;
                                case 'pink':
                                   $row['service_name'] = 'rosa';
                                break;
                                case 'berry':
                                   $row['service_name'] = 'mora';
                                break;
                                case 'chocolate':
                                   $row['service_name'] = 'chocolate';
                                break;
                                
                            }
                        }*/
                        

                        if ($row['cm_id'] == $value['cm_id']) {
                            $obj = (object) array(
                                        'service_id'    => $row['mcm_service_id'],
                                        'service_name'  => $row['service_name'],
                                        'service_image' => $row['service_image'] ? (file_exists(APP_DIR . '/' . SKINTONE_IMAGE . $row['service_image']) ? (BASE_URL . SKINTONE_IMAGE . $row['service_image']) : CATEGORY_DEFAULT_IMAGE) : CATEGORY_DEFAULT_IMAGE
                            );
                            if ($row['mcm_service_name'] == 'master_lips_color') {
                                array_push($service['ca_lip_color'], $obj);
                            } else if ($row['mcm_service_name'] == 'master_eyeshadow') {
                                array_push($service['ca_eye_shadow'], $obj);
                            } else if ($row['mcm_service_name'] == 'master_skintone') {
                                array_push($service['ca_skintone'], $obj);
                            }
                        }
                    }
                    $value['service'] = $service;

                    array_push($return, $value);
                }
            } else {
                
            }
        }
        return $return;
    }

    /**
     * FUNCTION - add_category
     * METHOD - GET
     */
    public function add_category($request, $response, $args) {

        if (!$_POST) {
            $response = array(
                'response_code' => 400,
                'status' => 'error',
                'message' => 'some fields required'
            );
            echo json_encode($response);
            die;
        }

        $response = array();

        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : "";
        $category_id = isset($_POST['category_id']) ? $_POST['category_id'] : "";
        $price = isset($_POST['price']) ? $_POST['price'] : "";
        $products = isset($_POST['products']) ? $_POST['products'] : "";
        $duration = isset($_POST['duration']) ? $_POST['duration'] : "";
        //print_r($user_id); die;

        $this->validation->rules('user_id', $user_id, 'required');
        $this->validation->rules('category_id', $category_id, 'required');


        if ($this->validation->error) {

            $response["error"] = true;
            $response["message"] = array_pop($this->validation->error);
        } else {
            $user_id = $this->decode_base64($user_id);
            //$category_id = $this->decode_base64($category_id);

            $params = (object) array(
                        'user_id' => $user_id,
                        'category_id' => $category_id,
                        'price' => money_format('%.2n', $price),
                        'products' => $products,
                        'duration' => $duration
            );

            $res = $this->Category_model->add_category($params);
            //print_r($res); die;


            if ($res['res'] == 1) {

                $response = array(
                    'response_code' => 200,
                    'status' => 'ok',
                    'message' => 'success',
                    'ac_id' => encode_base64($res['id'])
                );
            } else if ($res['res'] == 0) {

                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'bad request'
                );
            } else if ($res['res'] == 2) {
                $response = array(
                    'response_code' => 400,
                    'status' => 'error',
                    'message' => 'Sorry, this user information already existed'
                );
            }
        }

        //echo json response

        echo json_encode($response);
    }

}
